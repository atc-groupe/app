# CompanyApp

## Initialization procedure

### NPM
run
```bash
npm install
```

### Environment
Create and setup `environment.ts` file to the server into `src/environments`

### Build the app
run
```bash
ng build
```
