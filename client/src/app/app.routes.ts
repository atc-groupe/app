import { Routes } from '@angular/router';

import { SignInComponent } from './views/sign-in/sign-in.component';
import { AppComponent } from './app.component';

import {
  authGuard,
  signInGuard,
  companyUserTypeGuard,
  customerUserTypeGuard,
  rootUserTypeGuard,
  userDataGuard,
} from './shared/guards';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: AppComponent,
    pathMatch: 'full',
    canActivate: [userDataGuard, authGuard, rootUserTypeGuard],
  },
  {
    path: 'sign-in',
    component: SignInComponent,
    canActivate: [userDataGuard, signInGuard],
  },
  {
    path: 'company',
    loadChildren: () =>
      import('./company-app/company-app.module').then(
        (m) => m.CompanyAppModule,
      ),
    canActivate: [userDataGuard, authGuard, companyUserTypeGuard],
  },
  {
    path: 'customer',
    loadChildren: () =>
      import('./customer-app/customer-app.module').then(
        (m) => m.CustomerAppModule,
      ),
    canActivate: [userDataGuard, authGuard, customerUserTypeGuard],
  },
];
