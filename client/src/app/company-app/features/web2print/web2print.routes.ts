import { Routes } from '@angular/router';
import { Web2printComponent } from './web2print.component';

export const WEB2PRINT_ROUTES: Routes = [
  { path: '', component: Web2printComponent },
];
