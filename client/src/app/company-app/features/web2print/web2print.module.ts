// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';

// Components
import { Web2printComponent } from './web2print.component';

// Routes
import { WEB2PRINT_ROUTES } from './web2print.routes';

@NgModule({
  declarations: [Web2printComponent],
  imports: [CoreModule, RouterModule.forChild(WEB2PRINT_ROUTES)],
})
export class Web2printModule {}
