import { AdUser } from '../../interfaces/ad-user.interface';
import { createReducer, on } from '@ngrx/store';
import * as AdActions from './ad.actions';

export const AD_REDUCER_KEY = 'active-directory';

export interface AdState {
  adUsers: AdUser[];
  fetchListError: string | null;
}

const INITIAL_STATE: AdState = {
  adUsers: [],
  fetchListError: null,
};

export const adReducer = createReducer(
  INITIAL_STATE,
  on(AdActions.tryFetchAdUsersAction, (state: AdState): AdState => {
    return {
      ...state,
      adUsers: [],
      fetchListError: null,
    };
  }),
  on(
    AdActions.fetchUsersListSuccessAction,
    (state: AdState, { adUsers }): AdState => {
      return {
        ...state,
        adUsers,
        fetchListError: null,
      };
    },
  ),
  on(
    AdActions.fetchUsersListErrorAction,
    (state: AdState, { error }): AdState => {
      return {
        ...state,
        adUsers: [],
        fetchListError: error,
      };
    },
  ),
);
