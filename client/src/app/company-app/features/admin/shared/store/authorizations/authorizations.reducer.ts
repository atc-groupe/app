import { Role } from '../../interfaces/role.interface';
import { Authorization } from '../../../../../../shared/interfaces';
import { createReducer, on } from '@ngrx/store';
import * as AuthorizationsActions from './authorizations.actions';

export const AUTHORIZATIONS_REDUCER_KEY = 'authorizations';

export interface AuthorizationsState {
  selectedRole: {
    role: Role | null;
    authorizations: Authorization[] | null;
    handleError: string | null;
  };
}

const INITIAL_STATE: AuthorizationsState = {
  selectedRole: {
    role: null,
    authorizations: null,
    handleError: null,
  },
};

export const authorizationsReducer = createReducer(
  INITIAL_STATE,
  on(
    AuthorizationsActions.selectRoleAction,
    (state: AuthorizationsState, { role }): AuthorizationsState => {
      return {
        ...state,
        selectedRole: {
          role,
          authorizations: null,
          handleError: null,
        },
      };
    },
  ),
  on(
    AuthorizationsActions.fetchSelectedRoleAuthorizationsSuccessAction,
    (state: AuthorizationsState, { authorizations }): AuthorizationsState => {
      return {
        ...state,
        selectedRole: {
          ...state.selectedRole,
          authorizations,
          handleError: null,
        },
      };
    },
  ),
  on(
    AuthorizationsActions.fetchSelectedRoleAuthorizationsErrorAction,
    (state: AuthorizationsState, { error }): AuthorizationsState => {
      return {
        ...state,
        selectedRole: {
          ...state.selectedRole,
          authorizations: null,
          handleError: error,
        },
      };
    },
  ),
  on(
    AuthorizationsActions.updateRoleAuthorizationSuccessAction,
    (state: AuthorizationsState): AuthorizationsState => {
      return {
        ...state,
        selectedRole: {
          ...state.selectedRole,
          handleError: null,
        },
      };
    },
  ),
  on(
    AuthorizationsActions.updateRoleAuthorizationErrorAction,
    (state: AuthorizationsState, { error }): AuthorizationsState => {
      return {
        ...state,
        selectedRole: {
          ...state.selectedRole,
          handleError: error,
        },
      };
    },
  ),
);
