import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RolesService } from '../../services/roles.service';
import * as RolesActions from './roles.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { Role } from '../../interfaces/role.interface';

@Injectable()
export class RolesEffects {
  tryFetchRolesEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RolesActions.tryFetchRolesAction),
      switchMap(() =>
        this._rolesService.findAll().pipe(
          map((roles) => RolesActions.fetchRolesSuccessAction({ roles })),
          catchError((err) =>
            of(
              RolesActions.fetchRolesErrorAction({ error: err.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  tryCreateJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RolesActions.tryCreateRoleAction),
      switchMap(({ role }) =>
        this._rolesService.createOne(role).pipe(
          map(() => RolesActions.createRoleSuccessAction()),
          catchError((err) =>
            of(
              RolesActions.createRoleErrorAction({ error: err.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RolesActions.tryUpdateRoleAction),
      switchMap(({ role }) =>
        this._rolesService.updateOne(role).pipe(
          map(() => RolesActions.updateRoleSuccessAction()),
          catchError((err) =>
            of(
              RolesActions.updateRoleErrorAction({ error: err.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  tryDeleteJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RolesActions.tryDeleteRoleAction),
      switchMap(({ id }) =>
        this._rolesService.deleteOne(id).pipe(
          map(() => RolesActions.deleteRoleSuccessAction()),
          catchError((err) =>
            of(
              RolesActions.deleteRoleErrorAction({ error: err.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  handleRoleEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(
        RolesActions.createRoleSuccessAction,
        RolesActions.updateRoleSuccessAction,
        RolesActions.deleteRoleSuccessAction,
      ),
      map(() => RolesActions.tryFetchRolesAction()),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _rolesService: RolesService,
  ) {}
}
