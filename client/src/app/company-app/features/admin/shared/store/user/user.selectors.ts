import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AdminState } from '../index';
import { USER_REDUCER_KEY, UserState } from './user.reducer';
import { ADMIN_REDUCERS_KEY } from '../store.constants';

const selectAdminState = createFeatureSelector<AdminState>(ADMIN_REDUCERS_KEY);
const selectUserState = createSelector(
  selectAdminState,
  (state: AdminState): UserState => state[USER_REDUCER_KEY],
);

export const selectHandleSelectedUserSuccess = createSelector(
  selectUserState,
  (state: UserState): boolean | null => state.handleUserSuccess,
);
export const selectHandleSelectedUserError = createSelector(
  selectUserState,
  (state: UserState): string | null => state.handleUserError,
);
