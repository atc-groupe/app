import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UsersService } from '../../../../../../shared/services/users.service';
import { UserActions } from '../../store/user';
import { catchError, map, of, switchMap } from 'rxjs';
import { tryFetchUsersListAction } from '../../../../../../shared/store/users/users.actions';
import { Store } from '@ngrx/store';

@Injectable()
export class UserEffects {
  tryCreateUserEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryCreateUserAction),
      switchMap(({ user }) =>
        this._userService.createOne(user).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({ error: error.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateUserEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryUpdateUserAction),
      switchMap(({ userId, user }) =>
        this._userService.updateOne(userId, user).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({ error: error.error.message }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateUserRoleEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryUpdateUserRoleAction),
      switchMap(({ userId, roleId }) =>
        this._userService.updateRole(userId, roleId).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateUserPasswordEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryUpdateUserPasswordAction),
      switchMap(({ userId, password }) =>
        this._userService.updatePassword(userId, password).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateUserIsActiveEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryUpdateUserIsActiveAction),
      switchMap(({ userId, isActive }) =>
        this._userService.updateIsActive(userId, isActive).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryRemoveUserEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.tryRemoveUserAction),
      switchMap(({ userId }) =>
        this._userService.deleteOne(userId).pipe(
          map(() => UserActions.handleUserSuccessAction()),
          catchError((error) =>
            of(
              UserActions.handleUserErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  handleUserSuccessAction$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.handleUserSuccessAction),
      map(() => tryFetchUsersListAction()),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _userService: UsersService,
    private _store: Store,
  ) {}
}
