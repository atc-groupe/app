import { createAction, props } from '@ngrx/store';
import { AdUser } from '../../interfaces/ad-user.interface';

export const tryFetchAdUsersAction = createAction(
  '[CompanyApp > admin] try fetch ad users list',
);
export const fetchUsersListSuccessAction = createAction(
  '[CompanyApp > admin] fetch ad users list success',
  props<{ adUsers: AdUser[] }>(),
);
export const fetchUsersListErrorAction = createAction(
  '[CompanyApp > admin] fetch ad users list error',
  props<{ error: string }>(),
);
