import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ROLES_REDUCER_KEY, RolesState } from './roles.reducer';
import { Role } from '../../interfaces/role.interface';
import { AdminState } from '../index';
import { ADMIN_REDUCERS_KEY } from '../store.constants';
import { UserType } from '../../../../../../shared/interfaces';

const selectAdminState = createFeatureSelector<AdminState>(ADMIN_REDUCERS_KEY);
const selectRoleState = createSelector(
  selectAdminState,
  (state: AdminState) => state[ROLES_REDUCER_KEY],
);

// Role list
export const selectRolesList = createSelector(
  selectRoleState,
  (
    state: RolesState,
  ): { [UserType.Employee]: Role[]; [UserType.Customer]: Role[] } =>
    state.roles,
);
export const selectEmployeeRoles = createSelector(
  selectRoleState,
  (state: RolesState): Role[] => state.roles[UserType.Employee],
);
export const selectCustomerRoles = createSelector(
  selectRoleState,
  (state: RolesState): Role[] => state.roles[UserType.Customer],
);
export const selectFetchListError = createSelector(
  selectRoleState,
  (state: RolesState): string | null => state.roles.fetchListError,
);

// Selected role
export const selectSelectedRole = createSelector(
  selectRoleState,
  (state: RolesState): Role | null => state.selectedRole.role,
);
export const selectHandleRoleSuccess = createSelector(
  selectRoleState,
  (state: RolesState): boolean | null => state.selectedRole.handleRoleSuccess,
);
export const selectHandleRoleError = createSelector(
  selectRoleState,
  (state: RolesState): string | null => state.selectedRole.handleRoleError,
);
