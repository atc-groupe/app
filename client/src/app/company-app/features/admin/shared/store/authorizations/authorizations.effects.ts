import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthorizationsService } from '../../../../../../shared/services/authorizations.service';
import * as AuthActions from './authorizations.actions';
import { catchError, map, of, switchMap } from 'rxjs';
@Injectable()
export class AuthorizationsEffects {
  tryFetchSelectedRoleAuthorizationsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(AuthActions.tryFetchSelectedRoleAuthorizationsAction),
      switchMap(({ roleId }) =>
        this._authService.getRoleAuthorizations(roleId).pipe(
          map((authorizations) =>
            AuthActions.fetchSelectedRoleAuthorizationsSuccessAction({
              authorizations,
            }),
          ),
          catchError((err) =>
            of(
              AuthActions.fetchSelectedRoleAuthorizationsErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateRoleAuthorizationEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(AuthActions.tryUpdateRoleAuthorizationAction),
      switchMap(({ roleAction, userType, subject, action, roleId }) =>
        this._authService
          .updateAuthorizationRole(
            roleAction,
            userType,
            subject,
            action,
            roleId,
          )
          .pipe(
            map(() =>
              AuthActions.tryFetchSelectedRoleAuthorizationsAction({ roleId }),
            ),
            catchError((err) =>
              of(
                AuthActions.updateRoleAuthorizationErrorAction({
                  error: err.error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _authService: AuthorizationsService,
  ) {}
}
