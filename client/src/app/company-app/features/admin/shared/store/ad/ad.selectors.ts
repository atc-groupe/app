import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AdminState } from '../index';
import { ADMIN_REDUCERS_KEY } from '../store.constants';
import { AD_REDUCER_KEY, AdState } from './ad.reducer';
import { AdUser } from '../../interfaces/ad-user.interface';

const selectAdminState = createFeatureSelector<AdminState>(ADMIN_REDUCERS_KEY);
const selectAdState = createSelector(
  selectAdminState,
  (state: AdminState) => state[AD_REDUCER_KEY],
);

export const selectAdUsersList = createSelector(
  selectAdState,
  (state: AdState): AdUser[] => state.adUsers,
);
export const selectFetchListError = createSelector(
  selectAdState,
  (state: AdState): string | null => state.fetchListError,
);
