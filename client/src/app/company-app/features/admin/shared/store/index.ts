import { ROLES_REDUCER_KEY, rolesReducer, RolesState } from './roles';
import { ActionReducerMap } from '@ngrx/store';
import {
  AUTHORIZATIONS_REDUCER_KEY,
  authorizationsReducer,
  AuthorizationsState,
} from './authorizations/authorizations.reducer';
import { USER_REDUCER_KEY, userReducer, UserState } from './user';
import { AD_REDUCER_KEY, adReducer, AdState } from './ad';

export interface AdminState {
  [ROLES_REDUCER_KEY]: RolesState;
  [AUTHORIZATIONS_REDUCER_KEY]: AuthorizationsState;
  [USER_REDUCER_KEY]: UserState;
  [AD_REDUCER_KEY]: AdState;
}

export const ADMIN_REDUCERS: ActionReducerMap<AdminState> = {
  [ROLES_REDUCER_KEY]: rolesReducer,
  [AUTHORIZATIONS_REDUCER_KEY]: authorizationsReducer,
  [USER_REDUCER_KEY]: userReducer,
  [AD_REDUCER_KEY]: adReducer,
};
