import { createReducer, on } from '@ngrx/store';
import * as UserActions from './user.actions';

export const USER_REDUCER_KEY = 'user';

export interface UserState {
  handleUserSuccess: boolean | null;
  handleUserError: string | null;
}

const INITIAL_STATE: UserState = {
  handleUserSuccess: null,
  handleUserError: null,
};

export const userReducer = createReducer(
  INITIAL_STATE,
  on(UserActions.handleUserSuccessAction, (state: UserState): UserState => {
    return {
      ...state,
      handleUserError: null,
      handleUserSuccess: true,
    };
  }),
  on(
    UserActions.handleUserErrorAction,
    (state: UserState, { error }): UserState => {
      return {
        ...state,
        handleUserError: error,
        handleUserSuccess: false,
      };
    },
  ),
  on(UserActions.clearHandleUserResultAction, (state: UserState): UserState => {
    return {
      ...state,
      handleUserError: null,
      handleUserSuccess: null,
    };
  }),
);
