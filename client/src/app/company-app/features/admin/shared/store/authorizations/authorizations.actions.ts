import { createAction, props } from '@ngrx/store';
import { Role } from '../../interfaces/role.interface';
import { Authorization, UserType } from '../../../../../../shared/interfaces';
import { Action, Subject } from '../../../../../../shared/enums';

// Selected role
export const selectRoleAction = createAction(
  '[CompanyApp > admin] select role',
  props<{ role: Role }>(),
);

// Selected role authorizations
export const tryFetchSelectedRoleAuthorizationsAction = createAction(
  '[CompanyApp > admin] try select role authorizations',
  props<{ roleId: string }>(),
);
export const fetchSelectedRoleAuthorizationsSuccessAction = createAction(
  '[CompanyApp > admin] select role authorizations success',
  props<{ authorizations: Authorization[] }>(),
);
export const fetchSelectedRoleAuthorizationsErrorAction = createAction(
  '[CompanyApp > admin] select role authorizations error',
  props<{ error: string }>(),
);

// Role authorization actions
export const tryUpdateRoleAuthorizationAction = createAction(
  '[CompanyApp > admin] try add role authorization',
  props<{
    roleAction: 'add' | 'remove';
    userType: UserType;
    subject: Subject;
    action: Action;
    roleId: string;
  }>(),
);
export const updateRoleAuthorizationSuccessAction = createAction(
  '[CompanyApp > admin] update role authorization error',
);
export const updateRoleAuthorizationErrorAction = createAction(
  '[CompanyApp > admin] update role authorization error',
  props<{ error: string }>(),
);
