import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as AdActions from './ad.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { ActiveDirectoryService } from '../../services/active-directory.service';

@Injectable()
export class AdEffects {
  tryFetchAdUsersEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(AdActions.tryFetchAdUsersAction),
      switchMap(() =>
        this._adService.findAdUsersList().pipe(
          map((adUsers) => AdActions.fetchUsersListSuccessAction({ adUsers })),
          catchError((err) =>
            of(
              AdActions.fetchUsersListErrorAction({ error: err.error.message }),
            ),
          ),
        ),
      ),
    ),
  );
  constructor(
    private _actions$: Actions,
    private _adService: ActiveDirectoryService,
  ) {}
}
