import { createAction, props } from '@ngrx/store';
import { Role, Roles } from '../../interfaces/role.interface';

// Roles list
export const tryFetchRolesAction = createAction(
  '[CompanyApp > admin] try fetch roles',
);
export const fetchRolesSuccessAction = createAction(
  '[CompanyApp > admin] fetch roles success',
  props<{ roles: Roles }>(),
);
export const fetchRolesErrorAction = createAction(
  '[CompanyApp > admin] fetch roles error',
  props<{ error: string }>(),
);

// Create role
export const tryCreateRoleAction = createAction(
  '[CompanyApp > admin] try create role',
  props<{ role: Role }>(),
);
export const createRoleSuccessAction = createAction(
  '[CompanyApp > admin] create role success',
);
export const createRoleErrorAction = createAction(
  '[CompanyApp > admin] create role error',
  props<{ error: string }>(),
);

// Update role
export const tryUpdateRoleAction = createAction(
  '[CompanyApp > admin] try update role',
  props<{ role: Role }>(),
);
export const updateRoleSuccessAction = createAction(
  '[CompanyApp > admin] update role success',
);
export const updateRoleErrorAction = createAction(
  '[CompanyApp > admin] update role error',
  props<{ error: string }>(),
);

// Delete role
export const tryDeleteRoleAction = createAction(
  '[CompanyApp > admin] try delete role',
  props<{ id: string }>(),
);
export const deleteRoleSuccessAction = createAction(
  '[CompanyApp > admin] delete role success',
);
export const deleteRoleErrorAction = createAction(
  '[CompanyApp > admin] delete role error',
  props<{ error: string }>(),
);

// clear errors
export const clearHandleRoleErrorAction = createAction(
  '[CompanyApp > admin] clear handle role error',
);
