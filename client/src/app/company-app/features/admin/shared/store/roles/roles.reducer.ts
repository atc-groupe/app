import { Role } from '../../interfaces/role.interface';
import { createReducer, on } from '@ngrx/store';
import * as RolesActions from './roles.actions';
import { UserType } from '../../../../../../shared/interfaces';

export const ROLES_REDUCER_KEY = 'roles';

export interface RolesState {
  roles: {
    [UserType.Employee]: Role[];
    [UserType.Customer]: Role[];
    fetchListError: string | null;
  };
  selectedRole: {
    role: Role | null;
    handleRoleSuccess: boolean | null;
    handleRoleError: string | null;
  };
}

const INITIAL_STATE: RolesState = {
  roles: {
    [UserType.Employee]: [],
    [UserType.Customer]: [],
    fetchListError: null,
  },
  selectedRole: {
    role: null,
    handleRoleError: null,
    handleRoleSuccess: null,
  },
};

export const rolesReducer = createReducer(
  INITIAL_STATE,
  on(
    RolesActions.fetchRolesSuccessAction,
    (state: RolesState, { roles }): RolesState => {
      return {
        ...state,
        roles: {
          ...roles,
          fetchListError: null,
        },
      };
    },
  ),
  on(
    RolesActions.fetchRolesErrorAction,
    (state: RolesState, { error }): RolesState => {
      return {
        ...state,
        roles: {
          [UserType.Employee]: [],
          [UserType.Customer]: [],
          fetchListError: error,
        },
      };
    },
  ),
  on(
    RolesActions.tryCreateRoleAction,
    RolesActions.tryUpdateRoleAction,
    RolesActions.tryDeleteRoleAction,
    RolesActions.clearHandleRoleErrorAction,
    (state: RolesState): RolesState => {
      return {
        ...state,
        selectedRole: {
          role: state.selectedRole.role,
          handleRoleSuccess: null,
          handleRoleError: null,
        },
      };
    },
  ),
  on(
    RolesActions.createRoleSuccessAction,
    RolesActions.updateRoleSuccessAction,
    RolesActions.deleteRoleSuccessAction,
    (state: RolesState): RolesState => {
      return {
        ...state,
        selectedRole: {
          role: null,
          handleRoleSuccess: true,
          handleRoleError: null,
        },
      };
    },
  ),
  on(
    RolesActions.createRoleErrorAction,
    RolesActions.updateRoleErrorAction,
    RolesActions.updateRoleErrorAction,
    RolesActions.deleteRoleErrorAction,
    (state: RolesState, { error }): RolesState => {
      return {
        ...state,
        selectedRole: {
          role: null,
          handleRoleSuccess: false,
          handleRoleError: error,
        },
      };
    },
  ),
);
