import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AdminState } from '../index';
import {
  AUTHORIZATIONS_REDUCER_KEY,
  AuthorizationsState,
} from './authorizations.reducer';
import { Role } from '../../interfaces/role.interface';
import { Authorization } from '../../../../../../shared/interfaces';
import { ADMIN_REDUCERS_KEY } from '../store.constants';

const selectAdminState = createFeatureSelector<AdminState>(ADMIN_REDUCERS_KEY);
const selectAuthorizationsState = createSelector(
  selectAdminState,
  (state: AdminState): AuthorizationsState => state[AUTHORIZATIONS_REDUCER_KEY],
);

// SelectedRole
export const selectAuthorizationsSelectedRole = createSelector(
  selectAuthorizationsState,
  (state: AuthorizationsState): Role | null => state.selectedRole.role,
);

export const selectSelectedRoleAuthorizations = createSelector(
  selectAuthorizationsState,
  (state: AuthorizationsState): Authorization[] | null =>
    state.selectedRole.authorizations,
);

export const selectSelectedRoleHandleError = createSelector(
  selectAuthorizationsState,
  (state: AuthorizationsState): string | null => state.selectedRole.handleError,
);
