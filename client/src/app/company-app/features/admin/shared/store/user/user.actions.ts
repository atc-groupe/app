import { createAction, props } from '@ngrx/store';
import { UserCreate } from '../../../../../../shared/interfaces';

// Create user
export const tryCreateUserAction = createAction(
  '[CompanyApp > admin] try create user',
  props<{ user: UserCreate }>(),
);

// Remove user
export const tryRemoveUserAction = createAction(
  '[CompanyApp > admin] try remove user',
  props<{ userId: string }>(),
);

// Update user
export const tryUpdateUserAction = createAction(
  '[CompanyApp > admin] try update user',
  props<{ userId: string; user: UserCreate }>(),
);

// Update user password
export const tryUpdateUserPasswordAction = createAction(
  '[CompanyApp > admin] try update user password',
  props<{ userId: string; password: string }>(),
);

// Update user role
export const tryUpdateUserRoleAction = createAction(
  '[CompanyApp > admin] try update user role',
  props<{ userId: string; roleId: string }>(),
);

// Update user isActive:
export const tryUpdateUserIsActiveAction = createAction(
  '[CompanyApp > admin] try update user isActive',
  props<{ userId: string; isActive: boolean }>(),
);

// Handle user actions
export const handleUserSuccessAction = createAction(
  '[CompanyApp > admin] handle user success',
);
export const handleUserErrorAction = createAction(
  '[CompanyApp > admin] handle user error',
  props<{ error: string }>(),
);
export const clearHandleUserResultAction = createAction(
  '[CompanyApp > admin] clear handle role result',
);
