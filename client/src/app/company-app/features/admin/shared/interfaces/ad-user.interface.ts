export interface AdUser {
  dn: string;
  distinguishedName: string;
  userPrincipalName: string;
  sAMAccountName: string;
  mail: string;
  lockoutTime: string;
  whenCreated: string;
  pwdLastSet: string;
  userAccountControl: string;
  sn: string;
  givenName: string;
  cn: string;
  displayName: string;
  description: string;
}
