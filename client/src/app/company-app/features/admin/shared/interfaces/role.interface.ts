import { UserRole } from '../../../../../shared/enums';
import { UserType } from '../../../../../shared/interfaces';

export interface Role {
  _id?: string;
  userType: UserType;
  label: string;
  level: UserRole;
  description: string | null;
}

export interface Roles {
  [UserType.Employee]: Role[];
  [UserType.Customer]: Role[];
}
