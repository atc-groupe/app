import { Injectable } from '@angular/core';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PasswordService {
  private _chars = environment.passwordGenerator.chars;
  private _length = environment.passwordGenerator.length;

  getRandomPassword() {
    let password: string = '';
    for (let i = 0; i <= this._length; i++) {
      const randomNumber = Math.floor(Math.random() * this._chars.length);
      password += this._chars.substring(randomNumber, randomNumber + 1);
    }

    return password;
  }
}
