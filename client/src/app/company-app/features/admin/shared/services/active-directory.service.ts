import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdUser } from '../interfaces/ad-user.interface';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ActiveDirectoryService {
  constructor(private _http: HttpClient) {}

  findAdUsersList(): Observable<AdUser[]> {
    return this._http.get<AdUser[]>(
      `${environment.api.url}/active-directory/users`,
    );
  }
}
