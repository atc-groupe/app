import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs';
import { Role, Roles } from '../interfaces/role.interface';

@Injectable({
  providedIn: 'root',
})
export class RolesService {
  constructor(private _http: HttpClient) {}

  findAll(): Observable<Roles> {
    return this._http.get<Roles>(`${environment.api.url}/roles`);
  }

  createOne(role: Role): Observable<Role> {
    return this._http.post<Role>(`${environment.api.url}/roles`, role);
  }

  updateOne(role: Role): Observable<Role> {
    return this._http.patch<Role>(
      `${environment.api.url}/roles/${role._id}`,
      role,
    );
  }

  deleteOne(id: string): Observable<void> {
    return this._http.delete<void>(`${environment.api.url}/roles/${id}`);
  }
}
