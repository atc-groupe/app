// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CoreModule } from '../../../shared/modules/core.module';
import { LayoutModule } from '../../shared/modules/layout.module';

// Components
import { AdminComponent } from './admin.component';
import { UsersComponent } from './views/users/users/users.component';
import { RolesComponent } from './views/users/roles/roles.component';
import { RoleEditComponent } from './views/users/roles/role-edit/role-edit.component';
import { RoleDeleteComponent } from './views/users/roles/role-delete/role-delete.component';
import { NavComponent } from '../../shared/components/nav/nav.component';
import { AuthorizationsComponent } from './views/users/authorizations/authorizations.component';
import { EmployeeEditComponent } from './views/users/users/employee-edit/employee-edit.component';
import { UserDeleteComponent } from './views/users/users/user-delete/user-delete.component';
import { UsersListComponent } from './views/users/users/users-list/users-list.component';
import { CustomerCreateComponent } from './views/users/users/customer-create/customer-create.component';
import { CustomerResetPasswordComponent } from './views/users/users/customer-reset-password/customer-reset-password.component';
import { CustomerUpdateRoleComponent } from './views/users/users/customer-update-role/customer-update-role.component';

// Routes
import { ADMIN_ROUTES } from './admin.routes';

// Store
import { RolesEffects } from './shared/store/roles';
import { ADMIN_REDUCERS_KEY } from './shared/store/store.constants';
import { ADMIN_REDUCERS } from './shared/store';
import { AuthorizationsEffects } from './shared/store/authorizations/authorizations.effects';
import { UserEffects } from './shared/store/user';
import { AdEffects } from './shared/store/ad';
import { JobsStatusComponent } from './views/jobs/jobs-status/jobs-status.component';
import { JobsStatusEditComponent } from './views/jobs/jobs-status/jobs-status-edit/jobs-status-edit.component';
import { ColorPickerComponent } from '../../shared/components/color-picker/color-picker.component';
import { ColorDirective } from '../../shared/directives/color.directive';

@NgModule({
  declarations: [
    AdminComponent,
    UsersComponent,
    RolesComponent,
    RoleEditComponent,
    RoleDeleteComponent,
    AuthorizationsComponent,
    EmployeeEditComponent,
    CustomerUpdateRoleComponent,
    CustomerResetPasswordComponent,
    UserDeleteComponent,
    UsersListComponent,
    CustomerCreateComponent,
    JobsStatusComponent,
    JobsStatusEditComponent,
  ],
  imports: [
    CoreModule,
    LayoutModule,
    RouterModule.forChild(ADMIN_ROUTES),
    StoreModule.forFeature(ADMIN_REDUCERS_KEY, ADMIN_REDUCERS),
    EffectsModule.forFeature([
      RolesEffects,
      AuthorizationsEffects,
      UserEffects,
      AdEffects,
    ]),
    ColorPickerComponent,
    ColorDirective,
  ],
})
export class AdminModule {}
