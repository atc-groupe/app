import { ModuleAuthorizations } from '../../../shared/interfaces';
import { Action, Module, Subject } from '../../shared/enums';

export const ADMIN_AUTHORIZATIONS: ModuleAuthorizations = {
  module: Module.Admin,
  label: 'Panel administrateur',
  subjectsAuthorizations: [
    {
      label: 'Utilisateurs',
      authorizations: [
        {
          subject: Subject.Users,
          action: Action.Manage,
          route: 'users/list',
          label: 'Gestion des utilisateurs',
        },
        {
          subject: Subject.Roles,
          action: Action.Manage,
          route: 'users/roles',
          label: 'Gestion des roles utilisateurs',
        },
      ],
    },
  ],
};
