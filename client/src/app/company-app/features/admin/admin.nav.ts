import { Nav } from '../../shared/interfaces';
import { Action, Subject } from '../../shared/enums';

export const ADMIN_NAV: Nav = {
  title: 'Menu',
  sections: [
    {
      route: 'users',
      icon: 'shield_person',
      label: 'Utilisateurs',
      active: true,
      expanded: true,
      items: [
        {
          route: 'list',
          label: 'Utilisateurs',
          authorizations: { subject: Subject.Users, action: Action.Manage },
        },
        {
          route: 'roles',
          label: 'Roles utilisateurs',
          authorizations: { subject: Subject.Roles, action: Action.Manage },
        },
      ],
    },
    {
      route: 'jobs',
      icon: 'package_2',
      label: 'Jobs',
      active: true,
      expanded: true,
      items: [
        {
          route: 'status',
          label: 'Affichage du statut des jobs',
          authorizations: { subject: Subject.Job, action: Action.Manage },
        },
      ],
    },
    {
      route: 'stock',
      icon: 'storage',
      label: 'Stock',
      active: true,
      expanded: true,
      items: [
        {
          route: 'articles',
          label: 'Paramètres de stockage des articles',
          authorizations: { subject: Subject.Stock, action: Action.ManageMeta },
        },
      ],
    },
  ],
};
