import { Component, OnInit } from '@angular/core';
import { UserActions, UserSelectors } from '../../../../shared/store/user';
import { RolesActions, RolesSelectors } from '../../../../shared/store/roles';
import {
  RelationsActions,
  RelationsSelectors,
} from '../../../../../../../shared/store/relations';
import { Store } from '@ngrx/store';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Clipboard } from '@angular/cdk/clipboard';
import { UserCreate, UserType } from '../../../../../../../shared/interfaces';
import { first } from 'rxjs';
import { PasswordService } from '../../../../shared/services/password.service';

@Component({
  selector: 'company-app-admin-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.scss'],
})
export class CustomerCreateComponent implements OnInit {
  private _success$ = this._store.select(
    UserSelectors.selectHandleSelectedUserSuccess,
  );
  public customerRoles$ = this._store.select(
    RolesSelectors.selectCustomerRoles,
  );
  public customers$ = this._store.select(
    RelationsSelectors.selectCustomersList,
  );
  public contacts$ = this._store.select(
    RelationsSelectors.selectRelationContacts,
  );
  public error$ = this._store.select(
    UserSelectors.selectHandleSelectedUserError,
  );
  public form = this._fb.group({
    customerId: [''],
    contact: ['', Validators.required],
    roleId: ['', Validators.required],
  });
  public success = false;
  public randomPassword = this._passwordService.getRandomPassword();
  public identifierError = false;
  public copied = false;

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<CustomerCreateComponent>,
    private _clipboard: Clipboard,
    private _passwordService: PasswordService,
  ) {
    this._store.dispatch(RolesActions.tryFetchRolesAction());
    this._store.dispatch(RelationsActions.tryFetchCustomersAction());
  }

  ngOnInit() {
    this.form
      .get('customerId')
      ?.valueChanges.subscribe((value: string | null) => {
        if (value) {
          this._store.dispatch(
            RelationsActions.tryFetchRelationContactsAction({ id: value }),
          );
        }
      });

    this.form.get('contact')?.valueChanges.subscribe((value: string | null) => {
      if (!value) {
        return;
      }
      const [, identifier] = value.split('|');
      this.identifierError = !identifier;
    });
  }

  saveUser() {
    const contact = this.form.get('contact')?.value;
    const roleId = this.form.get('roleId')?.value;

    if (!contact || !roleId) {
      return;
    }

    const [mpId, identifier] = contact?.split('|');

    const user: UserCreate = {
      type: UserType.Customer,
      mpId,
      identifier,
      roleId,
    };

    this._store.dispatch(UserActions.tryCreateUserAction({ user }));
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      this.success = result!;
    });
  }

  copyPasswordToClipboard() {
    this._clipboard.copy(this.randomPassword);
    this.copied = true;

    setTimeout(() => {
      this.copied = false;
    }, 1000);
  }
}
