import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Clipboard } from '@angular/cdk/clipboard';
import {
  AppUser,
  CompanyUser,
  CustomerUser,
  UserType,
} from '../../../../../../../shared/interfaces';
import { UserSelectors, UserActions } from '../../../../shared/store/user';
import { first } from 'rxjs';

@Component({
  selector: 'company-app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.scss'],
})
export class UserDeleteComponent {
  public error$ = this._store.select(
    UserSelectors.selectHandleSelectedUserError,
  );
  private _success$ = this._store.select(
    UserSelectors.selectHandleSelectedUserSuccess,
  );

  public success = false;
  public user = this.data.user;

  constructor(
    private _dialogRef: MatDialogRef<UserDeleteComponent>,
    private _store: Store,
    private _fb: FormBuilder,
    private _clipboard: Clipboard,
    @Inject(MAT_DIALOG_DATA)
    private data: { user: AppUser },
  ) {}

  removeUser() {
    this._store.dispatch(
      UserActions.tryRemoveUserAction({ userId: this.user._id }),
    );
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      this.success = result!;
    });
  }
}
