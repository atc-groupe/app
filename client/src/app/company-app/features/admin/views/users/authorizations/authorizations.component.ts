import { Component, OnInit } from '@angular/core';
import { filter, first, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthorizationsService } from '../../../../../../shared/services/authorizations.service';
import * as AuthSelectors from '../../../shared/store/authorizations/authorizations.selectors';
import * as AuthActions from '../../../shared/store/authorizations/authorizations.actions';
import { Action, Subject } from '../../../../../../shared/types/enums.types';
import {
  Authorization,
  ModuleAuthorizations,
  UserType,
} from '../../../../../../shared/interfaces';
import { Role } from '../../../shared/interfaces/role.interface';

@Component({
  selector: 'app-authorizations',
  templateUrl: './authorizations.component.html',
  styleUrls: ['./authorizations.component.scss'],
})
export class AuthorizationsComponent implements OnInit {
  private _subscription = new Subscription();
  private _selectedRole$ = this._store.select(
    AuthSelectors.selectAuthorizationsSelectedRole,
  );

  public roleAuthorizations$ = this._store.select(
    AuthSelectors.selectSelectedRoleAuthorizations,
  );
  public declaredAuthorizations: ModuleAuthorizations[] = [];
  public authorizationError$ = this._store.select(
    AuthSelectors.selectSelectedRoleHandleError,
  );

  public selectedAuthorizations: { subject: Subject; action: Action }[] = [];
  public role: Role | null = null;

  constructor(
    private _store: Store,
    private _authService: AuthorizationsService,
  ) {}

  ngOnInit() {
    this._subscription.add(
      this.roleAuthorizations$
        .pipe(filter((v) => v !== null))
        .subscribe((authorizations) => {
          authorizations!.forEach((auth) => {
            this.selectedAuthorizations.push({
              subject: auth.subject,
              action: auth.action,
            });
          });
        }),
    );

    this._selectedRole$.pipe(first((v) => v !== null)).subscribe((role) => {
      this.role = role;
      this.declaredAuthorizations =
        this._authService.getAuthorizationsDeclarations(role!.userType);
    });
  }

  isSelected(authorization: Authorization): boolean {
    return (
      this.selectedAuthorizations.find(
        (item) =>
          item.subject === authorization.subject &&
          item.action === authorization.action,
      ) !== undefined
    );
  }

  toggleAuthorization(authorization: Authorization): void {
    if (this.isSelected(authorization)) {
      this.selectedAuthorizations = this.selectedAuthorizations.filter(
        (item) => {
          return !(
            item.subject === authorization.subject &&
            item.action === authorization.action
          );
        },
      );
      this._store.dispatch(
        AuthActions.tryUpdateRoleAuthorizationAction({
          roleAction: 'remove',
          userType: this.role!.userType,
          subject: authorization.subject,
          action: authorization.action,
          roleId: this.role!._id!,
        }),
      );
    } else {
      this._store.dispatch(
        AuthActions.tryUpdateRoleAuthorizationAction({
          roleAction: 'add',
          userType: UserType.Employee,
          subject: authorization.subject,
          action: authorization.action,
          roleId: this.role!._id!,
        }),
      );
      this.selectedAuthorizations.push({
        subject: authorization.subject,
        action: authorization.action,
      });
    }
  }
}
