import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  tryCreateRoleAction,
  tryUpdateRoleAction,
} from '../../../../shared/store/roles/roles.actions';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  selectHandleRoleError,
  selectHandleRoleSuccess,
} from '../../../../shared/store/roles/roles.selectors';
import { first } from 'rxjs';
import { Role } from '../../../../shared/interfaces/role.interface';
import { UserType } from '../../../../../../../shared/interfaces';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss'],
})
export class RoleEditComponent {
  public form = this._fb.group({
    label: [this.data.role?.label, Validators.required],
    level: [this.data.role?.level],
    description: [this.data.role?.description],
  });
  public error$ = this._store.select(selectHandleRoleError);
  public action: 'create' | 'update';
  public userType: UserType;
  public role: Role | undefined;
  public levels = ['admin', 'manager', 'user', 'guest'];

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _dialogRef: MatDialogRef<RoleEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    private data: {
      action: 'create' | 'update';
      userType?: UserType;
      role?: Role;
    },
  ) {
    this.action = data.action;
    this.role = data.role;
    this.userType =
      this.action === 'create' ? data.userType! : data.role!.userType;
    switch (this.userType) {
      case UserType.Employee:
        this.levels = ['admin', 'manager', 'user', 'guest'];
        break;
      case UserType.Customer:
        this.levels = ['manager', 'user'];
        break;
    }
  }

  saveRole() {
    const label = this.form.get('label')!.value;
    const level = this.form.get('level')!.value;
    const description = this.form.get('description')!.value;

    if (!label || !level) {
      return;
    }

    const role: Role = {
      userType: this.userType,
      label,
      level,
      description: description ? description : '',
    };

    this.action === 'create'
      ? this._store.dispatch(tryCreateRoleAction({ role }))
      : this._store.dispatch(
          tryUpdateRoleAction({ role: { ...role, _id: this.role!._id } }),
        );

    this._store
      .select(selectHandleRoleSuccess)
      .pipe(first((result: boolean | null) => result !== null))
      .subscribe((result) => {
        if (result === true) {
          this._dialogRef.close();
        }
      });
  }
}
