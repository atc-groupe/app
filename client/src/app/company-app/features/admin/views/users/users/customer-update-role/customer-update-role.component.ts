import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { FormBuilder, Validators } from '@angular/forms';
import * as RolesSelectors from '../../../../shared/store/roles/roles.selectors';
import * as RolesActions from '../../../../shared/store/roles/roles.actions';
import {
  AppUser,
  CompanyUser,
  CustomerUser,
  UserType,
} from '../../../../../../../shared/interfaces';
import { first, Observable } from 'rxjs';
import { Role } from '../../../../shared/interfaces/role.interface';
import { UserSelectors, UserActions } from '../../../../shared/store/user';

@Component({
  selector: 'company-app-customer-update-role',
  templateUrl: './customer-update-role.component.html',
  styleUrls: ['./customer-update-role.component.scss'],
})
export class CustomerUpdateRoleComponent {
  public roles$: Observable<Role[]>;
  public error$ = this._store.select(
    UserSelectors.selectHandleSelectedUserError,
  );
  private success$ = this._store.select(
    UserSelectors.selectHandleSelectedUserSuccess,
  );

  public user = this.data.user;
  public form = this._fb.group({
    roleId: [this.data.user.role._id, Validators.required],
  });

  constructor(
    private _dialogRef: MatDialogRef<CustomerUpdateRoleComponent>,
    private _store: Store,
    private _fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    private data: { user: AppUser },
  ) {
    this._store.dispatch(RolesActions.tryFetchRolesAction());
    switch (data.user.type) {
      case UserType.Employee: {
        this.roles$ = this._store.select(RolesSelectors.selectEmployeeRoles);
        const user = this.user as CompanyUser;
        break;
      }
      case UserType.Customer: {
        this.roles$ = this._store.select(RolesSelectors.selectCustomerRoles);
        const user = this.user as CustomerUser;
      }
    }
  }

  saveUser() {
    const roleId = this.form.get('roleId')?.value;

    if (!roleId) {
      return;
    }

    this._store.dispatch(
      UserActions.tryUpdateUserRoleAction({
        userId: this.user._id,
        roleId,
      }),
    );
    this.success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result) {
        this._dialogRef.close();
      }
    });
  }
}
