import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersSelectors } from '../../../../../../shared/store/users';
import { tryFetchUsersListAction } from '../../../../../../shared/store/users/users.actions';
import { MatDialog } from '@angular/material/dialog';
import { UserActions } from '../../../shared/store/user';
import { AppUser, UserType } from '../../../../../../shared/interfaces';
import { UserDeleteComponent } from './user-delete/user-delete.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { CustomerCreateComponent } from './customer-create/customer-create.component';
import { CustomerResetPasswordComponent } from './customer-reset-password/customer-reset-password.component';
import { CustomerUpdateRoleComponent } from './customer-update-role/customer-update-role.component';

@Component({
  selector: 'company-app-admin-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  protected readonly UserType = UserType;

  public employeeUsers$ = this._store.select(
    UsersSelectors.selectEmployeeUsers,
  );
  public customerUsers$ = this._store.select(
    UsersSelectors.selectCustomersUsers,
  );

  constructor(private _store: Store, private _dialog: MatDialog) {
    this._store.dispatch(tryFetchUsersListAction());
  }

  addEmployee() {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(EmployeeEditComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user: null },
    });
  }

  addCustomer() {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(CustomerCreateComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user: null },
    });
  }

  updateEmployee({ user }: { user: AppUser }) {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(EmployeeEditComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user },
    });
  }

  updateRole({ user }: { user: AppUser }) {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(CustomerUpdateRoleComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user },
    });
  }

  resetPassword({ user }: { user: AppUser }) {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(CustomerResetPasswordComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user },
    });
  }

  deleteUser({ user }: { user: AppUser }) {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._dialog.open(UserDeleteComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { user },
    });
  }

  toggleIsActive({ user }: { user: AppUser }) {
    this._store.dispatch(UserActions.clearHandleUserResultAction());
    this._store.dispatch(
      UserActions.tryUpdateUserIsActiveAction({
        userId: user._id,
        isActive: !user.isActive,
      }),
    );
  }
}
