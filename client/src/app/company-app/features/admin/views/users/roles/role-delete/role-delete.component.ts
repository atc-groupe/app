import { Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Role } from '../../../../shared/interfaces/role.interface';
import { tryDeleteRoleAction } from '../../../../shared/store/roles/roles.actions';
import {
  selectHandleRoleError,
  selectHandleRoleSuccess,
} from '../../../../shared/store/roles/roles.selectors';
import { first } from 'rxjs';

@Component({
  selector: 'app-role-delete',
  templateUrl: './role-delete.component.html',
  styleUrls: ['./role-delete.component.scss'],
})
export class RoleDeleteComponent {
  public error$ = this._store.select(selectHandleRoleError);
  public success = false;

  constructor(
    private _store: Store,
    private _dialogRef: MatDialogRef<RoleDeleteComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { role: Role },
  ) {}

  deleteRole() {
    this._store.dispatch(tryDeleteRoleAction({ id: this.data.role._id! }));
    this._store
      .select(selectHandleRoleSuccess)
      .pipe(first((result: boolean | null) => result !== null))
      .subscribe((result) => {
        this.success = result!;
      });
  }
}
