import { Component } from '@angular/core';
import { selectRolesList } from '../../../shared/store/roles/roles.selectors';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { RoleEditComponent } from './role-edit/role-edit.component';
import * as RolesActions from '../../../shared/store/roles/roles.actions';
import * as AuthActions from '../../../shared/store/authorizations/authorizations.actions';
import { Role } from '../../../shared/interfaces/role.interface';
import { RoleDeleteComponent } from './role-delete/role-delete.component';
import { UserType } from '../../../../../../shared/interfaces';

@Component({
  selector: 'app-role-list',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
})
export class RolesComponent {
  public roles$ = this._store.select(selectRolesList);
  public userTypes = [UserType.Employee, UserType.Customer];

  constructor(private _store: Store, private _dialog: MatDialog) {
    this._store.dispatch(RolesActions.tryFetchRolesAction());
  }

  addRole(userType: UserType) {
    this._store.dispatch(RolesActions.clearHandleRoleErrorAction());
    this._dialog.open(RoleEditComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { action: 'create', userType: userType },
    });
  }

  editRole(role: Role) {
    this._store.dispatch(RolesActions.clearHandleRoleErrorAction());
    this._dialog.open(RoleEditComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { action: 'update', role },
    });
  }

  deleteRole(role: Role) {
    this._store.dispatch(RolesActions.clearHandleRoleErrorAction());
    this._dialog.open(RoleDeleteComponent, {
      width: '100%',
      maxWidth: '50rem',
      data: { role },
    });
  }

  editAuthorizations(role: Role) {
    this._store.dispatch(AuthActions.selectRoleAction({ role }));
    this._store.dispatch(
      AuthActions.tryFetchSelectedRoleAuthorizationsAction({
        roleId: role._id!,
      }),
    );
  }
}
