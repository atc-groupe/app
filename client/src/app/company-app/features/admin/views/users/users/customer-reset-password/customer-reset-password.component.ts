import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { AppUser } from '../../../../../../../shared/interfaces';
import { UserSelectors, UserActions } from '../../../../shared/store/user';
import { Clipboard } from '@angular/cdk/clipboard';
import { PasswordService } from '../../../../shared/services/password.service';
import { first } from 'rxjs';

@Component({
  selector: 'company-app-customer-reset-password',
  templateUrl: './customer-reset-password.component.html',
  styleUrls: ['./customer-reset-password.component.scss'],
})
export class CustomerResetPasswordComponent {
  public error$ = this._store.select(
    UserSelectors.selectHandleSelectedUserError,
  );
  private _success$ = this._store.select(
    UserSelectors.selectHandleSelectedUserSuccess,
  );

  public success = false;
  public randomPassword = this._passwordService.getRandomPassword();
  public user = this.data.user;
  public copied = false;

  constructor(
    private _dialogRef: MatDialogRef<CustomerResetPasswordComponent>,
    private _store: Store,
    private _fb: FormBuilder,
    private _clipboard: Clipboard,
    private _passwordService: PasswordService,
    @Inject(MAT_DIALOG_DATA)
    private data: { user: AppUser },
  ) {}

  saveUser() {
    this._store.dispatch(
      UserActions.tryUpdateUserPasswordAction({
        userId: this.user._id,
        password: this.randomPassword,
      }),
    );
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      this.success = result!;
    });
  }

  copyPasswordToClipboard() {
    this._clipboard.copy(this.randomPassword);
    this.copied = true;

    setTimeout(() => {
      this.copied = false;
    }, 1000);
  }
}
