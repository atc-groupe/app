import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AppUser,
  CompanyUser,
  CustomerUser,
  UserType,
} from '../../../../../../../shared/interfaces';

@Component({
  selector: 'company-app-admin-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  @Input() users: AppUser[] | null = null;
  @Input() usersType!: UserType;

  @Output() public addUser = new EventEmitter<{ userType: UserType }>();
  @Output() public updateUser = new EventEmitter<{ user: AppUser }>();
  @Output() public updateRole = new EventEmitter<{ user: AppUser }>();
  @Output() public deleteUser = new EventEmitter<{ user: AppUser }>();
  @Output() public toggleIsActive = new EventEmitter<{ user: AppUser }>();
  @Output() public resetPassword = new EventEmitter<{ user: AppUser }>();

  protected readonly UserType = UserType;
  public title = '';

  ngOnInit() {
    switch (this.usersType) {
      case UserType.Employee:
        this.title = 'collaborateurs ATC';
        break;
      case UserType.Customer:
        this.title = 'Clients';
        break;
    }
  }

  emitAddUser() {
    this.addUser.emit({ userType: this.usersType });
  }

  emitUpdateUser(user: AppUser) {
    this.updateUser.emit({ user });
  }

  emitUpdateUserRole(user: AppUser) {
    this.updateRole.emit({ user });
  }

  emitDeleteUser(user: AppUser) {
    this.deleteUser.emit({ user });
  }

  emitToggleIsActive(user: AppUser) {
    this.toggleIsActive.emit({ user });
  }

  emitResetPassword(user: AppUser) {
    this.resetPassword.emit({ user });
  }

  getUserSubtitle(user: AppUser): string {
    switch (this.usersType) {
      case UserType.Employee:
        const employee = user as CompanyUser;
        return employee.department;
      case UserType.Customer:
        const customer = user as CustomerUser;
        return 'A traiter';
    }
  }
}
