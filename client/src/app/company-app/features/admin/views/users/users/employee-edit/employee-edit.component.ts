import { Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  EmployeesActions,
  EmployeesSelectors,
} from '../../../../../../../shared/store/employees';
import { FormBuilder, Validators } from '@angular/forms';
import { UserActions, UserSelectors } from '../../../../shared/store/user';
import { first } from 'rxjs';
import {
  CompanyUser,
  UserCreate,
  UserType,
} from '../../../../../../../shared/interfaces';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Clipboard } from '@angular/cdk/clipboard';
import { AdSelectors, AdActions } from '../../../../shared/store/ad';
import { RolesActions, RolesSelectors } from '../../../../shared/store/roles';

@Component({
  selector: 'company-app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss'],
})
export class EmployeeEditComponent {
  private _success$ = this._store.select(
    UserSelectors.selectHandleSelectedUserSuccess,
  );

  public employees$ = this._store.select(
    EmployeesSelectors.selectEmployeesList,
  );
  public employeeRoles$ = this._store.select(
    RolesSelectors.selectEmployeeRoles,
  );
  public adUsers$ = this._store.select(AdSelectors.selectAdUsersList);
  public error$ = this._store.select(
    UserSelectors.selectHandleSelectedUserError,
  );
  public user = this.data.user;

  public form = this._fb.group({
    mpId: [this.user ? this.user.mpId : '', Validators.required],
    identifier: [this.user ? this.user.identifier : '', Validators.required],
    roleId: [this.user ? this.user.role._id : '', Validators.required],
  });
  public success = false;

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<EmployeeEditComponent>,
    private _clipboard: Clipboard,
    @Inject(MAT_DIALOG_DATA)
    private data: { user: CompanyUser | null },
  ) {
    this._store.dispatch(EmployeesActions.tryFetchEmployeesListAction());
    this._store.dispatch(RolesActions.tryFetchRolesAction());
    this._store.dispatch(AdActions.tryFetchAdUsersAction());
  }

  saveUser() {
    const mpId = this.form.get('mpId')?.value;
    const identifier = this.form.get('identifier')?.value;
    const roleId = this.form.get('roleId')?.value;
    if (!mpId || !identifier || !roleId) {
      return;
    }

    const user: UserCreate = {
      type: UserType.Employee,
      mpId,
      identifier,
      roleId,
    };

    this.user
      ? this._store.dispatch(
          UserActions.tryUpdateUserAction({ userId: this.user._id, user }),
        )
      : this._store.dispatch(UserActions.tryCreateUserAction({ user }));
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      this.success = result!;
    });
  }
}
