import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { first, Subscription } from 'rxjs';
import { ArticleMeta } from '../../../../../shared/interfaces/stock/article-meta.interface';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  StockActions,
  StockSelectors,
} from '../../../../../shared/store/stock';
import { ProductType } from '../../../../../shared/enums';
import { Article, ProductList } from '../../../../../shared/interfaces';
import { StockArticleMetaService } from '../../../../../shared/services';
import { ArticleMetaEditComponent } from '../article-meta-edit/article-meta-edit.component';
import { ArticleMetaDeleteComponent } from '../article-meta-delete/article-meta-delete.component';

@Component({
  selector: 'app-stock-article',
  standalone: true,
  imports: [
    CommonModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
  ],
  templateUrl: './article-meta.component.html',
  styleUrls: ['./article-meta.component.scss'],
})
export class ArticleMetaComponent implements OnInit, OnDestroy {
  public articlesMeta$ = this._articlesMetaService.articlesMeta$;
  public fetchError$ = this._articlesMetaService.fetchError$;
  public productList: ProductList | null = null;
  private _subscription = new Subscription();

  constructor(
    private _dialog: MatDialog,
    private _store: Store,
    private _articlesMetaService: StockArticleMetaService,
  ) {}

  ngOnInit() {
    this._store.dispatch(
      StockActions.tryFetchProductListAction({
        productType: ProductType.Article,
      }),
    );

    this._articlesMetaService.findAll().subscribe();

    this._subscription.add(
      this._store
        .select(StockSelectors.selectList)
        .pipe(first((v) => v !== null))
        .subscribe((productList) => {
          this.productList = productList;
        }),
    );
  }

  openArticleMetaEdit(articleMeta?: ArticleMeta) {
    this._articlesMetaService.clearHandleError();

    this._subscription.add(
      this._dialog
        .open(ArticleMetaEditComponent, {
          width: '45rem',
          data: {
            articleMeta: articleMeta ? articleMeta : null,
            productList: this.productList,
          },
        })
        .afterClosed()
        .subscribe(() => {
          this._articlesMetaService.findAll().subscribe();
        }),
    );
  }

  openArticleMetaDelete(articleMeta: ArticleMeta) {
    this._articlesMetaService.clearHandleError();

    this._subscription.add(
      this._dialog
        .open(ArticleMetaDeleteComponent, {
          width: '45rem',
          data: {
            articleMeta,
          },
        })
        .afterClosed()
        .subscribe(() => {
          this._articlesMetaService.findAll().subscribe();
        }),
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
