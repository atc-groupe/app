import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { ArticleMeta } from '../../../../../shared/interfaces/stock/article-meta.interface';
import {
  Article,
  Product,
  ProductGroup,
  ProductList,
  ProductListItem,
} from '../../../../../shared/interfaces';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';
import { PRODUCT_TYPES_GROUPS } from '../../../../../shared/constants/product-types-groups.constant';
import { Subscription } from 'rxjs';
import { StockArticleMetaService } from '../../../../../shared/services';
import { Store } from '@ngrx/store';
import {
  StockActions,
  StockSelectors,
} from '../../../../../shared/store/stock';

@Component({
  selector: 'app-article-meta-add',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
  ],
  templateUrl: './article-meta-edit.component.html',
  styleUrls: ['./article-meta-edit.component.scss'],
})
export class ArticleMetaEditComponent implements OnInit, OnDestroy {
  public articleMeta: ArticleMeta | null = null;
  public productList: ProductList | null = null;
  public productGroups: ProductGroup[] = [];
  public productListItems: ProductListItem[] = [];
  public article: Article | null = null;
  public error$ = this._stockArticleMetaService.handleError$;

  public form = this._fb.group({
    groupType: new FormControl<number | null>(null, Validators.required),
    productGroup: new FormControl<string | null>(null, Validators.required),
    product: new FormControl<number | null>(null, Validators.required),
    packQuantity: new FormControl<number | null>(null, Validators.required),
    packUnitLabel: ['', Validators.required],
  });

  private _subscription = new Subscription();

  constructor(
    private _dialogRef: MatDialogRef<ArticleMetaEditComponent>,
    private _stockArticleMetaService: StockArticleMetaService,
    private _fb: FormBuilder,
    private _store: Store,
    @Inject(MAT_DIALOG_DATA)
    private data: {
      articleMeta: ArticleMeta | null;
      productList: ProductList | null;
    },
  ) {
    this.articleMeta = this.data.articleMeta;
    this.productList = this.data.productList;
  }

  ngOnInit() {
    this._subscription.add(
      this.form.get('groupType')?.valueChanges.subscribe((value) => {
        if (!this.productList || !value) {
          return;
        }

        const selectedGroupType = this.productList.find(
          (groupType) => groupType.groupType === value,
        );

        if (selectedGroupType) {
          this.productGroups = selectedGroupType.groups;

          if (this.productGroups.length === 1) {
            this.form
              .get('productGroup')
              ?.setValue(this.productGroups[0].description);
          }
        }
      }),
    );

    this._subscription.add(
      this.form.get('productGroup')?.valueChanges.subscribe((value) => {
        if (!this.productGroups || !value) {
          return;
        }

        const selectedProductGroup = this.productGroups.find(
          (group) => group.description === value,
        );

        if (selectedProductGroup) {
          this.productListItems = selectedProductGroup.products;
        }
      }),
    );

    this._subscription.add(
      this.form.get('product')?.valueChanges.subscribe((value) => {
        if (!value) {
          return;
        }

        this._store.dispatch(
          StockActions.tryFetchProductAction({
            productType: ProductType.Article,
            productId: value,
          }),
        );
      }),
    );

    this._subscription.add(
      this._store.select(StockSelectors.selectProduct).subscribe((product) => {
        this.article = product as Article;
      }),
    );

    if (this.articleMeta) {
      this.form.get('packQuantity')?.setValue(this.articleMeta.packQuantity);
      this.form.get('packUnitLabel')?.setValue(this.articleMeta.packUnitLabel);
      this.form.get('groupType')?.setValue(this.articleMeta.article.groupType);
      this.form.get('product')?.setValue(this.articleMeta.mpArticleId);
    }
  }

  get title() {
    return this.articleMeta ? 'Modifier' : 'Ajouter';
  }

  getGroupTypeLabel(groupType: ProductTypeGroup): string | null {
    const item = PRODUCT_TYPES_GROUPS.get(ProductType.Article)!.get(groupType);

    return item ? item : null;
  }

  saveArticleMeta() {
    const mpArticleId = this.form.get('product')?.value;
    const packQuantity = this.form.get('packQuantity')?.value;
    const packUnitLabel = this.form.get('packUnitLabel')?.value;

    if (!mpArticleId || !packQuantity || !packUnitLabel) {
      return;
    }

    const dto = {
      mpArticleId,
      packQuantity,
      packUnitLabel,
    };

    const observable = this.articleMeta
      ? this._stockArticleMetaService.updateOne(this.articleMeta._id, dto)
      : this._stockArticleMetaService.insertOne(dto);

    observable.subscribe(() => {
      this._dialogRef.close();
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
