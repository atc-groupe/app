import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { StockArticleMetaService } from '../../../../../shared/services';
import { ArticleMeta } from '../../../../../shared/interfaces/stock/article-meta.interface';

@Component({
  selector: 'app-article-meta-delete',
  standalone: true,
  imports: [CommonModule, MatDialogModule],
  templateUrl: './article-meta-delete.component.html',
  styleUrls: ['./article-meta-delete.component.scss'],
})
export class ArticleMetaDeleteComponent {
  public articleMeta: ArticleMeta | null = null;

  constructor(
    private _dialogRef: MatDialogRef<ArticleMetaDeleteComponent>,
    private _stockArticleMetaService: StockArticleMetaService,
    @Inject(MAT_DIALOG_DATA)
    private data: {
      articleMeta: ArticleMeta;
    },
  ) {
    this.articleMeta = this.data.articleMeta;
  }

  onConfirmRemove() {
    if (!this.articleMeta) {
      return;
    }

    this._stockArticleMetaService
      .deleteOne(this.articleMeta._id)
      .subscribe(() => {
        this._dialogRef.close();
      });
  }
}
