import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MetaJobsStatus } from '../../../../../../shared/interfaces';
import {
  MetaJobsStatusSelectors,
  MetaJobsStatusActions,
} from '../../../../../../shared/store/meta-jobs-status';
import { first } from 'rxjs';

@Component({
  selector: 'app-jobs-status-edit',
  templateUrl: './jobs-status-edit.component.html',
  styleUrls: ['./jobs-status-edit.component.scss'],
})
export class JobsStatusEditComponent {
  public status: MetaJobsStatus | null = null;
  public textColors: { label: string; value: string }[] = [
    { label: 'Fonçé', value: '#222' },
    { label: 'Clair', value: '#ccc' },
  ];
  public form = this._fb.group({
    label: ['', Validators.required],
    color: ['', Validators.required],
    textColor: ['', Validators.required],
  });

  public error$ = this._store.select(MetaJobsStatusSelectors.selectHandleError);
  private _success$ = this._store.select(
    MetaJobsStatusSelectors.selectHandleSuccess,
  );

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _dialogRef: MatDialogRef<JobsStatusEditComponent>,
    @Inject(MAT_DIALOG_DATA) data: { status: MetaJobsStatus },
  ) {
    this.status = data.status;
    this.form.get('label')?.setValue(this.status.label);
    this.form.get('color')?.setValue(this.status.color);
    this.form.get('textColor')?.setValue(this.status.textColor);
  }

  saveStatus() {
    const label = this.form.get('label')?.value;
    const color = this.form.get('color')?.value;
    const textColor = this.form.get('textColor')?.value;

    if (!label || !color || !textColor || !this.status?._id) {
      return;
    }

    this._store.dispatch(
      MetaJobsStatusActions.tryUpdateStatusAction({
        id: this.status._id,
        status: { label, color, textColor },
      }),
    );
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this._store.dispatch(MetaJobsStatusActions.tryFetchStatusListAction());
        this._dialogRef.close();
      }
    });
  }

  getFormTitle(): string {
    return this.status
      ? `Modifier le statut ${this.status.name}`
      : 'Ajouter un statut';
  }
}
