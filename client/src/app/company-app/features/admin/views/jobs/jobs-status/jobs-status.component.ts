import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  MetaJobsStatusSelectors,
  MetaJobsStatusActions,
} from '../../../../../shared/store/meta-jobs-status';
import { MatDialog } from '@angular/material/dialog';
import { JobsStatusEditComponent } from './jobs-status-edit/jobs-status-edit.component';
import { MetaJobsStatus } from '../../../../../shared/interfaces';

@Component({
  selector: 'app-jobs-status',
  templateUrl: './jobs-status.component.html',
  styleUrls: ['./jobs-status.component.scss'],
})
export class JobsStatusComponent {
  public statusList$ = this._store.select(MetaJobsStatusSelectors.selectStatus);

  constructor(private _store: Store, private _dialog: MatDialog) {}

  syncFromMultiPress() {
    this._store.dispatch(MetaJobsStatusActions.trySyncFromMultiPress());
  }

  openEditStatus(status: MetaJobsStatus) {
    this._dialog.open(JobsStatusEditComponent, {
      width: '60rem',
      data: { status },
    });
  }
}
