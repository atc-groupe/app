import { Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './views/users/users/users.component';
import { RolesComponent } from './views/users/roles/roles.component';
import { AuthorizationsComponent } from './views/users/authorizations/authorizations.component';
import { JobsStatusComponent } from './views/jobs/jobs-status/jobs-status.component';
import { ArticleMetaComponent } from './views/stock/article-meta/article-meta.component';

export const ADMIN_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'users/list',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'users/list', component: UsersComponent },
      { path: 'users/roles', component: RolesComponent },
      {
        path: 'users/roles/authorizations',
        component: AuthorizationsComponent,
      },
      { path: 'jobs/status', component: JobsStatusComponent },
      { path: 'stock/articles', component: ArticleMetaComponent },
    ],
  },
];
