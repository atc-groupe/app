import { Component } from '@angular/core';
import { ADMIN_NAV } from './admin.nav';
import { NavService } from '../../shared/services/nav.service';

@Component({
  selector: 'company-app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  public nav$ = this._navService.getNavWithAuthorizations(ADMIN_NAV);

  constructor(private _navService: NavService) {}
}
