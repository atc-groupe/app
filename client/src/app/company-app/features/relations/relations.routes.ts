import { Routes } from '@angular/router';
import { RelationsComponent } from './relations.component';

export const RELATIONS_ROUTES: Routes = [
  { path: '', component: RelationsComponent },
];
