// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { LayoutModule } from '../../shared/modules/layout.module';
import { RouterModule } from '@angular/router';

// Components
import { RelationsComponent } from './relations.component';

// Routes
import { RELATIONS_ROUTES } from './relations.routes';

@NgModule({
  declarations: [RelationsComponent],
  imports: [CoreModule, LayoutModule, RouterModule.forChild(RELATIONS_ROUTES)],
})
export class RelationsModule {}
