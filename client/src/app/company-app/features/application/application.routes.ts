import { Routes } from '@angular/router';
import { ApplicationComponent } from './application.component';

export const APPLICATION_ROUTES: Routes = [
  { path: '', component: ApplicationComponent },
];
