// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';

// Components
import { ApplicationComponent } from './application.component';

// Routes
import { APPLICATION_ROUTES } from './application.routes';

@NgModule({
  declarations: [ApplicationComponent],
  imports: [CoreModule, RouterModule.forChild(APPLICATION_ROUTES)],
})
export class ApplicationModule {}
