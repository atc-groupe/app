import { ModuleAuthorizations } from '../../../shared/interfaces';
import { Action, Module, Subject } from '../../shared/enums';

export const JOBS_AUTHORIZATIONS: ModuleAuthorizations = {
  module: Module.Jobs,
  label: 'Jobs',
  subjectsAuthorizations: [
    {
      label: 'Job',
      authorizations: [
        {
          subject: Subject.Job,
          action: Action.ManagePao,
          label: 'Gestion des données PAO',
          description: 'Vérif fichiers commentaires',
        },
        {
          subject: Subject.Job,
          action: Action.ChangeJobStatus,
          label: 'Changement de statut du job',
        },
      ],
    },
    {
      label: 'Reservations matières',
      authorizations: [
        {
          subject: Subject.PaoReservations,
          action: Action.Manage,
          label: 'Ajouter, modifier ou supprimer une reservation matière',
        },
        {
          subject: Subject.PaoReservations,
          action: Action.ChangeJobStatus,
          label: "Modifier le statut d'une réservation matière",
          description: 'Utilisé par le responsable des reservations matières',
        },
        {
          subject: Subject.PaoReservations,
          action: Action.Handle,
          label: 'Traiter une réservation matière',
          description: 'Traiter, et déstocker une réservation matière',
        },
      ],
    },
  ],
};
