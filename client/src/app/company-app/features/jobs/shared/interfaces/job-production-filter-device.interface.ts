export interface JobProductionFilterDevice {
  device: string;
  isActive: boolean;
}
