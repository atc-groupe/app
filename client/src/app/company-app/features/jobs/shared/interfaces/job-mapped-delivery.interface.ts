export interface JobMappedDeliveryItem {
  id: number;
  quantity: number; // mp run
  packedPer: number; //
  ordering: number;
  description: string;
  remark: string;
  extraInfo?: string;
  weight: number;
  status: string;
  timestamp: Date;
  productType?: string;
  device?: string | null;
  surface?: number | null;
  deliveryServiceFinishs?: string[] | null;
  hasSheetPrinting?: boolean | null;
  hasNumericScoreCutting?: boolean | null;
  hasZundCutting?: boolean | null;
  hasKits?: boolean | null;
}

export interface JobMappedDelivery {
  sendingDate: Date | null;
  deliveryDate: Date | null;
  deliveryMethod: string;
  company: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  countryCode: string;
  extraAddress?: string[];
  contactName: string;
  phone: string;
  email: string;
  relation: string;
  relationNumber: number;
  totalWeight: number;
  items: JobMappedDeliveryItem[];
}
