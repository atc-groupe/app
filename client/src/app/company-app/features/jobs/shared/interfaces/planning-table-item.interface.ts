import { JobSyncStatusType } from '../../../../shared/types/job-sync-status.type';

export interface PlanningTableItem {
  ordering: number;
  jobNumber: string;
  customer: string;
  status: string;
  factory: string[];
  sendingDate: string | null;
  subJobCount: number;
  printingSurface: number;
  manufacturedPieceCount: number;
  comment: string | null;
  syncStatus: JobSyncStatusType;
}

export type PlanningTableItems = PlanningTableItem[];

export interface MachinePlanningTableItems {
  machineId: number;
  items: PlanningTableItems;
}

export type PlanningTableItemsGroup = MachinePlanningTableItems[];
