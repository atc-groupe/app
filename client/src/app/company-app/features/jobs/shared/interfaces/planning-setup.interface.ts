export interface PlanningSetupOperationEmployee {
  name: string;
  id: number;
}

export interface PlanningSetupMachineOperation {
  name: string;
  speed: number;
  standard: boolean;
  operation: string;
  number: number;
  employees: PlanningSetupOperationEmployee[];
}

export interface PlanningSetupMachine {
  id: number;
  name: string;
  ordering: number;
  color: string;
  remark: string;
  operations: PlanningSetupMachineOperation[];
}

export interface PlanningSetupGroup {
  department: number;
  name: string;
  machines: PlanningSetupMachine[];
}

export type PlanningSetup = PlanningSetupGroup[];
