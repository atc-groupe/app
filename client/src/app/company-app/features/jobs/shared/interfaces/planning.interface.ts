import { Job } from '../../../../shared/interfaces';

export interface PlanningItemCardDay {
  date: Date;
  time: number; // in minutes;
  fixedTimeActive: boolean;
  fixedTime: number; // in minutes
  ordering: number;
}

export interface PlanningItemCard {
  startDate: Date;
  productionTime: number;
  calculatedTime: number;
  employee: string;
  extraInfo: string;
  information: string;
  completed: boolean;
  job: {
    operation: string;
    paperOrdered: boolean;
    paperAvailable: boolean;
    notPlanned: boolean;
    filesReady: boolean;
  };
}

export interface PlanningItem {
  date: Date;
  time: number; // in minutes
  ordering: number;
  card: PlanningItemCard;
  job: Job;
}

export interface PlanningMachineItem {
  machineName: string;
  machineId: number;
  items: PlanningItem[];
}

export type PlanningMachinesItems = PlanningMachineItem[];
