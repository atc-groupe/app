export enum JobsUserSettings {
  DefaultPlanningGroup = 'jobs.planning.group',
  DefaultPlanningMachine = 'jobs.planning.machine',
}
