import { Injectable } from '@angular/core';
import { Job, SubJob } from '../../../../shared/interfaces';
import { JobProductionFilterDevice } from '../interfaces';
import { SubJobProdReferenceService } from './sub-job-prod-reference.service';

@Injectable({ providedIn: 'root' })
export class JobProductionFilterHelperService {
  public readonly NO_PROD = 'Sans prod';
  public readonly INVOICE = 'Devis';

  constructor(private _subJobProdRefService: SubJobProdReferenceService) {}

  getInitialDevices(job: Job): JobProductionFilterDevice[] | null {
    if (!job.subJobs) {
      return null;
    }

    const devicesFilters: JobProductionFilterDevice[] =
      job.meta.productionTypes.map((device) => {
        return {
          device,
          isActive: true,
        };
      });

    if (job.subJobs) {
      const hasNoProdSubJob = job.subJobs.some(
        (subJob) =>
          subJob.productionData === null && !this._isInvoiceSubJob(subJob),
      );

      if (hasNoProdSubJob) {
        devicesFilters.push({ device: this.NO_PROD, isActive: false });
      }

      const hasDevSubJob = job.subJobs.some((subJob) =>
        this._isInvoiceSubJob(subJob),
      );

      if (hasDevSubJob) {
        devicesFilters.push({ device: this.INVOICE, isActive: false });
      }
    }

    return devicesFilters;
  }

  toggleDeviceFilter(
    devicesFilters: JobProductionFilterDevice[] | null,
    device: string,
  ): JobProductionFilterDevice[] | null {
    if (!devicesFilters) {
      return null;
    }

    return devicesFilters.map((item) => {
      if (item.device === device) {
        return {
          device: item.device,
          isActive: !item.isActive,
        };
      }

      return item;
    });
  }

  getFilteredSubJobs(
    baseSubJobs: SubJob[] | null,
    filterDevices: JobProductionFilterDevice[] | null,
    search: string | null,
  ): SubJob[] | null {
    if (!filterDevices || !baseSubJobs) {
      return null;
    }

    const subJobs = this._filterSubJobsByDevice(baseSubJobs, filterDevices);

    if (!search) {
      return subJobs.length ? subJobs : null;
    }

    return this._filterSubJobsByRef(subJobs, search);
  }

  private _isInvoiceSubJob(subJob: SubJob) {
    const checklist = subJob.checklist;
    const descriptionItem = checklist.find(
      (item) => item.label.toLowerCase() === 'description',
    );

    if (!descriptionItem) {
      return false;
    }

    return /^de?v[_ -]*.*/.test(descriptionItem.value.trim().toLowerCase());
  }

  private _filterSubJobsByDevice(
    subJobs: SubJob[],
    filterDevices: JobProductionFilterDevice[],
  ): SubJob[] {
    return subJobs.filter((subJob) => {
      const devFilterActive = filterDevices.find(
        (item) => item.device === this.INVOICE && item.isActive,
      );

      if (this._isInvoiceSubJob(subJob)) {
        return devFilterActive;
      }

      const subJobDevices = subJob.productionData?.devices;
      const noProdFilterActive = filterDevices.some(
        (item) => item.device === this.NO_PROD && item.isActive,
      );

      if (!subJobDevices || !subJobDevices.length) {
        return noProdFilterActive;
      }

      return subJobDevices.some((subJobDevice) => {
        return filterDevices.some((filter) => {
          return filter.isActive && subJobDevice.includes(filter.device);
        });
      });
    });
  }

  private _filterSubJobsByRef(subJobs: SubJob[], search: string) {
    return subJobs.filter((subJob) => {
      const prodRef = this._subJobProdRefService.getDisplayIndex(subJob.index);
      if (
        search.length === 1 ||
        (search.length === 2 && search.startsWith('Z')) ||
        (search.length === 3 && search.startsWith('ZZ'))
      ) {
        return search === prodRef;
      }
      if (subJob.description.toLowerCase().includes(search.toLowerCase())) {
        return true;
      }
      if (!subJob.productionData) {
        return false;
      }

      return subJob.productionData.numericLayers?.some((numericLayer) => {
        if (
          numericLayer.sheetName?.toLowerCase().includes(search.toLowerCase())
        ) {
          return true;
        }

        return numericLayer.mediaName
          .toLowerCase()
          .includes(search.toLowerCase());
      });
    });
  }
}
