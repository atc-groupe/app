import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class JobPlanningService {
  constructor(private _http: HttpClient) {}

  updatePlanningComment(
    jobNumber: string,
    comment: string | null,
  ): Observable<void> {
    return this._http.patch<void>(
      `${environment.api.url}/jobs/${jobNumber}/planning`,
      { comment },
    );
  }
}
