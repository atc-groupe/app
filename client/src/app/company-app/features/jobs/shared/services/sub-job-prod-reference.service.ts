import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SubJobProdReferenceService {
  private _alphabet = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];

  getDisplayIndex(index: number): string {
    const zNumber = Math.floor((index + 1) / 27);
    const alphabetIndex = (index + 1) % 27;
    if (zNumber === 0) {
      return this._alphabet[index];
    }

    let ref = '';
    for (let i = 0; i < zNumber; i++) {
      ref += 'Z';
    }

    return `${ref}${this._alphabet[alphabetIndex]}`;
  }
}
