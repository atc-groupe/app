import { Injectable } from '@angular/core';
import {
  PlanningItem,
  PlanningMachinesItems,
  PlanningTableItem,
  PlanningTableItems,
  PlanningTableItemsGroup,
} from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class PlanningItemsMappingService {
  getMappedGroup(items: PlanningMachinesItems): PlanningTableItemsGroup {
    return items.map((item) => {
      return {
        machineId: item.machineId,
        items: this._getMappedCollection(item.items),
      };
    });
  }

  private _getMappedCollection(items: PlanningItem[]): PlanningTableItems {
    return items.map((item): PlanningTableItem => {
      return {
        ordering: item.ordering,
        jobNumber: item.job.mp.number,
        customer: item.job.mp.company,
        status: item.job.mp.jobStatus,
        factory: item.job.meta.productionTypes,
        sendingDate: item.job.mp.sendingDate,
        subJobCount: item.job.subJobs ? item.job.subJobs.length : 0,
        printingSurface: item.job.meta.printingSurface,
        manufacturedPieceCount: item.job.meta.manufacturedPiecesCount,
        comment: item.job.planning?.comment ?? null,
        syncStatus: item.job.syncStatus,
      };
    });
  }
}
