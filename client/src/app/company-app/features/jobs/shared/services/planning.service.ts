import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlanningMachinesItems, PlanningSetup } from '../interfaces';
import { environment } from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlanningService {
  constructor(private _http: HttpClient) {}

  getSetup(): Observable<PlanningSetup> {
    return this._http.get<PlanningSetup>(
      `${environment.api.url}/planning/setup`,
    );
  }

  getGroupItems(
    startDate: Date,
    endDate: Date,
    department: number,
    group: string,
  ): Observable<PlanningMachinesItems> {
    return this._http.get<PlanningMachinesItems>(
      `${environment.api.url}/planning/lines`,
      {
        params: {
          startDate: startDate.toISOString(),
          endDate: endDate.toISOString(),
          department,
          group,
        },
      },
    );
  }
}
