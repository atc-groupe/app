import { Injectable } from '@angular/core';
import { Job, SubJobDeliveryAddress } from '../../../../shared/interfaces';
import { JobMappedDelivery, JobMappedDeliveryItem } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class JobViewDeliveriesMappingService {
  getMappedDeliveries(job: Job): JobMappedDelivery[] {
    if (!job.deliveryAddresses) {
      return [];
    }

    return job.deliveryAddresses.reduce((acc: JobMappedDelivery[], address) => {
      const mappedAddress = this._getMappedAddress(address);
      const mappedItem = this._getMappedAddressItem(job, address);

      if (this._isNotProductionItem(mappedItem)) {
        return acc;
      }

      if (!acc.length) {
        mappedAddress.items.push(mappedItem);
        mappedAddress.totalWeight += mappedItem.weight;
        acc.push(mappedAddress);
        return acc;
      }

      const existingAddress = acc.find((item) =>
        this._isEqualAddress(item, mappedAddress),
      );

      if (existingAddress) {
        existingAddress.items.push(mappedItem);
        existingAddress.totalWeight += mappedItem.weight;
      } else {
        mappedAddress.items.push(mappedItem);
        mappedAddress.totalWeight += mappedItem.weight;
        acc.push(mappedAddress);
      }

      return acc;
    }, []);
  }

  private _getMappedAddress(address: SubJobDeliveryAddress): JobMappedDelivery {
    return {
      sendingDate: address.mp.sendingDate,
      deliveryDate: address.mp.deliveryDate,
      deliveryMethod: address.mp.deliveryMethod,
      company: address.mp.company,
      address: address.mp.address,
      zipCode: address.mp.zipCode,
      city: address.mp.city,
      country: address.mp.country,
      countryCode: address.mp.countryCode,
      extraAddress: address.mp.extraAddress,
      contactName: address.mp.contactName,
      phone: address.mp.phone,
      email: address.mp.email,
      relation: address.mp.relation,
      relationNumber: address.mp.relationNumber,
      totalWeight: 0,
      items: [],
    };
  }

  private _getMappedAddressItem(
    job: Job,
    address: SubJobDeliveryAddress,
  ): JobMappedDeliveryItem {
    const subJob = job.subJobs?.find((subJob) => subJob._id === address.subJob);

    const item: JobMappedDeliveryItem = {
      id: address.mp.id,
      quantity: address.mp.quantity,
      packedPer: address.mp.packedPer,
      ordering: address.mp.ordering,
      description: address.mp.description,
      remark: address.mp.remark,
      extraInfo: address.mp.extraInfo,
      weight: address.mp.weight,
      status: address.mp.status,
      timestamp: address.mp.timestamp,
    };

    if (subJob) {
      item.productType = subJob.productType;

      if (subJob.productionData) {
        item.surface = subJob.productionData.meta.surface;
        item.deliveryServiceFinishs =
          subJob.productionData.meta.deliveryServiceFinishs;
        item.hasSheetPrinting = subJob.productionData.meta.hasSheetPrinting;
        item.hasNumericScoreCutting =
          subJob.productionData.meta.hasNumericScoreCutting;
        item.hasZundCutting = subJob.productionData.meta.hasZundCutting;
        item.hasKits = subJob.productionData.meta.hasKits;
      }
    }

    return item;
  }

  private _isEqualAddress(
    address1: JobMappedDelivery,
    address2: JobMappedDelivery,
  ): boolean {
    return (
      address1.sendingDate === address2.sendingDate &&
      address1.deliveryDate === address2.deliveryDate &&
      address1.deliveryMethod === address2.deliveryMethod &&
      address1.company === address2.company &&
      address1.address === address2.address &&
      address1.zipCode === address2.zipCode &&
      address1.city === address2.city &&
      address1.country === address2.country &&
      address1.countryCode === address2.countryCode &&
      address1.contactName === address2.contactName &&
      address1.phone === address2.phone &&
      address1.email === address2.email &&
      address1.relation === address2.relation &&
      address1.relationNumber === address2.relationNumber
    );
  }

  private _isNotProductionItem(deliveryItem: JobMappedDeliveryItem): boolean {
    if (this._isInvoiceDeliveryItem(deliveryItem)) {
      return true;
    }

    if (!deliveryItem.productType) {
      return false;
    }

    if (['pose', 'colis'].includes(deliveryItem.productType.toLowerCase())) {
      return true;
    }

    return false;
  }

  private _isInvoiceDeliveryItem(deliveryItem: JobMappedDeliveryItem): boolean {
    return /^de?v[_ -]*.*/.test(deliveryItem.description.trim().toLowerCase());
  }
}
