import { Injectable } from '@angular/core';
import { PlanningTableItems } from '../interfaces';
import { Job } from '../../../../shared/interfaces';

@Injectable({ providedIn: 'root' })
export class PlanningSearchItemsMappingService {
  getMappedData(jobs: Job[]): PlanningTableItems {
    return jobs.map((job) => {
      return {
        ordering: 0,
        jobNumber: job.mp.number,
        customer: job.mp.company,
        status: job.mp.jobStatus,
        factory: job.meta.productionTypes,
        sendingDate: job.mp.sendingDate,
        subJobCount: job.subJobs ? job.subJobs.length : 0,
        printingSurface: job.meta.printingSurface,
        manufacturedPieceCount: job.meta.manufacturedPiecesCount,
        comment: job.planning?.comment ?? null,
        syncStatus: job.syncStatus,
      };
    });
  }
}
