import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { debounceTime, first, Subscription } from 'rxjs';

@Directive({
  selector: '[planningScroll]',
})
export class ScrollDirective implements AfterViewInit, OnDestroy {
  @Input() setInitScroll?: EventEmitter<number>;

  @Output() planningScroll = new EventEmitter<number>();
  @HostListener('scroll') onScroll() {
    this.planningScroll.emit(this.el.nativeElement.scrollTop);
  }

  private _subscription = new Subscription();

  constructor(private el: ElementRef<HTMLDivElement>) {}

  ngAfterViewInit() {
    if (this.setInitScroll) {
      this._subscription = this.setInitScroll
        .pipe(debounceTime(5))
        .subscribe((scroll) => {
          this.el.nativeElement.scrollTo(0, scroll);
        });
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
