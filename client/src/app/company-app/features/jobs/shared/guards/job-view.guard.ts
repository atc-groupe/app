import { CanActivateFn } from '@angular/router';
import { JobActions } from '../../shared/store/job';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';

export const JobViewGuard: CanActivateFn = (route) => {
  const jobNumber = route.paramMap.get('number');

  if (!jobNumber) {
    return false;
  }

  inject(Store).dispatch(
    JobActions.tryFetchJobAction({ jobNumber: parseInt(jobNumber) }),
  );

  return true;
};
