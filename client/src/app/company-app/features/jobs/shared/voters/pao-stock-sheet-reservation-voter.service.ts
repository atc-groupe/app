import { Injectable } from '@angular/core';
import { AuthorizationsService } from '../../../../../shared/services/authorizations.service';
import {
  Action,
  PaoStockSheetReservationStatus,
  Subject,
} from '../../../../shared/enums';
import { JobPaoStockSheetReservation } from '../../../../shared/interfaces';

@Injectable({ providedIn: 'root' })
export class PaoStockSheetReservationVoter {
  private _canManage = false; // For PAO service
  private _canHandle = false; // For Reservation service

  constructor(private _authService: AuthorizationsService) {
    this._authService
      .canActivate(Subject.PaoReservations, Action.Manage)
      .subscribe((canActivate) => {
        this._canManage = canActivate;
      });

    this._authService
      .canActivate(Subject.PaoReservations, Action.Handle)
      .subscribe((canActivate) => {
        this._canHandle = canActivate;
      });
  }

  canSendPaoResaGlobally(reservations: JobPaoStockSheetReservation[]): boolean {
    if (!this._canManage) {
      return false;
    }

    return reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Created,
    );
  }

  canSendPaoResa(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canManage) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Created;
  }

  canUpdateOrDeletePaoResa(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canManage) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Created;
  }

  canHandlePaoResaStatusGlobally(
    reservations: JobPaoStockSheetReservation[],
  ): boolean {
    if (!this._canHandle) {
      return false;
    }

    const hasPending = reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Pending,
    );

    const hasValidStatus = reservations.every((resa) => {
      return (
        resa.status === PaoStockSheetReservationStatus.Created ||
        resa.status === PaoStockSheetReservationStatus.Pending
      );
    });

    return hasPending && hasValidStatus;
  }

  canUndoHandlingGlobally(
    reservations: JobPaoStockSheetReservation[],
  ): boolean {
    if (!this._canHandle) {
      return false;
    }

    const hasTreated = reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Treated,
    );

    const hasValidStatus = reservations.every((resa) => {
      return (
        resa.status === PaoStockSheetReservationStatus.Created ||
        resa.status === PaoStockSheetReservationStatus.Treated
      );
    });

    return hasTreated && hasValidStatus;
  }

  canResetPaoResaStatusToPendingGlobally(
    reservations: JobPaoStockSheetReservation[],
  ): boolean {
    if (!this._canHandle) {
      return false;
    }

    const hasOrdered = reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Ordered,
    );

    const hasValidStatus = reservations.every((resa) => {
      return (
        resa.status === PaoStockSheetReservationStatus.Created ||
        resa.status === PaoStockSheetReservationStatus.Ordered
      );
    });

    return hasOrdered && hasValidStatus;
  }

  canReturnPaoResaGlobally(
    reservations: JobPaoStockSheetReservation[],
  ): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Pending,
    );
  }

  canUndoHandlingPaoResa(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Treated;
  }

  canReturnPaoResa(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Pending;
  }

  canHandleResaGlobally(reservations: JobPaoStockSheetReservation[]): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Pending,
    );
  }

  /*
    This authorization is used to treat, or set to standBy or ordered status the PAO Reservation
   */
  canHandlePaoResa(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Pending;
  }

  canResetPaoResaToPending(reservation: JobPaoStockSheetReservation): boolean {
    if (!this._canHandle) {
      return false;
    }

    return reservation.status === PaoStockSheetReservationStatus.Ordered;
  }
}
