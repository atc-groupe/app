import { Injectable } from '@angular/core';
import { AuthorizationsService } from '../../../../../shared/services/authorizations.service';
import { Action, Subject } from '../../../../shared/enums';

@Injectable({ providedIn: 'root' })
export class JobStatusVoterService {
  private _canChangeJobStatus = false;

  constructor(private _authService: AuthorizationsService) {
    this._authService
      .canActivate(Subject.Job, Action.ChangeJobStatus)
      .subscribe((canActivate) => {
        this._canChangeJobStatus = canActivate;
      });
  }

  get canChangeJobStatus(): boolean {
    return this._canChangeJobStatus;
  }
}
