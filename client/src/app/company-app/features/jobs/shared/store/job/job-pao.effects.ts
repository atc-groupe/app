import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as JobActions from './job.actions';
import * as JobSelectors from './job.selectors';
import {
  catchError,
  EMPTY,
  map,
  of,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs';
import { JobPaoService } from '../../../../../shared/services/job-pao.service';
import { Store } from '@ngrx/store';

@Injectable()
export class JobPaoEffects {
  tryChangeJobPAOCheckCommentEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryChangeJobPaoCheckCommentAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ checkComment }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .updateCheckComment(job.mp.number, checkComment)
          .pipe(
            tap(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
            }),
            map(() => JobActions.changePaoCheckCommentSuccessAction()),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryAddPaoStockSheetReservationEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryAddPaoReservationAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ reservation }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .insertStockSheetReservation(job.mp.number, reservation)
          .pipe(
            map(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
              return JobActions.addPaoReservationSuccessAction();
            }),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryUpdatePaoStockSheetReservationEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryUpdatePaoReservationAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ id, reservation }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .updateStockSheetReservation(job.mp.number, id, reservation)
          .pipe(
            map(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
              return JobActions.updatePaoReservationSuccessAction();
            }),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryRemovePaoStockSheetReservationEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryRemovePaoReservationAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ id }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .removeStockSheetReservation(job.mp.number, id)
          .pipe(
            map(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
              return JobActions.removePaoReservationSuccessAction();
            }),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryChangePaoStockSheetReservationsStatusesEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryChangePaoReservationsStatusesAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ dto }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .changeStockSheetReservationsStatuses(job.mp.number, dto)
          .pipe(
            map(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
              return JobActions.changePaoReservationsStatusesSuccessAction();
            }),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryChangePaoStockSheetReservationStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryChangePaoReservationStatusAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ reservationId, dto }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPaoService
          .changeStockSheetReservationStatus(job.mp.number, reservationId, dto)
          .pipe(
            map(() => {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
              return JobActions.changePaoReservationStatusSuccessAction();
            }),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _jobPaoService: JobPaoService,
  ) {}
}
