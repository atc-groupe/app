import {
  Job,
  StockJobReservations,
  SubJob,
} from '../../../../../shared/interfaces';
import { createReducer, on } from '@ngrx/store';
import * as JobActions from './job.actions';
import { JobProductionFilterDevice } from '../../interfaces';

export const JOB_REDUCER_KEY = 'job';

export interface JobState {
  job: Job | null;
  stockReservations: StockJobReservations | null;
  fetchError: string | null;
  handleSuccess: boolean | null;
  handleError: string | null;
  changeStatusSuccess: boolean | null;
  syncError: string | null;
  isLoading: boolean;
  productionFilters: {
    subJobs: SubJob[] | null;
    search: string | null;
    devices: JobProductionFilterDevice[] | null;
  };
}

const INITIAL_STATE: JobState = {
  job: null,
  stockReservations: null,
  fetchError: null,
  handleSuccess: null,
  handleError: null,
  changeStatusSuccess: null,
  syncError: null,
  isLoading: false,
  productionFilters: {
    subJobs: null,
    search: null,
    devices: [],
  },
};

export const jobReducer = createReducer(
  INITIAL_STATE,
  on(JobActions.tryFetchJobAction, (state): JobState => {
    return {
      ...state,
      job: null,
      fetchError: null,
      handleSuccess: null,
      handleError: null,
      isLoading: true,
    };
  }),
  on(JobActions.fetchJobSuccessAction, (state, { job }): JobState => {
    return {
      ...state,
      job,
      fetchError: null,
      isLoading: false,
    };
  }),
  on(JobActions.tryChangeJobPlanningCommentAction, (state): JobState => {
    return {
      ...state,
      handleSuccess: null,
      handleError: null,
    };
  }),
  on(
    JobActions.changeJobPlanningCommentSuccessAction,
    JobActions.changePaoCheckCommentSuccessAction,
    (state): JobState => {
      return {
        ...state,
        handleSuccess: true,
        handleError: null,
      };
    },
  ),
  on(JobActions.handleJobErrorAction, (state, { error }): JobState => {
    return {
      ...state,
      handleSuccess: false,
      handleError: error,
    };
  }),
  on(
    JobActions.trySyncJobAction,
    JobActions.tryFetchPendingJobAction,
    (state): JobState => {
      return {
        ...state,
        isLoading: true,
        changeStatusSuccess: null,
        syncError: null,
      };
    },
  ),
  on(JobActions.syncJobSuccess, (state): JobState => {
    return {
      ...state,
    };
  }),
  on(JobActions.syncJobError, (state, { error }): JobState => {
    return {
      ...state,
      isLoading: false,
      changeStatusSuccess: false,
      syncError: error,
    };
  }),
  on(JobActions.tryChangeJobStatusAction, (state): JobState => {
    return {
      ...state,
      changeStatusSuccess: null,
      syncError: null,
    };
  }),
  on(JobActions.tryCheckJobStatusChangedAction, (state): JobState => {
    return {
      ...state,
      changeStatusSuccess: null,
      syncError: null,
    };
  }),
  on(JobActions.checkJobStatusChangedSuccessAction, (state): JobState => {
    return {
      ...state,
      changeStatusSuccess: true,
      syncError: null,
      isLoading: false,
    };
  }),
  on(JobActions.tryAddPaoReservationAction, (state): JobState => {
    return {
      ...state,
      handleSuccess: null,
      handleError: null,
    };
  }),
  on(
    JobActions.addPaoReservationSuccessAction,
    JobActions.updatePaoReservationSuccessAction,
    JobActions.removePaoReservationSuccessAction,
    JobActions.changePaoReservationsStatusesSuccessAction,
    JobActions.changePaoReservationStatusSuccessAction,
    (state): JobState => {
      return {
        ...state,
        handleSuccess: true,
      };
    },
  ),
  on(JobActions.tryFetchStockReservationsAction, (state): JobState => {
    return {
      ...state,
      fetchError: null,
    };
  }),
  on(
    JobActions.fetchStockReservationsSuccessAction,
    (state, { reservations }): JobState => {
      return {
        ...state,
        stockReservations: reservations,
        fetchError: null,
      };
    },
  ),
  on(
    JobActions.fetchStockReservationsErrorAction,
    (state, { error }): JobState => {
      return {
        ...state,
        fetchError: error,
      };
    },
  ),
  on(
    JobActions.initProductionFilters,
    (state, { subJobs, devices }): JobState => {
      return {
        ...state,
        fetchError: null,
        isLoading: false,
        productionFilters: {
          ...state.productionFilters,
          subJobs,
          devices,
        },
      };
    },
  ),
  on(JobActions.setProductionFilterSubJobs, (state, { subJobs }): JobState => {
    return {
      ...state,
      productionFilters: {
        ...state.productionFilters,
        subJobs,
      },
    };
  }),
  on(JobActions.setProductionFilterSearch, (state, { search }): JobState => {
    return {
      ...state,
      productionFilters: {
        ...state.productionFilters,
        search,
      },
    };
  }),
  on(JobActions.setProductionFilterDevices, (state, { devices }): JobState => {
    return {
      ...state,
      productionFilters: {
        ...state.productionFilters,
        devices,
      },
    };
  }),
);
