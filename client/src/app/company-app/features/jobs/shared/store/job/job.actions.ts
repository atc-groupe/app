import { createAction, props } from '@ngrx/store';
import {
  Job,
  StockJobReservations,
  SubJob,
} from '../../../../../shared/interfaces';
import {
  PaoStockSheetReservationCreateDto,
  PaoStockSheetReservationStatusDto,
  PaoStockSheetReservationUpdateDto,
} from '../../../../../shared/dto';
import { JobProductionFilterDevice } from '../../interfaces';

// ------ Fetch job
export const tryFetchJobAction = createAction(
  '[CompanyApp > Jobs > Job] try fetch job',
  props<{ jobNumber: number }>(),
);
export const tryFetchPendingJobAction = createAction(
  '[CompanyApp > Jobs > Job] try fetch pending job',
  props<{ jobNumber: number }>(),
);
export const fetchJobSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] fetch job success',
  props<{ job: Job }>(),
);
export const fetchJobErrorAction = createAction(
  '[CompanyApp > Jobs > Job] fetch job error',
  props<{ error: string }>(),
);

// ------ Job status ------
export const tryChangeJobStatusAction = createAction(
  '[CompanyApp > Jobs > Job] try change job status',
  props<{ jobNumber: string; jobStatus: number }>(),
);
export const tryCheckJobStatusChangedAction = createAction(
  '[CompanyApp > Jobs > Job] try check job status changed',
  props<{ jobNumber: string; oldStatus: number; newStatus: number }>(),
);
export const checkJobStatusChangedSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] check job status changed success',
);

// ------ Job comment ------
export const tryChangeJobPlanningCommentAction = createAction(
  '[CompanyApp > Jobs > Job] try change job comment',
  props<{ comment: string | null }>(),
);
export const changeJobPlanningCommentSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] change job planning comment success',
);

// ------ Job PAO checkComment ------
export const tryChangeJobPaoCheckCommentAction = createAction(
  '[CompanyApp > Jobs > Job] try change job PAO checkComment',
  props<{ checkComment: string | null }>(),
);
export const changePaoCheckCommentSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] change job PAO check comment success',
);

// ------ Job sync ------
export const trySyncJobAction = createAction(
  '[CompanyApp > Jobs > Job] try sync job',
  props<{ jobId: number }>(),
);
export const syncJobSuccess = createAction(
  '[CompanyApp > Jobs > Job] sync job success',
);
export const syncJobError = createAction(
  '[CompanyApp > Jobs > Job] sync job error',
  props<{ error: string }>(),
);

// ------ handle error ------
export const handleJobErrorAction = createAction(
  '[CompanyApp > Jobs > Job] handle job error',
  props<{ error: string }>(),
);

// ------ PAO Reservation ------
export const tryAddPaoReservationAction = createAction(
  '[CompanyApp > Jobs > Job] try add pao reservation',
  props<{ reservation: PaoStockSheetReservationCreateDto }>(),
);
export const addPaoReservationSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] add pao reservation success',
);
export const tryUpdatePaoReservationAction = createAction(
  '[CompanyApp > Jobs > Job] try update pao reservation',
  props<{ id: string; reservation: PaoStockSheetReservationUpdateDto }>(),
);
export const updatePaoReservationSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] update pao reservation success',
);
export const tryRemovePaoReservationAction = createAction(
  '[CompanyApp > Jobs > Job] try remove pao reservation',
  props<{ id: string }>(),
);
export const removePaoReservationSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] remove pao reservation success',
);
export const tryChangePaoReservationsStatusesAction = createAction(
  '[CompanyApp > Jobs > Job] try change pao reservations statuses',
  props<{ dto: PaoStockSheetReservationStatusDto }>(),
);
export const changePaoReservationsStatusesSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] change pao reservations statuses success',
);
export const tryChangePaoReservationStatusAction = createAction(
  '[CompanyApp > Jobs > Job] try send pao reservation',
  props<{ reservationId: string; dto: PaoStockSheetReservationStatusDto }>(),
);
export const changePaoReservationStatusSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] send pao reservation success',
);

// ------ Stock Reservations ------
export const tryFetchStockReservationsAction = createAction(
  '[CompanyApp > Jobs > Job] try fetch stock reservations',
  props<{ jobNumber: string }>(),
);
export const fetchStockReservationsSuccessAction = createAction(
  '[CompanyApp > Jobs > Job] fetch stock reservations success',
  props<{ reservations: StockJobReservations }>(),
);
export const fetchStockReservationsErrorAction = createAction(
  '[CompanyApp > Jobs > Job] fetch stock reservations error',
  props<{ error: string }>(),
);

// ------ Production filters ------
export const initProductionFilters = createAction(
  '[CompanyApp > Jobs > Job] init production filters',
  props<{
    subJobs: SubJob[] | null;
    devices: JobProductionFilterDevice[] | null;
  }>(),
);
export const setProductionFilterSubJobs = createAction(
  '[CompanyApp > Jobs > Job] set production filter sub jobs',
  props<{ subJobs: SubJob[] | null }>(),
);
export const setProductionFilterSearch = createAction(
  '[CompanyApp > Jobs > Job] set production filter search',
  props<{ search: string | null }>(),
);
export const setProductionFilterDevices = createAction(
  '[CompanyApp > Jobs > Job] set production filter devices',
  props<{ devices: JobProductionFilterDevice[] | null }>(),
);
export const toggleProductionFilterDevice = createAction(
  '[CompanyApp > Jobs > Job] toggle production filter device',
  props<{ device: string }>(),
);
