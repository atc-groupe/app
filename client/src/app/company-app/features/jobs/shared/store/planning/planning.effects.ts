import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as PlanningActions from './planning.actions';
import * as PlanningSelectors from './planning.selectors';
import {
  catchError,
  debounceTime,
  EMPTY,
  map,
  of,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs';
import { PlanningService } from '../../services/planning.service';
import { Store } from '@ngrx/store';
import { PlanningItemsMappingService } from '../../services/planning-items-mapping.service';
import { JobsService } from '../../../../../shared/services/jobs.service';
import { PlanningSearchItemsMappingService } from '../../services/planning-search-items-mapping.service';

@Injectable()
export class PlanningEffects {
  tryFetchSetupEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(PlanningActions.tryFetchSetupAction),
      switchMap(() =>
        this._planningService.getSetup().pipe(
          map((setup) => PlanningActions.fetchSetupSuccessAction({ setup })),
          catchError((err) =>
            of(
              PlanningActions.fetchSetupErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryFetchPlanningDataEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(PlanningActions.tryFetchPlanningData),
      withLatestFrom(
        this._store.select(PlanningSelectors.selectSearch),
        this._store.select(PlanningSelectors.selectCurrentDate),
        this._store.select(PlanningSelectors.selectCurrentGroup),
      ),
      debounceTime(150),
      switchMap(([action, search, currentDate, currentGroup]) => {
        if (search) {
          return of(PlanningActions.trySearchJobsAction({ search }));
        }

        if (!currentGroup) {
          return EMPTY;
        }

        return this._planningService
          .getGroupItems(
            currentDate,
            currentDate,
            currentGroup.department,
            currentGroup.name,
          )
          .pipe(
            map((items) =>
              PlanningActions.fetchGroupItemsSuccessAction({
                items: this._mappingService.getMappedGroup(items),
              }),
            ),
            catchError((err) =>
              of(
                PlanningActions.fetchGroupItemsErrorAction({
                  error: err.error.message,
                }),
              ),
            ),
          );
      }),
    ),
  );

  trySearchJobsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(PlanningActions.trySearchJobsAction),
      tap(() => {
        this._store.dispatch(
          PlanningActions.setIsLoadingAction({ isLoading: true }),
        );
      }),
      debounceTime(1000),
      switchMap(({ search }) =>
        this._jobsService.fetchJobs({ fullSearch: search }).pipe(
          map((jobs) =>
            PlanningActions.searchJobsSuccessAction({
              items: this._searchMappingService.getMappedData(jobs),
            }),
          ),
          catchError((err) =>
            of(
              PlanningActions.searchJobsErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  changePlanningVariableDataEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(
        PlanningActions.setPreviousDateAction,
        PlanningActions.setNextDateAction,
        PlanningActions.setTodayDateAction,
        PlanningActions.setCurrentSettingsAction,
        PlanningActions.clearSearchJobsAction,
      ),
      map(() => PlanningActions.tryFetchPlanningData()),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _planningService: PlanningService,
    private _jobsService: JobsService,
    private _mappingService: PlanningItemsMappingService,
    private _searchMappingService: PlanningSearchItemsMappingService,
    private _store: Store,
  ) {}
}
