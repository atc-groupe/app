import { createAction, props } from '@ngrx/store';
import {
  PlanningSetup,
  PlanningTableItems,
  PlanningTableItemsGroup,
} from '../../interfaces';

// ------ Planning setup ------
export const tryFetchSetupAction = createAction(
  '[CompanyApp > Jobs > Planning] try fetch planning setup',
);
export const fetchSetupSuccessAction = createAction(
  '[CompanyApp > Jobs > Planning] fetch planning setup success',
  props<{ setup: PlanningSetup }>(),
);
export const fetchSetupErrorAction = createAction(
  '[CompanyApp > Jobs > Planning] fetch planning setup error',
  props<{ error: string }>(),
);

// ------ Planning items ------
export const tryFetchPlanningData = createAction(
  '[CompanyApp > Jobs > Planning] try fetch planning items',
);
export const fetchGroupItemsSuccessAction = createAction(
  '[CompanyApp > Jobs > Planning] fetch planning items success',
  props<{ items: PlanningTableItemsGroup }>(),
);
export const fetchGroupItemsErrorAction = createAction(
  '[CompanyApp > Jobs > Planning] fetch planning items error',
  props<{ error: string }>(),
);

// ------ Planning current date ------
export const setNextDateAction = createAction(
  '[CompanyApp > Jobs > Planning] set next current date',
);
export const setPreviousDateAction = createAction(
  '[CompanyApp > Jobs > Planning] set previous current date',
);
export const setTodayDateAction = createAction(
  '[CompanyApp > Jobs > Planning] set current date to today',
);

// ------ Planning current group
export const setCurrentSettingsAction = createAction(
  '[CompanyApp > Jobs > Planning] set current settings',
  props<{ groupName: string; machineId: number }>(),
);

// ------ Planning is loading
export const setIsLoadingAction = createAction(
  '[CompanyApp > Jobs > Planning] set is loading',
  props<{ isLoading: boolean }>(),
);

// ------ Jobs search -------
export const trySearchJobsAction = createAction(
  '[CompanyApp > Jobs > Planning] try search jobs',
  props<{ search: string }>(),
);
export const searchJobsSuccessAction = createAction(
  '[CompanyApp > Jobs > Planning] search jobs success',
  props<{ items: PlanningTableItems }>(),
);
export const searchJobsErrorAction = createAction(
  '[CompanyApp > Jobs > Planning] search jobs success',
  props<{ error: string }>(),
);
export const clearSearchJobsAction = createAction(
  '[CompanyApp > Jobs > Planning] clear search',
);

// ------ Scroll ------
export const trySetScrollAction = createAction(
  '[CompanyApp > Jobs > Planning] set scroll',
  props<{ scroll: number }>(),
);
export const setScrollAction = createAction(
  '[CompanyApp > Jobs > Planning] set scroll',
  props<{ scroll: number }>(),
);
