import {
  PlanningSetup,
  PlanningSetupGroup,
  PlanningSetupMachine,
  PlanningTableItemsGroup,
  PlanningTableItems,
} from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import * as PlanningActions from './planning.actions';

export const PLANNING_REDUCER_KEY = 'planning';

export interface PlanningState {
  setup: PlanningSetup | null;
  selectedGroupItems: PlanningTableItemsGroup;
  displayedItems: PlanningTableItems;
  currentDate: Date;
  currentGroup: PlanningSetupGroup | null;
  currentMachine: PlanningSetupMachine | null;
  search: string | null;
  isLoading: boolean;
  fetchSuccess: boolean | null;
  fetchError: string | null;
  scroll: number;
}

const initialDate = new Date();

const INITIAL_STATE: PlanningState = {
  setup: null,
  selectedGroupItems: [],
  displayedItems: [],
  currentDate: initialDate,
  currentGroup: null,
  currentMachine: null,
  search: null,
  isLoading: false,
  fetchSuccess: null,
  fetchError: null,
  scroll: 0,
};

export const planningReducer = createReducer(
  INITIAL_STATE,
  on(PlanningActions.tryFetchSetupAction, (state): PlanningState => {
    return {
      ...state,
      fetchSuccess: null,
      fetchError: null,
    };
  }),
  on(
    PlanningActions.fetchSetupSuccessAction,
    (state, { setup }): PlanningState => {
      const initGroup = setup.length ? setup[0] : null;

      return {
        ...state,
        setup,
        currentGroup: initGroup,
        currentMachine: initGroup?.machines.length
          ? initGroup.machines[0]
          : null,
      };
    },
  ),
  on(
    PlanningActions.fetchSetupErrorAction,
    (state, { error }): PlanningState => {
      return {
        ...state,
        setup: [],
        fetchError: error,
      };
    },
  ),
  on(PlanningActions.tryFetchPlanningData, (state): PlanningState => {
    return {
      ...state,
      selectedGroupItems: [],
      isLoading: true,
      fetchSuccess: null,
      fetchError: null,
    };
  }),
  on(
    PlanningActions.fetchGroupItemsSuccessAction,
    (state, { items }): PlanningState => {
      const displayedItems = items.find(
        (item) => item.machineId === state.currentMachine?.id,
      );

      return {
        ...state,
        selectedGroupItems: items,
        displayedItems: displayedItems ? displayedItems.items : [],
        isLoading: false,
        fetchSuccess: true,
        fetchError: null,
      };
    },
  ),
  on(
    PlanningActions.fetchGroupItemsErrorAction,
    (state, { error }): PlanningState => {
      return {
        ...state,
        selectedGroupItems: [],
        isLoading: false,
        fetchSuccess: false,
        fetchError: error,
      };
    },
  ),
  on(PlanningActions.setTodayDateAction, (state): PlanningState => {
    const date = new Date();

    return {
      ...state,
      currentDate: date,
      search: null,
      scroll: 0,
    };
  }),
  on(PlanningActions.setNextDateAction, (state): PlanningState => {
    const newDate = new Date(state.currentDate);
    const inc = newDate.getDay() === 5 ? 3 : 1;
    newDate.setDate(newDate.getDate() + inc);

    return {
      ...state,
      currentDate: newDate,
      search: null,
      scroll: 0,
    };
  }),
  on(PlanningActions.setPreviousDateAction, (state): PlanningState => {
    const newDate = new Date(state.currentDate);
    const inc = newDate.getDay() === 1 ? 3 : 1;
    newDate.setDate(newDate.getDate() - inc);

    return {
      ...state,
      currentDate: newDate,
      search: null,
      scroll: 0,
    };
  }),
  on(
    PlanningActions.setCurrentSettingsAction,
    (state, { groupName, machineId }): PlanningState => {
      const group = state.setup!.find((group) => group.name === groupName);
      const machine = group?.machines.find((item) => item.id === machineId);
      return {
        ...state,
        currentGroup: group ?? null,
        currentMachine: machine ?? null,
      };
    },
  ),
  on(
    PlanningActions.setIsLoadingAction,
    (state, { isLoading }): PlanningState => {
      return {
        ...state,
        isLoading,
      };
    },
  ),
  on(
    PlanningActions.trySearchJobsAction,
    (state, { search }): PlanningState => {
      return {
        ...state,
        search,
      };
    },
  ),
  on(
    PlanningActions.searchJobsSuccessAction,
    (state, { items }): PlanningState => {
      return {
        ...state,
        displayedItems: items,
        isLoading: false,
      };
    },
  ),
  on(PlanningActions.clearSearchJobsAction, (state): PlanningState => {
    return {
      ...state,
      search: null,
    };
  }),
  on(PlanningActions.setScrollAction, (state, { scroll }): PlanningState => {
    return {
      ...state,
      scroll,
    };
  }),
);
