import {
  PLANNING_REDUCER_KEY,
  planningReducer,
  PlanningState,
} from './planning';
import { ActionReducerMap } from '@ngrx/store';
import { JOB_REDUCER_KEY, jobReducer, JobState } from './job';

export interface JobsState {
  [PLANNING_REDUCER_KEY]: PlanningState;
  [JOB_REDUCER_KEY]: JobState;
}

export const JOBS_REDUCERS: ActionReducerMap<JobsState> = {
  [PLANNING_REDUCER_KEY]: planningReducer,
  [JOB_REDUCER_KEY]: jobReducer,
};
