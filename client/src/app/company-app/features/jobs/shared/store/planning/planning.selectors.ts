import { createFeatureSelector, createSelector } from '@ngrx/store';
import { JobsState } from '../index';
import { JOBS_REDUCER_KEY } from '../store.constants';
import { PLANNING_REDUCER_KEY, PlanningState } from './planning.reducer';
import {
  PlanningSetup,
  PlanningSetupGroup,
  PlanningSetupMachine,
  PlanningTableItemsGroup,
  PlanningTableItems,
} from '../../interfaces';

const selectJobsState = createFeatureSelector<JobsState>(JOBS_REDUCER_KEY);
const selectPlanningState = createSelector(
  selectJobsState,
  (state): PlanningState => state[PLANNING_REDUCER_KEY],
);

export const selectSetup = createSelector(
  selectPlanningState,
  (state): PlanningSetup | null => state.setup,
);

export const selectSelectedGroupItems = createSelector(
  selectPlanningState,
  (state): PlanningTableItemsGroup => state.selectedGroupItems,
);

export const selectDisplayedItems = createSelector(
  selectPlanningState,
  (state): PlanningTableItems => state.displayedItems,
);

export const selectSearch = createSelector(
  selectPlanningState,
  (state): string | null => state.search,
);
export const selectIsLoading = createSelector(
  selectPlanningState,
  (state): boolean => state.isLoading,
);
export const selectFetchSuccess = createSelector(
  selectPlanningState,
  (state): boolean | null => state.fetchSuccess,
);
export const selectFetchError = createSelector(
  selectPlanningState,
  (state): string | null => state.fetchError,
);
export const selectCurrentDate = createSelector(
  selectPlanningState,
  (state): Date => state.currentDate,
);
export const selectCurrentGroup = createSelector(
  selectPlanningState,
  (state): PlanningSetupGroup | null => state.currentGroup,
);
export const selectCurrentMachine = createSelector(
  selectPlanningState,
  (state): PlanningSetupMachine | null => state.currentMachine,
);
export const selectScroll = createSelector(
  selectPlanningState,
  (state): number => state.scroll,
);
