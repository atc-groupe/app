import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as JobActions from './job.actions';
import * as JobSelectors from './job.selectors';
import { UsersSelectors } from '../../../../../../shared/store/users';
import {
  catchError,
  debounceTime,
  delay,
  EMPTY,
  map,
  of,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs';
import { Store } from '@ngrx/store';
import { PlanningActions } from '../planning';
import { JobPlanningService } from '../../services/job-planning.service';
import { StockService, JobsService } from '../../../../../shared/services';
import { tryFetchJobAction } from './job.actions';
import { JobProductionFilterHelperService } from '../../services/job-production-filter-helper.service';

@Injectable()
export class JobEffects {
  tryFetchJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryFetchJobAction),
      switchMap(({ jobNumber }) =>
        this._jobsService.fetchJob(jobNumber).pipe(
          map((job) => {
            const devices =
              this._jobProdFiltersHelperService.getInitialDevices(job);
            this._store.dispatch(
              JobActions.initProductionFilters({
                subJobs: this._jobProdFiltersHelperService.getFilteredSubJobs(
                  job.subJobs ? job.subJobs : null,
                  devices,
                  null,
                ),
                devices,
              }),
            );
            return JobActions.fetchJobSuccessAction({ job });
          }),
          catchError((err) =>
            of(JobActions.fetchJobErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  tryFetchPendingJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryFetchPendingJobAction),
      delay(2000),
      switchMap(({ jobNumber }) =>
        this._jobsService.fetchJob(jobNumber).pipe(
          map((job) => {
            const devices =
              this._jobProdFiltersHelperService.getInitialDevices(job);
            this._store.dispatch(
              JobActions.initProductionFilters({
                subJobs: this._jobProdFiltersHelperService.getFilteredSubJobs(
                  job.subJobs ? job.subJobs : null,
                  devices,
                  null,
                ),
                devices,
              }),
            );
            return JobActions.fetchJobSuccessAction({ job });
          }),
          catchError((err) =>
            of(JobActions.fetchJobErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  tryChangeJobStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryChangeJobStatusAction),
      withLatestFrom(
        this._store.select(UsersSelectors.selectAuthenticatedUser),
        this._store.select(JobSelectors.selectJob),
      ),
      switchMap(([{ jobNumber, jobStatus }, user, job]) => {
        if (!user) {
          return of(
            JobActions.handleJobErrorAction({
              error: `Un problème est survenu, Veuillez re-essayer. Si le problème persiste, contactez l'administrateur`,
            }),
          );
        }

        const reason = `[API] par ${user.firstName.toUpperCase()} ${user.lastName.toUpperCase()}`;

        return this._jobsService
          .changeStatus(jobNumber, jobStatus, reason)
          .pipe(
            map(() =>
              JobActions.tryCheckJobStatusChangedAction({
                jobNumber,
                oldStatus: job!.mp.jobStatusNumber,
                newStatus: jobStatus,
              }),
            ),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  tryCheckJobStatusChangedEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryCheckJobStatusChangedAction),
      debounceTime(300),
      switchMap(({ jobNumber, oldStatus, newStatus }) =>
        this._jobsService.fetchJob(parseInt(jobNumber)).pipe(
          map((job) => {
            if (job.mp.jobStatusNumber === newStatus) {
              return JobActions.checkJobStatusChangedSuccessAction();
            }

            return JobActions.tryCheckJobStatusChangedAction({
              jobNumber,
              oldStatus,
              newStatus,
            });
          }),
        ),
      ),
    ),
  );

  tryChangeJobPlanningCommentEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryChangeJobPlanningCommentAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ comment }, job]) => {
        if (!job) {
          return EMPTY;
        }

        return this._jobPlanningService
          .updatePlanningComment(job.mp.number, comment)
          .pipe(
            map(() => JobActions.changeJobPlanningCommentSuccessAction()),
            catchError((err) =>
              of(JobActions.handleJobErrorAction({ error: err.error.message })),
            ),
          );
      }),
    ),
  );

  changeJobPlanningCommentEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.changeJobPlanningCommentSuccessAction),
      map(() => PlanningActions.tryFetchPlanningData()),
    ),
  );

  trySyncJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.trySyncJobAction),
      withLatestFrom(this._store.select(JobSelectors.selectJob)),
      switchMap(([{ jobId }, job]) =>
        this._jobsService.sync(jobId).pipe(
          tap(() => {
            if (job) {
              this._store.dispatch(
                JobActions.tryFetchJobAction({
                  jobNumber: parseInt(job.mp.number),
                }),
              );
            }
          }),
          delay(1000),
          map(() => JobActions.syncJobSuccess()),
          catchError((err) => {
            if (job) {
              this._store.dispatch(
                tryFetchJobAction({ jobNumber: parseInt(job.mp.number) }),
              );
            }

            return of(JobActions.syncJobError({ error: err.error.message }));
          }),
        ),
      ),
    ),
  );

  tryFetchStockReservationsEffect = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.tryFetchStockReservationsAction),
      switchMap(({ jobNumber }) =>
        this._stockService.getStockJobReservations(jobNumber).pipe(
          map((reservations) =>
            JobActions.fetchStockReservationsSuccessAction({ reservations }),
          ),
          catchError((err) =>
            of(
              JobActions.fetchStockReservationsErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  productionDataFilterSearchEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.setProductionFilterSearch),
      withLatestFrom(
        this._store.select(JobSelectors.selectJob),
        this._store.select(JobSelectors.selectProdFiltersDevices),
      ),
      switchMap(([{ search }, job, filterDevices]) => {
        if (!job || !job.subJobs) {
          return of(JobActions.setProductionFilterSubJobs({ subJobs: null }));
        }

        const subJobs = this._jobProdFiltersHelperService.getFilteredSubJobs(
          job.subJobs,
          filterDevices,
          search,
        );

        return of(JobActions.setProductionFilterSubJobs({ subJobs }));
      }),
    ),
  );

  productionDataFilterDevicesEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobActions.toggleProductionFilterDevice),
      withLatestFrom(
        this._store.select(JobSelectors.selectJob),
        this._store.select(JobSelectors.selectProdFiltersDevices),
        this._store.select(JobSelectors.selectProdFiltersSearch),
      ),
      switchMap(([{ device }, job, filterDevices, search]) => {
        if (!job || !job.subJobs) {
          return of(JobActions.setProductionFilterSubJobs({ subJobs: null }));
        }

        const devices = this._jobProdFiltersHelperService.toggleDeviceFilter(
          filterDevices,
          device,
        );
        const subJobs = this._jobProdFiltersHelperService.getFilteredSubJobs(
          job?.subJobs,
          devices,
          search,
        );
        this._store.dispatch(
          JobActions.setProductionFilterSubJobs({ subJobs }),
        );

        return of(JobActions.setProductionFilterDevices({ devices }));
      }),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _jobsService: JobsService,
    private _jobPlanningService: JobPlanningService,
    private _jobProdFiltersHelperService: JobProductionFilterHelperService,
    private _stockService: StockService,
    private _store: Store,
  ) {}
}
