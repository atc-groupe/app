import { createFeatureSelector, createSelector } from '@ngrx/store';
import { JobsState } from '../index';
import { JOBS_REDUCER_KEY } from '../store.constants';
import { JOB_REDUCER_KEY, JobState } from './job.reducer';
import {
  Job,
  StockJobReservations,
  SubJob,
} from '../../../../../shared/interfaces';
import { JobProductionFilterDevice } from '../../interfaces';

const selectJobsState = createFeatureSelector<JobsState>(JOBS_REDUCER_KEY);
const selectJobState = createSelector(
  selectJobsState,
  (state): JobState => state[JOB_REDUCER_KEY],
);

export const selectJob = createSelector(
  selectJobState,
  (state): Job | null => state.job,
);
export const selectFetchError = createSelector(
  selectJobState,
  (state): string | null => state.fetchError,
);
export const selectHandleSuccess = createSelector(
  selectJobState,
  (state): boolean | null => state.handleSuccess,
);
export const selectHandleError = createSelector(
  selectJobState,
  (state): string | null => state.handleError,
);
export const selectChangeStatusSuccess = createSelector(
  selectJobState,
  (state): boolean | null => state.changeStatusSuccess,
);
export const selectSyncError = createSelector(
  selectJobState,
  (state): string | null => state.syncError,
);
export const selectIsLoading = createSelector(
  selectJobState,
  (state): boolean => state.isLoading,
);
export const selectStockReservations = createSelector(
  selectJobState,
  (state): StockJobReservations | null => state.stockReservations,
);
export const selectProdFiltersSubJobs = createSelector(
  selectJobState,
  (state): SubJob[] | null => state.productionFilters.subJobs,
);
export const selectProdFiltersSearch = createSelector(
  selectJobState,
  (state): string | null => state.productionFilters.search,
);
export const selectProdFiltersDevices = createSelector(
  selectJobState,
  (state): JobProductionFilterDevice[] | null =>
    state.productionFilters.devices,
);
