import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobProductionFilterDevice } from '../../../../shared/interfaces';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { JobActions } from '../../../../shared/store/job';
import { JobProductionFilterHelperService } from '../../../../shared/services/job-production-filter-helper.service';

@Component({
  selector: 'job-view-production-filters',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './job-view-production-filters.component.html',
  styleUrls: ['./job-view-production-filters.component.scss'],
})
export class JobViewProductionFiltersComponent implements OnInit, OnDestroy {
  @Input() search: string | null = null;
  @Input() deviceFilters: JobProductionFilterDevice[] | null = null;

  public form = this._fb.group({ filter: [''] });
  private _subscription = new Subscription();

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _filterHelper: JobProductionFilterHelperService,
  ) {}

  ngOnInit() {
    this._subscription.add(
      this.form
        .get('filter')
        ?.valueChanges.pipe(debounceTime(200))
        .subscribe((value) => {
          this._store.dispatch(
            JobActions.setProductionFilterSearch({ search: value }),
          );
        }),
    );
  }

  public onToggleDevice(device: string) {
    this._store.dispatch(JobActions.toggleProductionFilterDevice({ device }));
  }

  public isInfoDevice(device: string): boolean {
    return (
      device === this._filterHelper.INVOICE ||
      device === this._filterHelper.NO_PROD
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
