import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { JobActions, JobSelectors } from '../../../../shared/store/job';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import {
  Job,
  JobPaoStockSheetReservation,
  Product,
} from '../../../../../../shared/interfaces';
import { BehaviorSubject, filter, Subscription } from 'rxjs';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import {
  PaoStockSheetReservationStatus,
  ProductType,
} from '../../../../../../shared/enums';
import { JobViewResaHandleItemComponent } from '../job-view-resa-handle-item/job-view-resa-handle-item.component';
import { StockService } from '../../../../../../shared/services/stock.service';

@Component({
  selector: 'app-job-view-resa-handle',
  standalone: true,
  imports: [
    CommonModule,
    LayoutModule,
    JobViewResaHandleItemComponent,
    RouterLink,
  ],
  templateUrl: './job-view-resa-handle.component.html',
  styleUrls: ['./job-view-resa-handle.component.scss'],
})
export class JobViewResaHandleComponent implements OnInit, OnDestroy {
  public job: Job | null = null;
  public user$ = this._store.select(UsersSelectors.selectCompanyUser);
  public paoReservations: JobPaoStockSheetReservation[] | null = null;
  public handle = new EventEmitter<void>();
  public results$ = new BehaviorSubject<(boolean | null)[]>([]);
  public productsMap = new Map<number, Product>();
  public ready = false;
  public freeStockError: string | null = null;
  public success: boolean | null = null;

  private _subscription = new Subscription();
  private _freeStockMap = new Map<number, number>();
  private _totalQuantities = new Map<number, number>();

  constructor(
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _stockService: StockService,
    private _router: Router,
  ) {
    const jobNumber = this._activatedRoute.snapshot.paramMap.get('number');

    if (!jobNumber) {
      return;
    }

    this._subscription.add(
      this._store
        .select(JobSelectors.selectJob)
        .pipe(filter((v) => v !== null))
        .subscribe((job) => {
          if (!job) {
            return;
          }

          this.job = job;

          if (job.pao && job.pao.stockSheetReservations) {
            this.paoReservations = job.pao.stockSheetReservations.filter(
              (resa) => resa.status === PaoStockSheetReservationStatus.Pending,
            );

            this.results$.next(this.paoReservations.map(() => null));

            this.paoReservations.forEach((resa) => {
              const quantity = this._totalQuantities.get(resa.productId)!;
              this._totalQuantities.set(
                resa.productId,
                quantity ? quantity + resa.quantity : resa.quantity,
              );
            });

            this._stockService
              .fetchProductsDetails(ProductType.Paper, [
                ...new Set(this.paoReservations.map((resa) => resa.productId)),
              ])
              .subscribe((products) => {
                products.forEach((product) => {
                  this.productsMap.set(product.id, product);
                  this._freeStockMap.set(product.id, product.freeStock);
                });

                this._checkFreeStock();
                this.ready = true;
              });
          }
        }),
    );
  }

  ngOnInit() {
    this.results$.pipe(filter((v) => v.length > 0)).subscribe((results) => {
      if (results.every((result) => result === true)) {
        this.success = true;
        return;
      }

      if (results.every((result) => result !== null)) {
        this.success = false;
        return;
      }
    });
  }

  get jobNumber(): string | null {
    return this.job ? this.job.mp.number : null;
  }

  getProduct(id: number): Product | null {
    const product = this.productsMap.get(id);
    if (!product) {
      this.freeStockError = "Une matière n'a pas été trouvée dans le stock";
      return null;
    }

    return product;
  }

  onQuantityChange(data: { productId: number; change: number }) {
    const quantity = this._totalQuantities.get(data.productId)!;

    this._totalQuantities.set(data.productId, quantity + data.change);
    this._checkFreeStock();
  }

  onFinish(index: number, result: boolean) {
    const results = this.results$.value;
    results[index] = result;

    this.results$.next(results);
  }

  handleReservation() {
    if (this.freeStockError) {
      return;
    }

    this.success = null;
    this.handle.emit();
  }

  private _checkFreeStock() {
    this.freeStockError = null;

    this._totalQuantities.forEach((quantity, key) => {
      const product = this.productsMap.get(key)!;
      const hasMoreThanOneResa =
        this.paoReservations!.filter((resa) => resa.productId === key).length >
        1;
      if (quantity > product.freeStock && hasMoreThanOneResa) {
        this.freeStockError = `La totalité des réservations pour la matière "${product.name}" sont suppérieures au stock disponible. Réservation totale: ${quantity}ex, Stock disponible: ${product.freeStock}ex`;
      }
    });
  }

  async onReturn() {
    this._store.dispatch(
      JobActions.tryFetchJobAction({ jobNumber: parseInt(this.jobNumber!) }),
    );

    await this._router.navigate(['..'], { relativeTo: this._activatedRoute });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
