import { Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { JobSelectors, JobActions } from '../../../../shared/store/job';
import { first } from 'rxjs';
import { JobPaoStockSheetReservation } from '../../../../../../shared/interfaces';

@Component({
  selector: 'app-job-view-resa-delete',
  templateUrl: './job-view-resa-delete.component.html',
  styleUrls: ['./job-view-resa-delete.component.scss'],
})
export class JobViewResaDeleteComponent {
  public error$ = this._store.select(JobSelectors.selectHandleError);
  private _success$ = this._store.select(JobSelectors.selectHandleSuccess);
  constructor(
    private _store: Store,
    private _dialogRef: MatDialogRef<JobViewResaDeleteComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { resa: JobPaoStockSheetReservation },
  ) {}

  onDeleteResa() {
    this._store.dispatch(
      JobActions.tryRemovePaoReservationAction({ id: this.data.resa._id }),
    );
    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this._dialogRef.close();
      }
    });
  }
}
