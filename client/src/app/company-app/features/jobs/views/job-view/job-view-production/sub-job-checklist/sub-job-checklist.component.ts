import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SubJob,
  SubJobChecklistItem,
} from '../../../../../../shared/interfaces';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import { Clipboard } from '@angular/cdk/clipboard';
import { SubJobProdReferenceService } from '../../../../shared/services/sub-job-prod-reference.service';

@Component({
  selector: 'sub-job-checklist',
  standalone: true,
  imports: [CommonModule, LayoutModule],
  templateUrl: './sub-job-checklist.component.html',
  styleUrls: ['./sub-job-checklist.component.scss'],
})
export class SubJobChecklistComponent implements OnInit {
  @Input() subJob: SubJob | null = null;
  @Input() atcProdRef: string | null = null;
  public checklist: SubJobChecklistItem[] | null = null;
  public checklistCopied = false;

  constructor(private _clipboard: Clipboard) {}

  ngOnInit() {
    if (this.subJob) {
      this.checklist = this.subJob.checklist;
    }
  }

  public copyChecklist() {
    if (!this.subJob || !this.checklist) {
      return;
    }

    let checklist = this.checklist.reduce((acc, item) => {
      if (acc === '') {
        acc += item.value;
        return acc;
      }

      acc += `\n${item.value}`;
      return acc;
    }, '');

    this._clipboard.copy(
      `${this.atcProdRef}\n${this.subJob.quantity1}ex.\n${checklist}`,
    );

    this.checklistCopied = true;
    setTimeout(() => {
      this.checklistCopied = false;
    }, 1000);
  }
}
