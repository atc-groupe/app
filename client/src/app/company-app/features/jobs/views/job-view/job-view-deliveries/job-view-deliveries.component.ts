import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobSelectors } from '../../../shared/store/job';
import { Job, SubJobDeliveryAddress } from '../../../../../shared/interfaces';
import { JobViewDeliveriesMappingService } from '../../../shared/services/job-view-deliveries-mapping.service';
import {
  JobMappedDelivery,
  JobMappedDeliveryItem,
} from '../../../shared/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-job-view-deliveries',
  templateUrl: './job-view-deliveries.component.html',
  styleUrls: ['./job-view-deliveries.component.scss'],
})
export class JobViewDeliveriesComponent implements OnDestroy {
  public deliveries: JobMappedDelivery[] | null = null;
  private _subscription = new Subscription();
  private _job$ = this._store.select(JobSelectors.selectJob);

  constructor(
    private _store: Store,
    private _deliveriesMappingService: JobViewDeliveriesMappingService,
  ) {
    this._subscription = this._job$.subscribe((job) => {
      if (!job || !job.deliveryAddresses) {
        return;
      }

      this.deliveries = this._deliveriesMappingService.getMappedDeliveries(job);
    });
  }

  public getDisplayDeliveryAddress(delivery: JobMappedDelivery): string {
    if (!delivery.address) {
      return '-';
    }

    return `${delivery.address}\n${delivery.zipCode} ${delivery.city}\n${delivery.country} [${delivery.countryCode}]`;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
