import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  JobPaoStockSheetReservation,
  Product,
} from '../../../../../../shared/interfaces';
import { CompanyUser } from '../../../../../../../shared/interfaces';
import { PaoStockSheetReservationStatus } from '../../../../../../shared/enums';
import { catchError, debounceTime, EMPTY, Subscription } from 'rxjs';
import { JobPaoService, JobPaoReservationHelperService } from '../../../../../../shared/services';
import { RouterLink } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'job-view-resa-undo-item',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    FormsModule,
  ],
  templateUrl: './job-view-resa-undo-item.component.html',
  styleUrls: ['./job-view-resa-undo-item.component.scss'],
})
export class JobViewResaUndoItemComponent implements OnInit, OnDestroy {
  @Input() jobNumber: string | null = null;
  @Input() resa: JobPaoStockSheetReservation | null = null;
  @Input() handle: EventEmitter<void> | null = null;
  @Input() user: CompanyUser | null = null;
  @Input() product: Product | null = null;

  @Output() quantityChange = new EventEmitter<{
    productId: number;
    change: number;
  }>();
  @Output() finish = new EventEmitter<boolean>();

  public handleError: string | null = null;
  public processing = false;
  public success: boolean | null = null;
  public status: PaoStockSheetReservationStatus | null = null;
  public statusLabel: string | null = null;
  public statusColor: string | null = null;
  public add: number | null = null;
  public active = true;

  private _subscription = new Subscription();

  constructor(
    private _jobPaoService: JobPaoService,
    private _jobReservationHelper: JobPaoReservationHelperService,
  ) {}

  ngOnInit() {
    if (!this.resa || this.resa.stockDecreaseCount === undefined) {
      return;
    }

    this.setStatus(this.resa.status);
    this.add = this.resa.stockDecreaseCount;

    this._subscription.add(
      this.handle?.subscribe(() => {
        this._handleStockReservation();
      }),
    );
  }

  get resaDisplayName(): string {
    return this.resa!.displayName;
  }

  get paoResaDisplaySizeTitle(): string {
    return this._jobReservationHelper.getPaoResaDisplaySizeTitle(this.resa!);
  }

  get paoResaDisplaySize(): string {
    return this._jobReservationHelper.getPaoResaDisplaySize(this.resa!);
  }

  get displayFileInfo(): string {
    return this._jobReservationHelper.getDisplayFileInfo(this.resa!);
  }

  public setStatus(status: PaoStockSheetReservationStatus): void {
    this.statusLabel = this._jobReservationHelper.getDisplayStatus(status);
    this.statusColor = this._jobReservationHelper.getStatusColor(status);
  }

  get productStockLink(): string {
    return `/company/stock/materials/${this.resa!.productType}/${
      this.resa!.productId
    }`;
  }

  get isHandled(): boolean {
    return this.success !== null;
  }

  get isHandleSuccess() {
    return this.success === true;
  }

  get isHandleError() {
    return this.success === false;
  }

  private _handleStockReservation(): void {
    const resa = this.resa!;
    if (this.success === true || this.add === null || !this.active) {
      return;
    }

    this.handleError = null;
    this.processing = true;

    setTimeout(() => {
      this._jobPaoService
        .handleStockSheetReservation(this.jobNumber!, resa._id, {
          action: 'undo',
          productType: resa.productType,
          productId: resa.productId,
          add: this.add!,
          subtract: 0,
          employeeNumber: this.user!.employeeNumber,
          description: `Annulation du traitement de la resa matière par ${
            this.user!.firstName
          } ${this.user!.lastName}`,
        })
        .pipe(
          debounceTime(300),
          catchError((err) => {
            this.processing = false;
            this.handleError = err.error.message;
            this.success = false;
            this.finish.emit(false);
            return EMPTY;
          }),
        )
        .subscribe(() => {
          this.success = true;
          this.processing = false;
          this.setStatus(PaoStockSheetReservationStatus.Pending);
          this.finish.emit(true);
        });
    }, 500);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
