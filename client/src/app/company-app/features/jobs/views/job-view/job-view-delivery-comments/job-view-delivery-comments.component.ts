import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobSelectors } from '../../../shared/store/job';

@Component({
  selector: 'app-job-view-delivery-comments',
  templateUrl: './job-view-delivery-comments.component.html',
  styleUrls: ['./job-view-delivery-comments.component.scss'],
})
export class JobViewDeliveryCommentsComponent {
  public job$ = this._store.select(JobSelectors.selectJob);

  constructor(private _store: Store) {}
}
