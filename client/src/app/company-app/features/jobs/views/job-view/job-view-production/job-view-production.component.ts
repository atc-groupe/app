import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { JobSelectors } from '../../../shared/store/job';
import { Job, SubJob } from '../../../../../shared/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { JobViewProductionMaterialComponent } from './job-view-production-material/job-view-production-material.component';
import { JobProductionFilterDevice } from '../../../shared/interfaces';

@Component({
  selector: 'app-job-view-production',
  templateUrl: './job-view-production.component.html',
  styleUrls: ['./job-view-production.component.scss'],
})
export class JobViewProductionComponent implements OnInit, OnDestroy {
  public job: Job | null = null;
  public prodFilterSubJobs$: Observable<SubJob[] | null>;
  public prodFilterSearch$: Observable<string | null>;
  public prodFilterDevices$: Observable<JobProductionFilterDevice[] | null>;
  private _subscription = new Subscription();

  constructor(private _store: Store, private _dialog: MatDialog) {
    this.prodFilterSubJobs$ = this._store.select(
      JobSelectors.selectProdFiltersSubJobs,
    );
    this.prodFilterSearch$ = this._store.select(
      JobSelectors.selectProdFiltersSearch,
    );
    this.prodFilterDevices$ = this._store.select(
      JobSelectors.selectProdFiltersDevices,
    );
  }

  ngOnInit() {
    this._store.select(JobSelectors.selectJob).subscribe((job) => {
      this.job = job;
    });
  }

  public openMaterialInfo(event: { mediaId: number }) {
    this._dialog.open(JobViewProductionMaterialComponent, {
      width: '60rem',
      data: { productId: event.mediaId },
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
