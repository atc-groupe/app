import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Job,
  JobPaoStockSheetReservation,
} from '../../../../../../shared/interfaces';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import { JobPaoReservationHelperService } from '../../../../../../shared/services/job-pao-reservation-helper.service';
import { PaoStockSheetReservationStatus } from '../../../../../../shared/enums';

@Component({
  selector: 'app-job-view-resa-print',
  standalone: true,
  imports: [CommonModule, LayoutModule],
  templateUrl: './job-view-resa-print.component.html',
  styleUrls: ['./job-view-resa-print.component.scss'],
})
export class JobViewResaPrintComponent {
  public job: Job | null = null;
  public paoReservations: JobPaoStockSheetReservation[] | null = null;
  public user: string | null = null;
  public date = new Date();

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: { job: Job },
    private _jobResaHelper: JobPaoReservationHelperService,
  ) {
    this.job = data.job;

    if (!this.job || !this.job.pao) {
      return;
    }

    this.paoReservations = this.job.pao.stockSheetReservations.filter(
      (resa) =>
        resa.status === PaoStockSheetReservationStatus.Treated ||
        resa.status === PaoStockSheetReservationStatus.Pending ||
        resa.status === PaoStockSheetReservationStatus.Ordered,
    );

    if (!this.paoReservations.length) {
      return;
    }

    this.user = this.paoReservations[0].userDisplayName;
  }

  getPaoResaDisplaySize(resa: JobPaoStockSheetReservation): string {
    return this._jobResaHelper.getPaoResaDisplaySize(resa);
  }

  getDisplayFileInfo(resa: JobPaoStockSheetReservation): string {
    return this._jobResaHelper.getDisplayFileInfo(resa);
  }

  isOrdered(status: PaoStockSheetReservationStatus): boolean {
    return status === PaoStockSheetReservationStatus.Ordered;
  }

  isPending(status: PaoStockSheetReservationStatus): boolean {
    return status === PaoStockSheetReservationStatus.Pending;
  }

  isTreated(status: PaoStockSheetReservationStatus): boolean {
    return status === PaoStockSheetReservationStatus.Treated;
  }
}
