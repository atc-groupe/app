import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  JobPaoStockSheetReservation,
  Product,
} from '../../../../../../shared/interfaces';
import { catchError, debounceTime, EMPTY, Subscription } from 'rxjs';
import { JobPaoReservationHelperService } from '../../../../../../shared/services/job-pao-reservation-helper.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CompanyUser } from '../../../../../../../shared/interfaces';
import { JobPaoService } from '../../../../../../shared/services/job-pao.service';
import { PaoStockSheetReservationStatus } from '../../../../../../shared/enums';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  selector: 'job-view-resa-handle-item',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    RouterLink,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
  ],
  templateUrl: './job-view-resa-handle-item.component.html',
  styleUrls: ['./job-view-resa-handle-item.component.scss'],
})
export class JobViewResaHandleItemComponent implements OnInit, OnDestroy {
  @Input() jobNumber: string | null = null;
  @Input() resa: JobPaoStockSheetReservation | null = null;
  @Input() handle: EventEmitter<void> | null = null;
  @Input() user: CompanyUser | null = null;
  @Input() product: Product | null = null;

  @Output() quantityChange = new EventEmitter<{
    productId: number;
    change: number;
  }>();
  @Output() finish = new EventEmitter<boolean>();

  public handleError: string | null = null;
  public processing = false;
  public success: boolean | null = null;
  public quantity = 0;
  public status: PaoStockSheetReservationStatus | null = null;
  public statusLabel: string | null = null;
  public statusColor: string | null = null;
  public stockChipColor: string | null = null;
  public active = true;

  private _subscription = new Subscription();
  private _oldQuantity = 0;

  constructor(
    private _jobPaoService: JobPaoService,
    private _jobReservationHelper: JobPaoReservationHelperService,
  ) {}

  ngOnInit() {
    if (!this.resa) {
      return;
    }

    this.quantity = this.resa.quantity;
    this._oldQuantity = this.resa.quantity;
    this.setStatus(this.resa.status);
    this.setStockChipColor();

    this._subscription.add(
      this.handle?.subscribe(() => {
        this._handleStockReservation();
      }),
    );
  }

  get resaDisplayName(): string {
    return this.resa!.displayName;
  }

  get paoResaDisplaySizeTitle(): string {
    return this._jobReservationHelper.getPaoResaDisplaySizeTitle(this.resa!);
  }

  get paoResaDisplaySize(): string {
    return this._jobReservationHelper.getPaoResaDisplaySize(this.resa!);
  }

  get displayFileInfo(): string {
    return this._jobReservationHelper.getDisplayFileInfo(this.resa!);
  }

  public setStatus(status: PaoStockSheetReservationStatus): void {
    this.statusLabel = this._jobReservationHelper.getDisplayStatus(status);
    this.statusColor = this._jobReservationHelper.getStatusColor(status);
  }

  get paperStock(): number | null {
    return this.product ? this.product.freeStock : null;
  }

  get resaQuantity(): number {
    return this.resa!.quantity;
  }

  get productStockLink(): string {
    return `/company/stock/materials/${this.resa!.productType}/${
      this.resa!.productId
    }`;
  }

  get maxQuantity(): number {
    if (!this.product || this.product.freeStock < 0) {
      return 0;
    }

    return this.product.freeStock > this.resa!.quantity
      ? this.resa!.quantity
      : this.product.freeStock;
  }

  setStockChipColor(): void {
    if (!this.product) {
      return;
    }

    this.stockChipColor =
      this.product.freeStock < this.quantity ? 'chip-error' : 'chip-success';
  }

  get displayForm(): boolean {
    return !this.processing && this.success === null;
  }

  get isHandled(): boolean {
    return this.success !== null;
  }

  get isHandleSuccess() {
    return this.success === true;
  }

  get isHandleError() {
    return this.success === false;
  }

  onQuantityChange() {
    const change = this.quantity - this._oldQuantity;
    if (!change) {
      return;
    }

    this.setStockChipColor();
    this.quantityChange.emit({ productId: this.product!.id, change });
    this._oldQuantity = this.quantity;
  }

  private _handleStockReservation(): void {
    if (
      this.success === true ||
      this.quantity > this.maxQuantity ||
      !this.active
    ) {
      return;
    }

    this.success = null;
    this.handleError = null;
    this.processing = true;
    const resa = this.resa!;

    setTimeout(() => {
      this._jobPaoService
        .handleStockSheetReservation(this.jobNumber!, resa._id, {
          action: 'handle',
          productType: resa.productType,
          productId: resa.productId,
          add: 0,
          subtract: this.quantity,
          employeeNumber: this.user!.employeeNumber,
          description: `Préparation resa matière par ${this.user!.firstName} ${
            this.user!.lastName
          }`,
        })
        .pipe(
          debounceTime(300),
          catchError((err) => {
            this.processing = false;
            this.handleError = err.error.message;
            this.success = false;
            this.finish.emit(false);
            return EMPTY;
          }),
        )
        .subscribe(() => {
          this.success = true;
          this.processing = false;
          this.setStatus(PaoStockSheetReservationStatus.Treated);
          this.finish.emit(true);
        });
    }, 500);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
