import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';

@Component({
  selector: 'sub-job-comment',
  standalone: true,
  imports: [CommonModule, LayoutModule],
  templateUrl: './sub-job-comment.component.html',
  styleUrls: ['./sub-job-comment.component.scss'],
})
export class SubJobCommentComponent {
  @Input() comment: string | null = null;
}
