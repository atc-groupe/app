import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubJob } from '../../../../../../shared/interfaces';
import { SubJobChecklistComponent } from '../sub-job-checklist/sub-job-checklist.component';
import { SubJobProdComponent } from '../sub-job-prod/sub-job-prod.component';
import { SubJobCommentComponent } from '../sub-job-comment/sub-job-comment.component';
import { SubJobHeaderComponent } from '../sub-job-header/sub-job-header.component';
import { SubJobProdReferenceService } from '../../../../shared/services/sub-job-prod-reference.service';

@Component({
  selector: 'app-sub-job',
  standalone: true,
  imports: [
    CommonModule,
    SubJobChecklistComponent,
    SubJobProdComponent,
    SubJobCommentComponent,
    SubJobHeaderComponent,
  ],
  templateUrl: './sub-job.component.html',
  styleUrls: ['./sub-job.component.scss'],
})
export class SubJobComponent implements OnInit {
  @Input() jobDevices: string[] | null = null;
  @Input() subJob: SubJob | null = null;
  @Output() openMaterialInfo = new EventEmitter<{ mediaId: number }>();

  public atcProdRef: string | null = null;
  public expanded = true;

  constructor(private _subJobProdRefService: SubJobProdReferenceService) {}

  ngOnInit() {
    if (!this.subJob) {
      return;
    }

    this.atcProdRef = this._subJobProdRefService.getDisplayIndex(
      this.subJob.index,
    );
  }

  onOpenMaterialInfo(event: { mediaId: number }): void {
    this.openMaterialInfo.emit(event);
  }
}
