import { Component, Inject, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  StockActions,
  StockSelectors,
} from '../../../../../../shared/store/stock';
import { ProductType } from '../../../../../../shared/enums';
import { first, Subscription } from 'rxjs';
import { Paper } from '../../../../../../shared/interfaces';

@Component({
  selector: 'app-job-view-production-material',
  templateUrl: './job-view-production-material.component.html',
  styleUrls: ['./job-view-production-material.component.scss'],
})
export class JobViewProductionMaterialComponent implements OnDestroy {
  public product: Paper | null = null;
  public products: Paper[] | null = null;
  public error: string | null = null;
  private _productError$ = this._store.select(
    StockSelectors.selectFetchProductError,
  );
  private _subscription = new Subscription();
  private _product$ = this._store.select(StockSelectors.selectProduct);
  private _products$ = this._store.select(StockSelectors.selectProducts);

  constructor(
    private _store: Store,
    @Inject(MAT_DIALOG_DATA) public data: { productId: number },
  ) {
    this._store.dispatch(
      StockActions.tryFetchProductAction({
        productType: ProductType.Paper,
        productId: this.data.productId,
      }),
    );

    this._store.dispatch(
      StockActions.tryFetchProductsFromProductIdAction({
        productType: ProductType.Paper,
        productId: this.data.productId,
      }),
    );

    this._subscription.add(
      this._product$.subscribe((product) => {
        this.product = product as Paper;
      }),
    );

    this._subscription.add(
      this._products$.subscribe((products) => {
        this.products = products as Paper[];
      }),
    );

    this._subscription.add(
      this._productError$.pipe(first((v) => v !== null)).subscribe((error) => {
        this.error = error;
      }),
    );
  }

  getPaperSize(paper: Paper): string {
    return `${paper.width} x ${paper.length} ${
      paper.type === 'roll' ? 'ml' : 'mm'
    }`;
  }

  getDisplayStock(paper: Paper): string {
    return paper.stockManagement
      ? `(${paper.stock} ${paper.displayUnit} en stock dont ${paper.freeStock} ${paper.displayUnit} libres)`
      : '(Stock non géré)';
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
