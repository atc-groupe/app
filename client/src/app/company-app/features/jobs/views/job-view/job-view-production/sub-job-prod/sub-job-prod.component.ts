import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SubJob,
  SubJobFinishingLayer,
  SubJobNumericLayer,
  SubJobProductionData,
} from '../../../../../../shared/interfaces';
import { MpSubJobFinishingType } from '../../../../../../shared/enums';
import { JobViewProductionMaterialComponent } from '../job-view-production-material/job-view-production-material.component';

@Component({
  selector: 'sub-job-prod',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sub-job-prod.component.html',
  styleUrls: ['./sub-job-prod.component.scss'],
})
export class SubJobProdComponent implements OnInit {
  @Input() subJob: SubJob | null = null;
  @Output() openMaterialInfo = new EventEmitter<{ mediaId: number }>();

  public prodData: SubJobProductionData | null = null;

  ngOnInit() {
    if (this.subJob) {
      this.prodData = this.subJob.productionData;
    }
  }

  public hasProductionData(): boolean {
    if (!this.prodData) {
      return false;
    }

    return (
      this.prodData.numericLayers !== undefined ||
      this.prodData.subcontracting !== undefined ||
      this.prodData.unlinkedFinishingLayers !== undefined
    );
  }

  public hasMoreThanOneNumericLayer(): boolean {
    return (
      this.prodData !== null &&
      this.prodData.numericLayers !== null &&
      this.prodData.numericLayers.length > 1
    );
  }

  public getNumericLayerCount(index: number): string | null {
    if (this.hasMoreThanOneNumericLayer()) {
      return `${index + 1} - `;
    }

    return null;
  }

  public getDisplayFinishingLayer(layer: SubJobFinishingLayer): string {
    switch (layer.finishingType) {
      case MpSubJobFinishingType.largeFormat:
        return `${this._cleanName(layer.operation.name)} > ${this._cleanName(
          layer.operation.operationType,
        )} ${layer.material ? ' > ' + layer.material.name : ''}`;
      case MpSubJobFinishingType.lamination: {
        const doubleSided = layer.operation.doubleSided ? 'R/V' : 'Recto';
        const materialName = layer.material
          ? layer.material.name
          : '[NON DÉFINIE]';
        return `${layer.operation.name} ${materialName} - ${doubleSided}`;
      }
    }
  }

  public isLaminationFinishingLayerMaterialUndefined(
    layer: SubJobFinishingLayer,
  ): boolean {
    if (layer.finishingType !== MpSubJobFinishingType.lamination) {
      return false;
    }

    return !layer.material;
  }

  public displayNumericLayerQuality(numericLayer: SubJobNumericLayer): boolean {
    const quality = this._cleanName(numericLayer.quality);

    return (
      quality.toLowerCase() !==
      numericLayer.device.toLowerCase().replaceAll('é', 'e')
    );
  }

  public getDisplayNumericLayerQuality(quality: string): string {
    return this._cleanName(quality);
  }

  public hasSubcontracting(): boolean {
    return !!(
      this.prodData &&
      this.prodData.subcontracting &&
      this.prodData.subcontracting.length
    );
  }

  onOpenMaterialInfo(mediaId: number): void {
    this.openMaterialInfo.emit({ mediaId });
  }

  private _cleanName(item: string): string {
    return item.replaceAll(/[0-9]*\. /g, '').replaceAll('é', 'e');
  }
}
