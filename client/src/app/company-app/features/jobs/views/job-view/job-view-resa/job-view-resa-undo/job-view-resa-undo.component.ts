import { Component, EventEmitter, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobActions, JobSelectors } from '../../../../shared/store/job';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import {
  Job,
  JobPaoStockSheetReservation,
  Product,
} from '../../../../../../shared/interfaces';
import { BehaviorSubject, filter, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { StockService } from '../../../../../../shared/services';
import {
  PaoStockSheetReservationStatus,
  ProductType,
} from '../../../../../../shared/enums';
import { JobViewResaHandleItemComponent } from '../job-view-resa-handle-item/job-view-resa-handle-item.component';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import { JobViewResaUndoItemComponent } from '../job-view-resa-undo-item/job-view-resa-undo-item.component';

@Component({
  selector: 'app-job-view-resa-undo',
  standalone: true,
  imports: [
    CommonModule,
    JobViewResaHandleItemComponent,
    LayoutModule,
    RouterLink,
    JobViewResaUndoItemComponent,
  ],
  templateUrl: './job-view-resa-undo.component.html',
  styleUrls: ['./job-view-resa-undo.component.scss'],
})
export class JobViewResaUndoComponent implements OnDestroy {
  public job: Job | null = null;
  public user$ = this._store.select(UsersSelectors.selectCompanyUser);
  public paoReservations: JobPaoStockSheetReservation[] | null = null;
  public handle = new EventEmitter<void>();
  public results$ = new BehaviorSubject<(boolean | null)[]>([]);
  public productsMap = new Map<number, Product>();
  public ready = false;
  public success: boolean | null = null;

  private _subscription = new Subscription();

  constructor(
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _stockService: StockService,
    private _router: Router,
  ) {
    const jobNumber = this._activatedRoute.snapshot.paramMap.get('number');

    if (!jobNumber) {
      return;
    }

    this._subscription.add(
      this._store
        .select(JobSelectors.selectJob)
        .pipe(filter((v) => v !== null))
        .subscribe((job) => {
          if (!job) {
            return;
          }

          this.job = job;

          if (job.pao && job.pao.stockSheetReservations) {
            this.paoReservations = job.pao.stockSheetReservations.filter(
              (resa) => resa.status === PaoStockSheetReservationStatus.Treated,
            );

            this.results$.next(this.paoReservations.map(() => null));

            this._stockService
              .fetchProductsDetails(ProductType.Paper, [
                ...new Set(this.paoReservations.map((resa) => resa.productId)),
              ])
              .subscribe((products) => {
                products.forEach((product) => {
                  this.productsMap.set(product.id, product);
                });

                this.ready = true;
              });
          }
        }),
    );
  }

  ngOnInit() {
    this.results$.pipe(filter((v) => v.length > 0)).subscribe((results) => {
      if (results.every((result) => result === true)) {
        this.success = true;
        return;
      }

      if (results.every((result) => result !== null)) {
        this.success = false;
        return;
      }
    });
  }

  get jobNumber(): string | null {
    return this.job ? this.job.mp.number : null;
  }

  getProduct(id: number): Product | null {
    const product = this.productsMap.get(id);
    if (!product) {
      return null;
    }

    return product;
  }

  onFinish(index: number, result: boolean) {
    const results = this.results$.value;
    results[index] = result;

    this.results$.next(results);
  }

  handleReservation() {
    this.success = null;
    this.handle.emit();
  }

  async onReturn() {
    this._store.dispatch(
      JobActions.tryFetchJobAction({ jobNumber: parseInt(this.jobNumber!) }),
    );

    await this._router.navigate(['..'], { relativeTo: this._activatedRoute });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
