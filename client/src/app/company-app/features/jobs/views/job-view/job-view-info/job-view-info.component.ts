import { Component } from '@angular/core';
import { JobSelectors } from '../../../shared/store/job';
import { Store } from '@ngrx/store';

@Component({
  selector: 'jobs-job-view-info',
  templateUrl: './job-view-info.component.html',
  styleUrls: ['./job-view-info.component.scss'],
})
export class JobViewInfoComponent {
  public job$ = this._store.select(JobSelectors.selectJob);

  constructor(private _store: Store) {}

  getHistoryLineDate(line: string): string {
    return line.slice(0, 16);
  }

  getHistoryLineContent(line: string): string {
    return line.slice(16, line.length);
  }
}
