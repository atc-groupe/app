import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobSelectors, JobActions } from '../../../shared/store/job';
import { AuthorizationsService } from '../../../../../../shared/services/authorizations.service';
import { Action, Subject } from '../../../../../shared/enums';
import { FormBuilder } from '@angular/forms';
import { first, Subscription } from 'rxjs';
import { Job } from '../../../../../shared/interfaces';

@Component({
  selector: 'app-job-view-pao',
  templateUrl: './job-view-pao.component.html',
  styleUrls: ['./job-view-pao.component.scss'],
})
export class JobViewPaoComponent implements OnDestroy {
  public error$ = this._store.select(JobSelectors.selectHandleError);
  public job: Job | null = null;
  public canActivate = false;
  public editMode = false;
  public form = this._fb.group({
    checkComment: [this.job?.pao?.checkComment],
  });

  private _subscription = new Subscription();
  private _job$ = this._store.select(JobSelectors.selectJob);
  private _success$ = this._store.select(JobSelectors.selectHandleSuccess);

  constructor(
    private _store: Store,
    private _authService: AuthorizationsService,
    private _fb: FormBuilder,
  ) {
    this._authService
      .canActivate(Subject.Job, Action.ManagePao)
      .subscribe((canActivate) => {
        this.canActivate = canActivate;
      });

    this._subscription = this._job$.subscribe((job) => {
      this.job = job;
    });
  }

  editCheckComment() {
    if (this.canActivate) {
      this.editMode = true;
    }
  }

  closeCheckComment() {
    this.editMode = false;
  }

  saveCheckComment() {
    const checkComment = this.form.get('checkComment')?.value ?? null;

    this._store.dispatch(
      JobActions.tryChangeJobPaoCheckCommentAction({ checkComment }),
    );

    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this.editMode = false;
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
