import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthorizationsService } from '../../../../../../shared/services/authorizations.service';
import {
  Action,
  PaoStockReservationGlobalStatus,
  PaoStockSheetReservationStatus,
  Subject,
} from '../../../../../shared/enums';
import { JobActions, JobSelectors } from '../../../shared/store/job';
import {
  Job,
  JobPaoStockSheetReservation,
  StockReservation,
} from '../../../../../shared/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { JobViewResaEditComponent } from './job-view-resa-edit/job-view-resa-edit.component';
import { JobViewResaDeleteComponent } from './job-view-resa-delete/job-view-resa-delete.component';
import { JobPaoReservationHelperService } from '../../../../../shared/services/job-pao-reservation-helper.service';
import { ActivatedRoute } from '@angular/router';
import { PaoStockSheetReservationVoter } from '../../../shared/voters';
import { JobViewResaPrintComponent } from './job-view-resa-print/job-view-resa-print.component';

@Component({
  selector: 'app-job-view-resa',
  templateUrl: './job-view-resa.component.html',
  styleUrls: ['./job-view-resa.component.scss'],
})
export class JobViewResaComponent implements OnDestroy {
  public error$ = this._store.select(JobSelectors.selectHandleError);
  public job: Job | null = null;
  public stockReservations: { title: string; products: StockReservation[] }[] =
    [];
  public paoReservations: JobPaoStockSheetReservation[] | null = null;
  public reservationGlobalStatus: {
    status: PaoStockReservationGlobalStatus;
    color: string;
  } | null = null;
  public canActivate = false;
  public canManage = false;
  public canHandle = false;

  private _subscription = new Subscription();
  private _job$ = this._store.select(JobSelectors.selectJob);
  private _stockJobReservations$ = this._store.select(
    JobSelectors.selectStockReservations,
  );

  constructor(
    private _store: Store,
    private _authService: AuthorizationsService,
    private _jobReservationHelper: JobPaoReservationHelperService,
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
    private _voter: PaoStockSheetReservationVoter,
  ) {
    this._authService
      .canActivate(Subject.PaoReservations, Action.Manage)
      .subscribe((canActivate) => {
        this.canManage = canActivate;
      });

    this._authService
      .canActivate(Subject.PaoReservations, Action.Handle)
      .subscribe((canActivate) => {
        this.canHandle = canActivate;
      });

    const jobNumber = this._activatedRoute.snapshot.paramMap.get('number');

    if (!jobNumber) {
      return;
    }

    this._store.dispatch(
      JobActions.tryFetchStockReservationsAction({ jobNumber }),
    );

    this._subscription = this._job$.subscribe((job) => {
      this.job = job;

      if (!job || !job.pao) {
        return;
      }

      this.paoReservations = job.pao.stockSheetReservations;
      this.reservationGlobalStatus = this._jobReservationHelper.getGlobalStatus(
        job.pao.stockSheetReservations,
      );
    });

    const stockSubscription = this._stockJobReservations$.subscribe(
      (reservations) => {
        if (!reservations) {
          return;
        }

        this.stockReservations = [];

        if (reservations.articles.length) {
          this.stockReservations.push({
            title: 'Articles',
            products: reservations.articles,
          });
        }

        if (reservations.customerProducts.length) {
          this.stockReservations.push({
            title: 'Produits clients',
            products: reservations.customerProducts,
          });
        }

        if (reservations.papers.length) {
          this.stockReservations.push({
            title: 'Matières',
            products: reservations.papers,
          });
        }
      },
    );

    this._subscription.add(stockSubscription);
  }

  openJobViewResaAdd() {
    this._dialog.open(JobViewResaEditComponent, {
      width: '60rem',
    });
  }

  openJobViewResaEdit(resa: JobPaoStockSheetReservation) {
    this._dialog.open(JobViewResaEditComponent, {
      width: '60rem',
      data: { resa },
    });
  }

  openJobViewResaDelete(resa: JobPaoStockSheetReservation) {
    this._dialog.open(JobViewResaDeleteComponent, {
      width: '50rem',
      data: { resa },
    });
  }

  get canDisplayPaoResaGlobalSendButton(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canSendPaoResaGlobally(this.paoReservations);
  }

  get canHandlePaoResaGlobally(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canHandleResaGlobally(this.paoReservations);
  }

  get canHandlePaoResaStatusGlobally(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canHandlePaoResaStatusGlobally(this.paoReservations);
  }

  get canUndoHandlingPaoResaGlobally(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canUndoHandlingGlobally(this.paoReservations);
  }

  get canResetPaoResaStatusToPendingGlobally(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canResetPaoResaStatusToPendingGlobally(
      this.paoReservations,
    );
  }

  get canReturnPaoResaGlobally(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this._voter.canReturnPaoResaGlobally(this.paoReservations);
  }

  canUpdateResa(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canUpdateOrDeletePaoResa(resa);
  }

  canSendResa(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canSendPaoResa(resa);
  }

  canDeleteResa(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canUpdateOrDeletePaoResa(resa);
  }

  canHandleResa(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canHandlePaoResa(resa);
  }

  canResetPaoResaStatusToPending(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canResetPaoResaToPending(resa);
  }

  canReturnPaoResa(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canReturnPaoResa(resa);
  }

  canUndoPaoResaHandling(resa: JobPaoStockSheetReservation): boolean {
    return this._voter.canUndoHandlingPaoResa(resa);
  }

  getPaoResaDisplaySizeTitle(resa: JobPaoStockSheetReservation) {
    return this._jobReservationHelper.getPaoResaDisplaySizeTitle(resa);
  }

  getPaoResaDisplaySize(resa: JobPaoStockSheetReservation): string {
    return this._jobReservationHelper.getPaoResaDisplaySize(resa);
  }

  getDisplayFileInfo(resa: JobPaoStockSheetReservation): string {
    return this._jobReservationHelper.getDisplayFileInfo(resa);
  }

  getStatusLabel(status: PaoStockSheetReservationStatus): string {
    return this._jobReservationHelper.getDisplayStatus(status);
  }

  getStatusColor(status: PaoStockSheetReservationStatus): string {
    return this._jobReservationHelper.getStatusColor(status);
  }

  getProductStockLink(resa: JobPaoStockSheetReservation): string {
    return `/company/stock/materials/${resa.productType}/${resa.productId}`;
  }

  get displayGlobalOtherActions(): boolean {
    if (!this.paoReservations) {
      return false;
    }

    return this.paoReservations.some(
      (resa) => resa.status !== PaoStockSheetReservationStatus.Created,
    );
  }

  sendGlobalResa(): void {
    if (
      !this.paoReservations ||
      !this._voter.canSendPaoResaGlobally(this.paoReservations)
    ) {
      // Todo: Display unauthorized message
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationsStatusesAction({
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Created,
          toStatus: PaoStockSheetReservationStatus.Pending,
        },
      }),
    );
  }

  sendResa(resa: JobPaoStockSheetReservation): void {
    if (!this._voter.canSendPaoResa(resa)) {
      // Todo: Display unauthorized message
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationStatusAction({
        reservationId: resa._id,
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Created,
          toStatus: PaoStockSheetReservationStatus.Pending,
        },
      }),
    );
  }

  returnPaoResaGlobally(): void {
    if (
      !this.paoReservations ||
      !this._voter.canReturnPaoResaGlobally(this.paoReservations)
    ) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationsStatusesAction({
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Pending,
          toStatus: PaoStockSheetReservationStatus.Created,
        },
      }),
    );
  }

  setPaoResaToOrderedStatusGlobally(): void {
    if (
      !this.paoReservations ||
      !this._voter.canHandlePaoResaStatusGlobally(this.paoReservations)
    ) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationsStatusesAction({
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Pending,
          toStatus: PaoStockSheetReservationStatus.Ordered,
        },
      }),
    );
  }

  resetPaoResaToPendingStatusGlobally(): void {
    if (
      !this.paoReservations ||
      !this._voter.canResetPaoResaStatusToPendingGlobally(this.paoReservations)
    ) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationsStatusesAction({
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Ordered,
          toStatus: PaoStockSheetReservationStatus.Pending,
        },
      }),
    );
  }

  resetPaoResaToPending(resa: JobPaoStockSheetReservation): void {
    if (!this._voter.canResetPaoResaToPending(resa)) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationStatusAction({
        reservationId: resa._id,
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Ordered,
          toStatus: PaoStockSheetReservationStatus.Pending,
        },
      }),
    );
  }

  setPaoResaStatusToDelivery(resa: JobPaoStockSheetReservation): void {
    if (!this._voter.canHandlePaoResa(resa)) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationStatusAction({
        reservationId: resa._id,
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Pending,
          toStatus: PaoStockSheetReservationStatus.Ordered,
        },
      }),
    );
  }

  returnPaoResa(resa: JobPaoStockSheetReservation): void {
    if (!this._voter.canReturnPaoResa(resa)) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangePaoReservationStatusAction({
        reservationId: resa._id,
        dto: {
          fromStatus: PaoStockSheetReservationStatus.Pending,
          toStatus: PaoStockSheetReservationStatus.Created,
        },
      }),
    );
  }

  printResa(): void {
    this._dialog.open(JobViewResaPrintComponent, {
      minWidth: '210mm',
      minHeight: '297mm',
      position: { top: '0' },
      data: { job: this.job },
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
