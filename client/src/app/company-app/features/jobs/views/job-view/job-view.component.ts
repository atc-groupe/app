import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobSelectors, JobActions } from '../../shared/store/job';
import { UiSelectors } from '../../../../shared/store/ui';
import { UsersSelectors } from '../../../../../shared/store/users';
import { Subscription } from 'rxjs';
import { Job, SubJob } from '../../../../shared/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { JobStatusComponent } from '../planning-container/job-status/job-status.component';
import { MetaJobsStatusSelectors } from '../../../../shared/store/meta-jobs-status';
import { JobStatusVoterService } from '../../shared/voters';
import { JOB_VIEW_NAV } from '../../job-view.nav';

@Component({
  selector: 'jobs-job-view',
  templateUrl: './job-view.component.html',
  styleUrls: ['./job-view.component.scss'],
})
export class JobViewComponent implements OnDestroy {
  public nav = JOB_VIEW_NAV;
  public job: Job | null = null;
  public isLoading = false;
  public user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);
  public syncError$ = this._store.select(JobSelectors.selectSyncError);
  public returnRoute$ = this._store.select(
    UiSelectors.selectJobViewReturnRoute,
  );
  public metaJobsStatusMap$ = this._store.select(
    MetaJobsStatusSelectors.selectStatusMap,
  );
  public canChangeJobStatus = false;

  private _subscription = new Subscription();
  private _isLoading$ = this._store.select(JobSelectors.selectIsLoading);
  private _job$ = this._store.select(JobSelectors.selectJob);

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _jobStatusVoter: JobStatusVoterService,
  ) {
    this._subscription.add(
      this._job$.subscribe((job) => {
        if (!job) {
          return;
        }

        if (job.syncStatus === 'pending') {
          console.log(job);
          this.isLoading = true;
          this._store.dispatch(
            JobActions.tryFetchPendingJobAction({
              jobNumber: parseInt(job.mp.number),
            }),
          );
        } else {
          this.isLoading = false;
        }

        this.job = job;
      }),
    );

    this._subscription.add(
      this._isLoading$.subscribe((isLoading) => {
        this.isLoading = isLoading;
      }),
    );

    this.canChangeJobStatus = this._jobStatusVoter.canChangeJobStatus;
  }

  syncJob(jobId: number) {
    this._store.dispatch(JobActions.trySyncJobAction({ jobId }));
  }

  openJobStatus(): void {
    if (!this.canChangeJobStatus) {
      return;
    }

    this._dialog
      .open(JobStatusComponent, {
        width: '100%',
        maxWidth: '50rem',
      })
      .afterClosed()
      .subscribe((result) => {
        if (result === 'cancel') {
          return;
        }

        this._store.dispatch(
          JobActions.tryFetchJobAction({
            jobNumber: parseInt(this.job!.mp.number),
          }),
        );
      });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
