import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  Job,
  MetaJobsStatus,
  MetaJobsStatusColors,
} from '../../../../../shared/interfaces';
import { Clipboard } from '@angular/cdk/clipboard';
import { AppUser } from '../../../../../../shared/interfaces';

@Component({
  selector: 'jobs-job-view-header',
  templateUrl: './job-view-header.component.html',
  styleUrls: ['./job-view-header.component.scss'],
})
export class JobViewHeaderComponent {
  @Input() job: Job | null = null;
  @Input() user: AppUser | null = null;
  @Input() returnRoute: string | null = null;
  @Input() metaStatusMap: Map<string, MetaJobsStatus> | null = null;
  @Input() syncError: string | null = null;
  @Input() canChangeJobStatus = false;

  @Output() sync = new EventEmitter<number>();
  @Output() changeStatus = new EventEmitter<void>();

  public copied = false;

  constructor(private _clipboard: Clipboard) {}

  triggerSync() {
    if (this.job) {
      this.sync.emit(this.job.mp.id);
    }
  }

  triggerChangeJobStatus() {
    if (!this.canChangeJobStatus) {
      return;
    }

    this.changeStatus.emit();
  }

  copyJobMailHeading() {
    this.copied = true;
    const job = this.job;
    const user = this.user;
    if (!job || !user) {
      return;
    }

    this._clipboard.copy(
      `BAT ${job.mp.company} [${
        job.mp.number
      }] // ${user.firstName[0].toUpperCase()}${user.lastName[0].toUpperCase()} // ${
        job.mp.description
      }`,
    );

    setTimeout(() => {
      this.copied = false;
    }, 1000);
  }

  get dotColorClass(): string | null {
    if (!this.job) {
      return null;
    }

    switch (this.job.syncStatus) {
      case 'success':
        return 'dot-success';
      case 'pending':
        return 'dot-warning';
      case 'error':
        return 'dot-error';
    }
  }

  getStatusColors(statusName: string): MetaJobsStatusColors {
    const defaultColors = { color: '#ddd', background: '#666' };
    if (!this.metaStatusMap) {
      return defaultColors;
    }
    const status = this.metaStatusMap.get(statusName);
    return status
      ? { color: status.textColor, background: status.color }
      : defaultColors;
  }
}
