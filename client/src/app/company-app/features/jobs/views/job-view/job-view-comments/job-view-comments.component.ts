import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobSelectors } from '../../../shared/store/job';
import { Job } from '../../../../../shared/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jobs-job-view-comments',
  templateUrl: './job-view-comments.component.html',
  styleUrls: ['./job-view-comments.component.scss'],
})
export class JobViewCommentsComponent implements OnDestroy {
  public job: Job | null = null;

  private _subscription = new Subscription();
  private _job$ = this._store.select(JobSelectors.selectJob);

  constructor(private _store: Store) {
    this._subscription = this._job$.subscribe((job) => {
      this.job = job;
      console.log(this.job);
    });
  }

  get hasNoComment(): boolean {
    if (!this.job) {
      return true;
    }

    return (
      !this.job.mp.infoSubContract &&
      !this.job.mp.infoExpedition &&
      !this.job.mp.infoFinishing &&
      !this.job.mp.infoPrint &&
      !this.job.mp.infoDigital &&
      !this.job.mp.infoPlanning &&
      !this.job.mp.infoGlobal
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
