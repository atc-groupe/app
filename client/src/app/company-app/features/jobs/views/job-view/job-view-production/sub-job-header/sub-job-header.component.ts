import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubJob } from '../../../../../../shared/interfaces';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'sub-job-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sub-job-header.component.html',
  styleUrls: ['./sub-job-header.component.scss'],
})
export class SubJobHeaderComponent {
  @Input() public subJob: SubJob | null = null;
  @Input() public atcProdRef: string | null = null;

  constructor(private _clipboard: Clipboard) {}

  onSelectRef() {
    const reference = this.atcProdRef;
    this._clipboard.copy(this.atcProdRef!);
    this.atcProdRef = 'Copié !';

    setTimeout(() => {
      this.atcProdRef = reference;
    }, 500);
  }
}
