import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import {
  PaperGroupType,
  PaperType,
  ProductType,
} from '../../../../../../shared/enums';
import { Store } from '@ngrx/store';
import {
  StockActions,
  StockSelectors,
} from '../../../../../../shared/store/stock';
import { BehaviorSubject, first, Subscription } from 'rxjs';
import {
  JobPaoStockSheetReservation,
  Paper,
  ProductGroup,
  ProductListItem,
} from '../../../../../../shared/interfaces';
import { JobActions, JobSelectors } from '../../../../shared/store/job';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PaoStockSheetReservationCreateDto } from '../../../../../../shared/dto';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import { CompanyUser } from '../../../../../../../shared/interfaces';

@Component({
  selector: 'app-job-view-resa-edit',
  templateUrl: './job-view-resa-edit.component.html',
  styleUrls: ['./job-view-resa-edit.component.scss'],
})
export class JobViewResaEditComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  public groups: ProductGroup[] | null = null;
  public papers: Paper[] | null = null;
  public paperType: PaperType | null = null;
  public customSize = false;
  public isNesting = true;
  public maxWidth = 0;
  public maxHeight = 0;
  public maxLength = 0;
  public form = this._fb.group({
    group: ['', Validators.required], // Bois, Carton, ...
    name: ['', Validators.required], // NewBond, ...
    paper: new FormControl<number | null>(null, [Validators.required]), // Real product
    quantity: new FormControl<number | null>(null, [Validators.required]),
    customSize: [false],
    width: new FormControl<number | undefined>(undefined, [
      Validators.required,
    ]),
    height: new FormControl<number | undefined>(undefined, [
      Validators.required,
    ]),
    length: ['', Validators.required],
    isDoubleSided: new FormControl<boolean | undefined>(false),
    isNesting: new FormControl<boolean | undefined>(true),
    numberBySheet: new FormControl<number | undefined>(undefined, [
      Validators.required,
    ]),
    paoComment: new FormControl<string | undefined>(undefined),
  });
  public list$ = this._store.select(StockSelectors.selectList);
  public products$ = new BehaviorSubject<ProductListItem[]>([]);
  public error$ = this._store.select(JobSelectors.selectHandleError);

  private _subscription = new Subscription();
  private _product$ = this._store.select(StockSelectors.selectProduct);
  private _success$ = this._store.select(JobSelectors.selectHandleSuccess);
  private _selectedProducts$ = this._store.select(
    StockSelectors.selectProducts,
  );
  private _authenticatedUser: CompanyUser | null = null;

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _dialogRef: MatDialogRef<JobViewResaEditComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { resa: JobPaoStockSheetReservation },
  ) {
    this._store.dispatch(
      StockActions.tryFetchProductListAction({
        productType: ProductType.Paper,
        params: { groupType: PaperGroupType.PlanoSheets },
      }),
    );

    const productsSubscription = this._selectedProducts$.subscribe(
      (products) => {
        this.papers = products as Paper[];

        if (this.data) {
          const paper = this.papers?.find(
            (paper) => paper.id === this.data.resa.productId,
          );
          if (!paper) {
            return;
          }

          this.maxWidth = paper.width;
          this.maxHeight = paper.length;
          this.maxLength = paper.length;
        }
      },
    );
    this._subscription.add(productsSubscription);
  }

  ngOnInit() {
    const listSubscription = this.list$.subscribe((list) => {
      if (!list) {
        return;
      }
      const groups: ProductGroup[] = [];
      list.forEach((groupType) => {
        groups.push(...groupType.groups);
      });

      this.groups = groups;

      if (this.data) {
        const resa = this.data.resa;
        this._store.dispatch(
          StockActions.tryFetchProductAction({
            productType: resa.productType,
            productId: resa.productId,
          }),
        );

        this._product$.pipe(first((v) => v !== null)).subscribe((product) => {
          if (!product) {
            return;
          }
          this.form.get('group')?.setValue(product!.groupDescription);
          const group = groups.find(
            (item) => item.description === product!.groupDescription,
          );
          const name = group!.products.find((item) =>
            item.articleIds.includes(product!.id),
          );
          this.form.get('name')?.setValue(name!.description);
          this.form.get('paper')?.setValue(product.id);
          this.form.get('quantity')?.setValue(resa.quantity);
          this.form.get('customSize')?.setValue(resa.isCustomSize);
          this.form.get('width')?.setValue(resa.width);
          this.form.get('height')?.setValue(resa.height);
          this.form.get('isDoubleSided')?.setValue(resa.isDoubleSided);
          this.form.get('isNesting')?.setValue(resa.isNesting);
          this.form.get('numberBySheet')?.setValue(resa.numberBySheet);
          this.form.get('paoComment')?.setValue(resa.paoComment);

          this.paperType = PaperType.Sheet;
        });
      }
    });
    this._subscription.add(listSubscription);

    this._store
      .select(UsersSelectors.selectCompanyUser)
      .pipe(first())
      .subscribe((user) => {
        this._authenticatedUser = user;
      });
  }

  ngAfterViewInit() {
    const groupSubscription = this.form
      .get('group')
      ?.valueChanges.subscribe((value) => {
        if (!value || !this.groups) {
          return;
        }

        const group = this.groups.find((group) => group.description === value);
        if (!group) {
          return;
        }

        this.products$.next(group.products);
        this.form.get('name')?.setValue('');
        this.form.get('paper')?.setValue(null);
        this.paperType = null;
        this.form.get('customSize')?.setValue(false);
      });
    this._subscription.add(groupSubscription);

    const nameSubscription = this.form
      .get('name')
      ?.valueChanges.subscribe((value) => {
        if (!value) {
          return;
        }

        const product = this.products$.value.find(
          (product) => product.description === value,
        );
        if (!product) {
          return;
        }

        this._store.dispatch(
          StockActions.tryFetchProductsAction({
            productType: ProductType.Paper,
            productIds: product.articleIds,
          }),
        );
        this.paperType = null;
        this.form.get('customSize')?.setValue(false);
      });
    this._subscription.add(nameSubscription);

    const paperSubscription = this.form
      .get('paper')
      ?.valueChanges.subscribe((value) => {
        if (!value || !this.papers) {
          return;
        }

        const paper = this.papers.find((paper) => paper.id === value);
        if (!paper) {
          return;
        }

        this.paperType = paper.type;
        this.maxWidth = paper.width;
        this.maxHeight = paper.length;
        this.maxLength = paper.length;
      });
    this._subscription.add(paperSubscription);

    const customSizeSubscription = this.form
      .get('customSize')
      ?.valueChanges.subscribe((value) => {
        if (value === null) {
          return;
        }

        this.customSize = value;
      });
    this._subscription.add(customSizeSubscription);

    const widthSubscription = this.form
      .get('width')
      ?.valueChanges.subscribe((value) => {
        if (!value || !this.maxWidth) {
          return;
        }

        if (value > this.maxWidth) {
          this.form.get('width')?.setValue(this.maxWidth);
        }
      });
    this._subscription.add(widthSubscription);

    const heightSubscription = this.form
      .get('height')
      ?.valueChanges.subscribe((value) => {
        if (!value || !this.maxHeight) {
          return;
        }

        if (value > this.maxHeight) {
          this.form.get('height')?.setValue(this.maxHeight);
        }
      });
    this._subscription.add(heightSubscription);

    const lengthSubscription = this.form
      .get('length')
      ?.valueChanges.subscribe((value) => {
        if (!value || !this.maxLength) {
          return;
        }

        if (parseInt(value) > this.maxLength) {
          this.form.get('length')?.setValue(this.maxLength.toString());
        }
      });
    this._subscription.add(lengthSubscription);

    const nestingSubscription = this.form
      .get('isNesting')
      ?.valueChanges.subscribe((value) => {
        if (value === null || value === undefined) {
          return;
        }

        this.isNesting = value;
      });
    this._subscription.add(nestingSubscription);
  }

  getSelectedProductDisplaySize(paper: Paper) {
    switch (paper.type) {
      case PaperType.Roll:
        return `${paper.width} x ${paper.length} ml`;
      case PaperType.Sheet:
        return `Epaisseur ${paper.thickness}mm - ${paper.width} x ${paper.length} mm`;
    }
  }

  getSelectedProductDisplayName(paper: Paper) {
    switch (paper.type) {
      case PaperType.Roll:
        return `${paper.name} - ${paper.width} x ${paper.length} ml`;
      case PaperType.Sheet:
        return `${paper.name} - ${paper.width} x ${paper.length} mm`;
    }
  }

  getCustomSizeLabel() {
    if (this.paperType === PaperType.Roll) {
      return 'Longueur sur mesure';
    }

    if (this.isNesting) {
      return 'Format de plaque partiel';
    } else {
      return 'Format sur mesure';
    }
  }

  hideQuantity() {
    return this.customSize && this.paperType === PaperType.Roll;
  }

  saveResa() {
    const paperId = this.form.get('paper')?.value;
    let quantity: any = this.form.get('quantity')?.value;
    const customSize = this.form.get('customSize')?.value;
    const width = this.form.get('width')?.value;
    const height = this.form.get('height')?.value;
    const length = this.form.get('length')?.value;
    const isDoubleSided = this.form.get('isDoubleSided')?.value;
    const isNesting = this.form.get('isNesting')?.value;
    const numberBySheet = this.form.get('numberBySheet')?.value;
    const paoComment = this.form.get('paoComment')?.value;

    if (this.paperType === PaperType.Roll && length) {
      quantity = 1;
    }

    if (!paperId || !quantity) {
      return;
    }

    const paper = this.papers?.find((paper) => paper.id === paperId);
    const user = this._authenticatedUser;

    if (!paper || !user) {
      return;
    }

    const reservation: PaoStockSheetReservationCreateDto = {
      productType: ProductType.Paper,
      productId: paperId,
      displayName: this.getSelectedProductDisplayName(paper),
      paperType: paper.type,
      quantity,
      isCustomSize: this.customSize,
      userDisplayName: `${user.firstName} ${user.lastName}`,
      userEmployeeNumber: user.employeeNumber,
    };

    // Sheet paper
    if (this.paperType === PaperType.Sheet) {
      if (isDoubleSided === undefined || isNesting === undefined) {
        return;
      }

      if (isDoubleSided !== null) {
        reservation.isDoubleSided = isDoubleSided;
      }

      if (isNesting !== null) {
        reservation.isNesting = isNesting;
      }

      // With custom size
      if (customSize === true) {
        if (!width || !height) {
          return;
        }

        if (!numberBySheet && isNesting === false) {
          return;
        }

        reservation.width = width;
        reservation.height = height;
      }

      if (numberBySheet && isNesting === false) {
        reservation.numberBySheet = numberBySheet;
      }
    }

    // Comment
    if (paoComment) {
      reservation.paoComment = paoComment;
    }

    this._store.dispatch(
      this.data
        ? JobActions.tryUpdatePaoReservationAction({
            id: this.data.resa._id,
            reservation,
          })
        : JobActions.tryAddPaoReservationAction({ reservation }),
    );

    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this._dialogRef.close();
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
