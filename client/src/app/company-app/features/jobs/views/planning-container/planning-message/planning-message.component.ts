import { Component, Input } from '@angular/core';

@Component({
  selector: 'jobs-planning-message',
  templateUrl: './planning-message.component.html',
  styleUrls: ['./planning-message.component.scss'],
})
export class PlanningMessageComponent {
  @Input() isLoading: boolean | null = false;
  @Input() search: string | null = null;
  @Input() loadingError: string | null = null;

  get displayMessage(): boolean {
    if (this.loadingError) {
      return false;
    }

    return !this.isLoading;
  }

  get message(): string {
    return this.search
      ? 'Aucun résultat trouvé pour cette recherche...'
      : 'Aucune donnée pour ce jour...';
  }
}
