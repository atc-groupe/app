import { Component } from '@angular/core';
import { first } from 'rxjs';
import { Store } from '@ngrx/store';
import { DialogRef } from '@angular/cdk/dialog';
import { JobActions, JobSelectors } from '../../../shared/store/job';

@Component({
  selector: 'app-job-comment',
  templateUrl: './job-comment.component.html',
  styleUrls: ['./job-comment.component.scss'],
})
export class JobCommentComponent {
  public selectedJob$ = this._store.select(JobSelectors.selectJob);
  public error$ = this._store.select(JobSelectors.selectHandleError);
  public commentUpdateSuccess$ = this._store.select(
    JobSelectors.selectHandleSuccess,
  );
  public comment: string | null = null;

  constructor(
    private _store: Store,
    private _dialogRef: DialogRef<JobCommentComponent>,
  ) {
    this.selectedJob$.pipe(first((v) => v !== null)).subscribe((job) => {
      if (job) {
        this.comment = job.planning ? job.planning.comment : null;
      }
    });
  }

  updateComment() {
    const comment = this.comment === '' ? null : this.comment;
    this._store.dispatch(
      JobActions.tryChangeJobPlanningCommentAction({ comment }),
    );
    this.commentUpdateSuccess$
      .pipe(first((value) => value === true || value === false))
      .subscribe((result: boolean | null): void => {
        if (result) {
          this._dialogRef.close();
        }
      });
  }
}
