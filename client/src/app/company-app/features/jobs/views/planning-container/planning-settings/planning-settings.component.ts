import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  PlanningSelectors,
  PlanningActions,
} from '../../../shared/store/planning';
import {
  UsersActions,
  UsersSelectors,
} from '../../../../../../shared/store/users';
import { FormBuilder } from '@angular/forms';
import { PlanningSetupMachine } from '../../../shared/interfaces';
import { first } from 'rxjs';
import { UserSetting } from '../../../../../../shared/interfaces';
import { JobsUserSettings } from '../../../shared/enum';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-planning-settings',
  templateUrl: './planning-settings.component.html',
  styleUrls: ['./planning-settings.component.scss'],
})
export class PlanningSettingsComponent implements OnInit, AfterViewInit {
  public setup$ = this._store.select(PlanningSelectors.selectSetup);
  public settingError$ = this._store.select(
    UsersSelectors.selectAuthenticatedUserHandleSettingError,
  );
  public machines: PlanningSetupMachine[] = [];
  public form = this._fb.group({
    group: [''],
    machine: [0],
    default: [false],
  });

  private _group$ = this._store.select(PlanningSelectors.selectCurrentGroup);
  private _machine$ = this._store.select(
    PlanningSelectors.selectCurrentMachine,
  );
  private _settingSuccess$ = this._store.select(
    UsersSelectors.selectAuthenticatedUserHandleSettingSuccess,
  );

  constructor(
    private _fb: FormBuilder,
    private _store: Store,
    private _dialogRef: MatDialogRef<PlanningSettingsComponent>,
  ) {}

  ngOnInit() {
    this._group$.pipe(first()).subscribe((group) => {
      if (group) {
        this.form.get('group')?.setValue(group.name);
        this.machines = group.machines;
      }
    });

    this._machine$.pipe(first()).subscribe((machine) => {
      this.form.get('machine')?.setValue(machine ? machine.id : null);
    });
  }

  ngAfterViewInit() {
    this.form.get('group')?.valueChanges.subscribe((value) => {
      if (!value) {
        return;
      }

      this.setup$.pipe(first()).subscribe((setup) => {
        if (setup) {
          const group = setup.find((item) => item.name === value);
          this.machines = group ? group.machines : [];
        }
      });
    });
  }

  saveSettings() {
    const groupName = this.form.get('group')?.value;
    const machineId = this.form.get('machine')?.value;
    const saveAsDefault = this.form.get('default')?.value;

    if (!groupName || !machineId) {
      return;
    }

    this._store.dispatch(
      PlanningActions.setCurrentSettingsAction({ groupName, machineId }),
    );

    if (saveAsDefault === true) {
      const groupSetting: UserSetting = {
        name: JobsUserSettings.DefaultPlanningGroup,
        value: groupName,
      };

      const machineSetting: UserSetting = {
        name: JobsUserSettings.DefaultPlanningMachine,
        value: machineId.toString(),
      };

      this._store.dispatch(
        UsersActions.tryUpdateAuthenticatedUserSettingsAction({
          settings: [groupSetting, machineSetting],
        }),
      );

      this._settingSuccess$
        .pipe(first((v) => v !== null))
        .subscribe((result) => {
          if (result === true) {
            this._dialogRef.close();
          }
        });
    } else {
      this._dialogRef.close();
    }
  }
}
