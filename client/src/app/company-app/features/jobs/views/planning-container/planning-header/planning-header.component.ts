import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  PlanningSetup,
  PlanningSetupGroup,
  PlanningSetupMachine,
} from '../../../shared/interfaces';
import { Store } from '@ngrx/store';

@Component({
  selector: 'jobs-planning-header',
  templateUrl: './planning-header.component.html',
  styleUrls: ['./planning-header.component.scss'],
})
export class PlanningHeaderComponent implements OnInit {
  @Input() public setup: PlanningSetup | null = null;
  @Input() public currentDate: Date | null = null;
  @Input() public group: PlanningSetupGroup | null = null;
  @Input() public machine: PlanningSetupMachine | null = null;
  @Input() public search: string | null = null;

  @Output() prevDate = new EventEmitter<void>();
  @Output() nextDate = new EventEmitter<void>();
  @Output() todayDate = new EventEmitter<void>();
  @Output() openSettings = new EventEmitter<void>();
  @Output() searchJob = new EventEmitter<string>();
  @Output() clearSearch = new EventEmitter<void>();

  public searchValue = '';

  constructor(private _store: Store) {}

  triggerPrevDate() {
    this.prevDate.emit();
  }

  triggerNextDate() {
    this.nextDate.emit();
  }

  triggerTodayDate() {
    this.todayDate.emit();
  }

  triggerOpenSettings() {
    this.openSettings.emit();
  }

  triggerSearchJob() {
    this.searchJob.emit(this.searchValue);
  }

  triggerClearSearch() {
    this.searchValue = '';
    this.clearSearch.emit();
  }

  ngOnInit() {
    if (this.search) {
      this.searchValue = this.search;
    }
  }
}
