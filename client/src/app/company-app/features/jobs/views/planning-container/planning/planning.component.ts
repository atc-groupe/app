import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { PlanningTableItem } from '../../../shared/interfaces';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import {
  MetaJobsStatus,
  MetaJobsStatusColors,
} from '../../../../../shared/interfaces';
import { JobSyncStatusType } from '../../../../../shared/types/job-sync-status.type';

@Component({
  selector: 'jobs-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent implements AfterViewInit {
  @Input() dataSource: MatTableDataSource<PlanningTableItem> | null = null;
  @Input() isLoading: boolean | null = false;
  @Input() metaStatusMap: Map<string, MetaJobsStatus> | null = null;
  @Input() canChangeJobStatus = false;

  @ViewChild(MatSort) sort!: MatSort;

  @Output() private changeStatus = new EventEmitter<number>();
  @Output() private changeComment = new EventEmitter<number>();
  @Output() private viewJob = new EventEmitter<number>();

  public displayedColumns = [
    'ordering',
    'jobNumber',
    'customer',
    'status',
    'factory',
    'subJobCount',
    'printingSurface',
    'manufacturedPieceCount',
    'sendingDate',
    'comment',
    'sync',
    'actions',
  ];

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.sort = this.sort;
    }
  }

  triggerChangeStatus(jobNumber: number) {
    if (!this.canChangeJobStatus) {
      return;
    }

    this.changeStatus.emit(jobNumber);
  }

  triggerChangeComment(jobNumber: number) {
    this.changeComment.emit(jobNumber);
  }

  triggerViewJob(jobNumber: number) {
    this.viewJob.emit(jobNumber);
  }

  getStatusColors(statusName: string): MetaJobsStatusColors {
    const defaultColors = { color: '#ddd', background: '#666' };
    if (!this.metaStatusMap) {
      return defaultColors;
    }
    const status = this.metaStatusMap.get(statusName);
    return status
      ? { color: status.textColor, background: status.color }
      : defaultColors;
  }

  getStatusLabel(statusName: string): string {
    if (!this.metaStatusMap) {
      return statusName;
    }

    const status = this.metaStatusMap.get(statusName);
    return status ? status.label : statusName;
  }

  hasSyncError(syncStatus: JobSyncStatusType): boolean {
    return syncStatus === 'error';
  }
}
