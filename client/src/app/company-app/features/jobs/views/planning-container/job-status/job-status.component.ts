import { Component, OnDestroy } from '@angular/core';
import {
  SystemActions,
  SystemSelectors,
} from '../../../../../../shared/store/system';
import { JobActions, JobSelectors } from '../../../shared/store/job';
import { Job } from '../../../../../shared/interfaces';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { DialogRef } from '@angular/cdk/dialog';
import { first, Subscription } from 'rxjs';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
})
export class JobStatusComponent implements OnDestroy {
  public selectedJob: Job | null = null;
  public jobStatus$ = this._store.select(SystemSelectors.selectJobStatusList);
  public error$ = this._store.select(JobSelectors.selectHandleError);
  public isLoading = false;
  public form = this._fb.group({
    status: [0, Validators.required],
  });
  public result = true;

  private _success$ = this._store.select(
    JobSelectors.selectChangeStatusSuccess,
  );
  private _selectedJob$ = this._store.select(JobSelectors.selectJob);
  private _subscription = new Subscription();

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    public dialogRef: DialogRef<JobStatusComponent | string>,
  ) {
    this._store.dispatch(SystemActions.tryFetchJobStatus());
    this._selectedJob$.pipe(first((v) => v !== null)).subscribe((job) => {
      this.selectedJob = job;
      this.form.get('status')?.setValue(job!.mp.jobStatusNumber);
    });
  }

  changeJobStatus() {
    this.isLoading = true;
    const status = this.form.get('status')?.value;
    if (!status || !this.selectedJob) {
      return;
    }

    this._store.dispatch(
      JobActions.tryChangeJobStatusAction({
        jobNumber: this.selectedJob!.mp.number,
        jobStatus: status,
      }),
    );

    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this.dialogRef.close();
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
