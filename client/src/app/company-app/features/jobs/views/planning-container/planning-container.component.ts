import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  PlanningActions,
  PlanningSelectors,
} from '../../shared/store/planning';
import { UiActions } from '../../../../shared/store/ui';
import { JobActions, JobSelectors } from '../../shared/store/job';
import { MetaJobsStatusSelectors } from '../../../../shared/store/meta-jobs-status';
import { MatTableDataSource } from '@angular/material/table';
import { PlanningTableItem } from '../../shared/interfaces';
import { BehaviorSubject, debounceTime, first, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { PlanningSettingsComponent } from './planning-settings/planning-settings.component';
import { JobStatusComponent } from './job-status/job-status.component';
import { JobCommentComponent } from './job-comment/job-comment.component';
import { Router } from '@angular/router';
import { JobStatusVoterService } from '../../shared/voters';

@Component({
  selector: 'app-planning-container',
  templateUrl: './planning-container.component.html',
  styleUrls: ['./planning-container.component.scss'],
})
export class PlanningContainerComponent implements AfterViewInit, OnDestroy {
  public setup$ = this._store.select(PlanningSelectors.selectSetup);
  public currentDate$ = this._store.select(PlanningSelectors.selectCurrentDate);
  public group$ = this._store.select(PlanningSelectors.selectCurrentGroup);
  public machine$ = this._store.select(PlanningSelectors.selectCurrentMachine);
  public isLoading$ = this._store.select(PlanningSelectors.selectIsLoading);
  public fetchError$ = this._store.select(PlanningSelectors.selectFetchError);
  public dataSource = new MatTableDataSource<PlanningTableItem>([]);
  public search$ = this._store.select(PlanningSelectors.selectSearch);
  public metaJobsStatusMap$ = this._store.select(
    MetaJobsStatusSelectors.selectStatusMap,
  );
  public setInitScroll = new EventEmitter<number>();
  public canChangeJobStatus = false;

  private _subscription = new Subscription();
  private _initScroll$ = this._store.select(PlanningSelectors.selectScroll);
  private _isLoading = false;
  private _scroll = new BehaviorSubject<number>(0);
  private _displayedItems$ = this._store.select(
    PlanningSelectors.selectDisplayedItems,
  );

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _router: Router,
    private _jobStatusVoter: JobStatusVoterService,
  ) {
    this._store.dispatch(PlanningActions.tryFetchPlanningData());
    this._subscription.add(
      this._displayedItems$.subscribe((items) => {
        this.dataSource.data = items;
        if (!items.length) {
          return;
        }

        this._initScroll$.pipe(first()).subscribe((scroll) => {
          this.setInitScroll.emit(scroll);
        });
      }),
    );
    this._subscription.add(
      this.isLoading$.subscribe((isLoading) => {
        this._isLoading = isLoading;
      }),
    );
    this._subscription.add(
      this._store
        .select(JobSelectors.selectChangeStatusSuccess)
        .subscribe((success) => {
          if (success) {
            this._store.dispatch(PlanningActions.tryFetchPlanningData());
          }
        }),
    );
    this.canChangeJobStatus = this._jobStatusVoter.canChangeJobStatus;
  }

  setPrevDate() {
    this._store.dispatch(PlanningActions.setPreviousDateAction());
  }

  setNextDate() {
    this._store.dispatch(PlanningActions.setNextDateAction());
  }

  setTodayDate() {
    this._store.dispatch(PlanningActions.setTodayDateAction());
  }

  openPlanningSettings() {
    this._dialog.open(PlanningSettingsComponent, {
      width: '100%',
      maxWidth: '50rem',
    });
  }

  openJobStatus(jobNumber: number): void {
    if (!this.canChangeJobStatus) {
      return;
    }

    this._store.dispatch(JobActions.tryFetchJobAction({ jobNumber }));
    this._dialog.open(JobStatusComponent, {
      width: '100%',
      maxWidth: '50rem',
    });
  }

  openJobComment(jobNumber: number): void {
    this._store.dispatch(JobActions.tryFetchJobAction({ jobNumber }));
    this._dialog.open(JobCommentComponent, {
      width: '100%',
      maxWidth: '50rem',
    });
  }

  searchJob(search: string) {
    search === ''
      ? this._store.dispatch(PlanningActions.clearSearchJobsAction())
      : this._store.dispatch(PlanningActions.trySearchJobsAction({ search }));
  }

  clearSearch() {
    this._store.dispatch(PlanningActions.clearSearchJobsAction());
  }

  get displayPlanning(): boolean {
    return !this._isLoading && this.dataSource.data.length > 0;
  }

  get displayPlanningMessage(): boolean {
    return this._isLoading || this.dataSource.data.length === 0;
  }

  registerScroll(scroll: number) {
    this._scroll.next(scroll);
  }

  navigateToJob(number: number) {
    this._store.dispatch(
      UiActions.setJobViewReturnRoute({ route: '/company/jobs/planning' }),
    );
    this._router.navigateByUrl(`/company/jobs/view/${number}`);
  }

  ngAfterViewInit() {
    this._subscription.add(
      this._scroll.pipe(debounceTime(500)).subscribe((scroll) => {
        this._store.dispatch(PlanningActions.setScrollAction({ scroll }));
      }),
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
