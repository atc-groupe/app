import { Nav } from '../../shared/interfaces';

export const JOB_VIEW_NAV: Nav = {
  sections: [
    {
      icon: 'lightbulb',
      label: 'INFORMATIONS',
      active: true,
      expanded: true,
      items: [
        { route: 'info', label: 'Informations générales' },
        { route: 'comments', label: 'Commentaires' },
      ],
    },
    {
      icon: 'shapes',
      label: 'PAO',
      active: true,
      expanded: true,
      items: [
        { route: 'pao', label: 'Commentaire & vérif.' },
        { route: 'resa', label: 'Resa matières' },
        { route: 'bat', label: 'BAT', disabled: true },
      ],
    },
    {
      icon: 'print',
      label: 'PRODUCTION',
      active: true,
      expanded: true,
      items: [{ route: 'sub-jobs', label: 'Sous-jobs' }],
    },
    {
      icon: 'local_shipping',
      label: 'LIVRAISON',
      active: true,
      expanded: true,
      items: [
        { route: 'delivery-comments', label: 'Commentaires' },
        { route: 'deliveries', label: 'Livraisons' },
      ],
    },
  ],
};
