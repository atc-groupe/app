// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../../shared/modules/layout.module';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Components
import { JobsComponent } from './jobs.component';
import { PlanningContainerComponent } from './views/planning-container/planning-container.component';
import { PlanningComponent } from './views/planning-container/planning/planning.component';
import { PlanningHeaderComponent } from './views/planning-container/planning-header/planning-header.component';
import { PlanningSettingsComponent } from './views/planning-container/planning-settings/planning-settings.component';
import { JobStatusComponent } from './views/planning-container/job-status/job-status.component';
import { JobCommentComponent } from './views/planning-container/job-comment/job-comment.component';
import { PlanningMessageComponent } from './views/planning-container/planning-message/planning-message.component';
import { JobViewComponent } from './views/job-view/job-view.component';
import { JobViewHeaderComponent } from './views/job-view/job-view-header/job-view-header.component';
import { JobViewInfoComponent } from './views/job-view/job-view-info/job-view-info.component';
import { JobViewCommentsComponent } from './views/job-view/job-view-comments/job-view-comments.component';
import { JobViewPaoComponent } from './views/job-view/job-view-pao/job-view-pao.component';
import { JobViewResaComponent } from './views/job-view/job-view-resa/job-view-resa.component';
import { JobViewResaEditComponent } from './views/job-view/job-view-resa/job-view-resa-edit/job-view-resa-edit.component';
import { JobViewResaDeleteComponent } from './views/job-view/job-view-resa/job-view-resa-delete/job-view-resa-delete.component';
import { JobViewProductionComponent } from './views/job-view/job-view-production/job-view-production.component';
import { JobViewProductionMaterialComponent } from './views/job-view/job-view-production/job-view-production-material/job-view-production-material.component';
import { JobViewDeliveriesComponent } from './views/job-view/job-view-deliveries/job-view-deliveries.component';
import { JobViewDeliveryCommentsComponent } from './views/job-view/job-view-delivery-comments/job-view-delivery-comments.component';

// Routes
import { JOBS_ROUTES } from './jobs.routes';

// Store
import { JOBS_REDUCER_KEY } from './shared/store/store.constants';
import { JOBS_REDUCERS } from './shared/store';
import { PlanningEffects } from './shared/store/planning';
import { JobEffects } from './shared/store/job';
import { JobPaoEffects } from './shared/store/job/job-pao.effects';

// Directives
import { ScrollDirective } from './shared/directives/scroll.directive';
import { ColorDirective } from '../../shared/directives/color.directive';
import { SubJobComponent } from './views/job-view/job-view-production/sub-job/sub-job.component';
import { JobViewProductionFiltersComponent } from './views/job-view/job-view-production/job-view-production-filters/job-view-production-filters.component';

@NgModule({
  declarations: [
    JobsComponent,
    PlanningContainerComponent,
    PlanningComponent,
    PlanningHeaderComponent,
    PlanningSettingsComponent,
    JobStatusComponent,
    JobCommentComponent,
    PlanningMessageComponent,
    ScrollDirective,
    JobViewComponent,
    JobViewHeaderComponent,
    JobViewInfoComponent,
    JobViewCommentsComponent,
    JobViewPaoComponent,
    JobViewResaComponent,
    JobViewResaEditComponent,
    JobViewResaDeleteComponent,
    JobViewProductionComponent,
    JobViewProductionMaterialComponent,
    JobViewDeliveriesComponent,
    JobViewDeliveryCommentsComponent,
  ],
  imports: [
    CoreModule,
    LayoutModule,
    FormsModule,
    RouterModule.forChild(JOBS_ROUTES),
    StoreModule.forFeature(JOBS_REDUCER_KEY, JOBS_REDUCERS),
    EffectsModule.forFeature([PlanningEffects, JobEffects, JobPaoEffects]),
    ColorDirective,
    SubJobComponent,
    JobViewProductionFiltersComponent,
  ],
})
export class JobsModule {}
