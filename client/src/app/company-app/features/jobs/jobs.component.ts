import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { PlanningActions, PlanningSelectors } from './shared/store/planning';
import { first } from 'rxjs';
import { UserSettingsService } from '../../../shared/services/user-settings.service';
import { JobsUserSettings } from './shared/enum';

@Component({
  selector: 'company-app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent {
  private _setup$ = this._store.select(PlanningSelectors.selectSetup);

  constructor(
    private _store: Store,
    private _userSettingsService: UserSettingsService,
  ) {
    this._setup$.pipe(first()).subscribe((setup) => {
      if (!setup) {
        this._store.dispatch(PlanningActions.tryFetchSetupAction());
      }
    });

    this._setup$.pipe(first((v) => v !== null)).subscribe(() => {
      this._userSettingsService
        .getSettingsMap()
        .pipe(first())
        .subscribe((settingMap) => {
          if (!settingMap) {
            return;
          }

          const group = settingMap.get(JobsUserSettings.DefaultPlanningGroup);
          const machine = settingMap.get(
            JobsUserSettings.DefaultPlanningMachine,
          );

          if (!group || !machine) {
            return;
          }

          this._store.dispatch(
            PlanningActions.setCurrentSettingsAction({
              groupName: group,
              machineId: Number(machine),
            }),
          );
        });
    });
  }
}
