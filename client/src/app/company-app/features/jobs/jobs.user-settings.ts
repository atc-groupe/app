import { AppUserSettingsGroup } from '../../../shared/interfaces';
import { AppUserSettingsCategory } from '../../../customer-app/shared/enums';
import { JobsUserSettings } from './shared/enum';

export const JOBS_USERS_SETTINGS: AppUserSettingsGroup = {
  category: AppUserSettingsCategory.JobsPlanning,
  description: 'Planning des jobs',
  settings: [
    {
      name: JobsUserSettings.DefaultPlanningGroup,
      label: 'Groupe par défaut',
      description: "Groupe par défaut pour l'affichage du planning",
    },
    {
      name: JobsUserSettings.DefaultPlanningMachine,
      label: 'Machine par défaut',
      description: "Machine par défaut pour l'affichage du planning",
    },
  ],
};
