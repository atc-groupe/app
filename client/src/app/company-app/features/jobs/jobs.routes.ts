import { Routes } from '@angular/router';
import { JobsComponent } from './jobs.component';
import { PlanningContainerComponent } from './views/planning-container/planning-container.component';
import { JobViewComponent } from './views/job-view/job-view.component';
import { JobViewInfoComponent } from './views/job-view/job-view-info/job-view-info.component';
import { JobViewGuard } from './shared/guards/job-view.guard';
import { JobViewCommentsComponent } from './views/job-view/job-view-comments/job-view-comments.component';
import { JobViewPaoComponent } from './views/job-view/job-view-pao/job-view-pao.component';
import { JobViewResaComponent } from './views/job-view/job-view-resa/job-view-resa.component';
import { JobViewProductionComponent } from './views/job-view/job-view-production/job-view-production.component';
import { JobViewDeliveriesComponent } from './views/job-view/job-view-deliveries/job-view-deliveries.component';
import { JobViewDeliveryCommentsComponent } from './views/job-view/job-view-delivery-comments/job-view-delivery-comments.component';
import { JobViewResaHandleComponent } from './views/job-view/job-view-resa/job-view-resa-handle/job-view-resa-handle.component';
import { JobViewResaUndoComponent } from './views/job-view/job-view-resa/job-view-resa-undo/job-view-resa-undo.component';

export const JOBS_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'planning',
  },
  {
    path: '',
    component: JobsComponent,
    children: [
      { path: 'planning', component: PlanningContainerComponent },
      {
        path: 'view/:number',
        component: JobViewComponent,
        canActivate: [JobViewGuard],
        children: [
          { path: '', redirectTo: 'info', pathMatch: 'full' },
          { path: 'info', component: JobViewInfoComponent },
          { path: 'comments', component: JobViewCommentsComponent },
          { path: 'pao', component: JobViewPaoComponent },
          { path: 'resa', component: JobViewResaComponent },
          { path: 'resa/handle', component: JobViewResaHandleComponent },
          { path: 'resa/cancel', component: JobViewResaUndoComponent },
          {
            path: 'delivery-comments',
            component: JobViewDeliveryCommentsComponent,
          },
          { path: 'deliveries', component: JobViewDeliveriesComponent },
          {
            path: 'sub-jobs',
            component: JobViewProductionComponent,
          },
        ],
      },
    ],
  },
];
