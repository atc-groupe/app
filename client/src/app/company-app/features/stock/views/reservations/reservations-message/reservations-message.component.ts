import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LayoutModule } from '../../../../../shared/modules/layout.module';

@Component({
  selector: 'stock-reservations-message',
  standalone: true,
  imports: [CommonModule, MatProgressSpinnerModule, LayoutModule],
  templateUrl: './reservations-message.component.html',
  styleUrls: ['./reservations-message.component.scss'],
})
export class ReservationsMessageComponent {
  @Input() isLoading = false;
  @Input() isEmpty = false;
}
