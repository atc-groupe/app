import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  Job,
  MetaJobsStatus,
  MetaJobsStatusColors,
} from '../../../../../shared/interfaces';
import { ColorDirective } from '../../../../../shared/directives/color.directive';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { PaoReservationListItem } from '../../../shared/interfaces';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { CdkTableModule } from '@angular/cdk/table';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterLink } from '@angular/router';
import { JobPaoReservationHelperService } from '../../../../../shared/services/job-pao-reservation-helper.service';

@Component({
  selector: 'stock-reservations-list',
  standalone: true,
  imports: [
    CommonModule,
    ColorDirective,
    CdkTableModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    RouterLink,
  ],
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.scss'],
})
export class ReservationsListComponent implements AfterViewInit {
  @Input() jobs: Job[] | null = null;
  @Input() metaStatusMap: Map<string, MetaJobsStatus> | null = null;
  @Input() dataSource: MatTableDataSource<PaoReservationListItem> | null = null;
  @Output() viewJob = new EventEmitter<string>();

  @ViewChild(MatSort) sort!: MatSort;

  public displayedColumns = [
    'jobNumber',
    'jobStatus',
    'customer',
    'sendingDate',
    'reservationCount',
    'globalStatus',
  ];

  constructor(private _reservationHelper: JobPaoReservationHelperService) {}

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.sort = this.sort;
    }
  }

  getJobStatusColors(statusName: string): MetaJobsStatusColors {
    const defaultColors = { color: '#ddd', background: '#666' };
    if (!this.metaStatusMap) {
      return defaultColors;
    }
    const status = this.metaStatusMap.get(statusName);
    return status
      ? { color: status.textColor, background: status.color }
      : defaultColors;
  }

  getJobStatusLabel(statusName: string): string {
    if (!this.metaStatusMap) {
      return statusName;
    }

    const status = this.metaStatusMap.get(statusName);
    return status ? status.label : statusName;
  }

  onViewJob(jobNumber: string): void {
    this.viewJob.emit(jobNumber);
  }
}
