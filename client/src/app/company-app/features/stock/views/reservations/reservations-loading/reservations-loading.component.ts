import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@Component({
  selector: 'stock-reservations-loading',
  standalone: true,
  imports: [CommonModule, MatProgressSpinnerModule],
  templateUrl: './reservations-loading.component.html',
  styleUrls: ['./reservations-loading.component.scss'],
})
export class ReservationsLoadingComponent {}
