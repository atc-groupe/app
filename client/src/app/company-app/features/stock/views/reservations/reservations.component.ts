import { Component, OnInit } from '@angular/core';
import { JobsService } from '../../../../shared/services/jobs.service';
import { catchError, EMPTY } from 'rxjs';
import { Job } from '../../../../shared/interfaces';
import { ReservationsListComponent } from './reservations-list/reservations-list.component';
import { Store } from '@ngrx/store';
import { MetaJobsStatusSelectors } from '../../../../shared/store/meta-jobs-status';
import { AsyncPipe, NgIf } from '@angular/common';
import { ReservationsLoadingComponent } from './reservations-loading/reservations-loading.component';
import { ReservationsMessageComponent } from './reservations-message/reservations-message.component';
import { MatTableDataSource } from '@angular/material/table';
import { PaoReservationListItem } from '../../shared/interfaces';
import { PaoReservationListMappingService } from '../../shared/services';
import { UiActions } from '../../../../shared/store/ui';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss'],
  standalone: true,
  imports: [
    ReservationsListComponent,
    AsyncPipe,
    NgIf,
    ReservationsLoadingComponent,
    ReservationsMessageComponent,
  ],
})
export class ReservationsComponent implements OnInit {
  public error: string | null = null;
  public jobs: Job[] | null = null;
  public isLoading = true;
  public isEmpty = false;
  public dataSource = new MatTableDataSource<PaoReservationListItem>([]);
  public metaJobsStatusMap$ = this._store.select(
    MetaJobsStatusSelectors.selectStatusMap,
  );

  constructor(
    private _JobsService: JobsService,
    private _store: Store,
    private _paoReservationsMapping: PaoReservationListMappingService,
    private _router: Router,
  ) {}

  ngOnInit() {
    this._JobsService
      .fetchJobs({
        hasPaoReservation: true,
        startStatusNumber: -1,
        endStatusNumber: 999,
      })
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((jobs) => {
        this.dataSource.data = this._paoReservationsMapping.getMappedList(jobs);
        this.jobs = jobs;
        this.isLoading = false;
        this.isEmpty = jobs.length === 0;
      });
  }

  get displayMessage(): boolean {
    return this.isLoading || this.isEmpty;
  }

  get displayList(): boolean {
    return !this.isLoading && !this.isEmpty;
  }

  onViewJob(jobNumber: string) {
    this._store.dispatch(
      UiActions.setJobViewReturnRoute({ route: '/company/stock/reservations' }),
    );
    this._router.navigateByUrl(`/company/jobs/view/${jobNumber}/resa`);
  }
}
