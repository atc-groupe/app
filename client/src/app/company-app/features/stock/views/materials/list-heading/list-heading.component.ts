import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownItems } from '../../../../../shared/interfaces';
import { PRODUCT_TYPE_DROPDOWN_GROUPS } from '../../../../../shared/constants/product-type-dropdown-groups.constant';
import { DropdownComponent } from '../../../../../shared/components/dropdown/dropdown.component';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'material-materials-list-heading',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    RouterLinkActive,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    DropdownComponent,
  ],
  templateUrl: './list-heading.component.html',
  styleUrls: ['./list-heading.component.scss'],
})
export class ListHeadingComponent implements OnInit {
  @Input() initType: ProductType | null = null;
  @Input() searchValue: string | null = '';
  @Output() typeChange = new EventEmitter<ProductType>();
  @Output() groupChange = new EventEmitter<ProductTypeGroup>();
  @Output() search = new EventEmitter<string | null>();

  public types: { value: ProductType; label: string; active: boolean }[] = [
    { value: ProductType.Paper, label: 'MATIÈRES', active: false },
    { value: ProductType.Article, label: 'ARTICLES', active: false },
    {
      value: ProductType.CustomerStock,
      label: 'PRODUITS CLIENTS',
      active: false,
    },
  ];
  public groups$ = new BehaviorSubject<DropdownItems>([]);

  ngOnInit() {
    if (this.initType !== null) {
      this.types.forEach((type) => {
        if (type.value === this.initType) {
          type.active = true;
          this.groups$.next(PRODUCT_TYPE_DROPDOWN_GROUPS.get(this.initType)!);
        }
      });
    }
  }

  onTypeChange(index: number): void {
    this.types.forEach((type) => {
      type.active = false;
    });
    const type = this.types[index];
    type.active = true;
    this.groups$.next(PRODUCT_TYPE_DROPDOWN_GROUPS.get(type.value)!);
    this.typeChange.emit(type.value);
  }

  onGroupChange(group: ProductTypeGroup): void {
    this.groupChange.emit(group);
  }

  onSearchMaterial() {
    this.search.emit(this.searchValue);
  }

  onClearSearch() {
    this.searchValue = '';
    this.search.emit(this.searchValue);
  }
}
