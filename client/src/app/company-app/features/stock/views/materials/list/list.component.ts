import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductList, ProductListItem } from '../../../../../shared/interfaces';
import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';
import { PRODUCT_TYPES_GROUPS } from '../../../../../shared/constants/product-types-groups.constant';
import { MaterialCategoryContainerComponent } from '../../../shared/components/material-category-container/material-category-container.component';
import { RelationList } from '../../../../../../shared/interfaces';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { BehaviorSubject, debounceTime, Subscription } from 'rxjs';
import { ScrollDirective } from '../../../shared/directives/scroll.directive';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'material-materials-list',
  standalone: true,
  imports: [
    CommonModule,
    MaterialCategoryContainerComponent,
    LayoutModule,
    ScrollDirective,
    RouterLink,
  ],
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements AfterViewInit {
  @Input() productList: ProductList | null = null;
  @Input() productType: ProductType | null = null;
  @Input() customerList: RelationList | null = null;
  @Input() initScroll = new EventEmitter<number>();
  @Output() scroll = new EventEmitter<number>();

  private _scroll$ = new BehaviorSubject<number>(0);
  private _subscription = new Subscription();

  registerScroll(scroll: number) {
    this._scroll$.next(scroll);
  }

  ngAfterViewInit() {
    this._subscription.add(
      this._scroll$.pipe(debounceTime(500)).subscribe((scroll) => {
        this.scroll.emit(scroll);
      }),
    );
  }

  getGroupTypeLabel(groupType: ProductTypeGroup): string | null {
    if (this.productType === null) {
      return null;
    }

    if (this.productType === ProductType.CustomerStock) {
      const customer = this.customerList?.find(
        (customer) => customer.number === groupType,
      );

      return customer ? customer.company : null;
    }

    const item = PRODUCT_TYPES_GROUPS.get(this.productType)!.get(groupType);

    return item ? item : null;
  }
}
