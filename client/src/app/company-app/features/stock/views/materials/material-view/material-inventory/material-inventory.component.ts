import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Product } from '../../../../../../shared/interfaces';
import { Action, ProductType, Subject } from '../../../../../../shared/enums';
import { CompanyUser } from '../../../../../../../shared/interfaces';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthorizationsService } from '../../../../../../../shared/services/authorizations.service';
import {
  StockArticleMetaService,
  StockService,
} from '../../../../../../shared/services';
import { Store } from '@ngrx/store';
import { catchError, EMPTY, first } from 'rxjs';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import { FormsModule } from '@angular/forms';
import { ArticleMeta } from '../../../../../../shared/interfaces/stock/article-meta.interface';

@Component({
  selector: 'app-material-inventory',
  standalone: true,
  imports: [CommonModule, LayoutModule, FormsModule],
  templateUrl: './material-inventory.component.html',
  styleUrls: ['./material-inventory.component.scss'],
})
export class MaterialInventoryComponent implements OnInit {
  public canHandleStock = false;
  public product: Product;
  public error: string | null = null;
  public info: string | null = null;
  public success: boolean | null = null;
  public quantity: number | null = null;
  private _productType: ProductType;
  private _user: CompanyUser | null = null;
  private _user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);
  private _articleMeta: ArticleMeta | null = null;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { productType: ProductType; product: Product },
    private _authService: AuthorizationsService,
    private _stockService: StockService,
    private _stockArticleMetaService: StockArticleMetaService,
    private _store: Store,
  ) {
    this._productType = data.productType;
    this.product = data.product;
    this.quantity = this.product.stock;
  }

  ngOnInit() {
    this._authService
      .canActivate(Subject.Stock, Action.Manage)
      .subscribe((canActivate) => {
        this.canHandleStock = canActivate;
      });

    this._user$.pipe(first()).subscribe((user) => {
      this._user = user as CompanyUser;
    });

    if (this._productType === ProductType.Article) {
      this._stockArticleMetaService
        .findOneByMpId(this.product.id)
        .subscribe((articleMeta) => {
          this._articleMeta = articleMeta;
          this.quantity = this.productStock;
        });
    }
  }

  get displayUnit(): string {
    if (this._productType === ProductType.Article && this._articleMeta) {
      return this._articleMeta.packUnitLabel;
    }

    return this.product.displayUnit;
  }

  get productStock(): number {
    if (this._productType === ProductType.Article && this._articleMeta) {
      return Math.floor(this.product.stock / this._articleMeta.packQuantity);
    }

    return this.product.freeStock;
  }

  handleStock() {
    this.error = null;
    this.info = null;

    if (!this._user || this.quantity === undefined || this.quantity === null) {
      return;
    }

    if (this._productType === ProductType.Article && this._articleMeta) {
      this.quantity = this.quantity * this._articleMeta.packQuantity;
    }

    if (this.quantity === this.product.stock) {
      this.info = `La valeur entrée est identique au stock actuel`;
      return;
    }

    if (this.quantity < 0) {
      this.error = `Impossible d'appliquer un stock négatif`;
      return;
    }

    const data: any =
      this.quantity > this.product.stock
        ? { subtract: 0, add: this.quantity - this.product.stock }
        : { subtract: this.product.stock - this.quantity, add: 0 };

    data.description = `Ajustement du stock par ${this._user.firstName} ${this._user.lastName}`;

    this._stockService
      .createStockMutation(this._productType, this.product.id, {
        employee_number: this._user.employeeNumber,
        data,
      })
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe(() => {
        this.success = true;
      });
  }
}
