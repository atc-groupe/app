import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListHeadingComponent } from './list-heading/list-heading.component';
import { ListComponent } from './list/list.component';
import { Store } from '@ngrx/store';
import { StockActions, StockSelectors } from '../../../../shared/store/stock';
import {
  RelationsSelectors,
  RelationsActions,
} from '../../../../../shared/store/relations';
import {
  MaterialsSelectors,
  MaterialsActions,
} from '../../shared/store/materials';
import { ProductType, ProductTypeGroup } from '../../../../shared/enums';
import { first, Subscription } from 'rxjs';
import { StockProductListParamsDto } from '../../../../shared/dto';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ProductList } from '../../../../shared/interfaces';

@Component({
  selector: 'app-materials-list',
  standalone: true,
  imports: [
    CommonModule,
    ListHeadingComponent,
    ListComponent,
    MatProgressSpinnerModule,
  ],
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss'],
})
export class MaterialsComponent implements OnInit, OnDestroy {
  public initType = ProductType.Paper;
  public isLoading = false;
  public productList: ProductList | null = null;
  public customerList$ = this._store.select(
    RelationsSelectors.selectCustomersList,
  );
  public productType$ = this._store.select(
    MaterialsSelectors.selectProductType,
  );
  public materialsState$ = this._store.select(
    MaterialsSelectors.selectMaterialsState,
  );
  public search$ = this._store.select(MaterialsSelectors.selectSearch);
  public initScroll = new EventEmitter<number>();

  private _subscription = new Subscription();
  private _group: ProductTypeGroup | null = null;
  private _productType: ProductType | null = null;
  private _isLoading$ = this._store.select(StockSelectors.selectIsLoading);
  private _productList$ = this._store.select(StockSelectors.selectList);
  private _initScroll$ = this._store.select(MaterialsSelectors.selectScroll);

  constructor(private _store: Store) {}

  ngOnInit() {
    this.materialsState$.pipe(first()).subscribe((state) => {
      this.initType = state.productType;

      const params: StockProductListParamsDto = {};

      if (state.search) {
        params.search = state.search;
      }

      if (state.group) {
        params.groupType = state.group;
      }

      this._store.dispatch(
        StockActions.tryFetchProductListAction({
          productType: state.productType,
          params: { ...params },
        }),
      );
    });

    this._store.dispatch(RelationsActions.tryFetchCustomersAction());

    this._subscription.add(
      this._isLoading$.subscribe((isLoading) => {
        this.isLoading = isLoading;
      }),
    );

    this._subscription.add(
      this.productType$.subscribe((productType) => {
        this._productType = productType;
      }),
    );

    this._subscription.add(
      this._productList$.subscribe((productList) => {
        this.productList = productList;
        this._initScroll$.pipe(first()).subscribe((scroll) => {
          this.initScroll.emit(scroll);
        });
      }),
    );
  }

  onTypeChange(productType: ProductType) {
    this._store.dispatch(MaterialsActions.resetScrollAction());
    this._store.dispatch(
      MaterialsActions.setProductTypeAction({ productType }),
    );
    this._store.dispatch(
      StockActions.tryFetchProductListAction({ productType }),
    );
  }

  onGroupChange(group: ProductTypeGroup): void {
    this._group = group;
    this._store.dispatch(MaterialsActions.setProductTypeGroupAction({ group }));
    this._store.dispatch(MaterialsActions.resetScrollAction());
    this._store.dispatch(
      StockActions.tryFetchProductListAction({
        productType: this._productType!,
        params: { groupType: group },
      }),
    );
  }

  onSearch(search: string | null): void {
    const params: StockProductListParamsDto = {};

    if (this._group) {
      params.groupType = this._group;
    }

    if (search) {
      params.search = search;
    }

    this._store.dispatch(MaterialsActions.setSearchAction({ search }));
    this._store.dispatch(MaterialsActions.resetScrollAction());
    this._store.dispatch(
      StockActions.trySearchProductListAction({
        productType: this._productType!,
        params,
      }),
    );
  }

  onScroll(scroll: number): void {
    this._store.dispatch(MaterialsActions.setScrollAction({ scroll }));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
