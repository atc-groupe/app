import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { ActivatedRoute, RouterLink } from '@angular/router';
import {
  StockActions,
  StockSelectors,
} from '../../../../../shared/store/stock';
import { Action, ProductType, Subject } from '../../../../../shared/enums';
import { Product } from '../../../../../shared/interfaces';
import { Subscription } from 'rxjs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AuthorizationsService } from '../../../../../../shared/services/authorizations.service';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { MatDialog } from '@angular/material/dialog';
import { MaterialsViewStockComponent } from './materials-view-stock/materials-view-stock.component';
import { MaterialStockHistoryComponent } from './material-stock-history/material-stock-history.component';
import { ProductViewInfoMappingService } from '../../../shared/services';
import { ProductDetail } from '../../../shared/interfaces';
import { MaterialInventoryComponent } from './material-inventory/material-inventory.component';
import { ArticleMeta } from '../../../../../shared/interfaces/stock/article-meta.interface';
import { StockArticleMetaService } from '../../../../../shared/services';
import { MaterialsSelectors } from '../../../shared/store/materials';
import { CancelMutationComponent } from './cancel-mutation/cancel-mutation.component';

@Component({
  selector: 'app-material-view',
  standalone: true,
  imports: [CommonModule, MatProgressSpinnerModule, LayoutModule, RouterLink],
  templateUrl: './material-view.component.html',
  styleUrls: ['./material-view.component.scss'],
})
export class MaterialViewComponent implements OnInit, OnDestroy {
  public products: Product[] | null = null;
  public isLoading = true;
  public canDecreaseStock: boolean = false;
  public canHandleStock: boolean = false;
  public productDetails: ProductDetail[] = [];

  private _subscription = new Subscription();
  private _products$ = this._store.select(StockSelectors.selectProducts);
  private _articlesMeta: ArticleMeta[] | null = null;
  private _productType: ProductType | null = null;
  private _productId: number | null = null;
  private _lastMutations: { productId: number; mutationId: number }[] = [];

  constructor(
    private _store: Store,
    private _authService: AuthorizationsService,
    private _dialog: MatDialog,
    private _productInfoMapping: ProductViewInfoMappingService,
    private _stockArticleMetaService: StockArticleMetaService,
    private _activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    this._productType = parseInt(
      this._activatedRoute.snapshot.paramMap.get('productType')!,
    ) as ProductType;
    this._productId = parseInt(
      this._activatedRoute.snapshot.paramMap.get('productId')!,
    );

    this._store.dispatch(
      StockActions.tryFetchProductsFromProductIdAction({
        productType: this._productType,
        productId: this._productId,
      }),
    );

    this._subscription = this._products$.subscribe((products) => {
      if (!products || !products.length) {
        return;
      }

      this.products = products;
      this.productDetails = this._productInfoMapping.getProductDetails(
        this._productType!,
        products[0],
      );

      this.isLoading = false;
    });

    this._subscription.add(
      this._store
        .select(MaterialsSelectors.selectLastMutations)
        .subscribe((mutations) => {
          this._lastMutations = mutations;
        }),
    );

    this._authService
      .canActivate(Subject.Stock, Action.Manage)
      .subscribe((auth) => {
        this.canHandleStock = auth;
      });

    this._authService
      .canActivate(Subject.Stock, Action.Decrease)
      .subscribe((auth) => {
        this.canDecreaseStock = auth;
      });

    if (this._productType === ProductType.Article) {
      this._stockArticleMetaService.findAll().subscribe((articlesMeta) => {
        this._articlesMeta = articlesMeta;
      });
    }
  }

  openProductViewStock(product: Product): void {
    this._dialog
      .open(MaterialsViewStockComponent, {
        width: '60rem',
        data: { productType: this._productType, product },
      })
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.isLoading = true;
          setTimeout(() => {
            this._store.dispatch(
              StockActions.tryFetchProductsFromProductIdAction({
                productType: this._productType!,
                productId: this._productId!,
              }),
            );
          }, 300);
        }
      });
  }

  openMaterialInventory(product: Product): void {
    this._dialog
      .open(MaterialInventoryComponent, {
        width: '60rem',
        data: { productType: this._productType, product },
      })
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.isLoading = true;
          setTimeout(() => {
            this._store.dispatch(
              StockActions.tryFetchProductsFromProductIdAction({
                productType: this._productType!,
                productId: this._productId!,
              }),
            );
          }, 300);
        }
      });
  }

  openProductStockHistory(product: Product): void {
    this._dialog.open(MaterialStockHistoryComponent, {
      width: '90rem',
      height: '70rem',
      data: { productType: this._productType, product },
      autoFocus: 'first-heading',
    });
  }

  openCancelMutation(product: Product): void {
    const lastMutation = this._lastMutations.find(
      (mutation) => mutation.productId === product.id,
    );

    if (!lastMutation) {
      return;
    }

    this._dialog
      .open(CancelMutationComponent, {
        width: '60rem',
        height: '40rem',
        data: {
          productType: this._productType,
          product,
          mutationId: lastMutation.mutationId,
        },
      })
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.isLoading = true;
          setTimeout(() => {
            this._store.dispatch(
              StockActions.tryFetchProductsFromProductIdAction({
                productType: this._productType!,
                productId: this._productId!,
              }),
            );
          }, 300);
        }
      });
  }

  get product(): Product | null {
    return this.products ? this.products[0] : null;
  }

  get displayItemsTitle(): boolean {
    if (!this.products) {
      return false;
    }

    return this.products.length > 1;
  }

  getProductDisplayStock(product: Product): string {
    return this._getDisplayStock(product, product.stock);
  }

  getProductDisplayFreeStock(product: Product): string {
    return this._getDisplayStock(product, product.freeStock);
  }

  getProductDisplayReservedStock(product: Product): string {
    return this._getDisplayStock(product, product.reserved);
  }

  viewDecreaseButton(product: Product): boolean {
    return this.canDecreaseStock && product.freeStock > 0;
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  displayCancelButton(product: Product): boolean {
    return (
      this._lastMutations.find(
        (mutation) => mutation.productId === product.id,
      ) !== undefined
    );
  }

  private _getDisplayStock(product: Product, stock: number) {
    const displayStock = `${product.stock} ${product.displayUnit}`;
    if (this._productType === ProductType.Article) {
      if (!this._articlesMeta) {
        return displayStock;
      }

      const articleMeta = this._articlesMeta.find(
        (item) => item.mpArticleId === product.id,
      );

      if (!articleMeta) {
        return displayStock;
      }

      const stockByContainer = Math.floor(stock / articleMeta.packQuantity);

      return `${stockByContainer} ${articleMeta.packUnitLabel}`;
    }

    return displayStock;
  }
}
