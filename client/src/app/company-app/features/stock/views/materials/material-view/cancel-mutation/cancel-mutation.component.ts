import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { ProductType } from '../../../../../../shared/enums';
import { Product, StockMutation } from '../../../../../../shared/interfaces';
import { Store } from '@ngrx/store';
import { MaterialsActions } from '../../../../shared/store/materials';
import { StockService } from '../../../../../../shared/services';
import { catchError, delay, EMPTY, Subscription } from 'rxjs';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import { CompanyUser } from '../../../../../../../shared/interfaces';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-cancel-mutation',
  standalone: true,
  imports: [CommonModule, MatDialogModule, MatProgressSpinnerModule],
  templateUrl: './cancel-mutation.component.html',
  styleUrls: ['./cancel-mutation.component.scss'],
})
export class CancelMutationComponent implements OnInit {
  public product: Product;
  public mutation: StockMutation | null = null;
  public error: string | null = null;
  public success: boolean | null = null;
  public isLoading = true;

  private _subscription = new Subscription();
  private _productType: ProductType;
  private _mutationId: number;
  private _authenticatedUser: CompanyUser | null = null;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: {
      productType: ProductType;
      product: Product;
      mutationId: number;
    },
    private _store: Store,
    private _stockService: StockService,
  ) {
    this.product = data.product;
    this._productType = data.productType;
    this._mutationId = data.mutationId;
  }

  ngOnInit() {
    this._stockService
      .getStockMutation(this._productType, this.product.id, this._mutationId)
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((mutation) => {
        this.mutation = mutation;
        this.isLoading = false;
      });

    this._subscription.add(
      this._store.select(UsersSelectors.selectCompanyUser).subscribe((user) => {
        this._authenticatedUser = user;
      }),
    );
  }

  onValidateCancel(): void {
    if (!this._authenticatedUser || !this.mutation) {
      return;
    }

    this.isLoading = true;

    this._stockService
      .updateStockMutation(
        this._productType,
        this.product.id,
        this._mutationId,
        {
          employee_number: this._authenticatedUser.employeeNumber,
          data: {
            add: this.mutation.subtract,
            subtract: 0,
            job_number: this.mutation.job_number,
            description: `Annulation du dernier déstockage par ${this._authenticatedUser.firstName} ${this._authenticatedUser.lastName}`,
          },
        },
      )
      .pipe(
        delay(300),
        catchError((err) => {
          this.isLoading = false;
          this.success = false;
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe(() => {
        this._store.dispatch(
          MaterialsActions.clearLastProductMutation({
            productId: this.product.id,
          }),
        );
        this.isLoading = false;
        this.success = true;
      });
  }
}
