import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Product,
  StockMutationResponse,
} from '../../../../../../shared/interfaces';
import { AuthorizationsService } from '../../../../../../../shared/services/authorizations.service';
import {
  Action,
  PaperType,
  ProductType,
  Subject,
} from '../../../../../../shared/enums';
import { LayoutModule } from '../../../../../../shared/modules/layout.module';
import { CoreModule } from '../../../../../../../shared/modules/core.module';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import {
  StockArticleMetaService,
  StockService,
} from '../../../../../../shared/services';
import { UsersSelectors } from '../../../../../../../shared/store/users';
import { MaterialsActions } from '../../../../shared/store/materials';
import { Store } from '@ngrx/store';
import { CompanyUser } from '../../../../../../../shared/interfaces';
import { catchError, delay, EMPTY, first } from 'rxjs';
import { ArticleMeta } from '../../../../../../shared/interfaces/stock/article-meta.interface';

@Component({
  selector: 'app-materials-view-stock',
  standalone: true,
  imports: [CommonModule, LayoutModule, CoreModule, FormsModule],
  templateUrl: './materials-view-stock.component.html',
  styleUrls: ['./materials-view-stock.component.scss'],
})
export class MaterialsViewStockComponent implements OnInit {
  public canDecreaseStock = false;
  public product: Product;
  public articleMeta: ArticleMeta | null = null;
  public error: string | null = null;
  public success: boolean | null = null;
  public form = this._fb.group({
    quantity: [1, Validators.required],
    jobNumber: [],
    comment: [''],
  });
  private _productType: ProductType;
  private _user: CompanyUser | null = null;
  private _user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { productType: ProductType; product: Product },
    private _fb: FormBuilder,
    private _authService: AuthorizationsService,
    private _stockService: StockService,
    private _stockArticleMetaService: StockArticleMetaService,
    private _store: Store,
  ) {
    this._productType = data.productType;
    this.product = data.product;
  }

  ngOnInit() {
    this._authService
      .canActivate(Subject.Stock, Action.Decrease)
      .subscribe((canActivate) => {
        this.canDecreaseStock = canActivate;
      });

    this._user$.pipe(first()).subscribe((user) => {
      this._user = user as CompanyUser;
    });

    if (this._productType === ProductType.Article) {
      this._stockArticleMetaService
        .findOneByMpId(this.product.id)
        .subscribe((articleMeta) => {
          this.articleMeta = articleMeta;
        });
    }
  }

  get displayRemoveMessage(): boolean {
    return this.productFreeStock < 1 && this.canDecreaseStock;
  }

  get canDisplayForm(): boolean {
    return this.productFreeStock > 0 && this.canDecreaseStock && !this.success;
  }

  get isPaperRollProduct(): boolean {
    return 'type' in this.product && this.product.type === PaperType.Roll;
  }

  get rollLength(): string | null {
    if (!('length' in this.product)) {
      return null;
    }

    return `${this.product.length} ${this.product.displayUnit}`;
  }

  get displayUnit(): string {
    if (this._productType === ProductType.Article && this.articleMeta) {
      return this.articleMeta.packUnitLabel;
    }

    return this.product.displayUnit;
  }

  get productFreeStock(): number {
    if (this._productType === ProductType.Article && this.articleMeta) {
      return Math.floor(this.product.freeStock / this.articleMeta.packQuantity);
    }

    return this.product.freeStock;
  }

  handleStock() {
    const quantity = this.form.get('quantity')?.value;
    const jobNumber = this.form.get('jobNumber')?.value;
    const comment = this.form.get('comment')?.value;

    if (!this._user || !quantity) {
      return;
    }

    const calculatedQuantity =
      this._productType === ProductType.Article && this.articleMeta
        ? quantity * this.articleMeta.packQuantity
        : quantity;

    if (calculatedQuantity > this.product.freeStock) {
      this.error = `Impossible de déstocker ${quantity} ${this.displayUnit}. La quantité disponible est de ${this.productFreeStock} ${this.displayUnit}.`;
      return;
    }

    const data: any = { subtract: calculatedQuantity, add: 0 };

    if (jobNumber) {
      data.job_number = jobNumber;
    }

    data.description = comment
      ? `${comment} - Déstockage par ${this._user.firstName} ${this._user.lastName}`
      : `Déstockage par ${this._user.firstName} ${this._user.lastName}`;

    this._stockService
      .createStockMutation(this._productType, this.product.id, {
        employee_number: this._user.employeeNumber,
        data,
      })
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((mutationResponse: StockMutationResponse) => {
        this._store.dispatch(
          MaterialsActions.setLastProductMutation({
            productId: this.product.id,
            mutationId: mutationResponse.mutation_id,
          }),
        );
        this.success = true;
      });
  }
}
