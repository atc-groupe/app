import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  StockArticleMetaService,
  StockService,
} from '../../../../../../shared/services';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { ProductType } from '../../../../../../shared/enums';
import {
  Product,
  StockMutationListItem,
} from '../../../../../../shared/interfaces';
import { BehaviorSubject, catchError, EMPTY } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ArticleMeta } from '../../../../../../shared/interfaces/stock/article-meta.interface';

@Component({
  selector: 'app-material-stock-history',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatDialogModule],
  templateUrl: './material-stock-history.component.html',
  styleUrls: ['./material-stock-history.component.scss'],
})
export class MaterialStockHistoryComponent implements OnInit {
  public product: Product;
  public mutations: StockMutationListItem[] | null = null;
  public date$ = new BehaviorSubject<Date>(new Date());
  public error: string | null = null;

  private _productType: ProductType;
  private _articleMeta: ArticleMeta | null = null;

  constructor(
    private _stockService: StockService,
    private _stockArticleMetaService: StockArticleMetaService,
    @Inject(MAT_DIALOG_DATA)
    public data: { productType: ProductType; product: Product },
  ) {
    this._productType = data.productType;
    this.product = data.product;
  }

  ngOnInit() {
    this._fetchMutations();

    if (this._productType === ProductType.Article) {
      this._stockArticleMetaService
        .findOneByMpId(this.product.id)
        .subscribe((articleMeta) => {
          this._articleMeta = articleMeta;
        });
    }
  }

  public onTriggerPrevDate() {
    const date = new Date(this.date$.value);
    date.setDate(date.getDate() - 1);
    this.date$.next(date);
    this._fetchMutations();
  }

  public onTriggerNextDate() {
    const date = new Date(this.date$.value);
    date.setDate(date.getDate() + 1);
    this.date$.next(date);
    this._fetchMutations();
  }

  get displayUnit(): string {
    if (this._productType === ProductType.Article && this._articleMeta) {
      return this._articleMeta.packUnitLabel;
    }

    return this.product.displayUnit;
  }

  getQuantity(quantity: number) {
    if (this._productType === ProductType.Article && this._articleMeta) {
      return Math.floor(quantity / this._articleMeta.packQuantity);
    }

    return quantity;
  }

  private _getFormattedStartDate(): string {
    return this.date$.value.toLocaleDateString().replaceAll('/', '-');
  }

  private _fetchMutations(): void {
    this._stockService
      .getStockMutations(
        this._productType,
        this.product.id,
        this._getFormattedStartDate(),
      )
      .pipe(
        catchError((err) => {
          this.error = err.error.message;
          return EMPTY;
        }),
      )
      .subscribe((mutations) => {
        this.mutations = mutations;
      });
  }
}
