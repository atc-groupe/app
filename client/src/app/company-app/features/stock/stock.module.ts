import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';

import { StockComponent } from './stock.component';
import { StockNavComponent } from './shared/components/stock-nav/stock-nav.component';

import { STOCK_ROUTES } from './stock.routes';
import { STOCK_REDUCER_KEY } from './shared/store/store.constants';
import { STOCK_REDUCERS } from './shared/store';

@NgModule({
  declarations: [StockComponent],
  imports: [
    CoreModule,
    RouterModule.forChild(STOCK_ROUTES),
    StoreModule.forFeature(STOCK_REDUCER_KEY, STOCK_REDUCERS),
    StockNavComponent,
  ],
})
export class StockModule {}
