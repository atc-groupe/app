import { ModuleAuthorizations } from '../../../shared/interfaces';
import { Action, Module, Subject } from '../../shared/enums';

export const STOCK_AUTHORIZATIONS: ModuleAuthorizations = {
  module: Module.Stock,
  label: 'Stock',
  subjectsAuthorizations: [
    {
      label: 'Stock',
      authorizations: [
        {
          subject: Subject.Stock,
          action: Action.Decrease,
          label: "Déstockage d'un produit",
          description: 'Possibilité de déstocker un article ou une matière',
        },
        {
          subject: Subject.Stock,
          action: Action.Manage,
          label: 'Modification du niveau de stock',
          description:
            "Possibilité de modifier le stock d'un article ou d'une matière",
        },
      ],
    },
  ],
};
