import { Routes } from '@angular/router';
import { StockComponent } from './stock.component';
import { MaterialsComponent } from './views/materials/materials.component';
import { ReservationsComponent } from './views/reservations/reservations.component';
import { MaterialViewComponent } from './views/materials/material-view/material-view.component';

export const STOCK_ROUTES: Routes = [
  {
    path: '',
    component: StockComponent,
    children: [
      { path: '', redirectTo: 'materials', pathMatch: 'full' },
      { path: 'materials', component: MaterialsComponent },
      {
        path: 'materials/:productType/:productId',
        component: MaterialViewComponent,
      },
      { path: 'reservations', component: ReservationsComponent },
    ],
  },
];
