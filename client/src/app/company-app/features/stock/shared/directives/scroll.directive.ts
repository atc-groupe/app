import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { debounceTime, Subscription } from 'rxjs';

@Directive({
  selector: '[stockScroll]',
  standalone: true,
})
export class ScrollDirective implements AfterViewInit, OnDestroy {
  @Input() setInitScroll?: EventEmitter<number>;
  @Output() stockScroll = new EventEmitter<number>();

  @HostListener('scroll') onScroll() {
    this.stockScroll.emit(this.el.nativeElement.scrollTop);
  }

  private _subscription = new Subscription();

  constructor(private el: ElementRef<HTMLDivElement>) {}

  ngAfterViewInit() {
    if (this.setInitScroll) {
      this._subscription = this.setInitScroll.subscribe((scroll) => {
        this.el.nativeElement.scrollTo(0, scroll);
      });
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
