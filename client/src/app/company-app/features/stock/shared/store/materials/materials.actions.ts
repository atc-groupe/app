import { createAction, props } from '@ngrx/store';
import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';

export const setProductTypeAction = createAction(
  '[CompanyApp > Material > Materials] set product type',
  props<{ productType: ProductType }>(),
);
export const setProductTypeGroupAction = createAction(
  '[CompanyApp > Material > Materials] set product type group',
  props<{ group: ProductTypeGroup | null }>(),
);
export const setSearchAction = createAction(
  '[CompanyApp > Material > Materials] set search',
  props<{ search: string | null }>(),
);
export const setScrollAction = createAction(
  '[CompanyApp > Material > Materials] set scroll',
  props<{ scroll: number }>(),
);
export const resetScrollAction = createAction(
  '[CompanyApp > Material > Materials] reset scroll',
);

// ------ Last Stock mutation ------
export const setLastProductMutation = createAction(
  '[CompanyApp > Material > Materials] set last mutation',
  props<{ productId: number; mutationId: number }>(),
);
export const clearLastProductMutation = createAction(
  '[CompanyApp > Material > Materials] reset last mutation id',
  props<{ productId: number }>(),
);
