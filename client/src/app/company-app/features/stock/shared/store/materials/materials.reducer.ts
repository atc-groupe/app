import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';
import { createReducer, on } from '@ngrx/store';
import * as MaterialsActions from './materials.actions';

export const MATERIALS_REDUCER_KEY = 'materials';

export interface MaterialsState {
  productType: ProductType;
  group: ProductTypeGroup | null;
  search: string | null;
  scroll: number;
  lastMutations: { productId: number; mutationId: number }[];
}

const INITIAL_STATE: MaterialsState = {
  productType: ProductType.Paper,
  group: null,
  search: null,
  scroll: 0,
  lastMutations: [],
};

export const materialsReducer = createReducer(
  INITIAL_STATE,
  on(
    MaterialsActions.setProductTypeAction,
    (state, { productType }): MaterialsState => {
      return {
        ...state,
        productType,
        search: null,
      };
    },
  ),
  on(
    MaterialsActions.setProductTypeGroupAction,
    (state, { group }): MaterialsState => {
      return {
        ...state,
        group,
      };
    },
  ),
  on(MaterialsActions.setSearchAction, (state, { search }): MaterialsState => {
    return {
      ...state,
      search,
    };
  }),
  on(MaterialsActions.setScrollAction, (state, { scroll }): MaterialsState => {
    return {
      ...state,
      scroll,
    };
  }),
  on(MaterialsActions.resetScrollAction, (state): MaterialsState => {
    return {
      ...state,
      scroll: 0,
    };
  }),
  on(
    MaterialsActions.setLastProductMutation,
    (state, mutation): MaterialsState => {
      return {
        ...state,
        lastMutations: [...state.lastMutations, mutation],
      };
    },
  ),
  on(
    MaterialsActions.clearLastProductMutation,
    (state, { productId }): MaterialsState => {
      return {
        ...state,
        lastMutations: state.lastMutations.filter(
          (mutation) => mutation.productId !== productId,
        ),
      };
    },
  ),
);
