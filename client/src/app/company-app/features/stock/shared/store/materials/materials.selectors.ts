import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MATERIALS_REDUCER_KEY, MaterialsState } from './materials.reducer';
import { STOCK_REDUCER_KEY } from '../store.constants';
import { StockState } from '../index';
import { ProductType, ProductTypeGroup } from '../../../../../shared/enums';

const selectMaterialState =
  createFeatureSelector<StockState>(STOCK_REDUCER_KEY);
export const selectMaterialsState = createSelector(
  selectMaterialState,
  (state): MaterialsState => state[MATERIALS_REDUCER_KEY],
);

export const selectProductType = createSelector(
  selectMaterialsState,
  (state): ProductType => state.productType,
);
export const selectProductTypeGroup = createSelector(
  selectMaterialsState,
  (state): ProductTypeGroup | null => state.group,
);
export const selectSearch = createSelector(
  selectMaterialsState,
  (state): string | null => state.search,
);
export const selectScroll = createSelector(
  selectMaterialsState,
  (state): number => state.scroll,
);
export const selectLastMutations = createSelector(
  selectMaterialsState,
  (state): { productId: number; mutationId: number }[] => state.lastMutations,
);
