import {
  MATERIALS_REDUCER_KEY,
  materialsReducer,
  MaterialsState,
} from './materials/materials.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface StockState {
  [MATERIALS_REDUCER_KEY]: MaterialsState;
}

export const STOCK_REDUCERS: ActionReducerMap<StockState> = {
  [MATERIALS_REDUCER_KEY]: materialsReducer,
};
