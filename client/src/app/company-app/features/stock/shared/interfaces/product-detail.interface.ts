export interface ProductDetail {
  label: string;
  value: string | number;
}
