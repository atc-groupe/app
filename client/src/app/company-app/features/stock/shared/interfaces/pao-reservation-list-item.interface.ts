import { PaoStockReservationGlobalStatus } from '../../../../shared/enums';

export interface PaoReservationListItem {
  jobNumber: string;
  jobStatus: string;
  customer: string;
  sendingDate: string | null;
  reservationCount: number;
  globalStatus: {
    status: PaoStockReservationGlobalStatus;
    color: string;
  };
}
