import { Injectable } from '@angular/core';
import { Job } from '../../../../shared/interfaces';
import { PaoReservationListItem } from '../interfaces';
import { JobPaoReservationHelperService } from '../../../../shared/services/job-pao-reservation-helper.service';

@Injectable({ providedIn: 'root' })
export class PaoReservationListMappingService {
  constructor(private _helper: JobPaoReservationHelperService) {}
  getMappedList(jobs: Job[]): PaoReservationListItem[] {
    return jobs.reduce(
      (acc: PaoReservationListItem[], job): PaoReservationListItem[] => {
        acc.push({
          jobNumber: job.mp.number,
          customer: job.mp.company,
          jobStatus: job.mp.jobStatus,
          sendingDate: job.mp.sendingDate,
          reservationCount: job.pao!.stockSheetReservations.length,
          globalStatus: this._helper.getGlobalStatus(
            job.pao!.stockSheetReservations,
          ),
        });

        return acc;
      },
      [],
    );
  }
}
