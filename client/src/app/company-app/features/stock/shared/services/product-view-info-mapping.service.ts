import { Injectable } from '@angular/core';
import { ProductDetail } from '../interfaces';
import { ProductType } from '../../../../shared/enums';
import {
  Article,
  CustomerStock,
  Paper,
  Product,
} from '../../../../shared/interfaces';

@Injectable({ providedIn: 'root' })
export class ProductViewInfoMappingService {
  getProductDetails(
    productType: ProductType,
    product: Product,
  ): ProductDetail[] {
    switch (productType) {
      case ProductType.Article:
        return this._getArticleDetails(product as Article);
      case ProductType.CustomerStock:
        return this._getCustomerStockDetails(product as CustomerStock);
      case ProductType.Paper:
        return this._getPaperDetails(product as Paper);
    }
  }

  private _getPaperDetails(paper: Paper): ProductDetail[] {
    return [
      { label: 'Couleur', value: paper.color },
      { label: 'Groupe', value: paper.groupDescription },
      { label: 'Type', value: paper.type === 'sheet' ? 'Plaque' : 'Rouleau' },
      { label: 'Largeur', value: `${paper.width} mm` },
      {
        label: paper.type === 'roll' ? 'Longueur' : 'Hauteur',
        value: `${paper.length} ${paper.type === 'roll' ? 'ml' : 'mm'}`,
      },
      { label: 'Epaisseur', value: `${paper.thickness} mm` },
      {
        label: 'Face imprimée',
        value: paper.insidePrint ? 'Intérieure' : 'Extérieure',
      },
      { label: 'Fournisseur', value: paper.supplier },
    ];
  }

  private _getArticleDetails(article: Article): ProductDetail[] {
    const data: ProductDetail[] = [];

    if (article.color) {
      data.push({ label: 'Couleur', value: article.color });
    }

    data.push(
      { label: 'Groupe', value: article.groupDescription },
      { label: 'Fournisseur', value: article.supplier },
    );

    if (article.unit) {
      data.push({ label: 'Unité de stockage', value: article.unit });
    }

    if (article.packedPer) {
      data.push({ label: 'Emballé par', value: article.packedPer });
    }

    return data;
  }

  private _getCustomerStockDetails(
    customerStock: CustomerStock,
  ): ProductDetail[] {
    return [
      { label: 'Type de produit', value: customerStock.productType },
      { label: 'Client', value: customerStock.groupDescription },
      { label: 'Largeur', value: customerStock.width },
      { label: 'Hauteur', value: customerStock.length },
      {
        label: 'Disponible sur le web',
        value: customerStock.notOnWeb ? 'Non' : 'Oui',
      },
    ];
  }
}
