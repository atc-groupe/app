import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'material-category-container',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './material-category-container.component.html',
  styleUrls: ['./material-category-container.component.scss'],
})
export class MaterialCategoryContainerComponent {
  @Input() expanded: boolean = true;
  @Input() title: string | null = '';
  @Input() class: string = '';

  toggle() {
    this.expanded = !this.expanded;
  }
}
