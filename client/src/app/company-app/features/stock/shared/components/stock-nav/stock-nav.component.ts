import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { LayoutModule } from '../../../../../shared/modules/layout.module';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'company-app-stock-nav',
  templateUrl: './stock-nav.component.html',
  styleUrls: ['./stock-nav.component.scss'],
  standalone: true,
  imports: [CommonModule, RouterLink, RouterLinkActive, LayoutModule],
})
export class StockNavComponent {}
