import { JobSyncStatusType } from '../../../../shared/types/job-sync-status.type';

export interface DeliveryPlanningItem {
  _id: string;
  number: string;
  jobManager: string;
  company: string;
  status: string;
  multiSendingDate: boolean;
  printingSurface: number;
  manufacturedPiecesCount: number;
  subJobsCount: number;
  hasSheetProduction: boolean;
  hasDeliveryServiceFinishs: boolean;
  deliveryServiceFinishs: string[];
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
  productionTypes: string[];
  productionTypesAsString: string;
  addressZipCodes: string;
  deliveryMethods: string;
  comment: string | null;
  syncStatus: JobSyncStatusType;
}
