import { Job } from '../../../../../shared/interfaces';
import { createReducer, on } from '@ngrx/store';
import * as JobsActions from './jobs.actions';
import { DeliveryPlanningItem } from '../../interfaces';

export const JOBS_REDUCER_KEY = 'jobs';

export interface JobsState {
  jobs: {
    list: DeliveryPlanningItem[];
    currentDate: string;
    fetchError: string | null;
    isLoading: boolean;
  };
  selectedJob: {
    job: Job | null;
    fetchError: string | null;
    updateCommentSuccess: boolean | null;
    updateCommentError: string | null;
    changeStatusSuccess: boolean | null;
    changeStatusError: string | null;
    isChangingStatus: boolean;
  };
}

const date = new Date().toLocaleDateString('en-US');

const INITIAL_STATE: JobsState = {
  jobs: {
    list: [],
    currentDate: date,
    fetchError: null,
    isLoading: false,
  },
  selectedJob: {
    job: null,
    fetchError: null,
    updateCommentSuccess: null,
    updateCommentError: null,
    changeStatusSuccess: null,
    changeStatusError: null,
    isChangingStatus: false,
  },
};

export const jobsReducer = createReducer(
  INITIAL_STATE,
  on(JobsActions.tryFetchJobsAction, (state): JobsState => {
    return {
      ...state,
      jobs: {
        ...state.jobs,
        list: [],
        fetchError: null,
        isLoading: true,
      },
    };
  }),
  on(JobsActions.fetchJobsSuccessAction, (state, { jobs }): JobsState => {
    return {
      ...state,
      jobs: {
        ...state.jobs,
        list: jobs,
        fetchError: null,
        isLoading: false,
      },
    };
  }),
  on(JobsActions.fetchJobsErrorAction, (state, { error }): JobsState => {
    return {
      ...state,
      jobs: {
        ...state.jobs,
        fetchError: error,
        isLoading: false,
      },
    };
  }),
  on(JobsActions.setJobsNextDateAction, (state): JobsState => {
    const newDate = new Date(state.jobs.currentDate);
    const inc = newDate.getDay() === 5 ? 3 : 1;
    newDate.setDate(newDate.getDate() + inc);

    return {
      ...state,
      jobs: {
        ...state.jobs,
        currentDate: newDate.toLocaleDateString('en-US'),
      },
    };
  }),
  on(JobsActions.setJobsPreviousDateAction, (state): JobsState => {
    const newDate = new Date(state.jobs.currentDate);
    const inc = newDate.getDay() === 1 ? 3 : 1;
    newDate.setDate(newDate.getDate() - inc);

    return {
      ...state,
      jobs: {
        ...state.jobs,
        currentDate: newDate.toLocaleDateString('en-US'),
      },
    };
  }),
  on(JobsActions.resetJobsDateAction, (state): JobsState => {
    const newDate = new Date(date);

    return {
      ...state,
      jobs: {
        ...state.jobs,
        currentDate: newDate.toLocaleDateString('en-US'),
      },
    };
  }),
  on(JobsActions.setJobsCurrentDateAction, (state, { date }): JobsState => {
    const newDate = new Date(date);

    return {
      ...state,
      jobs: {
        ...state.jobs,
        currentDate: newDate.toLocaleDateString('en-US'),
      },
    };
  }),
  on(JobsActions.tryFetchSelectedJobAction, (state): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        job: null,
        fetchError: null,
        updateCommentSuccess: null,
        updateCommentError: null,
        changeStatusSuccess: null,
        changeStatusError: null,
      },
    };
  }),
  on(JobsActions.fetchSelectedJobSuccessAction, (state, { job }): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        job,
        fetchError: null,
      },
    };
  }),
  on(JobsActions.fetchSelectedJobErrorAction, (state, { error }): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        fetchError: error,
      },
    };
  }),
  on(JobsActions.tryUpdateJobDeliveryCommentAction, (state): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        updateCommentSuccess: null,
      },
    };
  }),
  on(JobsActions.updateJobDeliveryCommentSuccessAction, (state): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        updateCommentSuccess: true,
        updateCommentError: null,
      },
    };
  }),
  on(
    JobsActions.updateJobDeliveryCommentErrorAction,
    (state, { error }): JobsState => {
      return {
        ...state,
        selectedJob: {
          ...state.selectedJob,
          updateCommentSuccess: false,
          updateCommentError: error,
        },
      };
    },
  ),
  on(JobsActions.tryChangeJobStatus, (state): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        changeStatusSuccess: null,
        changeStatusError: null,
        isChangingStatus: true,
      },
    };
  }),
  on(JobsActions.changeJobStatusSuccessAction, (state): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        changeStatusSuccess: true,
        changeStatusError: null,
        isChangingStatus: false,
      },
    };
  }),
  on(JobsActions.changeJobStatusErrorAction, (state, { error }): JobsState => {
    return {
      ...state,
      selectedJob: {
        ...state.selectedJob,
        changeStatusSuccess: false,
        changeStatusError: error,
        isChangingStatus: false,
      },
    };
  }),
);
