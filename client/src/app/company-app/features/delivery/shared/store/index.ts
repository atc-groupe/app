import { JOBS_REDUCER_KEY, jobsReducer, JobsState } from './jobs';
import { ActionReducerMap } from '@ngrx/store';

export interface DeliveryState {
  [JOBS_REDUCER_KEY]: JobsState;
}

export const DELIVERY_REDUCERS: ActionReducerMap<DeliveryState> = {
  [JOBS_REDUCER_KEY]: jobsReducer,
};
