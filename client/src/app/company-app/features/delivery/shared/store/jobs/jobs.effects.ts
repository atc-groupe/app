import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { JobsService } from '../../../../../shared/services';
import { JobsListMappingService } from '../../services/jobs-list-mapping.service';
import { JobDeliveryService } from '../../services/job-delivery.service';
import {
  catchError,
  debounceTime,
  EMPTY,
  map,
  of,
  switchMap,
  withLatestFrom,
} from 'rxjs';
import { Job } from '../../../../../shared/interfaces';
import { Action, Store } from '@ngrx/store';
import * as JobsActions from './jobs.actions';
import * as JobsSelectors from './jobs.selectors';
import { UsersSelectors } from '../../../../../../shared/store/users';

@Injectable()
export class JobsEffects {
  tryFetchJobsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobsActions.tryFetchJobsAction),
      withLatestFrom(this._store.select(JobsSelectors.selectJobsCurrentDate)),
      debounceTime(300),
      switchMap(([, date]: [action: Action, date: string]) =>
        this._jobsService
          .fetchJobs({ sendingDate: date, endStatusNumber: 710 })
          .pipe(
            map((jobs: Job[]) =>
              JobsActions.fetchJobsSuccessAction({
                jobs: this._planningService.getMappedCollection(jobs),
              }),
            ),
            catchError((error) => {
              return of(
                JobsActions.fetchJobsErrorAction({
                  error: error.error.message,
                }),
              );
            }),
          ),
      ),
    ),
  );

  tryFetchSelectedJobEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobsActions.tryFetchSelectedJobAction),
      switchMap(({ jobNumber }: { jobNumber: number }) =>
        this._jobsService.fetchJob(jobNumber).pipe(
          map((job: Job) => JobsActions.fetchSelectedJobSuccessAction({ job })),
          catchError((error) =>
            of(
              JobsActions.fetchSelectedJobErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  changeStateEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(
        JobsActions.setJobsNextDateAction,
        JobsActions.setJobsPreviousDateAction,
        JobsActions.resetJobsDateAction,
        JobsActions.setJobsCurrentDateAction,
        JobsActions.updateJobDeliveryCommentSuccessAction,
      ),
      map(() => JobsActions.tryFetchJobsAction()),
    ),
  );

  tryUpdateDeliveryCommentEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobsActions.tryUpdateJobDeliveryCommentAction),
      withLatestFrom(this._store.select(JobsSelectors.selectSelectedJob)),
      switchMap(
        ([{ comment }, job]: [{ comment: string | null }, Job | null]) => {
          if (!job) {
            return EMPTY;
          }

          return this._jobDeliveryService
            .updateComment(parseInt(job.mp.number), comment)
            .pipe(
              map((job: Job) =>
                JobsActions.updateJobDeliveryCommentSuccessAction(),
              ),
              catchError((error) => {
                console.log(error);
                return of(
                  JobsActions.updateJobDeliveryCommentErrorAction({
                    error: error.error.message,
                  }),
                );
              }),
            );
        },
      ),
    ),
  );

  tryChangeJobStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobsActions.tryChangeJobStatus),
      withLatestFrom(
        this._store.select(UsersSelectors.selectAuthenticatedUser),
      ),
      switchMap(([{ jobNumber, jobStatus }, user]) => {
        if (!user) {
          return of(
            JobsActions.changeJobStatusErrorAction({
              error: `Un problème est survenu, Veuillez re-essayer. Si le problème persiste, contactez l'administrateur`,
            }),
          );
        }

        const reason = `[API] par ${user.firstName.toUpperCase()} ${user.lastName.toUpperCase()}`;

        return this._jobsService
          .changeStatus(jobNumber, jobStatus, reason)
          .pipe(
            map(() =>
              JobsActions.tryCheckJobStatusChangedAction({
                jobNumber,
                jobStatus,
              }),
            ),
            catchError((err) =>
              of(
                JobsActions.changeJobStatusErrorAction({
                  error: err.error.message,
                }),
              ),
            ),
          );
      }),
    ),
  );

  tryCheckJobStatusChangedEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(JobsActions.tryCheckJobStatusChangedAction),
      debounceTime(300),
      switchMap(({ jobNumber, jobStatus }) =>
        this._jobsService.fetchJob(parseInt(jobNumber)).pipe(
          map((job) => {
            if (job.mp.jobStatusNumber === jobStatus) {
              return JobsActions.changeJobStatusSuccessAction();
            }

            return JobsActions.tryCheckJobStatusChangedAction({
              jobNumber,
              jobStatus,
            });
          }),
        ),
      ),
    ),
  );

  constructor(
    private _store: Store,
    private _actions$: Actions,
    private _jobsService: JobsService,
    private _jobDeliveryService: JobDeliveryService,
    private _planningService: JobsListMappingService,
  ) {}
}
