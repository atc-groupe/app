import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DeliveryState } from '../index';
import { DELIVERY_REDUCER_KEY } from '../store.constants';
import { JOBS_REDUCER_KEY, JobsState } from './jobs.reducer';
import { Job } from '../../../../../shared/interfaces';
import { DeliveryPlanningItem } from '../../interfaces';

const selectDeliveryState =
  createFeatureSelector<DeliveryState>(DELIVERY_REDUCER_KEY);
const selectJobsState = createSelector(
  selectDeliveryState,
  (state: DeliveryState) => state[JOBS_REDUCER_KEY],
);

// ------ Jobs ------
export const selectJobs = createSelector(
  selectJobsState,
  (state: JobsState): DeliveryPlanningItem[] => state.jobs.list,
);
export const selectJobsCurrentDate = createSelector(
  selectJobsState,
  (state: JobsState): string => state.jobs.currentDate,
);
export const selectJobsFetchError = createSelector(
  selectJobsState,
  (state: JobsState): string | null => state.jobs.fetchError,
);
export const selectJobsIsLoading = createSelector(
  selectJobsState,
  (state: JobsState): boolean => state.jobs.isLoading,
);

// ------ Job ------
export const selectSelectedJob = createSelector(
  selectJobsState,
  (state: JobsState): Job | null => state.selectedJob.job,
);
export const selectSelectedJobFetchError = createSelector(
  selectJobsState,
  (state: JobsState): string | null => state.selectedJob.fetchError,
);
export const selectSelectedJobUpdateCommentSuccess = createSelector(
  selectJobsState,
  (state: JobsState): boolean | null => state.selectedJob.updateCommentSuccess,
);
export const selectSelectedJobUpdateCommentError = createSelector(
  selectJobsState,
  (state: JobsState): string | null => state.selectedJob.updateCommentError,
);
export const selectSelectedJobChangeStatusSuccess = createSelector(
  selectJobsState,
  (state: JobsState): boolean | null => state.selectedJob.changeStatusSuccess,
);
export const selectSelectedJobChangeStatusError = createSelector(
  selectJobsState,
  (state: JobsState): string | null => state.selectedJob.changeStatusError,
);
export const selectSelectedJobIsChangingStatus = createSelector(
  selectJobsState,
  (state: JobsState): boolean => state.selectedJob.isChangingStatus,
);
