import { createAction, props } from '@ngrx/store';
import { Job } from '../../../../../shared/interfaces';
import { DeliveryPlanningItem } from '../../interfaces';

// ------ Jobs ------
// Jobs list
export const tryFetchJobsAction = createAction(
  '[CompanyApp > delivery] try fetch jobs',
);
export const fetchJobsSuccessAction = createAction(
  '[CompanyApp > delivery] fetch jobs success',
  props<{ jobs: DeliveryPlanningItem[] }>(),
);
export const fetchJobsErrorAction = createAction(
  '[CompanyApp > delivery] fetch jobs error',
  props<{ error: string }>(),
);

// Jobs current date
export const resetJobsDateAction = createAction(
  '[CompanyApp > delivery] reset jobs date',
);
export const setJobsNextDateAction = createAction(
  '[CompanyApp > delivery] set jobs next date',
);
export const setJobsPreviousDateAction = createAction(
  '[CompanyApp > delivery] set jobs previous date',
);
export const setJobsCurrentDateAction = createAction(
  '[CompanyApp > delivery] set jobs current date',
  props<{ date: string }>(),
);

// ------ SelectedJob ------
// Selected Job
export const tryFetchSelectedJobAction = createAction(
  '[ delivery ] try fetch selectedJob',
  props<{ jobNumber: number }>(),
);
export const fetchSelectedJobSuccessAction = createAction(
  '[ delivery ] fetch selected job success',
  props<{ job: Job }>(),
);
export const fetchSelectedJobErrorAction = createAction(
  '[ delivery ] fetch selected job error',
  props<{ error: string }>(),
);

// Delivery comment
export const tryUpdateJobDeliveryCommentAction = createAction(
  '[CompanyApp > delivery] try update job delivery comment',
  props<{ comment: string | null }>(),
);
export const updateJobDeliveryCommentSuccessAction = createAction(
  '[CompanyApp > delivery] update job delivery comment success',
);
export const updateJobDeliveryCommentErrorAction = createAction(
  '[CompanyApp > delivery] update job delivery comment error',
  props<{ error: string }>(),
);

// Change job status
export const tryChangeJobStatus = createAction(
  '[CompanyApp > delivery] try change job status',
  props<{ jobNumber: string; jobStatus: number }>(),
);
export const tryCheckJobStatusChangedAction = createAction(
  '[CompanyApp > delivery] try check job status changed',
  props<{ jobNumber: string; jobStatus: number }>(),
);
export const changeJobStatusSuccessAction = createAction(
  '[CompanyApp > delivery] change job status success',
);
export const changeJobStatusErrorAction = createAction(
  '[CompanyApp > delivery] change job status error',
  props<{ error: string }>(),
);
