import { Injectable } from '@angular/core';
import { Job, SubJobDeliveryAddress } from '../../../../shared/interfaces';

@Injectable()
export class JobDeliveriesMappingService {
  getUniqueAddresses(job: Job): SubJobDeliveryAddress[] {
    const deliveries = job.deliveryAddresses;

    if (!deliveries || !deliveries.length) {
      return [];
    }

    return deliveries.reduce((acc: SubJobDeliveryAddress[], address) => {
      if (!this._alreadyContainsAddress(acc, address)) {
        acc.push(address);
      }

      return acc;
    }, []);
  }

  private _alreadyContainsAddress(
    addresses: SubJobDeliveryAddress[],
    address: SubJobDeliveryAddress,
  ): boolean {
    if (!addresses.length) {
      return false;
    }

    return addresses.some((item) => this._isIdenticalAddress(item, address));
  }

  private _isIdenticalAddress(
    address1: SubJobDeliveryAddress,
    address2: SubJobDeliveryAddress,
  ): boolean {
    return (
      address1.mp.address === address2.mp.address &&
      address1.mp.zipCode === address2.mp.zipCode &&
      address1.mp.city === address2.mp.city
      // address1.mp.country === address2.mp.country
    );
  }
}
