import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Job } from '../../../../shared/interfaces';
import { Observable } from 'rxjs';
import { environment } from '../../../../../../environments/environment';

@Injectable()
export class JobDeliveryService {
  constructor(private _http: HttpClient) {}

  updateComment(jobNumber: number, comment: string | null): Observable<Job> {
    return this._http.patch<Job>(
      `${environment.api.url}/jobs/${jobNumber}/delivery`,
      { comment: comment },
    );
  }
}
