import { Injectable } from '@angular/core';
import { Job } from '../../../../shared/interfaces';
import { DeliveryPlanningItem } from '../interfaces';

@Injectable()
export class JobsListMappingService {
  getMappedCollection(jobs: Job[]): DeliveryPlanningItem[] {
    const planningItems: DeliveryPlanningItem[] = [];

    jobs.forEach((job) => {
      const item = this._getMappedJob(job);
      if (item.deliveryMethods.length) {
        planningItems.push(item);
      }
    });

    return planningItems;
  }

  private _getMappedJob(job: Job): DeliveryPlanningItem {
    const addressZipCodes = job.meta.addressZipCodes.reduce(
      (acc, v, i, t) => (i === 0 ? v : `${acc}, ${v}`),
      '',
    );

    const deliveryMethods = job.meta.deliveryMethods.reduce(
      (acc, v, i, t) => (i === 0 ? v : `${acc}, ${v}`),
      '',
    );

    const productionTypesAsString = job.meta.productionTypes.reduce(
      (acc, v, i, t) => (i === 0 ? v : `${acc}, ${v}`),
      '',
    );

    let rawJobManager = job.mp.jobManager ? job.mp.jobManager : job.mp.creator;
    const arrayJobManager = rawJobManager.split(' ');
    arrayJobManager[0] = arrayJobManager[0][0] + '.';
    const jobManager = arrayJobManager.join(' ');

    return {
      _id: job._id,
      number: job.mp.number,
      jobManager,
      company: job.mp.company,
      status: job.mp.jobStatus,
      multiSendingDate: job.meta.deliverySendingDates.length > 1,
      printingSurface: job.meta.printingSurface,
      manufacturedPiecesCount: job.meta.manufacturedPiecesCount,
      subJobsCount: job.subJobs ? job.subJobs.length : 0,
      hasSheetProduction: job.meta.hasSheetProduction,
      hasDeliveryServiceFinishs: job.meta.deliveryServiceFinishs.length > 0,
      deliveryServiceFinishs: job.meta.deliveryServiceFinishs,
      hasNumericScoreCutting: job.meta.hasNumericScoreCutting,
      hasZundCutting: job.meta.hasZundCutting,
      hasKits: job.meta.hasKits,
      productionTypes: job.meta.productionTypes,
      productionTypesAsString,
      addressZipCodes,
      deliveryMethods,
      comment: job.delivery ? job.delivery.comment : null,
      syncStatus: job.syncStatus,
    };
  }
}
