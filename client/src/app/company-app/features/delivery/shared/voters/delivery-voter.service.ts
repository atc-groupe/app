import { Injectable } from '@angular/core';
import { AuthorizationsService } from '../../../../../shared/services/authorizations.service';
import { Action, Subject } from '../../../../shared/enums';

@Injectable({ providedIn: 'root' })
export class DeliveryVoterService {
  private _canChangeStatus = false;

  constructor(private _authService: AuthorizationsService) {
    this._authService
      .canActivate(Subject.DeliveryModule, Action.ChangeJobStatus)
      .subscribe((canActivate) => {
        this._canChangeStatus = canActivate;
      });
  }

  get canChangeJobStatus(): boolean {
    return this._canChangeStatus;
  }
}
