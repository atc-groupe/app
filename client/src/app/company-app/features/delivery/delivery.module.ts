// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Component
import { DeliveryComponent } from './delivery.component';
import { PlanningContainerComponent } from './views/planning-container/planning-container.component';

// Routes
import { DELIVERY_ROUTES } from './delivery.routes';

// Store
import { DELIVERY_REDUCER_KEY } from './shared/store/store.constants';
import { DELIVERY_REDUCERS } from './shared/store';
import { PlanningComponent } from './views/planning-container/planning/planning.component';
import { PlanningHeaderComponent } from './views/planning-container/planning-header/planning-header.component';
import { PlanningMessageComponent } from './views/planning-container/planning-message/planning-message.component';
import { LayoutModule } from '../../shared/modules/layout.module';
import { JobsEffects } from './shared/store/jobs/jobs.effects';
import { JobDeliveriesMappingService } from './shared/services/job-deliveries-mapping.service';
import { JobDeliveryService } from './shared/services/job-delivery.service';
import { JobsListMappingService } from './shared/services/jobs-list-mapping.service';
import { JobCommentComponent } from './views/planning-container/job-comment/job-comment.component';
import { FormsModule } from '@angular/forms';
import { JobResumeComponent } from './views/planning-container/job-resume/job-resume.component';
import { PlanningPrintComponent } from './views/planning-container/planning-print/planning-print.component';
import { JobFinishsResumeComponent } from './views/planning-container/job-finishs-resume/job-finishs-resume.component';
import { JobStatusComponent } from './views/planning-container/job-status/job-status.component';
import { ColorDirective } from '../../shared/directives/color.directive';

@NgModule({
  declarations: [
    DeliveryComponent,
    PlanningContainerComponent,
    PlanningComponent,
    PlanningHeaderComponent,
    PlanningMessageComponent,
    JobCommentComponent,
    JobResumeComponent,
    PlanningPrintComponent,
    JobFinishsResumeComponent,
    JobStatusComponent,
  ],
  providers: [
    JobDeliveriesMappingService,
    JobDeliveryService,
    JobsListMappingService,
  ],
  imports: [
    CoreModule,
    LayoutModule,
    FormsModule,
    RouterModule.forChild(DELIVERY_ROUTES),
    StoreModule.forFeature(DELIVERY_REDUCER_KEY, DELIVERY_REDUCERS),
    EffectsModule.forFeature([JobsEffects]),
    ColorDirective,
  ],
})
export class DeliveryModule {}
