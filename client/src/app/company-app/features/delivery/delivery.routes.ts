import { Routes } from '@angular/router';
import { DeliveryComponent } from './delivery.component';
import { PlanningContainerComponent } from './views/planning-container/planning-container.component';

export const DELIVERY_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'planning',
  },
  {
    path: '',
    component: DeliveryComponent,
    children: [{ path: 'planning', component: PlanningContainerComponent }],
  },
];
