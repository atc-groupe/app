import { ModuleAuthorizations } from '../../../shared/interfaces';
import { Action, Module, Subject } from '../../shared/enums';

export const DELIVERY_AUTHORIZATIONS: ModuleAuthorizations = {
  module: Module.Delivery,
  label: 'Expéditions',
  subjectsAuthorizations: [
    {
      label: 'Statut du job',
      authorizations: [
        {
          subject: Subject.DeliveryModule,
          action: Action.ChangeJobStatus,
          label:
            "Changement du statut d'un job depuis le planning d'expéditions",
        },
      ],
    },
  ],
};
