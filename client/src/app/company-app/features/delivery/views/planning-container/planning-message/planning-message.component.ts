import { Component, Input } from '@angular/core';

@Component({
  selector: 'company-app-delivery-planning-message',
  templateUrl: './planning-message.component.html',
  styleUrls: ['./planning-message.component.scss'],
})
export class PlanningMessageComponent {
  @Input() isLoading: boolean | null = false;
  @Input() loadingError: string | null = null;
}
