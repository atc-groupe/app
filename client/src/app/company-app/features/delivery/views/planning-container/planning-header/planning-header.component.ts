import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'company-app-delivery-planning-header',
  templateUrl: './planning-header.component.html',
  styleUrls: ['./planning-header.component.scss'],
})
export class PlanningHeaderComponent {
  @Input() currentDate: string | null = null;
  @Output() nextDate = new EventEmitter<void>();
  @Output() resetDate = new EventEmitter<void>();
  @Output() previousDate = new EventEmitter<void>();
  @Output() filter = new EventEmitter<string>();
  @Output() print = new EventEmitter<void>();

  emitNextDate(): void {
    this.nextDate.emit();
  }

  emitResetDate(): void {
    this.resetDate.emit();
  }

  emitPreviousDate(): void {
    this.previousDate.emit();
  }

  emitPrint(): void {
    this.print.emit();
  }

  applyFilter(event: Event): void {
    const filterInput = event.target as HTMLInputElement;

    this.filter.emit(filterInput.value.toLowerCase());
  }
}
