import { Component } from '@angular/core';
import { JobsSelectors } from '../../../shared/store/jobs';
import { SubJobDeliveryAddress } from '../../../../../shared/interfaces';
import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';
import { JobDeliveriesMappingService } from '../../../shared/services/job-deliveries-mapping.service';

@Component({
  selector: 'company-app-delivery-job-resume',
  templateUrl: './job-resume.component.html',
  styleUrls: ['./job-resume.component.scss'],
})
export class JobResumeComponent {
  public selectedJob$ = this._store.select(JobsSelectors.selectSelectedJob);
  public deliveryAddresses: SubJobDeliveryAddress[] = [];
  private _destroyed = new Subject<void>();

  constructor(
    private _store: Store,
    private _jobDeliveriesMappingService: JobDeliveriesMappingService,
  ) {
    this.selectedJob$.pipe(takeUntil(this._destroyed)).subscribe((job) => {
      if (job) {
        this.deliveryAddresses =
          this._jobDeliveriesMappingService.getUniqueAddresses(job);
      }
    });
  }

  ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
