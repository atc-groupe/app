import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  SystemActions,
  SystemSelectors,
} from '../../../../../../shared/store/system';
import { JobsSelectors, JobsActions } from '../../../shared/store/jobs';
import { FormBuilder, Validators } from '@angular/forms';
import { first, Subscription } from 'rxjs';
import { Job } from '../../../../../shared/interfaces';
import { DialogRef } from '@angular/cdk/dialog';

@Component({
  selector: 'company-app-delivery-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
})
export class JobStatusComponent implements OnDestroy {
  public selectedJob: Job | null = null;
  public jobStatus$ = this._store.select(SystemSelectors.selectJobStatusList);
  public error$ = this._store.select(
    JobsSelectors.selectSelectedJobChangeStatusError,
  );
  public form = this._fb.group({
    status: [0, Validators.required],
  });
  public isUpdating = false;

  private _subscription = new Subscription();
  private _success$ = this._store.select(
    JobsSelectors.selectSelectedJobChangeStatusSuccess,
  );
  private _selectedJob$ = this._store.select(JobsSelectors.selectSelectedJob);

  constructor(
    private _store: Store,
    private _fb: FormBuilder,
    private _dialogRef: DialogRef<JobStatusComponent>,
  ) {
    this._store.dispatch(SystemActions.tryFetchJobStatus());
    this._selectedJob$.pipe(first((v) => v !== null)).subscribe((job) => {
      this.selectedJob = job;
      this.form.get('status')?.setValue(job!.mp.jobStatusNumber);
    });
    this._subscription.add(
      this._store
        .select(JobsSelectors.selectSelectedJobIsChangingStatus)
        .subscribe((isUpdating) => {
          this.isUpdating = isUpdating;
        }),
    );
  }

  changeJobStatus() {
    const status = this.form.get('status')?.value;
    if (!status || !this.selectedJob) {
      return;
    }

    this._store.dispatch(
      JobsActions.tryChangeJobStatus({
        jobNumber: this.selectedJob!.mp.number,
        jobStatus: status,
      }),
    );

    this._success$.pipe(first((v) => v !== null)).subscribe((result) => {
      if (result === true) {
        this._dialogRef.close();
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
