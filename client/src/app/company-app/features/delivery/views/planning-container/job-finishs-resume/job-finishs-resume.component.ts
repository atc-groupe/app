import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobsSelectors } from '../../../shared/store/jobs';

@Component({
  selector: 'app-jobs-finish-resume',
  templateUrl: './job-finishs-resume.component.html',
  styleUrls: ['./job-finishs-resume.component.scss'],
})
export class JobFinishsResumeComponent {
  public selectedJob$ = this._store.select(JobsSelectors.selectSelectedJob);

  constructor(private _store: Store) {}
}
