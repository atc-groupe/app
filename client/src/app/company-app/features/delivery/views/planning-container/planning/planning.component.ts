import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DeliveryPlanningItem } from '../../../shared/interfaces';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';
import {
  MetaJobsStatus,
  MetaJobsStatusColors,
} from '../../../../../shared/interfaces';
import { JobSyncStatusType } from '../../../../../shared/types/job-sync-status.type';

@Component({
  selector: 'company-app-delivery-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
})
export class PlanningComponent implements AfterViewInit, OnDestroy {
  @Input() dataSource!: MatTableDataSource<DeliveryPlanningItem>;
  @Input() metaStatusMap: Map<string, MetaJobsStatus> | null = null;
  @Input() canChangeJobStatus = false;

  @Output() viewJobResume = new EventEmitter<number>();
  @Output() viewJobFinishResume = new EventEmitter<number>();
  @Output() setJobComment = new EventEmitter<number>();
  @Output() viewJob = new EventEmitter<number>();
  @Output() changeJobStatus = new EventEmitter<number>();

  @ViewChild(MatSort) sort!: MatSort;

  private _destroyed = new Subject<void>();

  public displayedColumns: string[] = [
    'number',
    'company',
    'status',
    'productionTypes',
    'printingSurface',
    'manufacturedPiecesCount',
    'subJobsCount',
    'hasSheetProduction',
    'hasDeliveryServiceFinishs',
    'hasNumericScoreCutting',
    'hasZundCutting',
    'hasKits',
    'multiSendingDate',
    'addressZipCodes',
    'deliveryMethods',
    'comment',
    'sync',
    'actions',
  ];

  constructor(_breakpointObserver: BreakpointObserver) {
    const layoutChanges = _breakpointObserver.observe(['(max-width: 1950px)']);

    layoutChanges.pipe(takeUntil(this._destroyed)).subscribe((result) => {
      this._displayColumns(result.matches);
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  selectJobResume(jobNumber: number, $event: MouseEvent): void {
    this.viewJobResume.emit(jobNumber);
  }

  selectJobFinishResume(jobNumber: number, hasFinishing: boolean): void {
    if (hasFinishing) {
      this.viewJobFinishResume.emit(jobNumber);
    }
  }

  selectJobComment(jobNumber: number, $event: MouseEvent): void {
    this.setJobComment.emit(jobNumber);
  }

  selectViewJob(jobNumber: number): void {
    this.viewJob.emit(jobNumber);
  }

  selectChangeJobStatus(jobNumber: number): void {
    if (this.canChangeJobStatus) {
      this.changeJobStatus.emit(jobNumber);
    }
  }

  getStatusColors(statusName: string): MetaJobsStatusColors {
    const defaultColors = { color: '#ddd', background: '#666' };
    if (!this.metaStatusMap) {
      return defaultColors;
    }
    const status = this.metaStatusMap.get(statusName);
    return status
      ? { color: status.textColor, background: status.color }
      : defaultColors;
  }

  getStatusLabel(statusName: string): string {
    if (!this.metaStatusMap) {
      return statusName;
    }

    const status = this.metaStatusMap.get(statusName);
    return status ? status.label : statusName;
  }

  hasSyncError(syncStatus: JobSyncStatusType): boolean {
    return syncStatus === 'error';
  }

  _displayColumns(isSmallSize: boolean): void {
    const companyExists = this.displayedColumns.includes('company');
    if (!isSmallSize && !companyExists) {
      this.displayedColumns.splice(1, 0, 'company');
      return;
    }

    if (isSmallSize && companyExists) {
      this.displayedColumns.splice(1, 1);
    }
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
