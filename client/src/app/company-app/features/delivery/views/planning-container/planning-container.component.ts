import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobsSelectors, JobsActions } from '../../shared/store/jobs';
import { UiActions } from '../../../../shared/store/ui';
import { DeliveryPlanningItem } from '../../shared/interfaces';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { JobCommentComponent } from './job-comment/job-comment.component';
import { JobResumeComponent } from './job-resume/job-resume.component';
import { PlanningPrintComponent } from './planning-print/planning-print.component';
import { JobFinishsResumeComponent } from './job-finishs-resume/job-finishs-resume.component';
import { JobStatusComponent } from './job-status/job-status.component';
import { Router } from '@angular/router';
import { MetaJobsStatusSelectors } from '../../../../shared/store/meta-jobs-status';
import { DeliveryVoterService } from '../../shared/voters/delivery-voter.service';

@Component({
  selector: 'company-app-delivery-planning-container',
  templateUrl: './planning-container.component.html',
  styleUrls: ['./planning-container.component.scss'],
})
export class PlanningContainerComponent {
  private _subscription = new Subscription();
  public planningItems$ = this._store.select(JobsSelectors.selectJobs);

  public dataSource = new MatTableDataSource<DeliveryPlanningItem>([]);
  public currentDate$ = this._store.select(JobsSelectors.selectJobsCurrentDate);
  public isLoading$ = this._store.select(JobsSelectors.selectJobsIsLoading);
  public loadingError$ = this._store.select(JobsSelectors.selectJobsFetchError);
  public metaJobsStatusMap$ = this._store.select(
    MetaJobsStatusSelectors.selectStatusMap,
  );
  public printWindowOpen = false;
  public canChangeJobStatus = false;

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _router: Router,
    private _voter: DeliveryVoterService,
  ) {
    this._store.dispatch(JobsActions.tryFetchJobsAction());
    this._subscription.add(
      this.planningItems$.subscribe((items) => {
        this.dataSource.data = items;
      }),
    );
    this._subscription.add(
      this._store
        .select(JobsSelectors.selectSelectedJobChangeStatusSuccess)
        .subscribe((success) => {
          if (success === true) {
            this._store.dispatch(JobsActions.tryFetchJobsAction());
          }
        }),
    );
    this.canChangeJobStatus = this._voter.canChangeJobStatus;
  }

  setPreviousDate() {
    this._store.dispatch(JobsActions.setJobsPreviousDateAction());
  }

  setNextDate() {
    this._store.dispatch(JobsActions.setJobsNextDateAction());
  }

  resetDate() {
    this._store.dispatch(JobsActions.resetJobsDateAction());
  }

  applyFilter(value: string) {
    this.dataSource.filter = value;
  }

  openJobComment(jobNumber: number): void {
    this._store.dispatch(JobsActions.tryFetchSelectedJobAction({ jobNumber }));
    this._dialog.open(JobCommentComponent, {
      width: '100%',
      maxWidth: '50rem',
    });
  }

  openJobStatus(jobNumber: number): void {
    if (!this.canChangeJobStatus) {
      return;
    }

    this._store.dispatch(JobsActions.tryFetchSelectedJobAction({ jobNumber }));
    this._dialog.open(JobStatusComponent, {
      width: '100%',
      maxWidth: '50rem',
    });
  }

  openJobResume(jobNumber: number): void {
    this._store.dispatch(JobsActions.tryFetchSelectedJobAction({ jobNumber }));
    this._dialog.open(JobResumeComponent, {
      minWidth: '70rem',
      maxWidth: 'calc(100vh - 3rem)',
    });
  }

  openJobFinishResume(jobNumber: number): void {
    this._store.dispatch(JobsActions.tryFetchSelectedJobAction({ jobNumber }));
    this._dialog.open(JobFinishsResumeComponent, {
      minWidth: '60rem',
      maxWidth: 'calc(100vh - 3rem)',
    });
  }

  openPrintPlanning(): void {
    this.printWindowOpen = true;
    this._dialog
      .open(PlanningPrintComponent, {
        minWidth: '420mm',
        minHeight: '297mm',
        position: { top: '0' },
      })
      .afterClosed()
      .subscribe(() => {
        this.printWindowOpen = false;
      });
  }

  viewJob(jobNumber: number) {
    this._store.dispatch(
      UiActions.setJobViewReturnRoute({ route: '/company/delivery/planning' }),
    );
    this._router.navigateByUrl(`/company/jobs/view/${jobNumber}/deliveries`);
  }
}
