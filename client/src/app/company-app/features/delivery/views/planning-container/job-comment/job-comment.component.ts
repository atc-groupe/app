import { Component, OnDestroy } from '@angular/core';
import { JobsSelectors, JobsActions } from '../../../shared/store/jobs';
import { DialogRef } from '@angular/cdk/dialog';
import { Store } from '@ngrx/store';
import { first } from 'rxjs';

@Component({
  selector: 'company-app-delivery-planning-comment',
  templateUrl: './job-comment.component.html',
  styleUrls: ['./job-comment.component.scss'],
})
export class JobCommentComponent {
  public selectedJob$ = this._store.select(JobsSelectors.selectSelectedJob);
  public error$ = this._store.select(
    JobsSelectors.selectSelectedJobUpdateCommentError,
  );
  public commentUpdateSuccess$ = this._store.select(
    JobsSelectors.selectSelectedJobUpdateCommentSuccess,
  );
  public comment: string | null = null;

  constructor(
    private _store: Store,
    private _dialogRef: DialogRef<JobCommentComponent>,
  ) {
    this.selectedJob$.pipe(first((v) => v !== null)).subscribe((job) => {
      if (job) {
        this.comment = job.delivery ? job.delivery.comment : null;
      }
    });
  }

  updateComment() {
    const comment = this.comment === '' ? null : this.comment;
    this._store.dispatch(
      JobsActions.tryUpdateJobDeliveryCommentAction({ comment }),
    );
    this.commentUpdateSuccess$
      .pipe(first((value) => value === true || value === false))
      .subscribe((result: boolean | null): void => {
        if (result) {
          this._dialogRef.close();
        }
      });
  }
}
