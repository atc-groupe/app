import {
  Component,
  ElementRef,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { JobsSelectors } from '../../../shared/store/jobs';
import { Store } from '@ngrx/store';
import { DialogRef } from '@angular/cdk/dialog';

@Component({
  selector: 'company-app-delivery-planning-print',
  templateUrl: './planning-print.component.html',
  styleUrls: ['./planning-print.component.scss'],
})
export class PlanningPrintComponent {
  public planningItems$ = this._store.select(JobsSelectors.selectJobs);
  public planningDate$ = this._store.select(
    JobsSelectors.selectJobsCurrentDate,
  );
  public today = new Date();

  @ViewChild('bis') public bis!: ElementRef<HTMLDivElement>;
  @ViewChildren('lines') public lines!: QueryList<
    ElementRef<HTMLTableRowElement>
  >;

  constructor(private _store: Store, private _dialogRef: DialogRef) {}

  closeDialog() {
    this._dialogRef.close();
  }
}
