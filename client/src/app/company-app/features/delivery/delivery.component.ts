import { Component } from '@angular/core';

@Component({
  selector: 'company-app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss'],
})
export class DeliveryComponent {}
