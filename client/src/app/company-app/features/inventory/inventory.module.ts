// Modules
import { NgModule } from '@angular/core';
import { CoreModule } from '../../../shared/modules/core.module';
import { RouterModule } from '@angular/router';

// Components
import { InventoryComponent } from './inventory.component';

// Routes
import { INVENTORY_ROUTES } from './inventory.routes';

@NgModule({
  declarations: [InventoryComponent],
  imports: [CoreModule, RouterModule.forChild(INVENTORY_ROUTES)],
})
export class InventoryModule {}
