import { Component } from '@angular/core';

@Component({
  selector: 'company-app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss'],
})
export class InventoryComponent {}
