export type JobSyncStatusType = 'success' | 'pending' | 'error';
