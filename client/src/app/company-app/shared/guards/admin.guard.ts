import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersSelectors } from '../../../shared/store/users';
import { first, map } from 'rxjs';
import { AppUser } from '../../../shared/interfaces';
import { UserRole } from '../../../shared/enums';

export const adminGuard = () => {
  return inject(Store)
    .select(UsersSelectors.selectAuthenticatedUser)
    .pipe(
      first((user: AppUser | null) => user !== null),
      map((user: AppUser | null) => {
        return (
          user?.role.level === UserRole.SuperAdmin ||
          user?.role.level === UserRole.Admin
        );
      }),
    );
};
