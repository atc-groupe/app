import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownItem, DropdownItems } from '../../interfaces';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ClickOutsideDirective } from '../../directives/click-outside.directive';

@Component({
  selector: 'app-dropdown',
  standalone: true,
  imports: [CommonModule, ClickOutsideDirective],
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit, OnDestroy {
  @Input() items: BehaviorSubject<DropdownItems> | DropdownItems | null = [];
  @Input() label: string = '';
  @Input() mode: 'select' | 'action' = 'select';
  @Input() direction: 'left' | 'right' = 'right';
  @Input() search: boolean = false;
  @Input() selected: any = null;
  @Input() class: string = '';
  @Output() select = new EventEmitter<any>();
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;

  public icon = 'expand_more';
  public expanded = false;
  public filter = '';
  public list$ = new BehaviorSubject<DropdownItems>([]);
  public selectedItem: DropdownItem | null = null;
  private _subscription = new Subscription();

  ngOnInit() {
    if (!this.items) {
      return;
    }

    if (this.items instanceof BehaviorSubject) {
      this.list$ = this.items;
      this._subscription = this.list$.subscribe(() => {
        this.selected = null;
        this._initializeSelectedItem();
      });
    } else {
      this.list$.next(this.items);
    }

    this._initializeSelectedItem();
  }

  toggle() {
    this.expanded = !this.expanded;

    if (this.search && this.expanded) {
      this.searchInput.nativeElement.focus();
    }
  }

  selectItem(item: DropdownItem) {
    this.expanded = false;
    this.selectedItem = item;
    this.select.emit(item.value);
  }

  isSelected(value: any) {
    if (this.mode === 'action') {
      return false;
    }

    return value === this.selected;
  }

  isLeft() {
    return this.direction === 'left';
  }

  isRight() {
    return this.direction === 'right';
  }

  clickOutside() {
    this.expanded = false;
  }

  filterList() {
    if (!this.items) {
      return;
    }

    const value = this.searchInput.nativeElement.value.toLowerCase().trim();
    const items = this._getItems();

    if (value === '') {
      this.list$.next(items);
      return;
    }

    const list = items.filter((item) =>
      item.label.trim().toLowerCase().includes(value),
    );
    this.list$.next(list);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _getItems(): DropdownItems {
    if (!this.items) {
      return [];
    }

    return this.items instanceof BehaviorSubject
      ? this.items.value
      : this.items;
  }

  private _initializeSelectedItem(): void {
    const items = this._getItems();
    if (this.selected) {
      const selectedItem = items.find((item) => item.value === this.selected);
      if (selectedItem) {
        this.selectedItem = selectedItem;
      }
    } else {
      this.selectedItem = items[0];
    }
  }
}
