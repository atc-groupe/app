import { Component, forwardRef, Input } from '@angular/core';
import {
  ControlValueAccessor,
  FormsModule,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
  standalone: true,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ColorPickerComponent),
    },
  ],
  imports: [NgClass, FormsModule],
})
export class ColorPickerComponent implements ControlValueAccessor {
  @Input() id: string | null = null;
  @Input() class: string | null = null;

  public innerValue: string = '';
  public isDisabled = false;
  private onChange!: any;
  private onTouched!: any;

  updateValue() {
    if (this.isDisabled) {
      return;
    }

    this.onChange(this.innerValue);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: any): void {
    this.innerValue = value;
  }
}
