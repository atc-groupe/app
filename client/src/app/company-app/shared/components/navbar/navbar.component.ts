import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CompanyUser } from '../../../../shared/interfaces';
import { UserRole } from '../../../../shared/enums';

@Component({
  selector: 'company-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input() user!: CompanyUser | null;
  @Output() logOut = new EventEmitter<void>();

  public isAdmin: boolean = false;

  ngOnInit(): void {
    this.isAdmin =
      this.user?.role.level === UserRole.SuperAdmin ||
      this.user?.role.level === UserRole.Admin;
  }

  get userDisplayName(): string {
    return `${this.user!.firstName} ${this.user!.lastName}`;
  }

  public onLogOut() {
    this.logOut.emit();
  }
}
