import { Component, Input, OnInit } from '@angular/core';
import { Nav, NavItem, NavSection } from '../../interfaces';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs';

@Component({
  selector: 'company-app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  @Input() public nav!: Nav;

  constructor(private _route: ActivatedRoute) {}

  ngOnInit(): void {
    this._route.firstChild?.url
      .pipe(first((url) => url.length > 0))
      .subscribe((url) => {
        this._resetSections();
        this.nav.sections.forEach((section) => {
          if (section.route && section.route === url[0].path) {
            section.active = true;
            return;
          }

          section.items.forEach((item) => {
            if (item.route === url[0].path) {
              section.active = true;
            }
          });
        });
      });
  }

  toggleSection(section: NavSection) {
    section.expanded = !section.expanded;
  }

  selectRoute(section: NavSection, item: NavItem) {
    if (item.disabled) {
      return;
    }

    this._resetSections();
    section.active = true;
  }

  getItemRoute(section: NavSection, item: NavItem): string | string[] | null {
    if (item.disabled) {
      return null;
    }

    return section.route ? [section.route, item.route] : item.route;
  }

  private _resetSections() {
    this.nav.sections.forEach((section) => (section.active = false));
  }
}
