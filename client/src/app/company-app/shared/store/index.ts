import { UI_REDUCER_KEY, uiReducer, UiState } from './ui';
import { ActionReducerMap } from '@ngrx/store';
import {
  META_JOBS_STATUS_REDUCER_KEY,
  metaJobsStatusReducer,
  MetaJobsStatusState,
} from './meta-jobs-status';
import { STOCK_REDUCER_KEY, stockReducer, StockState } from './stock';

export interface CompanyAppState {
  [META_JOBS_STATUS_REDUCER_KEY]: MetaJobsStatusState;
  [STOCK_REDUCER_KEY]: StockState;
  [UI_REDUCER_KEY]: UiState;
}

export const COMPANY_APP_REDUCERS: ActionReducerMap<CompanyAppState> = {
  [META_JOBS_STATUS_REDUCER_KEY]: metaJobsStatusReducer,
  [STOCK_REDUCER_KEY]: stockReducer,
  [UI_REDUCER_KEY]: uiReducer,
};
