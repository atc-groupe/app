import { createAction, props } from '@ngrx/store';
import { ProductType } from '../../enums';
import { Product, ProductList } from '../../interfaces';
import { StockProductListParamsDto } from '../../dto';

// ------ Product list ------
export const tryFetchProductListAction = createAction(
  '[CustomerApp > Jobs > stock] try fetch product list',
  props<{ productType: ProductType; params?: StockProductListParamsDto }>(),
);
export const trySearchProductListAction = createAction(
  '[CustomerApp > Jobs > stock] try search product list',
  props<{ productType: ProductType; params?: StockProductListParamsDto }>(),
);

export const fetchProductListSuccessAction = createAction(
  '[CustomerApp > Jobs > stock] fetch product list success',
  props<{ list: ProductList }>(),
);

// ------ Product details ------
export const tryFetchProductAction = createAction(
  '[CustomerApp > Jobs > stock] try fetch product',
  props<{ productType: ProductType; productId: number }>(),
);
export const fetchProductSuccessAction = createAction(
  '[CustomerApp > Jobs > stock] fetch product success',
  props<{ product: Product }>(),
);

// ------ Products Details ------
export const tryFetchProductsAction = createAction(
  '[CustomerApp > Jobs > stock] try fetch products',
  props<{ productType: ProductType; productIds: number[] }>(),
);
export const tryFetchProductsFromProductIdAction = createAction(
  '[CustomerApp > Jobs > stock] try fetch products from product id',
  props<{ productType: ProductType; productId: number }>(),
);
export const fetchProductsSuccessAction = createAction(
  '[CustomerApp > Jobs > stock] fetch products success',
  props<{ products: Product[] }>(),
);

// ------ Fetch error ------
export const fetchListErrorAction = createAction(
  '[CustomerApp > Jobs > stock] fetch list error',
  props<{ error: string }>(),
);
export const fetchProductErrorAction = createAction(
  '[CustomerApp > Jobs > stock] fetch product error',
  props<{ error: string }>(),
);
export const fetchProductsErrorAction = createAction(
  '[CustomerApp > Jobs > stock] fetch products error',
  props<{ error: string }>(),
);

// ------ Is Loading ------
export const changeIsLoading = createAction(
  '[CustomerApp > Jobs > stock] change is loading',
  props<{ value: boolean }>(),
);
