import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { StockService } from '../../services/stock.service';
import * as StockActions from './stock.actions';
import { catchError, debounceTime, map, of, switchMap } from 'rxjs';

@Injectable()
export class StockEffects {
  tryFetchProductListEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StockActions.tryFetchProductListAction),
      switchMap(({ productType, params }) =>
        this._stockService.fetchProductList(productType, params).pipe(
          map((list) => StockActions.fetchProductListSuccessAction({ list })),
          catchError((err) =>
            of(StockActions.fetchListErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  trySearchProductListEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StockActions.trySearchProductListAction),
      debounceTime(500),
      switchMap(({ productType, params }) =>
        this._stockService.fetchProductList(productType, params).pipe(
          map((list) => StockActions.fetchProductListSuccessAction({ list })),
          catchError((err) =>
            of(StockActions.fetchListErrorAction({ error: err.error.message })),
          ),
        ),
      ),
    ),
  );

  tryFetchProductEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StockActions.tryFetchProductAction),
      switchMap(({ productType, productId }) =>
        this._stockService.fetchProductDetails(productType, productId).pipe(
          map((product) => StockActions.fetchProductSuccessAction({ product })),
          catchError((err) =>
            of(
              StockActions.fetchProductErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryFetchProductsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StockActions.tryFetchProductsAction),
      switchMap(({ productType, productIds }) =>
        this._stockService.fetchProductsDetails(productType, productIds).pipe(
          map((products) =>
            StockActions.fetchProductsSuccessAction({ products }),
          ),
          catchError((err) =>
            of(
              StockActions.fetchProductsErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryFetchProductsFromProductIdEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StockActions.tryFetchProductsFromProductIdAction),
      debounceTime(300),
      switchMap(({ productType, productId }) =>
        this._stockService
          .fetchProductsDetailsFromProductId(productType, productId)
          .pipe(
            map((products) =>
              StockActions.fetchProductsSuccessAction({ products }),
            ),
            catchError((err) =>
              of(
                StockActions.fetchListErrorAction({ error: err.error.message }),
              ),
            ),
          ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _stockService: StockService,
  ) {}
}
