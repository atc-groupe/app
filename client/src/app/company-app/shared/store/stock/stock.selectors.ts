import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CompanyAppState } from '../index';
import { STOCK_REDUCER_KEY, StockState } from './stock.reducer';
import { Product, ProductList } from '../../interfaces';

const selectCompanyAppState =
  createFeatureSelector<CompanyAppState>('CompanyApp');
const selectStockState = createSelector(
  selectCompanyAppState,
  (state): StockState => state[STOCK_REDUCER_KEY],
);

export const selectList = createSelector(
  selectStockState,
  (state): ProductList | null => state.list,
);
export const selectProduct = createSelector(
  selectStockState,
  (state): Product | null => state.selectedProduct,
);
export const selectProducts = createSelector(
  selectStockState,
  (state): Product[] | null => state.selectedProducts,
);
export const selectFetchListError = createSelector(
  selectStockState,
  (state): string | null => state.fetchListError,
);
export const selectFetchProductError = createSelector(
  selectStockState,
  (state): string | null => state.fetchProductError,
);
export const selectFetchProductsError = createSelector(
  selectStockState,
  (state): string | null => state.fetchProductsError,
);
export const selectIsLoading = createSelector(
  selectStockState,
  (state): boolean => state.isLoading,
);
