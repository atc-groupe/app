import { Product, ProductList } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import * as StockActions from './stock.actions';

export const STOCK_REDUCER_KEY = 'stock';

export interface StockState {
  list: ProductList | null;
  selectedProduct: Product | null;
  selectedProducts: Product[] | null; // same product name but different formats and thickness
  isLoading: boolean;
  fetchListError: string | null;
  fetchProductError: string | null;
  fetchProductsError: string | null;
}

const INITIAL_STATE: StockState = {
  list: null,
  selectedProduct: null,
  selectedProducts: null,
  isLoading: false,
  fetchListError: null,
  fetchProductError: null,
  fetchProductsError: null,
};

export const stockReducer = createReducer(
  INITIAL_STATE,
  on(
    StockActions.tryFetchProductAction,
    StockActions.trySearchProductListAction,
    (state): StockState => {
      return {
        ...state,
        selectedProduct: null,
        fetchProductError: null,
        isLoading: true,
      };
    },
  ),
  on(
    StockActions.fetchProductListSuccessAction,
    (state, { list }): StockState => {
      return {
        ...state,
        list,
        selectedProduct: null,
        selectedProducts: null,
        fetchListError: null,
        isLoading: false,
      };
    },
  ),
  on(StockActions.tryFetchProductsFromProductIdAction, (state): StockState => {
    return {
      ...state,
      selectedProducts: null,
      fetchProductsError: null,
      isLoading: false,
    };
  }),
  on(
    StockActions.fetchProductSuccessAction,
    (state, { product }): StockState => {
      return {
        ...state,
        selectedProduct: product,
        fetchProductError: null,
        isLoading: false,
      };
    },
  ),
  on(
    StockActions.fetchProductsSuccessAction,
    (state, { products }): StockState => {
      return {
        ...state,
        selectedProducts: products,
        fetchProductsError: null,
        isLoading: false,
      };
    },
  ),
  on(StockActions.fetchListErrorAction, (state, { error }): StockState => {
    return {
      ...state,
      fetchListError: error,
      isLoading: false,
    };
  }),
  on(StockActions.fetchProductErrorAction, (state, { error }): StockState => {
    return {
      ...state,
      fetchProductError: error,
      isLoading: false,
    };
  }),
  on(StockActions.fetchProductsErrorAction, (state, { error }): StockState => {
    return {
      ...state,
      fetchProductsError: error,
      isLoading: false,
    };
  }),
  on(StockActions.changeIsLoading, (state, { value }): StockState => {
    return {
      ...state,
      isLoading: value,
    };
  }),
);
