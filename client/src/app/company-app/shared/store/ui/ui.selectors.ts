import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UI_REDUCER_KEY, UiState } from './ui.reducer';
import { CompanyAppState } from '../index';

const selectCompanyAppState =
  createFeatureSelector<CompanyAppState>('CompanyApp');
const selectUiState = createSelector(
  selectCompanyAppState,
  (state): UiState => state[UI_REDUCER_KEY],
);
export const selectJobViewReturnRoute = createSelector(
  selectUiState,
  (state): string | null => state.jobViewReturnRoute,
);
