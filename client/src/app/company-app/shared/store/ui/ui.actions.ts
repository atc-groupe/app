import { createAction, props } from '@ngrx/store';

export const setJobViewReturnRoute = createAction(
  '[CompanyApp > ui] set job view return route',
  props<{ route: string }>(),
);
