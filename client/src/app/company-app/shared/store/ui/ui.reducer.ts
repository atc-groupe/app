import { createReducer, on } from '@ngrx/store';
import * as UiActions from './ui.actions';

export const UI_REDUCER_KEY = 'ui';

export interface UiState {
  jobViewReturnRoute: string | null;
}

const INITIAL_STATE: UiState = {
  jobViewReturnRoute: '/company/jobs/planning',
};

export const uiReducer = createReducer(
  INITIAL_STATE,
  on(UiActions.setJobViewReturnRoute, (state, { route }): UiState => {
    return {
      ...state,
      jobViewReturnRoute: route,
    };
  }),
);
