import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MetaJobsStatusService } from '../../services/meta-jobs-status.service';
import * as StatusActions from './meta-jobs-status.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable()
export class MetaJobsStatusEffects {
  tryFetchMetaJobsStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StatusActions.tryFetchStatusListAction),
      switchMap(() =>
        this._statusService.findAll().pipe(
          map((status) =>
            StatusActions.fetchStatusListSuccessAction({ status }),
          ),
          catchError((err) =>
            of(
              StatusActions.fetchStatusListErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  trySyncFromMultiPressEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StatusActions.trySyncFromMultiPress),
      switchMap(() =>
        this._statusService.syncFromMultiPress().pipe(
          map((status) => {
            this._store.dispatch(StatusActions.handleStatusSuccessAction());
            return StatusActions.tryFetchStatusListAction();
          }),
          catchError((err) =>
            of(
              StatusActions.handleStatusErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  tryUpdateMetaJobsStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(StatusActions.tryUpdateStatusAction),
      switchMap(({ id, status }) =>
        this._statusService.updateOne(id, status).pipe(
          map((status) => StatusActions.handleStatusSuccessAction()),
          catchError((err) =>
            of(
              StatusActions.handleStatusErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _store: Store,
    private _statusService: MetaJobsStatusService,
  ) {}
}
