import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CompanyAppState } from '../index';
import {
  META_JOBS_STATUS_REDUCER_KEY,
  MetaJobsStatusState,
} from './meta-jobs-status.reducer';
import { MetaJobsStatus, MetaJobsStatusColors } from '../../interfaces';

const selectCompanyAppState =
  createFeatureSelector<CompanyAppState>('CompanyApp');
const selectMetaJobsStatusState = createSelector(
  selectCompanyAppState,
  (state): MetaJobsStatusState => state[META_JOBS_STATUS_REDUCER_KEY],
);

export const selectStatus = createSelector(
  selectMetaJobsStatusState,
  (state): MetaJobsStatus[] | null => state.status,
);

export const selectStatusMap = createSelector(
  selectMetaJobsStatusState,
  (state): Map<string, MetaJobsStatus> => state.statusMap,
);

export const selectFetchError = createSelector(
  selectMetaJobsStatusState,
  (state): string | null => state.fetchError,
);

export const selectHandleError = createSelector(
  selectMetaJobsStatusState,
  (state): string | null => state.handleError,
);

export const selectHandleSuccess = createSelector(
  selectMetaJobsStatusState,
  (state): boolean | null => state.handleSuccess,
);
