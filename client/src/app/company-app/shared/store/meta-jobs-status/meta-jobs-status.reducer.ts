import { MetaJobsStatus, MetaJobsStatusColors } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import * as StatusActions from './meta-jobs-status.actions';

export const META_JOBS_STATUS_REDUCER_KEY = 'meta-jobs-status';

export interface MetaJobsStatusState {
  status: MetaJobsStatus[] | null;
  statusMap: Map<string, MetaJobsStatus>;
  fetchError: string | null;
  handleError: string | null;
  handleSuccess: boolean | null;
}

const INITIAL_STATE: MetaJobsStatusState = {
  status: null,
  statusMap: new Map(),
  fetchError: null,
  handleError: null,
  handleSuccess: null,
};

export const metaJobsStatusReducer = createReducer(
  INITIAL_STATE,
  on(StatusActions.tryFetchStatusListAction, (state): MetaJobsStatusState => {
    return {
      ...state,
      status: null,
      fetchError: null,
    };
  }),
  on(
    StatusActions.fetchStatusListSuccessAction,
    (state, { status }): MetaJobsStatusState => {
      const statusMap = new Map<string, MetaJobsStatus>();
      status.forEach((item) => {
        statusMap.set(item.name, item);
      });
      return {
        ...state,
        status,
        statusMap,
        fetchError: null,
      };
    },
  ),
  on(
    StatusActions.fetchStatusListSuccessAction,
    (state, { status }): MetaJobsStatusState => {
      return {
        ...state,
        status,
        fetchError: null,
      };
    },
  ),
  on(
    StatusActions.trySyncFromMultiPress,
    StatusActions.tryUpdateStatusAction,
    StatusActions.tryDeleteStatusAction,
    (state): MetaJobsStatusState => {
      return {
        ...state,
        handleSuccess: null,
        handleError: null,
      };
    },
  ),
  on(StatusActions.handleStatusSuccessAction, (state): MetaJobsStatusState => {
    return {
      ...state,
      handleSuccess: true,
      handleError: null,
    };
  }),
  on(
    StatusActions.handleStatusErrorAction,
    (state, { error }): MetaJobsStatusState => {
      return {
        ...state,
        handleSuccess: false,
        handleError: error,
      };
    },
  ),
);
