import { createAction, props } from '@ngrx/store';
import { MetaJobsStatus } from '../../interfaces';
import { MetaJobsStatusUpdateDto } from '../../dto';

export const tryFetchStatusListAction = createAction(
  '[CompanyApp > MetaJobsStatus] try fetch status list',
);

export const fetchStatusListSuccessAction = createAction(
  '[CompanyApp > MetaJobsStatus] fetch status list success',
  props<{ status: MetaJobsStatus[] }>(),
);

export const fetchStatusListErrorAction = createAction(
  '[CompanyApp > MetaJobsStatus] fetch status list error',
  props<{ error: string }>(),
);

export const trySyncFromMultiPress = createAction(
  '[CompanyApp > MetaJobsStatus] try sync from multipress',
);

export const tryUpdateStatusAction = createAction(
  '[CompanyApp > MetaJobsStatus] try update status',
  props<{ id: string; status: MetaJobsStatusUpdateDto }>(),
);

export const tryDeleteStatusAction = createAction(
  '[CompanyApp > MetaJobsStatus] try delete status',
  props<{ id: string }>(),
);

export const handleStatusSuccessAction = createAction(
  '[CompanyApp > MetaJobsStatus] handle status success',
);

export const handleStatusErrorAction = createAction(
  '[CompanyApp > MetaJobsStatus] handle status error',
  props<{ error: string }>(),
);
