export enum PaoStockSheetReservationStatus {
  Created = 'created',
  Pending = 'pending',
  Ordered = 'ordered',
  Treated = 'treated',
}
