export enum PaperType {
  Sheet = 'sheet',
  Roll = 'roll',
}
