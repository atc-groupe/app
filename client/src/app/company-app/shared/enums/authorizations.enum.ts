export enum Action {
  View = 'view',
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
  Manage = 'manage', // View, create, update & delete
  ChangeJobStatus = 'changeStatus',
  ManagePao = 'managePao',
  UpdateDeliveryComment = 'updateDeliveryComment',
  Decrease = 'decrease',
  Handle = 'handle',
  ManageMeta = 'manageMeta',
}

export enum Subject {
  Users = 'users',
  Roles = 'roles',
  DeliveryModule = 'deliveryModule',
  Job = 'job',
  Stock = 'stock',
  PaoReservations = 'paoReservations',
}
