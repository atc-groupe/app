export enum PaoStockReservationGlobalStatus {
  Empty = 'Vide',
  Created = 'Création',
  Pending = 'A traiter',
  StandBy = 'Standby',
  Ordered = 'Commandé',
  InProgress = 'En cours',
  Treated = 'Traitée',
  Cancelled = 'Annulée',
}
