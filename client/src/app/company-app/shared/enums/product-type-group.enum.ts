export enum ArticleGroupType {
  All = 0,
  Printout = 1,
  Films = 2,
  Proofs = 3,
  Plates = 4,
  Packaging = 5,
  Foil = 6,
  DieCutting = 7,
  OtherFinish = 8,
  WireO = 9,
  OtherPrePress = 10,
  Lamination = 11,
  LargeFormat = 12,
  Supplements = 50,
  Inks = 90,
}

export enum CustomerStockGroupType {
  All = 0,
}

export enum PaperGroupType {
  PlanoSheets = 0,
  Envelopes = 1,
  RollKg = 2,
  RollMl = 3,
  All = 4,
}

export type ProductTypeGroup =
  | ArticleGroupType
  | CustomerStockGroupType
  | PaperGroupType;
