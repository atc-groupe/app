export enum Module {
  Admin = 'admin',
  Jobs = 'jobs',
  Delivery = 'delivery',
  Stock = 'stock',
}
