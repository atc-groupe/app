import {
  ArticleGroupType,
  CustomerStockGroupType,
  PaperGroupType,
  ProductType,
} from '../enums';
import { DropdownItems, ProductTypeDropdownItem } from '../interfaces';

const PRODUCT_TYPE_DROPDOWN_GROUPS = new Map<ProductType, DropdownItems>();

PRODUCT_TYPE_DROPDOWN_GROUPS.set(ProductType.Article, [
  { label: 'Tous', value: ArticleGroupType.All },
  { label: 'Divers', value: ArticleGroupType.OtherFinish },
  { label: 'Plastifications', value: ArticleGroupType.Lamination },
  { label: 'Grand format', value: ArticleGroupType.LargeFormat },
  { label: 'Encres', value: ArticleGroupType.Inks },
]);

PRODUCT_TYPE_DROPDOWN_GROUPS.set(ProductType.CustomerStock, [
  { label: 'Tous', value: CustomerStockGroupType.All },
]);

PRODUCT_TYPE_DROPDOWN_GROUPS.set(ProductType.Paper, [
  { label: 'Tous', value: PaperGroupType.All },
  { label: 'Plaques & feuilles', value: PaperGroupType.PlanoSheets },
  { label: 'Rouleaux', value: PaperGroupType.RollMl },
]);

export { PRODUCT_TYPE_DROPDOWN_GROUPS };
