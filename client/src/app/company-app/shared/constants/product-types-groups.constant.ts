import {
  ArticleGroupType,
  PaperGroupType,
  ProductType,
  ProductTypeGroup,
} from '../enums';

const PRODUCT_TYPES_GROUPS = new Map<
  ProductType,
  Map<ProductTypeGroup, string>
>();

const ARTICLES_MAP = new Map<ProductTypeGroup, string>([
  [ArticleGroupType.All, 'Tous'],
  [ArticleGroupType.Printout, 'Impression'],
  [ArticleGroupType.Films, 'Films'],
  [ArticleGroupType.Proofs, 'Epreuves'],
  [ArticleGroupType.Plates, 'Plaques'],
  [ArticleGroupType.Packaging, 'Packaging'],
  [ArticleGroupType.Foil, 'Feuilles'],
  [ArticleGroupType.DieCutting, 'Découpe'],
  [ArticleGroupType.OtherFinish, 'Divers'],
  [ArticleGroupType.Lamination, 'Lamination'],
  [ArticleGroupType.LargeFormat, 'Grand format'],
  [ArticleGroupType.Supplements, 'Compléments'],
  [ArticleGroupType.Inks, 'Encres'],
]);

PRODUCT_TYPES_GROUPS.set(ProductType.Article, ARTICLES_MAP);

const PAPER_MAP = new Map<ProductTypeGroup, string>([
  [PaperGroupType.All, 'Tous'],
  [PaperGroupType.PlanoSheets, 'Plaques & feuilles'],
  [PaperGroupType.Envelopes, 'Envelopes'],
  [PaperGroupType.RollKg, 'Rouleaux (Kg)'],
  [PaperGroupType.RollMl, 'Rouleaux'],
]);

PRODUCT_TYPES_GROUPS.set(ProductType.Paper, PAPER_MAP);

export { PRODUCT_TYPES_GROUPS };
