import { PaoStockSheetReservationStatus, PaperType } from '../enums';

export interface PaoStockSheetReservationCreateDto {
  productType: number;
  productId: number;
  displayName: string;
  paperType: PaperType;
  quantity: number;
  isCustomSize: boolean;
  width?: number;
  height?: number;
  length?: number;
  isDoubleSided?: boolean;
  isNesting?: boolean;
  numberBySheet?: number;
  userDisplayName: string;
  userEmployeeNumber: number;
  paoComment?: string;
}

export interface PaoStockSheetReservationUpdateDto {
  productType: number;
  productId: number;
  displayName: string;
  paperType: PaperType;
  quantity: number;
  isCustomSize: boolean;
  width?: number;
  height?: number;
  length?: number;
  isDoubleSided?: boolean;
  isNesting?: boolean;
  numberBySheet?: number;
  mutationId?: number;
  stockDecreaseCount?: number;
  userDisplayName: string;
  userEmployeeNumber: number;
  paoComment?: string;
}

export interface PaoStockSheetReservationStatusDto {
  fromStatus: PaoStockSheetReservationStatus;
  toStatus: PaoStockSheetReservationStatus;
}
