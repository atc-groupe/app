export interface StockProductListParamsDto {
  groupType?: number;
  search?: string;
}
