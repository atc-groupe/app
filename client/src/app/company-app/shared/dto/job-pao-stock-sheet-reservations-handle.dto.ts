export interface JobPaoStockSheetReservationsHandle {
  employeeNumber: number;
  description: string;
  productType: number;
  productId: number;
  action: 'handle' | 'undo';
  subtract: number;
  add: number;
}
