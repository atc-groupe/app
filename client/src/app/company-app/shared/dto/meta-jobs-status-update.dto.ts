export interface MetaJobsStatusUpdateDto {
  label: string;
  color: string;
  textColor: string;
}
