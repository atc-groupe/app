export interface ArticleMetaDto {
  mpArticleId: number;
  packQuantity: number;
  packUnitLabel: string;
}
