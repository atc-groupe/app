import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  Product,
  ProductList,
  StockJobReservations,
  StockMutation,
  StockMutationListItem,
  StockMutationResponse,
} from '../interfaces';
import { environment } from '../../../../environments/environment';
import { StockProductListParamsDto, StockMutationDto } from '../dto';
import { ProductType } from '../enums';

@Injectable({ providedIn: 'root' })
export class StockService {
  constructor(private _http: HttpClient) {}

  fetchProductList(
    productType: number,
    params?: StockProductListParamsDto,
  ): Observable<ProductList> {
    if (params !== undefined) {
      return this._http.get<ProductList>(
        `${environment.api.url}/stock/products/${productType}/list`,
        { params: { ...params } },
      );
    }
    return this._http.get<ProductList>(
      `${environment.api.url}/stock/products/${productType}/list`,
    );
  }

  fetchProductDetails(
    productType: number,
    productId: number,
  ): Observable<Product> {
    return this._http.get<Product>(
      `${environment.api.url}/stock/products/${productType}/${productId}`,
    );
  }

  fetchProductsDetails(
    productType: number,
    productIds: number[],
  ): Observable<Product[]> {
    return this._http.get<Product[]>(
      `${environment.api.url}/stock/products/${productType}`,
      { params: { ids: productIds.toString() } },
    );
  }

  fetchProductsDetailsFromProductId(
    productType: number,
    productId: number,
  ): Observable<Product[]> {
    return this._http.get<Product[]>(
      `${environment.api.url}/stock/products/${productType}/formats`,
      { params: { productId } },
    );
  }

  createStockMutation(
    productType: ProductType,
    productId: number,
    dto: StockMutationDto,
  ): Observable<StockMutationResponse> {
    return this._http.post<StockMutationResponse>(
      `${environment.api.url}/stock/products/${productType}/${productId}/mutations`,
      dto,
    );
  }

  updateStockMutation(
    productType: ProductType,
    productId: number,
    mutationId: number,
    dto: StockMutationDto,
  ): Observable<StockMutationResponse> {
    return this._http.patch<StockMutationResponse>(
      `${environment.api.url}/stock/products/${productType}/${productId}/mutations/${mutationId}`,
      dto,
    );
  }

  getStockMutations(
    productType: ProductType,
    productId: number,
    startDate: string,
  ): Observable<StockMutationListItem[]> {
    return this._http.get<StockMutationListItem[]>(
      `${environment.api.url}/stock/products/${productType}/${productId}/mutations`,
      { params: { startdate: startDate } },
    );
  }

  getStockMutation(
    productType: ProductType,
    productId: number,
    mutationId: number,
  ): Observable<StockMutation> {
    return this._http.get<StockMutation>(
      `${environment.api.url}/stock/products/${productType}/${productId}/mutations/${mutationId}`,
    );
  }

  getStockJobReservations(jobNumber: string): Observable<StockJobReservations> {
    return this._http.get<StockJobReservations>(
      `${environment.api.url}/stock/reservations/${jobNumber}`,
    );
  }
}
