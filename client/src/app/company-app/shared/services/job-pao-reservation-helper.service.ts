import { Injectable } from '@angular/core';
import {
  PaoStockReservationGlobalStatus,
  PaoStockSheetReservationStatus,
  PaperType,
} from '../enums';
import { JobPaoStockSheetReservation } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class JobPaoReservationHelperService {
  getPaoResaDisplaySizeTitle(resa: JobPaoStockSheetReservation) {
    if (resa.isCustomSize && resa.paperType === PaperType.Sheet) {
      return 'Format fini / Quantité';
    }

    return 'Quantité';
  }

  getPaoResaDisplaySize(resa: JobPaoStockSheetReservation): string {
    if (resa.isCustomSize) {
      const size = `${resa.width} x ${resa.height} mm`;
      const sheetLabel = resa.quantity === 1 ? 'Plaque' : 'Plaques';
      if (resa.isNesting) {
        return `${size} - ${resa.quantity} ${sheetLabel}`;
      }

      const detail =
        resa.quantity === 1 ? '' : ` soit ${resa.numberBySheet}ex par plaque`;
      return `${size} - ${resa.numberBySheet! * resa.quantity}ex (${
        resa.quantity
      } ${sheetLabel}${detail})`;
    }

    return `${resa.quantity} ${
      resa.quantity === 1 ? 'Plaque entière' : 'Plaques entières'
    }`;
  }

  getDisplayFileInfo(resa: JobPaoStockSheetReservation): string {
    return `${resa.isNesting ? 'AMALGAMME ZUND' : 'AU FORMAT'} - ${
      resa.isDoubleSided ? 'RECTO/VERSO' : 'RECTO'
    }`;
  }

  getDisplayStatus(status: PaoStockSheetReservationStatus): string {
    switch (status) {
      case PaoStockSheetReservationStatus.Created:
        return 'Création';
      case PaoStockSheetReservationStatus.Pending:
        return 'En attente de traitement';
      case PaoStockSheetReservationStatus.Ordered:
        return 'En commande';
      case PaoStockSheetReservationStatus.Treated:
        return 'Traitée';
    }
  }

  getStatusColor(status: PaoStockSheetReservationStatus): string {
    switch (status) {
      case PaoStockSheetReservationStatus.Created:
        return 'secondary';
      case PaoStockSheetReservationStatus.Pending:
        return 'info';
      case PaoStockSheetReservationStatus.Ordered:
        return 'warning';
      case PaoStockSheetReservationStatus.Treated:
        return 'success';
    }
  }

  getGlobalStatus(reservations: JobPaoStockSheetReservation[]): {
    status: PaoStockReservationGlobalStatus;
    color: string;
  } {
    if (!reservations.length) {
      return { status: PaoStockReservationGlobalStatus.Empty, color: '' };
    }

    let hasCreated = false;
    let hasPending = false;
    let hasOrdered = false;
    let hasTreated = false;

    reservations.forEach((reservation) => {
      switch (reservation.status) {
        case PaoStockSheetReservationStatus.Created:
          hasCreated = true;
          break;
        case PaoStockSheetReservationStatus.Pending:
          hasPending = true;
          break;
        case PaoStockSheetReservationStatus.Ordered:
          hasOrdered = true;
          break;
        case PaoStockSheetReservationStatus.Treated:
          hasTreated = true;
          break;
      }
    });

    if (hasCreated && !hasPending && !hasOrdered && !hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.Created,
        color: 'secondary',
      };
    }

    if (!hasCreated && hasPending && !hasOrdered && !hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.Pending,
        color: 'info',
      };
    }

    if (!hasCreated && !hasPending && !hasOrdered && !hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.StandBy,
        color: 'warning',
      };
    }

    if (!hasCreated && !hasPending && hasOrdered && !hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.Ordered,
        color: 'warning',
      };
    }

    if (!hasCreated && !hasPending && !hasOrdered && hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.Treated,
        color: 'success',
      };
    }

    if (!hasCreated && !hasPending && !hasOrdered && !hasTreated) {
      return {
        status: PaoStockReservationGlobalStatus.Cancelled,
        color: 'error',
      };
    }

    if (hasCreated && hasPending) {
      return {
        status: PaoStockReservationGlobalStatus.Pending,
        color: 'info',
      };
    }

    if (hasCreated || hasOrdered || hasPending) {
      return {
        status: PaoStockReservationGlobalStatus.InProgress,
        color: 'info',
      };
    }

    return {
      status: PaoStockReservationGlobalStatus.Treated,
      color: 'success',
    };
  }

  hasSomeCreatedStatus(reservations: JobPaoStockSheetReservation[]): boolean {
    if (!reservations.length) {
      return false;
    }

    return reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Created,
    );
  }

  hasSomeReservationActiveStatus(
    reservations: JobPaoStockSheetReservation[],
  ): boolean {
    if (!reservations.length) {
      return false;
    }

    return reservations.some((resa) => {
      return (
        resa.status === PaoStockSheetReservationStatus.Pending ||
        resa.status === PaoStockSheetReservationStatus.Ordered
      );
    });
  }

  hasSomePendingStatus(reservations: JobPaoStockSheetReservation[]) {
    if (!reservations.length) {
      return false;
    }

    return reservations.some(
      (resa) => resa.status === PaoStockSheetReservationStatus.Pending,
    );
  }
}
