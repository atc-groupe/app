import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MetaJobsStatus } from '../interfaces';
import { environment } from '../../../../environments/environment';
import { MetaJobsStatusUpdateDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class MetaJobsStatusService {
  constructor(private _http: HttpClient) {}

  findAll(): Observable<MetaJobsStatus[]> {
    return this._http.get<MetaJobsStatus[]>(
      `${environment.api.url}/meta/jobs/status`,
    );
  }

  syncFromMultiPress(): Observable<void> {
    return this._http.get<void>(`${environment.api.url}/meta/jobs/status/sync`);
  }

  updateOne(
    id: string,
    dto: MetaJobsStatusUpdateDto,
  ): Observable<MetaJobsStatus> {
    return this._http.patch<MetaJobsStatus>(
      `${environment.api.url}/meta/jobs/status/${id}`,
      dto,
    );
  }
}
