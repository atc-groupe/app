import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  ApiCollectionResponse,
  Job,
  JobQueryParams,
  StockJobReservations,
} from '../interfaces';
import { environment } from '../../../../environments/environment';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class JobsService {
  constructor(private _http: HttpClient) {}

  fetchJobs(queryParams: JobQueryParams): Observable<Job[]> {
    const params: { [key: string]: string | number } = {};
    Object.entries(queryParams).forEach(
      ([key, value]: [string, string | number | Date]): void => {
        if (value instanceof Date) {
          value = value.toLocaleDateString('en-US');
        }

        params[key] = value;
      },
    );

    return this._http
      .get<ApiCollectionResponse<Job>>(`${environment.api.url}/jobs`, {
        params,
      })
      .pipe(map((response): Job[] => response.documents));
  }

  fetchJob(number: number): Observable<Job> {
    return this._http.get<Job>(`${environment.api.url}/jobs/${number}`);
  }

  changeStatus(
    jobNumber: string,
    statusNumber: number,
    reason: string,
  ): Observable<{ result: string }> {
    return this._http.patch<{ result: string }>(
      `${environment.api.url}/jobs/${jobNumber}/status`,
      {
        statusNumber,
        reason,
      },
    );
  }

  sync(jobId: number): Observable<void> {
    return this._http.get<void>(`${environment.api.url}/jobs/${jobId}/sync`);
  }
}
