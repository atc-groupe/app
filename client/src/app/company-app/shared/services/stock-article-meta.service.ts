import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, EMPTY, Observable, tap } from 'rxjs';
import { ArticleMeta } from '../interfaces/stock/article-meta.interface';
import { environment } from '../../../../environments/environment';
import { ArticleMetaDto } from '../dto';

@Injectable({ providedIn: 'root' })
export class StockArticleMetaService {
  public articlesMeta$ = new BehaviorSubject<ArticleMeta[] | null>(null);
  public fetchError$ = new BehaviorSubject<string | null>(null);
  public handleError$ = new BehaviorSubject<string | null>(null);

  constructor(private _http: HttpClient) {}

  findAll(): Observable<ArticleMeta[]> {
    this.fetchError$.next(null);
    return this._http
      .get<ArticleMeta[]>(`${environment.api.url}/stock/meta/articles`)
      .pipe(
        tap((articlesMeta) => this.articlesMeta$.next(articlesMeta)),
        catchError((err) => {
          this.fetchError$.next(err.error.message);
          return EMPTY;
        }),
      );
  }

  findOneByMpId(mpId: number): Observable<ArticleMeta> {
    this.fetchError$.next(null);
    return this._http
      .get<ArticleMeta>(`${environment.api.url}/stock/meta/articles/${mpId}`)
      .pipe(
        catchError((err) => {
          this.fetchError$.next(err.error.message);
          return EMPTY;
        }),
      );
  }

  insertOne(dto: ArticleMetaDto): Observable<ArticleMeta> {
    this.handleError$.next(null);
    return this._http
      .post<ArticleMeta>(`${environment.api.url}/stock/meta/articles`, dto)
      .pipe(
        catchError((err) => {
          this.handleError$.next(err.error.message);
          return EMPTY;
        }),
      );
  }

  updateOne(id: string, dto: ArticleMetaDto): Observable<ArticleMeta> {
    this.handleError$.next(null);
    return this._http
      .patch<ArticleMeta>(
        `${environment.api.url}/stock/meta/articles/${id}`,
        dto,
      )
      .pipe(
        catchError((err) => {
          this.handleError$.next(err.error.message);
          return EMPTY;
        }),
      );
  }

  deleteOne(id: string): Observable<ArticleMeta> {
    this.handleError$.next(null);
    return this._http
      .delete<ArticleMeta>(`${environment.api.url}/stock/meta/articles/${id}`)
      .pipe(
        catchError((err) => {
          this.handleError$.next(err.error.message);
          return EMPTY;
        }),
      );
  }

  clearHandleError(): void {
    this.handleError$.next(null);
  }
}
