import { Injectable } from '@angular/core';
import { AuthorizationsService } from '../../../shared/services/authorizations.service';
import { Nav, NavAuthorization, NavSection } from '../interfaces';
import { AppUser, Authorization } from '../../../shared/interfaces';
import { Store } from '@ngrx/store';
import { UsersSelectors } from '../../../shared/store/users';
import { first, map, Observable } from 'rxjs';
import { UserRole } from '../../../shared/enums';

@Injectable({
  providedIn: 'root',
})
export class NavService {
  private _user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);

  constructor(
    private _authService: AuthorizationsService,
    private _store: Store,
  ) {}

  getNavWithAuthorizations(nav: Nav): Observable<Nav> {
    return this._user$.pipe(
      first(),
      map((user: AppUser | null) => {
        if (user && user.role.level === UserRole.SuperAdmin) {
          return nav;
        }

        const authorizations = user ? user.authorizations : [];

        nav.sections = nav.sections.map((section): NavSection => {
          if (section.authorizations) {
            if (!this._canActivate(authorizations, section.authorizations)) {
              section.disabled = true;
            }
          }

          section.items = section.items.map((navItem) => {
            if (navItem.authorizations) {
              if (!this._canActivate(authorizations, navItem.authorizations)) {
                navItem.disabled = true;
              }
            }

            return navItem;
          });

          return section;
        });

        return nav;
      }),
    );
  }

  private _canActivate(
    userAuthorizations: Authorization[],
    itemAuthorizations: NavAuthorization,
  ): boolean {
    return (
      userAuthorizations.find(
        (auth) =>
          auth.subject === itemAuthorizations.subject &&
          auth.action === itemAuthorizations.action,
      ) !== undefined
    );
  }
}
