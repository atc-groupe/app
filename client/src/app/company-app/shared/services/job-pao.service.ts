import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import {
  PaoStockSheetReservationCreateDto,
  PaoStockSheetReservationStatusDto,
  PaoStockSheetReservationUpdateDto,
} from '../dto/job-pao-stock-sheet-reservation.dto';
import { Job } from '../interfaces';
import { JobPaoStockSheetReservationsHandle } from '../dto';

@Injectable({ providedIn: 'root' })
export class JobPaoService {
  constructor(private _http: HttpClient) {}

  updateCheckComment(
    jobNumber: string,
    checkComment: string | null,
  ): Observable<void> {
    return this._http.patch<void>(
      `${environment.api.url}/jobs/${jobNumber}/pao/check-comment`,
      { checkComment },
    );
  }

  insertStockSheetReservation(
    jobNumber: string,
    reservation: PaoStockSheetReservationCreateDto,
  ): Observable<void> {
    return this._http.post<void>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations`,
      reservation,
    );
  }

  updateStockSheetReservation(
    jobNumber: string,
    id: string,
    reservation: PaoStockSheetReservationUpdateDto,
  ): Observable<void> {
    return this._http.patch<void>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations/${id}`,
      reservation,
    );
  }

  changeStockSheetReservationsStatuses(
    jobNumber: string,
    dto: PaoStockSheetReservationStatusDto,
  ): Observable<Job> {
    return this._http.patch<Job>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations/status`,
      dto,
    );
  }

  changeStockSheetReservationStatus(
    jobNumber: string,
    reservationId: string,
    dto: PaoStockSheetReservationStatusDto,
  ): Observable<Job> {
    return this._http.patch<Job>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations/${reservationId}/status`,
      dto,
    );
  }

  removeStockSheetReservation(jobNumber: string, id: string): Observable<void> {
    return this._http.delete<void>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations/${id}`,
    );
  }

  handleStockSheetReservation(
    jobNumber: string,
    reservationId: string,
    dto: JobPaoStockSheetReservationsHandle,
  ): Observable<void> {
    return this._http.patch<void>(
      `${environment.api.url}/jobs/${jobNumber}/pao/stock-sheet-reservations/${reservationId}/handle`,
      dto,
    );
  }
}
