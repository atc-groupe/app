import { Action, Subject } from '../enums';

export interface NavAuthorization {
  subject: Subject;
  action: Action;
}

export interface NavItem {
  route: string;
  label: string;
  authorizations?: NavAuthorization;
  disabled?: true;
}

export interface NavSection {
  route?: string;
  icon: string | null;
  label: string;
  active: boolean;
  expanded: boolean;
  authorizations?: NavAuthorization;
  disabled?: true;
  items: NavItem[];
}

export interface Nav {
  title?: string;
  sections: NavSection[];
}
