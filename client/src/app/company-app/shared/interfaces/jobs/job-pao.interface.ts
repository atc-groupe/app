import { PaoStockSheetReservationStatus } from '../../enums';

export interface JobPaoStockSheetReservation {
  _id: string;
  productType: number;
  productId: number;
  displayName: string;
  paperType: string;
  isCustomSize: boolean;
  quantity: number;
  width?: number;
  height?: number;
  isDoubleSided: boolean;
  isNesting: boolean;
  numberBySheet?: number;
  status: PaoStockSheetReservationStatus;
  mutationId?: number;
  stockDecreaseCount?: number;
  userDisplayName: string;
  userEmployeeNumber: number;
  paoComment?: string;
  reservationComment?: string;
}

export interface JobPao {
  checkComment: string | null;
  stockSheetReservations: JobPaoStockSheetReservation[];
}
