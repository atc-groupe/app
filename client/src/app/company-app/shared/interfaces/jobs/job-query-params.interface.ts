export interface JobQueryParams {
  sendingDate?: string;
  startSendingDate?: string;
  endSendingDate?: string;
  statusNumber?: number;
  statusNumberList?: number[];
  startStatusNumber?: number;
  endStatusNumber?: number;
  jobEndNumber?: string;
  fullSearch?: string;
  hasPaoReservation?: boolean;
  populate?: boolean;
  countPerPage?: number;
  pageIndex?: number;
}
