import { JobMp } from './job-mp.interface';
import { JobMeta } from './job-meta.interface';
import { SubJobDeliveryAddress } from '../sub-job';
import { SubJob } from '../sub-job';
import { JobDelivery } from './job-delivery.interface';
import { JobPlanning } from './job-planning.interface';
import { JobPao } from './job-pao.interface';
import { JobSyncStatusType } from '../../types/job-sync-status.type';

export interface Job {
  _id: string;
  createdAt: Date;
  updatedAt: Date;
  syncStatus: JobSyncStatusType;
  mp: JobMp;
  meta: JobMeta;
  delivery?: JobDelivery;
  planning?: JobPlanning;
  pao?: JobPao;
  subJobs?: SubJob[];
  deliveryAddresses?: SubJobDeliveryAddress[];
}
