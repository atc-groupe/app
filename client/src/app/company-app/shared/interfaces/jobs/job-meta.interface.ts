export interface JobMeta {
  printingSurface: number;
  modelCount: number; // Number of different files for a job
  manufacturedPiecesCount: number; // Total quantity of manufactured pieces.
  productionTypes: string[]; // List of fabrication types. e.g. EPSON, SEMI-DECOUPE, ...
  deliveryServiceFinishs: string[];
  hasSheetProduction: boolean;
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
  addressCount: number; // Number of different delivery addresses
  addressZipCodes: string[];
  deliveryMethods: string[];
  deliverySendingDates: string[];
}
