export interface MetaJobsStatus {
  _id?: string;
  id: string;
  name: string;
  label: string;
  color: string;
  textColor: string;
  number: number;
}
