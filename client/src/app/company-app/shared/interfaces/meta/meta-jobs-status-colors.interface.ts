export interface MetaJobsStatusColors {
  color: string;
  background: string;
}
