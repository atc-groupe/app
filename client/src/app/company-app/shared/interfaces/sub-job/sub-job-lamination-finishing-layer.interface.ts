import { MpSubJobFinishingType } from '../../enums';

export interface SubJobLaminationFinishingLayer {
  finishingType: MpSubJobFinishingType.lamination;
  numericId: number | null;
  operation: {
    name: string;
    width: number;
    height: number;
    doubleSided: boolean;
  };
  material: {
    name: string;
    id: number;
  };
}
