export interface SubJobChecklistItem {
  label: string;
  value: string;
}
