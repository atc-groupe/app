import { SubJobDeliveryAddressMp } from './sub-job-delivery-address-mp.interface';

export interface SubJobDeliveryAddress {
  mp: SubJobDeliveryAddressMp;
  _id: string;
  job: string;
  subJob: string;
}
