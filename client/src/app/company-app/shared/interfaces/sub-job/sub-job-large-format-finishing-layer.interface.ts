import { MpSubJobFinishingType } from '../../enums';

export interface SubJobLargeFormatFinishingLayer {
  finishingType: MpSubJobFinishingType.largeFormat;
  numericId: number | null;
  operation: {
    name: string;
    operationType: string;
  };
  sides: {
    left: boolean;
    right: boolean;
    top: boolean;
    bottom: boolean;
  };
  material: {
    name: string;
    id: number;
  } | null;
}
