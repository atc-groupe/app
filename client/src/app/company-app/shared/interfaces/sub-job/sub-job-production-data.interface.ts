import { SubJobProductionMeta } from './sub-job-production-meta.interface';
import { SubJobNumericLayer } from './sub-job-numeric-layer.interface';
import { SubJobFinishingLayer } from './sub-job-finishing-layer.type';
import { SubJobSubcontracting } from './sub-job-production-subcontracting.interface';

export interface SubJobProductionData {
  devices: string[];
  meta: SubJobProductionMeta;
  numericLayers: SubJobNumericLayer[] | null;
  unlinkedFinishingLayers: SubJobFinishingLayer[] | null;
  subcontracting: SubJobSubcontracting[] | null;
}
