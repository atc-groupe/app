import { SubJobSubcontractingItem } from './sub-job-production-subcontracting-item.interface';

export interface SubJobSubcontracting {
  relation: string;
  relationNumber: number;
  contactName: string | null;
  sendingDate: string;
  deliveryDate: string;
  items: SubJobSubcontractingItem[];
}
