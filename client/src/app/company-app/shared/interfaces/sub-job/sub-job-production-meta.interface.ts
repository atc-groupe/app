export interface SubJobProductionMeta {
  surface: number | null;
  hasSheetPrinting: boolean;
  deliveryServiceFinishs: string[] | null;
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
}
