import { SubJobLaminationFinishingLayer } from './sub-job-lamination-finishing-layer.interface';
import { SubJobLargeFormatFinishingLayer } from './sub-job-large-format-finishing-layer.interface';

/**
 This interfaces matches the data extracted from finishing key
 of workflow/handleJobDetailInfo MultiPress API endpoint
 */
export type SubJobFinishingLayer =
  | SubJobLaminationFinishingLayer
  | SubJobLargeFormatFinishingLayer;
