import { SubJobChecklistItem } from './sub-job-checklist-item.interface';
import { SubJobProductionData } from './sub-job-production-data.interface';

export interface SubJob {
  _id: string;
  index: number;
  mpId: number;
  description: string;
  quantity1: number;
  quantity2?: number;
  quantity3?: number;
  checklist: SubJobChecklistItem[];
  productType: string;
  productNumber: number;
  paoInfo: string;
  paoCheckInfo: string;
  productionData: SubJobProductionData | null;
  job: string;
}
