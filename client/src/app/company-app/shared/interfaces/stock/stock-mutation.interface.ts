export interface StockMutation {
  description: string;
  subtract: number;
  add: number;
  job_number: number;
  date: Date;
  input_method: number;
  product_id: number;
  source_reference: string;
}
