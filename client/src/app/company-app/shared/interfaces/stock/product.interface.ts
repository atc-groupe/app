import { Article } from './article.interface';
import { CustomerStock } from './customer-stock.interface';
import { Paper } from './paper.interface';

export type Product = Article | CustomerStock | Paper;
