export interface StockMutationResponse {
  mutation_id: number;
  result: string;
}
