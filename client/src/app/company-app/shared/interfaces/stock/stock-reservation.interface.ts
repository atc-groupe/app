import { Product } from './product.interface';
import { ProductType } from '../../enums';

export interface StockReservation {
  productType: ProductType;
  productId: number;
  productName: string;
  jobNumber: number;
  description: string;
  reserved: number;
  unitId: number;
  unitText: string;
  stock: number;
  articleNumber: string;
  type: string;
  extraInfo: string;
  size: string;
  remark: string;
  product: Product;
}
