import { StockReservation } from './index';

export interface StockJobReservations {
  articles: StockReservation[];
  customerProducts: StockReservation[];
  papers: StockReservation[];
}
