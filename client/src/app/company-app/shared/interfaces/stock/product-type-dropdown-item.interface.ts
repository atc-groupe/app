import { ProductTypeGroup } from '../../enums';

export interface ProductTypeDropdownItem {
  label: string;
  value: ProductTypeGroup;
}
