import { Article } from './article.interface';

export interface ArticleMeta {
  _id: string;
  mpArticleId: number;
  packQuantity: number;
  packUnitLabel: string;
  article: Article;
}
