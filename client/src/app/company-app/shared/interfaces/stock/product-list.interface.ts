export interface ProductListItem {
  description: string;
  articleIds: number[];
}

export interface ProductGroup {
  groupType: number;
  description: string;
  products: ProductListItem[];
}

export interface ProductGroupType {
  groupType: number;
  groups: ProductGroup[];
}

export type ProductList = ProductGroupType[];
