export interface DropdownItem {
  label: string;
  value: any;
  icon?: string;
}

export type DropdownItems = DropdownItem[];
