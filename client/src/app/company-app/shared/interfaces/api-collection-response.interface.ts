export interface ApiCollectionResponse<T> {
  documents: T[];
  documentCount?: number;
  pageCount?: number;
  pageIndex?: number;
}
