import { Directive, HostBinding, Input, OnInit } from '@angular/core';
import { MetaJobsStatusColors } from '../interfaces';

@Directive({
  selector: '[companyAppColor]',
  standalone: true,
})
export class ColorDirective implements OnInit {
  @Input() companyAppColor!: MetaJobsStatusColors;
  @HostBinding('style.backgroundColor') backgroundColor!: string;
  @HostBinding('style.color') color!: string;

  ngOnInit() {
    this.backgroundColor = this.companyAppColor.background
      ? this.companyAppColor.background
      : '#555';
    this.color = this.companyAppColor.color
      ? this.companyAppColor.color
      : '#ddd';
  }
}
