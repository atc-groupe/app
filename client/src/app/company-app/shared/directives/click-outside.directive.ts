import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
} from '@angular/core';

@Directive({
  selector: '[clickOutside]',
  standalone: true,
})
export class ClickOutsideDirective {
  @Output() clickOutside = new EventEmitter<void>();

  @HostListener('document:click', ['$event.target']) public onClick(
    target: HTMLElement,
  ) {
    const clickInside = this.el.nativeElement.contains(target);

    const isToggleMenuButton =
      target.classList.contains('toggle-button') &&
      this.el.nativeElement.contains(target);

    if (!clickInside && !isToggleMenuButton) {
      this.clickOutside.emit();
    }
  }

  constructor(private el: ElementRef) {}
}
