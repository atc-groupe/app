// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutModule } from './shared/modules/layout.module';
import { CoreModule } from '../shared/modules/core.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

// Components
import { CompanyAppComponent } from './company-app.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { MessageComponent } from './shared/components/message/message.component';
import { FooterComponent } from './shared/components/footer/footer.component';

// Routes
import { COMPANY_APP_ROUTES } from './company-app.routes';

// Store
import { COMPANY_APP_REDUCERS } from './shared/store';
import { MetaJobsStatusEffects } from './shared/store/meta-jobs-status';
import { StockEffects } from './shared/store/stock';

@NgModule({
  declarations: [
    CompanyAppComponent,
    NavbarComponent,
    MessageComponent,
    FooterComponent,
  ],
  imports: [
    CoreModule,
    LayoutModule,
    RouterModule.forChild(COMPANY_APP_ROUTES),
    StoreModule.forFeature('CompanyApp', COMPANY_APP_REDUCERS, {}),
    EffectsModule.forFeature(MetaJobsStatusEffects, StockEffects),
  ],
})
export class CompanyAppModule {}
