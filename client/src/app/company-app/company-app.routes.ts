import { Routes } from '@angular/router';
import { CompanyAppComponent } from './company-app.component';
import { adminGuard } from './shared/guards/admin.guard';

export const COMPANY_APP_ROUTES: Routes = [
  {
    path: '',
    component: CompanyAppComponent,
    children: [
      {
        path: '',
        redirectTo: 'delivery',
        pathMatch: 'full',
      },
      {
        path: 'relations',
        loadChildren: () =>
          import('./features/relations/relations.module').then(
            (m) => m.RelationsModule,
          ),
      },
      {
        path: 'web2print',
        loadChildren: () =>
          import('./features/web2print/web2print.module').then(
            (m) => m.Web2printModule,
          ),
      },
      {
        path: 'jobs',
        loadChildren: () =>
          import('./features/jobs/jobs.module').then((m) => m.JobsModule),
      },
      {
        path: 'stock',
        loadChildren: () =>
          import('./features/stock/stock.module').then((m) => m.StockModule),
      },
      {
        path: 'delivery',
        loadChildren: () =>
          import('./features/delivery/delivery.module').then(
            (m) => m.DeliveryModule,
          ),
      },
      {
        path: 'application',
        loadChildren: () =>
          import('./features/application/application.module').then(
            (m) => m.ApplicationModule,
          ),
      },
      {
        path: 'inventory',
        loadChildren: () =>
          import('./features/inventory/inventory.module').then(
            (m) => m.InventoryModule,
          ),
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('./features/admin/admin.module').then((m) => m.AdminModule),
        canActivate: [adminGuard],
      },
    ],
  },
];
