import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersSelectors, UsersActions } from '../shared/store/users';
import { MetaJobsStatusActions } from './shared/store/meta-jobs-status';
import { Router } from '@angular/router';

@Component({
  selector: 'company-app',
  templateUrl: './company-app.component.html',
  styleUrls: ['./company-app.component.scss'],
})
export class CompanyAppComponent implements OnInit {
  public authenticatedUser$ = this._store.select(
    UsersSelectors.selectCompanyUser,
  );

  constructor(private _store: Store, private _router: Router) {}

  ngOnInit() {
    this._store.dispatch(MetaJobsStatusActions.tryFetchStatusListAction());
  }

  public onLogout() {
    this._store.dispatch(UsersActions.tryLogOutAction());
  }
}
