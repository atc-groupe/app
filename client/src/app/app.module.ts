// Modules
import { isDevMode, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { CoreModule } from './shared/modules/core.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

// Components
import { AppComponent } from './app.component';
import { SignInComponent } from './views/sign-in/sign-in.component';

// Routes
import { APP_ROUTES } from './app.routes';

// Store
import { APP_REDUCERS } from './shared/store';
import { UsersEffects } from './shared/store/users';
import { RouterEffects } from './shared/store/router/router.effects';
import { EmployeesEffects } from './shared/store/employees';
import { RelationsEffects } from './shared/store/relations';
import { SystemEffects } from './shared/store/system';

// Locale
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeFr);

@NgModule({
  declarations: [AppComponent, SignInComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    EffectsModule,
    RouterModule.forRoot(APP_ROUTES, { paramsInheritanceStrategy: 'always' }),
    StoreModule.forRoot(APP_REDUCERS, {}),
    EffectsModule.forRoot([
      UsersEffects,
      RouterEffects,
      EmployeesEffects,
      RelationsEffects,
      SystemEffects,
    ]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
    StoreRouterConnectingModule.forRoot(),
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'fr-FR' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
