import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ClipboardModule } from '@angular/cdk/clipboard';

const MODULES = [
  CommonModule,
  ReactiveFormsModule,
  HttpClientModule,
  ClipboardModule,
];

@NgModule({
  imports: MODULES,
  exports: MODULES,
})
export class CoreModule {}
