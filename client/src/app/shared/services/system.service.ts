import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SystemPullDownList } from '../enums';
import { Observable } from 'rxjs';
import { SystemList } from '../interfaces/system.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SystemService {
  constructor(private _http: HttpClient) {}

  fetchPullDownList(name: SystemPullDownList): Observable<SystemList> {
    return this._http.get<SystemList>(
      `${environment.api.url}/system/pull-down-list/${name}`,
    );
  }
}
