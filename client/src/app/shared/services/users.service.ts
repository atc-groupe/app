import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  AppUser,
  UserCreate,
  UserCredentials,
  Users,
  UserSetting,
} from '../interfaces';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private _http: HttpClient) {}

  public signIn(credentials: UserCredentials): Observable<AppUser> {
    return this._http.post<AppUser>(
      `${environment.api.url}/auth/sign-in`,
      credentials,
    );
  }

  public fetchAuthenticatedUser(): Observable<AppUser | null> {
    return this._http.get<AppUser | null>(`${environment.api.url}/users/me`);
  }

  public logout(): Observable<void> {
    return this._http.get<void>(`${environment.api.url}/auth/logout`);
  }

  public fetchUsersList(): Observable<Users> {
    return this._http.get<Users>(`${environment.api.url}/users`);
  }

  public createOne(user: UserCreate): Observable<AppUser> {
    return this._http.post<AppUser>(`${environment.api.url}/users`, user);
  }

  public updateOne(userId: string, user: UserCreate): Observable<AppUser> {
    return this._http.patch<AppUser>(
      `${environment.api.url}/users/${userId}`,
      user,
    );
  }

  public updateRole(userId: string, roleId: string) {
    return this._http.patch<AppUser>(`${environment.api.url}/users/${userId}`, {
      roleId,
    });
  }

  public updatePassword(userId: string, password: string) {
    return this._http.patch<AppUser>(`${environment.api.url}/users/${userId}`, {
      password,
    });
  }

  public updateIsActive(userId: string, isActive: boolean) {
    return this._http.patch<AppUser>(`${environment.api.url}/users/${userId}`, {
      isActive,
    });
  }

  public deleteOne(userId: string): Observable<void> {
    return this._http.delete<void>(`${environment.api.url}/users/${userId}`);
  }

  public upsertSetting(
    userId: string,
    setting: UserSetting,
  ): Observable<AppUser> {
    return this._http.post<AppUser>(
      `${environment.api.url}/users/${userId}/setting`,
      setting,
    );
  }

  public removeSetting(
    userId: string,
    settingName: string,
  ): Observable<AppUser> {
    return this._http.delete<AppUser>(
      `${environment.api.url}/users/${userId}/setting`,
    );
  }

  public upsertManySettings(
    userId: string,
    settings: UserSetting[],
  ): Observable<AppUser> {
    return this._http.post<AppUser>(
      `${environment.api.url}/users/${userId}/settings`,
      { settings },
    );
  }
}
