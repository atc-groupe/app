import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RelationType } from '../enums/relation-type.enum';
import { Observable } from 'rxjs';
import {
  RelationContact,
  RelationList,
} from '../interfaces/relation.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RelationsService {
  constructor(private _http: HttpClient) {}

  findAll(type: RelationType): Observable<RelationList> {
    return this._http.get<RelationList>(`${environment.api.url}/relations`, {
      params: { type },
    });
  }

  findContacts(id: string): Observable<RelationContact[]> {
    return this._http.get<RelationContact[]>(
      `${environment.api.url}/relations/${id}/contacts`,
    );
  }
}
