import { Injectable } from '@angular/core';
import {
  Authorization,
  ModuleAuthorizations,
  RouteAuthorization,
  UserType,
} from '../interfaces';
import { ADMIN_AUTHORIZATIONS } from '../../company-app/features/admin/admin.authorizations';
import { JOBS_AUTHORIZATIONS } from '../../company-app/features/jobs/jobs.authorizations';
import { first, map, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Store } from '@ngrx/store';
import { Action, App, Module, Subject, UserRole } from '../enums';
import { UsersSelectors } from '../store/users';
import { STOCK_AUTHORIZATIONS } from '../../company-app/features/stock/stock.authorizations';
import { DELIVERY_AUTHORIZATIONS } from '../../company-app/features/delivery/delivery.authorizations';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationsService {
  private _declaredAuthorizations: { [key: string]: ModuleAuthorizations[] } = {
    [App.Company]: [
      ADMIN_AUTHORIZATIONS,
      DELIVERY_AUTHORIZATIONS,
      JOBS_AUTHORIZATIONS,
      STOCK_AUTHORIZATIONS,
    ],
    [App.Customer]: [],
  };

  private _routeAuthorizations: RouteAuthorization[];
  private user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);

  constructor(private _http: HttpClient, private _store: Store) {
    const companyRouteAuthorizations = this._getDeclaredRoutesAuthorizations(
      App.Company,
    );

    const customerRouteAuthorizations = this._getDeclaredRoutesAuthorizations(
      App.Customer,
    );

    this._routeAuthorizations = [
      ...companyRouteAuthorizations,
      ...customerRouteAuthorizations,
    ];
  }

  getAuthorizationsDeclarations(userType: UserType): ModuleAuthorizations[] {
    const appType = userType === UserType.Employee ? App.Company : App.Customer;
    return this._declaredAuthorizations[appType];
  }

  getRoleAuthorizations(roleId: string): Observable<Authorization[]> {
    return this._http.get<Authorization[]>(
      `${environment.api.url}/authorizations`,
      {
        params: { roleId },
      },
    );
  }

  updateAuthorizationRole(
    roleAction: 'add' | 'remove',
    userType: UserType,
    subject: Subject,
    action: Action,
    roleId: string,
  ): Observable<void> {
    return this._http.patch<void>(
      `${environment.api.url}/authorizations/roles`,
      {
        userType,
        roleAction,
        subject,
        action,
        roleId,
      },
    );
  }

  getUserAuthorizations(): Observable<Authorization[]> {
    return this.user$.pipe(
      first(),
      map((user) => (user ? user.authorizations : [])),
    );
  }

  canActivate(subject: Subject, action: Action): Observable<boolean> {
    return this.user$.pipe(
      first(),
      map((user): boolean => {
        if (!user) {
          return false;
        }

        if (
          user.role.level === UserRole.SuperAdmin ||
          user.role.level === UserRole.Admin
        ) {
          return true;
        }

        return (
          user.authorizations.find(
            (auth) => auth.subject === subject && auth.action === action,
          ) !== undefined
        );
      }),
    );
  }

  canActivateRoute(routeUrl: string): Observable<boolean> {
    const urlFragments = routeUrl.split('/');
    urlFragments.shift();
    const [app, module, ...moduleRouteFragments] = urlFragments;
    const moduleRoute = moduleRouteFragments.join('/');

    const authorization = this._routeAuthorizations.find((item) => {
      return (
        item.app === app && item.module === module && item.route === moduleRoute
      );
    });

    if (!authorization) {
      return of(true);
    }

    return this.canActivate(authorization.subject, authorization.action);
  }

  private _getDeclaredRoutesAuthorizations(appType: App): RouteAuthorization[] {
    const getRouteAuthorizations = (
      app: App,
      module: Module,
      authorizations: Authorization[],
    ): RouteAuthorization[] => {
      return authorizations
        .filter((item) => item.route !== undefined)
        .map((item): RouteAuthorization => {
          return {
            app,
            module,
            subject: item.subject,
            action: item.action,
            route: item.route!,
          };
        });
    };

    return this._declaredAuthorizations[appType].reduce(
      (acc: RouteAuthorization[], modulaAuth) => {
        if (modulaAuth.authorizations) {
          acc.push(
            ...getRouteAuthorizations(
              App.Company,
              modulaAuth.module,
              modulaAuth.authorizations,
            ),
          );
        }

        if (modulaAuth.subjectsAuthorizations) {
          modulaAuth.subjectsAuthorizations.forEach((subAuth) => {
            acc.push(
              ...getRouteAuthorizations(
                App.Company,
                modulaAuth.module,
                subAuth.authorizations,
              ),
            );
          });
        }

        return acc;
      },
      [],
    );
  }
}
