import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersSelectors } from '../store/users';
import { map, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserSettingsService {
  private _user$ = this._store.select(UsersSelectors.selectAuthenticatedUser);

  constructor(private _store: Store) {}

  hasSetting(settingName: string): Observable<boolean> {
    return this._user$.pipe(
      map((user) => {
        if (!user) {
          return false;
        }

        const setting = user.settings.find(
          (setting) => setting.name === settingName,
        );

        return setting !== undefined;
      }),
    );
  }

  getSettingValue(settingName: string): Observable<string | null> {
    return this._user$.pipe(
      map((user) => {
        if (!user) {
          return null;
        }

        const setting = user.settings.find(
          (setting) => setting.name === settingName,
        );

        return setting !== undefined ? setting.value : null;
      }),
    );
  }

  getSettingsMap(): Observable<Map<string, string> | null> {
    return this._user$.pipe(
      map((user) => {
        if (!user || !user.settings.length) {
          return null;
        }

        const map = new Map<string, string>();
        user.settings.forEach((setting) => {
          map.set(setting.name, setting.value);
        });

        return map;
      }),
    );
  }
}
