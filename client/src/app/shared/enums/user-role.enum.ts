export enum UserRole {
  SuperAdmin = 'superAdmin',
  Admin = 'admin',
  Manager = 'manager',
  User = 'user',
  Guest = 'guest',
}
