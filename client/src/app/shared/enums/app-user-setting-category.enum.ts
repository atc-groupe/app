import { AppUserSettingsCategory as CompanyAppSettingsCategory } from '../../company-app/shared/enums';
import { AppUserSettingsCategory as CustomerAppSettingsCategory } from '../../customer-app/shared/enums';

export type AppUserSettingsCategory =
  | CompanyAppSettingsCategory
  | CustomerAppSettingsCategory;
