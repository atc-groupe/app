import { Module as CompanyAppModule } from '../../company-app/shared/enums';
import { Module as CustomerAppModule } from '../../customer-app/shared/enums';

export type Module = CompanyAppModule | CustomerAppModule;
