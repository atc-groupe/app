import * as CompanyAppEnums from '../../company-app/shared/enums';
import * as CustomerAppEnums from '../../customer-app/shared/enums';

export type Subject = CompanyAppEnums.Subject | CustomerAppEnums.Subject;
