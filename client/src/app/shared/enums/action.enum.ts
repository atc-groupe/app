import * as CompanyAppEnums from '../../company-app/shared/enums';
import * as CustomerAppEnums from '../../customer-app/shared/enums';

export type Action = CompanyAppEnums.Action | CustomerAppEnums.Action;
