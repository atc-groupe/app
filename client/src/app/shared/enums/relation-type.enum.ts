export enum RelationType {
  Client = 'C',
  Prospect = 'P',
  Fournisseur = 'F',
  AncienContact = 'A',
}
