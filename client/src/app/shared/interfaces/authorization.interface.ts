import { Action, Subject } from '../types/enums.types';
import { App, Module } from '../enums';

export interface Authorization {
  subject: Subject; // Can add CustomerAppEnums later
  action: Action; // Can add CustomerAppEnums later
  label: string;
  route?: string; // Used for guard. based on feature module. e.g. for module admin: /users -> /company/admin/users
  description?: string;
  denyMessage?: string;
  roles?: string[]; // List of roles ids
}

/**
 * Only used for display usage.
 * Allow to group authorizations declarations around a subject.
 */
export interface SubjectAuthorizations {
  label: string;
  authorizations: Authorization[];
}

export interface ModuleAuthorizations {
  module: Module;
  label: string; // Used to display module description in admin panel
  authorizations?: Authorization[];
  subjectsAuthorizations?: SubjectAuthorizations[];
}

export interface RouteAuthorization {
  app: App;
  module: Module;
  route: string;
  subject: Subject;
  action: Action;
}
