import { RelationType } from '../enums/relation-type.enum';

export interface RelationContact {
  id: string;
  relationNumber: number;
  firstName: string;
  lastName: string;
  completeName: string;
  phone: string;
  mobile: string;
  email: string;
  title: string;
  opening: string;
}

export interface Relation {
  id: string;
  number: number;
  type: RelationType;
  name: string;
  visitAddress: string;
  visitZipCode: string;
  visitCity: string;
  phone: string;
  mobilePhone: string;
  email: string;
  salesMan: string;
  jobManager: string;
  contacts: RelationContact[];
}

export interface RelationListItem {
  id: string;
  number: number;
  company: string;
}

export type RelationList = RelationListItem[];
