export * from './authorization.interface';
export * from './employee.interface';
export * from './relation.interface';
export * from './system.interface';
export * from './user.interface';
export * from './app-user-setting.interface';
