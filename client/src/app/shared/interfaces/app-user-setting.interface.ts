import { AppUserSettingsCategory } from '../enums';

export interface AppUserSetting {
  name: string;
  label: string;
  description?: string;
}

export interface AppUserSettingsGroup {
  category: AppUserSettingsCategory;
  description: string;
  settings: AppUserSetting[];
}
