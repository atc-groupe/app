export interface Employee {
  id: string;
  employee_number: number;
  name: string;
  department: string;
}
