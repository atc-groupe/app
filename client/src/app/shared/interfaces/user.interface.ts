import { UserRole } from '../enums';
import { Authorization } from './authorization.interface';
import { CompanyUserDepartment } from '../../company-app/shared/enums';

export interface UserSetting {
  name: string;
  value: string;
}

export interface User {
  _id: string;
  type: UserType;
  mpId: string;
  identifier: string;
  email: string;
  firstName: string;
  lastName: string;
  isActive: boolean;
  role: {
    _id: string;
    label: string;
    level: UserRole;
  };
  settings: UserSetting[];
  authorizations: Authorization[];
}

export interface CompanyUser extends User {
  employeeNumber: number;
  department: CompanyUserDepartment;
}

export interface CustomerUser extends User {
  relationNumber: number;
}

export type AppUser = CompanyUser | CustomerUser;

export enum UserType {
  Employee = 'employee',
  Customer = 'customer',
}

export interface Users {
  [UserType.Employee]: CompanyUser[];
  [UserType.Customer]: CustomerUser[];
}

export interface UserCredentials {
  identifier: string;
  password: string;
}

export interface UserCreate {
  type: UserType;
  mpId: string;
  identifier: string;
  password?: string;
  roleId: string;
}
