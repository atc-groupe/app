export interface SystemListItem {
  number: number;
  description: string | null;
  label: string;
}

export type SystemList = SystemListItem[];
