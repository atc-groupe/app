import {
  Relation,
  RelationContact,
  RelationList,
} from '../../interfaces/relation.interface';
import { createReducer, on } from '@ngrx/store';
import * as RelationsActions from './relations.actions';

export const RELATION_REDUCER_KEY = 'relations';

export interface RelationsState {
  relations: {
    customers: RelationList;
    fetchError: string | null;
  };
  relation: {
    relation: Relation | null;
    contacts: RelationContact[] | null;
    fetchError: string | null;
  };
}

const INITIAL_STATE: RelationsState = {
  relations: {
    customers: [],
    fetchError: null,
  },
  relation: {
    relation: null,
    contacts: null,
    fetchError: null,
  },
};

export const relationsReducer = createReducer(
  INITIAL_STATE,
  on(
    RelationsActions.fetchCustomersSuccessAction,
    (
      state: RelationsState,
      { customers }: { customers: RelationList },
    ): RelationsState => {
      return {
        ...state,
        relations: {
          ...state.relations,
          customers,
          fetchError: null,
        },
      };
    },
  ),
  on(
    RelationsActions.fetchRelationContactsSuccessAction,
    (
      state: RelationsState,
      { contacts }: { contacts: RelationContact[] },
    ): RelationsState => {
      return {
        ...state,
        relation: {
          ...state.relation,
          contacts,
          fetchError: null,
        },
      };
    },
  ),
  on(
    RelationsActions.fetchRelationsErrorAction,
    (state: RelationsState, { error }): RelationsState => {
      return {
        ...state,
        relations: {
          ...state.relations,
          fetchError: error,
        },
      };
    },
  ),
  on(
    RelationsActions.fetchRelationErrorAction,
    (state: RelationsState, { error }): RelationsState => {
      return {
        ...state,
        relation: {
          ...state.relation,
          fetchError: error,
        },
      };
    },
  ),
);
