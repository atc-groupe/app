import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RELATION_REDUCER_KEY, RelationsState } from './relations.reducer';
import {
  Relation,
  RelationContact,
  RelationList,
} from '../../interfaces/relation.interface';

const selectRelationsState =
  createFeatureSelector<RelationsState>(RELATION_REDUCER_KEY);

// ------ Relations ------
export const selectCustomersList = createSelector(
  selectRelationsState,
  (state: RelationsState): RelationList => state.relations.customers,
);
export const selectFetchRelationsError = createSelector(
  selectRelationsState,
  (state: RelationsState): string | null => state.relations.fetchError,
);

// ------ Relation ------
export const selectRelation = createSelector(
  selectRelationsState,
  (state: RelationsState): Relation | null => state.relation.relation,
);
export const selectRelationContacts = createSelector(
  selectRelationsState,
  (state: RelationsState): RelationContact[] | null => state.relation.contacts,
);
export const selectRelationFetchError = createSelector(
  selectRelationsState,
  (state: RelationsState): string | null => state.relation.fetchError,
);
