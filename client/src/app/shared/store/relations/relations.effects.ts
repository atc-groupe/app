import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as RelationsActions from './relations.actions';
import { RelationsService } from '../../services/relations.service';
import { catchError, map, of, switchMap } from 'rxjs';
import { RelationType } from '../../enums/relation-type.enum';

@Injectable()
export class RelationsEffects {
  tryFetchCustomersEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RelationsActions.tryFetchCustomersAction),
      switchMap(() =>
        this._relationsService.findAll(RelationType.Client).pipe(
          map((customers) =>
            RelationsActions.fetchCustomersSuccessAction({ customers }),
          ),
          catchError((err) => of(RelationsActions.fetchRelationsErrorAction)),
        ),
      ),
    ),
  );

  tryFetchRelationContactsEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(RelationsActions.tryFetchRelationContactsAction),
      switchMap(({ id }) =>
        this._relationsService.findContacts(id).pipe(
          map((contacts) =>
            RelationsActions.fetchRelationContactsSuccessAction({ contacts }),
          ),
          catchError((err) => of(RelationsActions.fetchRelationErrorAction)),
        ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _relationsService: RelationsService,
  ) {}
}
