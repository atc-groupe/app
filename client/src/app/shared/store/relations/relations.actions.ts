import { createAction, props } from '@ngrx/store';
import {
  RelationContact,
  RelationList,
} from '../../interfaces/relation.interface';

// ------ Relations list ------
export const fetchRelationsErrorAction = createAction(
  '[relations] fetch relations error',
  props<{ error: string }>(),
);

//  Customers list
export const tryFetchCustomersAction = createAction(
  '[relations] try fetch customers',
);
export const fetchCustomersSuccessAction = createAction(
  '[relations] fetch customers success',
  props<{ customers: RelationList }>(),
);

// ------ Relation ------
export const fetchRelationErrorAction = createAction(
  '[relations] fetch relation error',
  props<{ error: string | null }>(),
);

// Relation contacts
export const tryFetchRelationContactsAction = createAction(
  '[relations] try fetch relation contacts',
  props<{ id: string }>(),
);
export const fetchRelationContactsSuccessAction = createAction(
  '[relations] fetch relation success',
  props<{ contacts: RelationContact[] }>(),
);
