import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SYSTEM_REDUCER_KEY, SystemState } from './system.reducer';
import { SystemList } from '../../interfaces/system.interface';

const selectSystemState =
  createFeatureSelector<SystemState>(SYSTEM_REDUCER_KEY);

export const selectJobStatusList = createSelector(
  selectSystemState,
  (state: SystemState): SystemList => state.pullDownLists.jobStatus,
);
export const selectSystemFetchError = createSelector(
  selectSystemState,
  (state: SystemState): string | null => state.fetchError,
);
