import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SystemService } from '../../services/system.service';
import * as SystemActions from './system.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { SystemPullDownList } from '../../enums';

@Injectable()
export class SystemEffects {
  tryFetchJobStatusEffect$ = createEffect(() =>
    this._actions$.pipe(
      ofType(SystemActions.tryFetchJobStatus),
      switchMap(() =>
        this._systemService
          .fetchPullDownList(SystemPullDownList.JobStatus)
          .pipe(
            map((items) =>
              SystemActions.fetchJobStatusSuccessAction({ items }),
            ),
            catchError((err) =>
              of(
                SystemActions.fetchJobStatusErrorAction({
                  error: err.error.message,
                }),
              ),
            ),
          ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _systemService: SystemService,
  ) {}
}
