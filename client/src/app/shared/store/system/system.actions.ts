import { createAction, props } from '@ngrx/store';
import { SystemList } from '../../interfaces/system.interface';

// ------ PullDownLists ------
export const tryFetchJobStatus = createAction(
  '[system] try fetch pull down list',
);
export const fetchJobStatusSuccessAction = createAction(
  '[system] fetch pull down list success',
  props<{ items: SystemList }>(),
);
export const fetchJobStatusErrorAction = createAction(
  '[system] fetch pull down list error',
  props<{ error: string }>(),
);
