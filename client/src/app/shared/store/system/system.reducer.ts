import { SystemList } from '../../interfaces/system.interface';
import { createReducer, on } from '@ngrx/store';
import * as SystemActions from './system.actions';

export const SYSTEM_REDUCER_KEY = 'system';

export interface SystemState {
  pullDownLists: {
    jobStatus: SystemList;
  };
  fetchError: string | null;
}

const INITIAL_STATE: SystemState = {
  pullDownLists: {
    jobStatus: [],
  },
  fetchError: null,
};

export const systemReducer = createReducer(
  INITIAL_STATE,
  on(
    SystemActions.fetchJobStatusSuccessAction,
    (state, { items }): SystemState => {
      return {
        ...state,
        pullDownLists: {
          ...state.pullDownLists,
          jobStatus: items,
        },
        fetchError: null,
      };
    },
  ),
  on(
    SystemActions.fetchJobStatusErrorAction,
    (state, { error }): SystemState => {
      return {
        ...state,
        fetchError: error,
      };
    },
  ),
);
