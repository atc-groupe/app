import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigationAction } from '@ngrx/router-store';
import { map, of, switchMap } from 'rxjs';
import { Router } from '@angular/router';
import { AuthorizationsService } from '../../services/authorizations.service';
import { Location } from '@angular/common';

@Injectable()
export class RouterEffects {
  handleRouteRequestEffect$ = createEffect(
    () =>
      this._actions$.pipe(
        ofType(routerNavigationAction),
        switchMap(({ payload }) => {
          return this._authService
            .canActivateRoute(payload.routerState.url)
            .pipe(
              map((result) => {
                if (!result) {
                  return this._router.navigateByUrl(this._router.url);
                }

                return;
              }),
            );
        }),
      ),
    { dispatch: false },
  );
  constructor(
    private _actions$: Actions,
    private _router: Router,
    private _location: Location,
    private _authService: AuthorizationsService,
  ) {}
}
