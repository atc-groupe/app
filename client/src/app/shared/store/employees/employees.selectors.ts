import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EMPLOYEES_REDUCER_KEY, EmployeesState } from './employees.reducer';
import { Employee } from '../../interfaces';

const selectEmployeesState = createFeatureSelector<EmployeesState>(
  EMPLOYEES_REDUCER_KEY,
);

export const selectEmployeesList = createSelector(
  selectEmployeesState,
  (state: EmployeesState): Employee[] => state.employees.items,
);
export const selectFetchEmployeesListError = createSelector(
  selectEmployeesState,
  (state: EmployeesState): string | null => state.employees.fetchListError,
);
