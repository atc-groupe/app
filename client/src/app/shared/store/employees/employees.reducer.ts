import { createReducer, on } from '@ngrx/store';
import * as EmployeesActions from './employees.actions';
import { Employee } from '../../interfaces';

export const EMPLOYEES_REDUCER_KEY = 'employees';

export interface EmployeesState {
  employees: {
    items: Employee[];
    fetchListError: string | null;
  };
}

const INITIAL_STATE: EmployeesState = {
  employees: {
    items: [],
    fetchListError: null,
  },
};

export const employeesReducer = createReducer(
  INITIAL_STATE,
  on(
    EmployeesActions.fetchEmployeesListSuccessAction,
    (state: EmployeesState, { employees }): EmployeesState => {
      return {
        ...state,
        employees: {
          items: employees,
          fetchListError: null,
        },
      };
    },
  ),
  on(
    EmployeesActions.fetchEmployeesListErrorAction,
    (state: EmployeesState, { error }): EmployeesState => {
      return {
        ...state,
        employees: {
          items: [],
          fetchListError: error,
        },
      };
    },
  ),
);
