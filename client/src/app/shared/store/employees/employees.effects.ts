import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as EmployeesActions from './employees.actions';
import { catchError, map, of, switchMap } from 'rxjs';
import { EmployeesService } from '../../services/employees.service';

@Injectable()
export class EmployeesEffects {
  tryFetchEmployeesListEffect = createEffect(() =>
    this._actions$.pipe(
      ofType(EmployeesActions.tryFetchEmployeesListAction),
      switchMap(() =>
        this._employeesService.findAll().pipe(
          map((employees) =>
            EmployeesActions.fetchEmployeesListSuccessAction({ employees }),
          ),
          catchError((error) =>
            of(
              EmployeesActions.fetchEmployeesListErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private _actions$: Actions,
    private _employeesService: EmployeesService,
  ) {}
}
