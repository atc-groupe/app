import { createAction, props } from '@ngrx/store';
import { Employee } from '../../interfaces';

// Employees list
export const tryFetchEmployeesListAction = createAction(
  '[employees] try fetch employees list',
);
export const fetchEmployeesListSuccessAction = createAction(
  '[employees] fetch employees list success',
  props<{ employees: Employee[] }>(),
);
export const fetchEmployeesListErrorAction = createAction(
  '[employees] fetch employees list error',
  props<{ error: string }>(),
);
