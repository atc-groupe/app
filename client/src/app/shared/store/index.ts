import { USERS_REDUCER_KEY, usersReducer, UsersState } from './users';
import { ActionReducerMap } from '@ngrx/store';
import { routerReducer, RouterState } from '@ngrx/router-store';
import {
  EMPLOYEES_REDUCER_KEY,
  employeesReducer,
  EmployeesState,
} from './employees';
import {
  RELATION_REDUCER_KEY,
  relationsReducer,
  RelationsState,
} from './relations';
import { SYSTEM_REDUCER_KEY, systemReducer, SystemState } from './system';

export interface AppState {
  [USERS_REDUCER_KEY]: UsersState;
  [EMPLOYEES_REDUCER_KEY]: EmployeesState;
  [RELATION_REDUCER_KEY]: RelationsState;
  [SYSTEM_REDUCER_KEY]: SystemState;
  router: RouterState;
}

export const APP_REDUCERS: ActionReducerMap<AppState> = {
  [USERS_REDUCER_KEY]: usersReducer,
  [EMPLOYEES_REDUCER_KEY]: employeesReducer,
  [RELATION_REDUCER_KEY]: relationsReducer,
  [SYSTEM_REDUCER_KEY]: systemReducer,
  router: routerReducer,
};
