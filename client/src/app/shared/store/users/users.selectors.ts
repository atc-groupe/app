import { createFeatureSelector, createSelector } from '@ngrx/store';
import { USERS_REDUCER_KEY, UsersState } from './users.reducer';
import {
  AppUser,
  CompanyUser,
  CustomerUser,
  Users,
  UserType,
} from '../../interfaces';

export const selectUserState =
  createFeatureSelector<UsersState>(USERS_REDUCER_KEY);

// Authenticated user

export const selectAuthenticatedUser = createSelector(
  selectUserState,
  (state: UsersState): AppUser | null => state.authenticatedUser.user,
);

export const selectCompanyUser = createSelector(
  selectUserState,
  (state: UsersState): CompanyUser | null =>
    state.authenticatedUser.user?.type === UserType.Employee
      ? (state.authenticatedUser.user as CompanyUser)
      : null,
);

export const selectCustomerUser = createSelector(
  selectUserState,
  (state: UsersState): CustomerUser | null =>
    state.authenticatedUser.user?.type === UserType.Customer
      ? (state.authenticatedUser.user as CustomerUser)
      : null,
);

export const selectIsAuthenticated = createSelector(
  selectUserState,
  (state: UsersState): boolean | null =>
    state.authenticatedUser.isAuthenticated,
);

export const selectAuthenticationError = createSelector(
  selectUserState,
  (state: UsersState): string | null =>
    state.authenticatedUser.authenticationError,
);

export const selectAuthenticatedUserHandleSettingError = createSelector(
  selectUserState,
  (state: UsersState): string | null =>
    state.authenticatedUser.handleSettingError,
);

export const selectAuthenticatedUserHandleSettingSuccess = createSelector(
  selectUserState,
  (state: UsersState): boolean | null =>
    state.authenticatedUser.handleSettingSuccess,
);

// Users list
export const selectUsersList = createSelector(
  selectUserState,
  (state: UsersState): Users => state.users,
);
export const selectEmployeeUsers = createSelector(
  selectUserState,
  (state: UsersState): CompanyUser[] => state.users[UserType.Employee],
);
export const selectCustomersUsers = createSelector(
  selectUserState,
  (state: UsersState): CustomerUser[] => state.users[UserType.Customer],
);
export const selectFetchUsersListError = createSelector(
  selectUserState,
  (state: UsersState): string | null => state.users.fetchListError,
);
