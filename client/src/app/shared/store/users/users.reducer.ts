import { AppUser, CompanyUser, CustomerUser, UserType } from '../../interfaces';
import { createReducer, on } from '@ngrx/store';
import * as UsersActions from './users.actions';
import { Authorization } from '../../interfaces';

export const USERS_REDUCER_KEY = 'users';

export interface UsersState {
  users: {
    [UserType.Employee]: CompanyUser[];
    [UserType.Customer]: CustomerUser[];
    fetchListError: string | null;
  };
  authenticatedUser: {
    user: AppUser | null;
    isAuthenticated: boolean | null;
    authenticationError: string | null;
    authorizations: Authorization[] | null;
    fetchAuthorizationsError: string | null;
    handleSettingSuccess: boolean | null;
    handleSettingError: string | null;
  };
}

const INITIAL_STATE: UsersState = {
  users: {
    [UserType.Employee]: [],
    [UserType.Customer]: [],
    fetchListError: null,
  },
  authenticatedUser: {
    user: null,
    isAuthenticated: null,
    authenticationError: null,
    authorizations: null,
    fetchAuthorizationsError: null,
    handleSettingSuccess: null,
    handleSettingError: null,
  },
};

export const usersReducer = createReducer(
  INITIAL_STATE,
  on(
    UsersActions.signInSuccessAction,
    (state: UsersState, { user }: { user: AppUser }): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          user: user,
          isAuthenticated: true,
          authenticationError: null,
        },
      };
    },
  ),
  on(
    UsersActions.signInErrorAction,
    (state: UsersState, { error }: { error: string }): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          user: null,
          isAuthenticated: false,
          authenticationError: error,
        },
      };
    },
  ),
  on(
    UsersActions.fetchAuthenticatedUserSuccessAction,
    (state: UsersState, { user }: { user: AppUser | null }): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          user: user,
          isAuthenticated: user !== null,
          authenticationError: null,
        },
      };
    },
  ),
  on(
    UsersActions.fetchAuthenticatedUserErrorAction,
    (state: UsersState): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          user: null,
          isAuthenticated: false,
        },
      };
    },
  ),
  on(
    UsersActions.tryUpsertAuthenticatedUserSettingAction,
    UsersActions.tryRemoveAuthenticatedUserSettingAction,
    (state: UsersState): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          handleSettingError: null,
          handleSettingSuccess: null,
        },
      };
    },
  ),
  on(
    UsersActions.handleAuthenticatedUserSettingSuccessAction,
    (state: UsersState, { user }): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          user,
          handleSettingSuccess: true,
          handleSettingError: null,
        },
      };
    },
  ),
  on(
    UsersActions.handleAuthenticatedUserSettingErrorAction,
    (state: UsersState, { error }): UsersState => {
      return {
        ...state,
        authenticatedUser: {
          ...state.authenticatedUser,
          handleSettingSuccess: false,
          handleSettingError: error,
        },
      };
    },
  ),
  on(
    UsersActions.fetchUsersListSuccessAction,
    (state: UsersState, { users }): UsersState => {
      return {
        ...state,
        users: {
          ...users,
          fetchListError: null,
        },
      };
    },
  ),
  on(
    UsersActions.fetchUsersListErrorAction,
    (state: UsersState, { error }): UsersState => {
      return {
        ...state,
        users: {
          [UserType.Employee]: [],
          [UserType.Customer]: [],
          fetchListError: error,
        },
      };
    },
  ),
  on(UsersActions.logoutSuccessAction, (state): UsersState => {
    return {
      ...state,
      authenticatedUser: {
        ...state.authenticatedUser,
        user: null,
        isAuthenticated: false,
        authorizations: null,
      },
    };
  }),
);
