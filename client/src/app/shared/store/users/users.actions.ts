import { createAction, props } from '@ngrx/store';
import { AppUser, UserCredentials, Users, UserSetting } from '../../interfaces';

// Sign-in
export const trySignInAction = createAction(
  '[users] try sign in',
  props<{ credentials: UserCredentials }>(),
);
export const signInSuccessAction = createAction(
  '[users] sign in success',
  props<{ user: AppUser }>(),
);
export const signInErrorAction = createAction(
  '[users] sign-in error',
  props<{ error: string }>(),
);

// Logout
export const tryLogOutAction = createAction('[user] try logout');
export const logoutSuccessAction = createAction('[user] logout success');

// Fetch user
export const tryFetchAuthenticatedUserAction = createAction(
  '[users] try fetch authenticated user',
);
export const fetchAuthenticatedUserSuccessAction = createAction(
  '[users] fetch authenticated user success',
  props<{ user: AppUser | null }>(),
);
export const fetchAuthenticatedUserErrorAction = createAction(
  '[users] fetch authenticated user error',
);

// Fetch Users list
export const tryFetchUsersListAction = createAction('[users] try fetch users');
export const fetchUsersListSuccessAction = createAction(
  '[users] fetch users success',
  props<{ users: Users }>(),
);
export const fetchUsersListErrorAction = createAction(
  '[users] fetch users error',
  props<{ error: string }>(),
);

// Authenticated user settings
export const tryUpsertAuthenticatedUserSettingAction = createAction(
  '[users] try upsert authenticated user setting',
  props<{ setting: UserSetting }>(),
);
export const tryRemoveAuthenticatedUserSettingAction = createAction(
  '[users] try remove authenticated user setting',
  props<{ settingName: string }>(),
);
export const tryUpdateAuthenticatedUserSettingsAction = createAction(
  '[users] try update many authenticated user settings',
  props<{ settings: UserSetting[] }>(),
);
export const handleAuthenticatedUserSettingSuccessAction = createAction(
  '[users] handle authenticated user setting success',
  props<{ user: AppUser }>(),
);
export const handleAuthenticatedUserSettingErrorAction = createAction(
  '[users] handle authenticated user setting error',
  props<{ error: string }>(),
);
