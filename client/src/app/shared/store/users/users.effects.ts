import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UsersService } from '../../services/users.service';
import * as UsersActions from './users.actions';
import * as UsersSelectors from './users.selectors';
import {
  catchError,
  EMPTY,
  map,
  of,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs';
import {
  AppUser,
  UserCredentials,
  Users,
  UserSetting,
  UserType,
} from '../../interfaces';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root',
})
export class UsersEffects {
  public trySignInEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.trySignInAction),
      switchMap(({ credentials }: { credentials: UserCredentials }) =>
        this._userService.signIn(credentials).pipe(
          map((user: AppUser) => {
            this._store.dispatch(UsersActions.signInSuccessAction({ user }));
            return user;
          }),
          tap((user) => {
            const route =
              user.type === UserType.Employee ? 'company' : 'customer';

            return this._router.navigateByUrl(route);
          }),
          catchError((error: any) =>
            of(UsersActions.signInErrorAction({ error: error.error.message })),
          ),
        ),
      ),
    ),
  );

  public tryFetchAuthenticatedUserEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.tryFetchAuthenticatedUserAction),
      switchMap(() =>
        this._userService.fetchAuthenticatedUser().pipe(
          map((user: AppUser | null) =>
            UsersActions.fetchAuthenticatedUserSuccessAction({ user }),
          ),
          catchError(() =>
            of(UsersActions.fetchAuthenticatedUserErrorAction()),
          ),
        ),
      ),
    ),
  );

  public tryUpsertAuthenticatedUserSettingEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.tryUpsertAuthenticatedUserSettingAction),
      withLatestFrom(
        this._store.select(UsersSelectors.selectAuthenticatedUser),
      ),
      switchMap(([{ setting }, user]) => {
        if (!user) {
          return EMPTY;
        }

        return this._userService.upsertSetting(user._id, setting).pipe(
          map((user: AppUser) =>
            UsersActions.handleAuthenticatedUserSettingSuccessAction({ user }),
          ),
          catchError((err) =>
            of(
              UsersActions.handleAuthenticatedUserSettingErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        );
      }),
    ),
  );

  public tryUpsertAuthenticatedUserSettingsEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.tryUpdateAuthenticatedUserSettingsAction),
      withLatestFrom(
        this._store.select(UsersSelectors.selectAuthenticatedUser),
      ),
      switchMap(([{ settings }, user]) => {
        if (!user) {
          return EMPTY;
        }

        return this._userService.upsertManySettings(user._id, settings).pipe(
          map((user: AppUser) =>
            UsersActions.handleAuthenticatedUserSettingSuccessAction({ user }),
          ),
          catchError((err) =>
            of(
              UsersActions.handleAuthenticatedUserSettingErrorAction({
                error: err.error.message,
              }),
            ),
          ),
        );
      }),
    ),
  );

  public tryFetchUsersListEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.tryFetchUsersListAction),
      switchMap(() =>
        this._userService.fetchUsersList().pipe(
          map((users: Users) =>
            UsersActions.fetchUsersListSuccessAction({ users }),
          ),
          catchError((error) =>
            of(
              UsersActions.fetchUsersListErrorAction({
                error: error.error.message,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  public tryLogoutEffect$ = createEffect(() =>
    this._action$.pipe(
      ofType(UsersActions.tryLogOutAction),
      switchMap(() =>
        this._userService.logout().pipe(
          map(() => {
            this._router.navigateByUrl('/sign-in');
            return UsersActions.logoutSuccessAction();
          }),
        ),
      ),
    ),
  );

  constructor(
    private _action$: Actions,
    private _userService: UsersService,
    private _router: Router,
    private _store: Store,
  ) {}
}
