import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { first, map } from 'rxjs';
import { UsersSelectors, UsersState } from '../store/users';

import { UserType } from '../interfaces';
import { App } from '../enums';

export const signInGuard: CanActivateFn = () => {
  const router = inject(Router);
  return inject(Store)
    .select(UsersSelectors.selectUserState)
    .pipe(
      first(
        (state: UsersState) => state.authenticatedUser.isAuthenticated !== null,
      ),
      map(({ authenticatedUser }) => {
        if (!authenticatedUser.isAuthenticated) {
          return true;
        }

        return router.createUrlTree([
          authenticatedUser.user!.type === UserType.Employee
            ? App.Company
            : App.Customer,
        ]);
      }),
    );
};
