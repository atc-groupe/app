import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersActions, UsersSelectors } from '../store/users';
import { first, map, Observable } from 'rxjs';
import { AppUser } from '../interfaces';

export const userDataGuard: CanActivateFn = (): Observable<true> => {
  const store = inject(Store);

  return store.select(UsersSelectors.selectAuthenticatedUser).pipe(
    first(),
    map((user: AppUser | null) => {
      if (!user) {
        store.dispatch(UsersActions.tryFetchAuthenticatedUserAction());
      }

      return true;
    }),
  );
};
