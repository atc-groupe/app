import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { first, map } from 'rxjs';

import { UsersSelectors } from '../store/users';
import { AppUser, UserType } from '../interfaces';
import { App, UserRole } from '../enums';

export const companyUserTypeGuard: CanActivateFn = () => {
  const router = inject(Router);
  return inject(Store)
    .select(UsersSelectors.selectAuthenticatedUser)
    .pipe(
      first((user) => user !== null),
      map((user: AppUser | null) => {
        if (user!.type === UserType.Customer) {
          return router.createUrlTree([App.Customer]);
        }

        return true;
      }),
    );
};

export const customerUserTypeGuard: CanActivateFn = () => {
  const router = inject(Router);
  return inject(Store)
    .select(UsersSelectors.selectCompanyUser)
    .pipe(
      first((user) => user !== null),
      map((user: AppUser | null) => {
        if (user!.role.level === UserRole.Admin) {
          return true;
        }

        if (user!.type === UserType.Employee) {
          return router.createUrlTree([App.Company]);
        }

        return true;
      }),
    );
};

export const rootUserTypeGuard: CanActivateFn = () => {
  const router = inject(Router);
  return inject(Store)
    .select(UsersSelectors.selectAuthenticatedUser)
    .pipe(
      first((user) => user !== null),
      map((user: AppUser | null) => {
        return router.createUrlTree([
          user!.type === UserType.Employee ? App.Company : App.Customer,
        ]);
      }),
    );
};
