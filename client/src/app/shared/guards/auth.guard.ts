import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { UsersSelectors } from '../store/users';
import { first, map } from 'rxjs';

export const authGuard = () => {
  const router = inject(Router);
  return inject(Store)
    .select(UsersSelectors.selectIsAuthenticated)
    .pipe(
      first((v) => v !== null),
      map((isAuthenticated: boolean | null) => {
        return isAuthenticated ? true : router.createUrlTree(['sign-in']);
      }),
    );
};
