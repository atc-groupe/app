import * as CompanyAppEnums from '../../company-app/shared/enums';
import * as CustomerAppEnums from '../../customer-app/shared/enums';

export type Module = CompanyAppEnums.Module | CustomerAppEnums.Module;
export type Subject = CompanyAppEnums.Subject | CustomerAppEnums.Subject;
export type Action = CompanyAppEnums.Action | CustomerAppEnums.Action;
