export abstract class AbstractVoter {
  protected constructor(private ids: string[]) {}
  protected isId(id: string): boolean {
    return this.ids.includes(id);
  }
}
