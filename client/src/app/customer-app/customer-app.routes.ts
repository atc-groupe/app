import { Routes } from '@angular/router';
import { CustomerAppComponent } from './customer-app.component';

export const CUSTOMER_APP_ROUTES: Routes = [
  { path: '', component: CustomerAppComponent },
];
