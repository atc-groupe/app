export enum Action {
  Send = 'send',
}

export enum Subject {
  OrderModule = 'orderModule',
}
