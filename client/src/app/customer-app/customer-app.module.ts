// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreModule } from '../shared/modules/core.module';

// Components
import { CustomerAppComponent } from './customer-app.component';

// Routes
import { CUSTOMER_APP_ROUTES } from './customer-app.routes';

@NgModule({
  declarations: [CustomerAppComponent],
  imports: [CoreModule, RouterModule.forChild(CUSTOMER_APP_ROUTES)],
})
export class CustomerAppModule {}
