import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { UsersActions, UsersSelectors } from '../../shared/store/users';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
  public error$ = this._store.select(UsersSelectors.selectAuthenticationError);
  public form = this._fb.group({
    identifier: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private _fb: FormBuilder, private _store: Store) {}

  public submitForm() {
    const identifier = this.form.get('identifier')?.value;
    const password = this.form.get('password')?.value;

    if (!identifier || !password) {
      return;
    }

    this._store.dispatch(
      UsersActions.trySignInAction({ credentials: { identifier, password } }),
    );
  }
}
