# API

## Initialization procedure

### NPM
run 
```bash
npm install
```

### Environment
Create and setup the `.env.local` file.

### Db init
To initialize app DB, send a post request to the dedicated endpoint:

```bash
curl -k -X POST https://localhost:5001/api/v1/init -d '{"mpId": "multipress-id", "identifier": "sAMAccountName"}' -H "Content-Type: application/json"
```

You should have the message: `{"message":"App DB has been initialized successfully"}`

### JWT RSA Keys
Generate JWT RSA Keys:

In the terminal, go into `server` folder execute the following commands:

```bash
mkdir rsa
cd rsa
ssh-keygen -t rsa -b 4096 -m PEM -f key
ssh-keygen -e -m PEM -f key > key.pub
```
