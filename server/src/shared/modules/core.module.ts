import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseApiConfigService } from '../../config';
import { APP_FILTER } from '@nestjs/core';
import { MpHttpErrorExceptionFilter } from '../filters/mp-http-error-exception.filter';
import { LogsModule } from '../../api/logs/logs.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env.local',
      isGlobal: true,
      cache: true,
      expandVariables: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../../public/client'),
    }),
    MongooseModule.forRootAsync({
      useClass: MongooseApiConfigService,
    }),
    LogsModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: MpHttpErrorExceptionFilter,
    },
  ],
  exports: [ConfigModule, ServeStaticModule, MongooseModule],
})
export class CoreModule {}
