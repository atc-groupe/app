import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { MpHttpConfigService } from '../../config';
import { CoreModule } from './core.module';
import { MpHttpAdapter } from '../services/mp-http-adapter.service';
import { LogsModule } from '../../api/logs/logs.module';

@Module({
  imports: [
    HttpModule.registerAsync({ useClass: MpHttpConfigService }),
    CoreModule,
    LogsModule,
  ],
  providers: [MpHttpAdapter],
  exports: [HttpModule, MpHttpAdapter],
})
export class MpHttpModule {}
