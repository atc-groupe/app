import { Module } from '@nestjs/common';
import {
  CollectionResponseHelper,
  DataMappingHelper,
  QueryHelper,
} from '../services';

const SERVICES = [DataMappingHelper, QueryHelper, CollectionResponseHelper];

@Module({
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class HelperModule {}
