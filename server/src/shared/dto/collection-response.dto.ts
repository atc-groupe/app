export class CollectionResponseDto {
  documents: any[];
  documentCount?: number;
  pageCount?: number;
  pageIndex?: number;
}
