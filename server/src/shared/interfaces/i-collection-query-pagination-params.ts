export class ICollectionQueryPaginationParams {
  countPerPage?: number;
  pageIndex?: number;
}
