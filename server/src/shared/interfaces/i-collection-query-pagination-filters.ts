export interface ICollectionQueryPaginationFilters {
  skip: number;
  limit: number;
}
