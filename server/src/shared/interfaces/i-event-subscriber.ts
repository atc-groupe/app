export interface IEventSubscriber {
  /**
   * Method triggered by event emitter `dispatchEvent()` method for `name` event.
   * @param name
   * @param event
   */
  onDispatchEvent(name: string, event: any): void;
}
