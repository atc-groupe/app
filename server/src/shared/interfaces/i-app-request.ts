import { Request } from 'express';
import { User } from '../../api/users/user.schema';

export interface IAppRequest extends Request {
  user: User;
}
