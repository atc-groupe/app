import { IEventSubscriber } from './i-event-subscriber';

export interface IEventDispatcher {
  /**
   * List of registered events subscribers.
   */
  subscribers: IEventSubscriber[];

  /**
   * Used to add an event subscriber
   * @param subscriber
   */
  addSubscriber(subscriber: IEventSubscriber): void;

  /**
   * Trigger all `name` event of subscribers `onDispatchEvent()` methods.
   * @param name
   * @param event
   */
  dispatchEvent(name: string, event: any): void;
}
