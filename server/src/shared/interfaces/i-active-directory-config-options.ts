export interface IActiveDirectoryConfigOptions {
  url: string;
  baseDN: string;
  username: string;
  password: string;
  user: string[];
}
