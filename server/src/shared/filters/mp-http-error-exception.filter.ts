import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';
import { MpApiErrorException, MpHttpAdapterException } from '../exceptions';
import { LogFactoryService } from '../../api/logs/log-factory.service';
import { LogContextEnum } from '../../api/logs/enums/log-context.enum';

@Catch()
export class MpHttpErrorExceptionFilter implements ExceptionFilter {
  private _logger;
  constructor(private _loggerFactory: LogFactoryService) {
    this._logger = _loggerFactory.get(LogContextEnum.mpApiCall);
  }
  catch(exception: any, host: ArgumentsHost): any {
    if (!(exception instanceof HttpException)) {
      console.log(exception);
    }

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();

    if (exception instanceof MpApiErrorException) {
      this._logger.error(`[${exception.errNumber}] ${exception.errText}`);
    }

    if (exception instanceof MpHttpAdapterException) {
      this._logger.error(
        `[MP HTTP ADAPTER] status: ${status}. Message: ${exception.message}`,
      );
    }

    if (exception.response) {
      return response.status(status).json(exception.response);
    }

    return response.status(status).json({
      statusCode: status,
      message: exception.message,
    });
  }
}
