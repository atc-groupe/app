import { Injectable } from '@nestjs/common';
import {
  ICollectionQueryPaginationParams,
  ICollectionQueryPaginationFilters,
} from '../interfaces';

@Injectable()
export class QueryHelper {
  getPaginationFilters(
    queryParams: ICollectionQueryPaginationParams,
  ): ICollectionQueryPaginationFilters | null {
    if (!queryParams.countPerPage) {
      return null;
    }

    const pageNo: number = queryParams.pageIndex ? queryParams.pageIndex : 0;

    return {
      skip: pageNo * queryParams.countPerPage,
      limit: queryParams.countPerPage,
    };
  }

  hasNoSearchParams(queryParams: ICollectionQueryPaginationParams): boolean {
    if (Object.keys(queryParams).length === 0) {
      return true;
    }

    const hasPageIndex = queryParams.pageIndex !== undefined;

    if (
      queryParams.countPerPage &&
      hasPageIndex &&
      Object.keys(queryParams).length === 2
    ) {
      return true;
    }

    if (
      (queryParams.countPerPage || hasPageIndex) &&
      Object.keys(queryParams).length === 1
    ) {
      return true;
    }

    return false;
  }
}
