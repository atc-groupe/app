import { Injectable } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { AxiosRequestConfig } from 'axios';
import { MpApiErrorException, MpHttpAdapterException } from '../exceptions';
import { HttpService } from '@nestjs/axios';
import { LogFactoryService } from '../../api/logs/log-factory.service';
import { LogContextEnum } from '../../api/logs/enums/log-context.enum';

@Injectable()
export class MpHttpAdapter {
  private _logger;
  constructor(
    private _http: HttpService,
    private _loggerFactory: LogFactoryService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.mpApiCall);
  }

  public get<T>(url: string, config?: AxiosRequestConfig): Observable<T> {
    return this._http.get(url, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => {
        if (err instanceof MpApiErrorException) {
          throw err;
        }

        throw new MpHttpAdapterException(err);
      }),
    );
  }

  public post<T>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Observable<T> {
    return this._http.post(url, data, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => {
        if (err instanceof MpApiErrorException) {
          throw err;
        }

        throw new MpHttpAdapterException(err);
      }),
    );
  }

  public put<T>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Observable<T> {
    return this._http.put(url, data, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => {
        if (err instanceof MpApiErrorException) {
          throw err;
        }

        throw new MpHttpAdapterException(err);
      }),
    );
  }

  public delete<T>(url: string, config?: AxiosRequestConfig): Observable<T> {
    return this._http.delete(url, config).pipe(
      map(({ data }): T => {
        if (data.errornumber) {
          throw new MpApiErrorException(data.errornumber, data.errortext);
        }

        return data;
      }),
      catchError((err) => {
        if (err instanceof MpApiErrorException) {
          throw err;
        }

        throw new MpHttpAdapterException(err);
      }),
    );
  }
}
