import { Injectable } from '@nestjs/common';

@Injectable()
export class DataMappingHelper {
  getDateOrNull(rawDate: string): string | null {
    if (rawDate === '0000-00-00T00:00:00Z') {
      return null;
    }

    const date = new Date(rawDate);
    return date.toLocaleDateString('en-US');
  }
}
