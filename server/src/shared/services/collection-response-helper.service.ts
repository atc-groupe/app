import { Injectable } from '@nestjs/common';
import { CollectionResponseDto } from '../dto/collection-response.dto';
import { ICollectionQueryPaginationParams } from '../interfaces';

@Injectable()
export class CollectionResponseHelper {
  async setPaginationData(
    collection: CollectionResponseDto,
    queryParams: ICollectionQueryPaginationParams,
    docCountQuery: Promise<number>,
  ): Promise<void> {
    if (!queryParams.countPerPage) {
      return;
    }

    const pageIndex = queryParams.pageIndex ? queryParams.pageIndex : 0;
    const docCount = await docCountQuery;

    collection.documentCount = docCount;
    collection.pageCount = Math.ceil(docCount / queryParams.countPerPage);
    collection.pageIndex = pageIndex;
  }
}
