export enum MpApiJobsErrorEnum {
  JOB_OR_QUOTATION_NOT_FOUND = 600,
  SUB_JOB_OR_QUOTATION_NOT_FOUND = 601,
  JOB_OR_QUOTATION_IN_USE = 602,
  SUB_JOB_OR_QUOTATION_IN_USE = 603,
  DELIVERY_ADDRESS_LINE_NOT_FOUND = 604,
  DELIVERY_ADDRESS_LINE_IN_USE = 605,
  NOT_POSSIBLE_TO_CHANGE_THIS_DATA = 606,
  INVALID_DATA_FOUND = 607,
}
