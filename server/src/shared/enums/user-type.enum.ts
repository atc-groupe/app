export enum UserTypeEnum {
  Employee = 'employee',
  Customer = 'customer',
}
