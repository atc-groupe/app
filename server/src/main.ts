import express = require('express');
import * as https from 'https';
import { readFileSync } from 'fs';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ExpressAdapter } from '@nestjs/platform-express';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import cookieParser = require('cookie-parser');

async function bootstrap() {
  const server = express();
  const app = await NestFactory.create(AppModule, new ExpressAdapter(server));
  const configService = app.get(ConfigService);

  const cookieParserSecret = configService.getOrThrow('COOKIE_PARSER_SECRET');
  const httpsKey = configService.getOrThrow<string>('HTTPS_SSL_KEY');
  const httpsCert = configService.getOrThrow<string>('HTTPS_SSL_CERT');
  const httpsPort = configService.getOrThrow<number>('HTTPS_PORT');

  app.setGlobalPrefix('api');
  app.enableCors();
  app.use(cookieParser(cookieParserSecret));
  app.enableVersioning({ type: VersioningType.URI, defaultVersion: '1' });
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  );

  await app.init();

  const httpsOptions = {
    key: readFileSync(httpsKey),
    cert: readFileSync(httpsCert),
  };

  https.createServer(httpsOptions, server).listen(httpsPort);
}
bootstrap();
