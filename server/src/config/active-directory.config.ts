import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IActiveDirectoryConfigOptions } from '../shared/interfaces/i-active-directory-config-options';

@Injectable()
export class ActiveDirectoryConfig {
  private readonly adUrl;
  private readonly adBaseDn;
  private readonly adUsername;
  private readonly adPassword;

  constructor(private _configService: ConfigService) {
    this.adUrl = this._configService.getOrThrow('AD_URL');
    this.adBaseDn = this._configService.getOrThrow('AD_BASE_DN');
    this.adUsername = this._configService.getOrThrow('AD_USERNAME');
    this.adPassword = this._configService.getOrThrow('AD_PASSWORD');
  }

  getConfigOptions(): IActiveDirectoryConfigOptions {
    return {
      url: this.adUrl,
      baseDN: this.adBaseDn,
      username: this.adUsername,
      password: this.adPassword,
      user: [
        'distinguishedName',
        'userPrincipalName',
        'sAMAccountName',
        'mail',
        'lockoutTime',
        'pwdLastSet',
        'userAccountControl',
        'employeeID',
        'sn',
        'givenName',
        'initials',
        'cn',
        'displayName',
        'comment',
        'description',
      ],
    };
  }
}
