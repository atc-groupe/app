import { Injectable } from '@nestjs/common';
import { HttpModuleOptions, HttpModuleOptionsFactory } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MpHttpConfigService implements HttpModuleOptionsFactory {
  constructor(private configService: ConfigService) {}

  createHttpOptions(): HttpModuleOptions {
    return {
      baseURL: this._getEnv('MP_API_BASE_URL'),
      headers: {
        authorization: `Bearer ${this._getEnv('MP_API_TOKEN')}`,
      },
    };
  }

  private _getEnv(key: string): string {
    return this.configService.getOrThrow<string>(key);
  }
}
