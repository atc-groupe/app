import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class MetaJobsStatus extends Document {
  @Prop({ type: String, isRequired: true })
  id: string;

  @Prop({ type: String, isRequired: true, unique: true })
  name: string;

  @Prop({ type: String, isRequired: true, unique: true })
  label: string;

  @Prop({ type: String, isRequired: true })
  color: string;

  @Prop({ type: String, isRequired: true })
  textColor: string;

  @Prop({ type: Number, isRequired: true })
  number: number;
}

export const MetaJobsStatusSchema =
  SchemaFactory.createForClass(MetaJobsStatus);
