import { IsString } from 'class-validator';

export class MetaJobsStatusDto {
  @IsString()
  label: string;

  @IsString()
  color: string;

  @IsString()
  textColor: string;
}
