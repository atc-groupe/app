import { Module } from '@nestjs/common';
import { MetaJobsStatusService } from './meta-jobs-status.service';
import { MetaJobsStatusController } from './meta-jobs-status.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  MetaJobsStatus,
  MetaJobsStatusSchema,
} from './meta-jobs-status.schema';
import { SystemModule } from '../../../system/system.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MetaJobsStatus.name, schema: MetaJobsStatusSchema },
    ]),
    SystemModule,
  ],
  providers: [MetaJobsStatusService],
  controllers: [MetaJobsStatusController],
})
export class MetaJobsStatusModule {}
