import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpException,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Patch,
} from '@nestjs/common';
import { MetaJobsStatusService } from './meta-jobs-status.service';
import { MetaJobsStatusDto } from './meta-jobs-status.dto';
import { MetaJobsStatus } from './meta-jobs-status.schema';

@Controller('meta/jobs/status')
export class MetaJobsStatusController {
  constructor(private _statusService: MetaJobsStatusService) {}

  @Get('sync')
  async syncFormMultiPress() {
    try {
      await this._statusService.syncFromMultiPress();
    } catch (err) {
      const message =
        err.name === 'MongoError' && err.code === 11000
          ? 'Un statut avec ce nom existe déjà.'
          : "Un problème est survenu lors de l'insertion des données";

      throw new BadRequestException(message, {
        cause: err,
      });
    }
  }

  @Get()
  async findAll(): Promise<MetaJobsStatus[]> {
    try {
      return await this._statusService.findAll();
    } catch (err) {
      throw new InternalServerErrorException(
        'Un problème est survenu lors de la récupération des données',
        { cause: err },
      );
    }
  }

  @Patch(':id')
  async updateOne(
    @Param('id') id: string,
    @Body() dto: MetaJobsStatusDto,
  ): Promise<MetaJobsStatus> {
    try {
      const status = await this._statusService.updateOne(id, dto);

      if (!status) {
        throw new NotFoundException('Status not found');
      }

      return status;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      const message =
        err.name === 'MongoError' && err.code === 11000
          ? 'Un statut avec ce nom existe déjà.'
          : "Un problème est survenu lors de l'insertion des données";

      throw new BadRequestException(message, {
        cause: err,
      });
    }
  }
}
