import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MetaJobsStatus } from './meta-jobs-status.schema';
import { Model } from 'mongoose';
import { MetaJobsStatusDto } from './meta-jobs-status.dto';
import { SystemService } from '../../../system/system.service';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class MetaJobsStatusService {
  constructor(
    @InjectModel(MetaJobsStatus.name)
    private _statusModel: Model<MetaJobsStatus>,
    private _systemService: SystemService,
  ) {}

  findAll(): Promise<MetaJobsStatus[]> {
    return this._statusModel.find().sort({ number: 'asc' }).exec();
  }

  updateOne(
    id: string,
    dto: MetaJobsStatusDto,
  ): Promise<MetaJobsStatus | null> {
    return this._statusModel
      .findByIdAndUpdate(id, dto, { new: true, runValidators: true })
      .exec();
  }

  async syncFromMultiPress(): Promise<void> {
    const appList = await this.findAll();
    const mpPullDownList = await firstValueFrom(
      this._systemService.findPullDownListByName('jobStatus'),
    );

    if (!mpPullDownList) {
      return;
    }

    for (const mpItem of mpPullDownList) {
      const status = appList.find((appStatus) => appStatus.id === mpItem.id);

      status
        ? await this._statusModel
            .findOneAndUpdate(
              { _id: status._id },
              {
                number: mpItem.number,
                name: mpItem.label,
                label: mpItem.label,
              },
            )
            .exec()
        : await this._statusModel.create({
            id: mpItem.id,
            name: mpItem.label,
            label: mpItem.label,
            color: '',
            textColor: '',
            number: mpItem.number,
          });
    }

    for (const appListItem of appList) {
      const mpItem = mpPullDownList.find(
        (mpItem) => mpItem.id === appListItem.id,
      );

      if (!mpItem) {
        await this._statusModel.findByIdAndRemove(appListItem._id);
      }
    }
  }
}
