import { Module } from '@nestjs/common';
import { MetaJobsStatusModule } from './meta-jobs-status/meta-jobs-status.module';

@Module({
  imports: [MetaJobsStatusModule],
})
export class JobsModule {}
