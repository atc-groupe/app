import { Module } from '@nestjs/common';
import { MpEmployeesListService } from './mp-employees-list.service';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { MpEmployeeService } from './mp-employee.service';
import { EmployeesController } from './employees.controller';

@Module({
  imports: [MpHttpModule],
  providers: [MpEmployeesListService, MpEmployeeService],
  controllers: [EmployeesController],
  exports: [MpEmployeesListService, MpEmployeeService],
})
export class EmployeesModule {}
