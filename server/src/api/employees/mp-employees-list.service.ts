import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { IMpEmployeeList, IMpEmployeeListItem } from './interfaces';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';

@Injectable()
export class MpEmployeesListService {
  constructor(private _http: MpHttpAdapter) {}

  findAll(): Observable<IMpEmployeeListItem[]> {
    return this._http.get<IMpEmployeeList>('/employees/getEmployeesList').pipe(
      map(({ employees }): IMpEmployeeListItem[] => {
        return employees.sort((a, b) => {
          const nameA = a.name.toUpperCase();
          const nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          return 0;
        });
      }),
    );
  }

  findOneByEmail(email: string): Observable<IMpEmployeeListItem | null> {
    const params = {
      filter: JSON.stringify([
        {
          field: 'email',
          comparator: '=',
          value: email,
        },
      ]),
    };

    return this._http
      .get<IMpEmployeeList>('/employees/getEmployeesList', { params })
      .pipe(
        map(({ employees }): IMpEmployeeListItem | null => {
          return employees.length === 1 ? employees[0] : null;
        }),
      );
  }
}
