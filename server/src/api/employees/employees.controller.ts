import { Controller, Get } from '@nestjs/common';
import { MpEmployeesListService } from './mp-employees-list.service';
import { Observable } from 'rxjs';
import { IEmployeeListItem } from './interfaces';

@Controller('employees')
export class EmployeesController {
  constructor(private _employeesService: MpEmployeesListService) {}

  @Get()
  findAll(): Observable<IEmployeeListItem[]> {
    return this._employeesService.findAll();
  }
}
