export interface IEmployeeListItem {
  id: string;
  employee_number: number;
  name: string;
  department: string;
}
