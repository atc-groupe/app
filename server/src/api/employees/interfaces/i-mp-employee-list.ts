import { IMpEmployeeListItem } from './i-mp-employee-list-item';

export interface IMpEmployeeList {
  employees: IMpEmployeeListItem[];
}
