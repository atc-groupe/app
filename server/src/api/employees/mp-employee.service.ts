import { Injectable } from '@nestjs/common';
import { catchError, Observable, of } from 'rxjs';
import { IMpEmployee } from './interfaces';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiEmployeesErrorEnum } from '../../shared/enums';

@Injectable()
export class MpEmployeeService {
  constructor(private _http: MpHttpAdapter) {}

  findOneById(mpId: string): Observable<IMpEmployee | null> {
    return this._http
      .get<IMpEmployee>('/employees/getEmployeeInfo', {
        params: { id: mpId },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiEmployeesErrorEnum.RECORD_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
