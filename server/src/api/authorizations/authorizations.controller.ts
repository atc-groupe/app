import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpException,
  InternalServerErrorException,
  Patch,
  Query,
} from '@nestjs/common';
import { AuthorizationsService } from './authorizations.service';
import { AuthorizationDto } from './authorization.dto';
import { RoleActionEnum } from './enums';
import { Authorization } from './authorizations.schema';

@Controller({
  path: 'authorizations',
  version: '1',
})
export class AuthorizationsController {
  constructor(private _authService: AuthorizationsService) {}

  @Get()
  async findByRole(@Query('roleId') roleId: string): Promise<Authorization[]> {
    try {
      return this._authService.findByRoleId(roleId);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Un problème est survenu lors de l'opération.",
        {
          cause: err,
        },
      );
    }
  }

  @Patch('roles')
  async updateAuthorizationRole(@Body() authDto: AuthorizationDto) {
    try {
      return authDto.roleAction === RoleActionEnum.Add
        ? await this._authService.addRole(authDto)
        : await this._authService.removeRole(authDto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Oups, une erreur est survenue. Veuillez quitter le menu et recommencer l'opération. Si l'erreur persiste veuillez contacter l'administrateur",
        { cause: err },
      );
    }
  }
}
