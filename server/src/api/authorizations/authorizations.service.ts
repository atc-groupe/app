import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Authorization } from './authorizations.schema';
import { Model, Types } from 'mongoose';
import { Role } from '../roles/roles.schema';
import { AuthorizationDto } from './authorization.dto';
import { RolesService } from '../roles/roles.service';
import { ActionEnum, SubjectEnum } from './enums';
import { UserTypeEnum } from '../../shared/enums';

@Injectable()
export class AuthorizationsService {
  constructor(
    @InjectModel(Authorization.name) private _authModel: Model<Authorization>,
    @Inject(forwardRef(() => RolesService))
    private _rolesService: RolesService,
  ) {}

  async addRole(authDto: AuthorizationDto) {
    const role = await this._getRoleFromId(authDto.roleId);
    const auth = await this._getFromUserTypeSubjectAndAction(
      authDto.userType,
      authDto.subject,
      authDto.action,
    );

    if (!auth) {
      return this._insertOne(authDto, role._id);
    }

    return this._authModel
      .updateOne(
        { subject: auth.subject, action: auth.action },
        { $push: { roles: role } },
      )
      .exec();
  }

  async removeRole(authDto: AuthorizationDto) {
    const role = await this._getRoleFromId(authDto.roleId);
    const auth = await this._getFromUserTypeSubjectAndAction(
      authDto.userType,
      authDto.subject,
      authDto.action,
    );

    if (!auth) {
      throw new BadRequestException('Authorization not found');
    }

    return this._authModel
      .updateOne(
        { subject: auth.subject, action: auth.action },
        { $pull: { roles: role._id } },
      )
      .exec();
  }

  async findByRoleId(id: string): Promise<Authorization[]> {
    const role = await this._rolesService.findOneById(id);

    if (!role) {
      return [];
    }

    return this._authModel.find({ roles: role._id }).exec();
  }

  async findListByUserTypeAndRoleId(
    userType: UserTypeEnum,
    roleId: string,
  ): Promise<Authorization[]> {
    const role = await this._rolesService.findOneById(roleId);

    if (!role) {
      return [];
    }

    return this._authModel
      .find({ userType, roles: role._id })
      .select('-userType -roles -__v')
      .exec();
  }

  private _getFromUserTypeSubjectAndAction(
    userType: UserTypeEnum,
    subject: SubjectEnum,
    action: ActionEnum,
  ): Promise<Authorization | null> {
    return this._authModel.findOne({ userType, subject, action }).exec();
  }

  private async _getRoleFromId(roleId: string): Promise<Role> {
    const role = await this._rolesService.findOneById(roleId);

    if (!role) {
      throw new BadRequestException(
        "Impossible d'enregistrer cette autorisation car le role utilisateur est introuvable",
      );
    }

    return role;
  }

  private _insertOne(authDto: AuthorizationDto, role: Types.ObjectId) {
    const newAuth = new this._authModel({
      userType: authDto.userType,
      subject: authDto.subject,
      action: authDto.action,
      roles: [role],
    });

    return newAuth.save();
  }
}
