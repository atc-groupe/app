export enum SubjectEnum {
  Users = 'users',
  Roles = 'roles',
  DeliveryModule = 'deliveryModule',
  Job = 'job',
  Stock = 'stock',
  PaoReservations = 'paoReservations',
}
