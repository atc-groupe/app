export enum ActionEnum {
  view = 'view',
  create = 'create',
  update = 'update',
  delete = 'delete',
  manage = 'manage', // View, create, update & delete
  changeStatus = 'changeStatus',
  managePao = 'managePao',
  updateDeliveryComment = 'updateDeliveryComment',
  Decrease = 'decrease',
  Handle = 'handle',
}
