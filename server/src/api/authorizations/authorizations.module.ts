import { forwardRef, Module } from '@nestjs/common';
import { AuthorizationsService } from './authorizations.service';
import { AuthorizationsController } from './authorizations.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Authorization, AuthorizationSchema } from './authorizations.schema';
import { RolesModule } from '../roles/roles.module';

@Module({
  imports: [
    forwardRef(() => RolesModule),
    MongooseModule.forFeature([
      { name: Authorization.name, schema: AuthorizationSchema },
    ]),
  ],
  providers: [AuthorizationsService],
  controllers: [AuthorizationsController],
  exports: [AuthorizationsService],
})
export class AuthorizationsModule {}
