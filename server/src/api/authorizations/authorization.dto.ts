import { ActionEnum, RoleActionEnum, SubjectEnum } from './enums';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { UserTypeEnum } from '../../shared/enums';

export class AuthorizationDto {
  @IsNotEmpty({ message: 'User type is required' })
  @IsEnum(UserTypeEnum, { message: 'Invalid user type' })
  userType: UserTypeEnum;

  @IsNotEmpty({ message: 'Subject is required' })
  @IsEnum(RoleActionEnum, { message: 'Invalid role action' })
  roleAction: RoleActionEnum;

  @IsNotEmpty({ message: 'Subject is required' })
  @IsEnum(SubjectEnum, { message: 'Invalid subject value' })
  subject: SubjectEnum;

  @IsNotEmpty({ message: 'Action is required' })
  @IsEnum(ActionEnum, { message: 'Invalid action value' })
  action: ActionEnum;

  @IsNotEmpty({ message: 'Role id is required' })
  @IsString({ message: 'Invalid role value' })
  roleId: string;
}
