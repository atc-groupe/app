import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SubjectEnum, ActionEnum } from './enums';
import { Document, Types } from 'mongoose';
import { Role } from '../roles/roles.schema';
import { UserTypeEnum } from '../../shared/enums';

@Schema()
export class Authorization extends Document {
  @Prop({ type: String, required: true })
  userType: UserTypeEnum;

  @Prop({ type: String, required: true })
  subject: SubjectEnum;

  @Prop({ type: String, required: true })
  action: ActionEnum;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Role' }] })
  roles: Role[];
}

export const AuthorizationSchema = SchemaFactory.createForClass(Authorization);
