import { Controller, Get, Query } from '@nestjs/common';
import { PlanningService } from './planning.service';
import { PlanningQueryDto } from './planning-query.dto';

@Controller({
  path: 'planning',
  version: '1',
})
export class PlanningController {
  constructor(private _planningService: PlanningService) {}

  @Get('lines')
  async findLines(@Query() queryParams: PlanningQueryDto) {
    return this._planningService.getPlanningLines(queryParams);
  }

  @Get('setup')
  getSetup() {
    return this._planningService.getPlanningSetup();
  }
}
