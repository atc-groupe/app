import { Module } from '@nestjs/common';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { MpPlanningService } from './mp-planning.service';
import { PlanningService } from './planning.service';
import { JobsModule } from '../jobs/jobs.module';
import { PlanningController } from './planning.controller';
import { LogsModule } from '../logs/logs.module';
import { PlanningMappingService } from './planning.mapping.service';

@Module({
  imports: [MpHttpModule, JobsModule, LogsModule],
  providers: [MpPlanningService, PlanningService, PlanningMappingService],
  controllers: [PlanningController],
})
export class PlanningModule {}
