import { IsBoolean, IsDate, IsEnum, IsString } from 'class-validator';
import { PlanningDepartment } from './planning.enum';

export class PlanningQueryDto {
  @IsDate({ message: 'Invalid start date' })
  startDate: Date;

  @IsDate({ message: 'Invalid end date' })
  endDate: Date;

  @IsEnum(PlanningDepartment, { message: 'Invalid planning department' })
  department: number;

  @IsString({ message: 'Invalid group name' })
  group: string;
}
