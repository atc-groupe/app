import { Injectable } from '@nestjs/common';
import { IMpPlanningLine, IMpPlanningCard, IPlanningItem } from './interfaces';
import { Job } from '../jobs/jobs/job.schema';

@Injectable()
export class PlanningMappingService {
  getMappedPlanningItem(
    line: IMpPlanningLine,
    card: IMpPlanningCard,
    job: Job,
  ): IPlanningItem {
    return {
      date: line.date,
      time: this.convertTimeFromHoursToMinute(line.time),
      ordering: line.ordering,
      card: {
        startDate: card.start_date,
        productionTime: card.production_time,
        calculatedTime: card.calculated_time,
        employee: card.employee,
        extraInfo: card.extra_info,
        information: card.information,
        completed: card.completed,
        job: {
          operation: card.job.operation,
          paperOrdered: card.job.paper_ordered,
          paperAvailable: card.job.paper_available,
          notPlanned: card.job.not_planned,
          filesReady: card.job.files_ready,
        },
        days: card.days.map((day) => {
          return {
            date: day.date,
            time: this.convertTimeFromHoursToMinute(day.time),
            fixedTimeActive: day.fixed_time_active,
            fixedTime: this.convertTimeFromHoursToMinute(day.fixed_time),
            ordering: day.ordering,
          };
        }),
      },
      job,
    };
  }

  convertTimeFromHoursToMinute(time: number): number {
    return Math.round(time * 60);
  }
}
