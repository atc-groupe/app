import { Injectable } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { PlanningQueryDto } from './planning-query.dto';
import { Observable } from 'rxjs';
import { IMpPlanningCard } from './interfaces';
import { TMpPlanningLines, TMpPlanningSetup } from './types';

@Injectable()
export class MpPlanningService {
  constructor(private _http: MpHttpAdapter) {}

  getPlanningLines(queryDto: PlanningQueryDto): Observable<TMpPlanningLines> {
    return this._http.get<TMpPlanningLines>(`/planning/getLines`, {
      params: {
        startdate: queryDto.startDate,
        stopdate: queryDto.endDate,
        department: queryDto.department,
        group: queryDto.group,
      },
    });
  }

  getPlanningCard(cardId: number): Observable<IMpPlanningCard> {
    return this._http.get<IMpPlanningCard>(`/planning/getLines`, {
      params: { id: cardId },
    });
  }

  getPlanningSetup(): Observable<TMpPlanningSetup> {
    return this._http.get<TMpPlanningSetup>(`/planning/getSetup`);
  }
}
