export enum PlanningDepartment {
  PrePress = 0,
  Digital = 2,
  Finishing = 3,
  Subcontractors = 4,
}
