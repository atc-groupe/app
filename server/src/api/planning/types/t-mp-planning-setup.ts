import { IMpPlanningSetupDepartment } from '../interfaces';

export type TMpPlanningSetup = IMpPlanningSetupDepartment[];
