import { IPlanningMachineItem } from '../interfaces';

export type TPlanningMachineItems = IPlanningMachineItem[];
