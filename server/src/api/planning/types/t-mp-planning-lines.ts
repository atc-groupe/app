import { IMpPlanningLine } from '../interfaces';

export type TMpPlanningLines = IMpPlanningLine[];
