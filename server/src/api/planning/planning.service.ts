import { Injectable } from '@nestjs/common';
import { MpPlanningService } from './mp-planning.service';
import {
  JobsService,
  MpJobsService,
  JobsSyncService,
} from '../jobs/jobs/services';
import { firstValueFrom, map } from 'rxjs';
import { PlanningQueryDto } from './planning-query.dto';
import { IPlanningMachineItem } from './interfaces';
import { Job } from '../jobs/jobs/job.schema';
import { LogFactoryService } from '../logs/log-factory.service';
import { Logger } from '../logs/logger';
import { LogContextEnum } from '../logs/enums/log-context.enum';
import { IMpPlanningCard, IPlanningItem, IMpPlanningLine } from './interfaces';
import { TPlanningMachineItems } from './types';
import { PlanningMappingService } from './planning.mapping.service';
import { TPlanningSetup } from './types/t-planning-setup';

@Injectable()
export class PlanningService {
  private _logger: Logger;

  constructor(
    private _mpPlanningService: MpPlanningService,
    private _jobsService: JobsService,
    private _mpJobService: MpJobsService,
    private _jobSyncService: JobsSyncService,
    private _logsFactory: LogFactoryService,
    private _mappingService: PlanningMappingService,
  ) {
    this._logger = this._logsFactory.get(LogContextEnum.planning);
  }

  async getPlanningLines(
    queryDto: PlanningQueryDto,
  ): Promise<TPlanningMachineItems> {
    // 1 - Fetch planning lines
    const lines = await firstValueFrom(
      this._mpPlanningService.getPlanningLines(queryDto),
    );

    if (!lines.length) {
      return [];
    }

    // 2 - Fetch planning cards from planning lines.
    const cardData = await this._getCardDataFromLines(lines);

    // 3 - Fetch jobs from cards
    const jobMap = await this._getJobMapFromCardData(cardData);

    const planningMachineItems: TPlanningMachineItems = [];

    for (const { jobNumber, line, card } of cardData) {
      const testMachineItem: IPlanningMachineItem | undefined =
        planningMachineItems.find(
          (machine) => machine.machineId === line.machine.id,
        );

      // Sync planning jobs that are not found in app db.
      if (!jobMap.has(card.job.job_number)) {
        this._logger.warn(
          `A job fetched from planning has not been found in app DB. Try to re-sync... Job number: ${jobNumber}`,
        );

        const mpJob = await firstValueFrom(
          this._mpJobService.findOneByNumber(jobNumber),
        );

        if (mpJob) {
          await this._jobSyncService.syncOne(mpJob.id, false);
        } else {
          this._logger.error(
            `A job fetched from MultiPress planning has not been found in MultiPress. Job number: ${jobNumber}`,
          );
        }

        const job = await this._jobsService.findOneByMpNumber(
          jobNumber.toString(),
          false,
        );

        if (job) {
          jobMap.set(jobNumber, job);
        } else {
          this._logger.error(
            `A job fetched from planning has not been found in app DB after re-sync. Operation aborted... Job number: ${jobNumber}`,
          );
        }
      }

      // Try reSync jobs when syncStatus is in error
      const job = jobMap.get(jobNumber);

      if (job && job.syncStatus === 'error') {
        await this._jobSyncService.syncOne(job.mp.id, false);
        const reSyncedJob = await this._jobsService.findOneByMpNumber(
          jobNumber.toString(),
          false,
        );

        if (reSyncedJob) {
          jobMap.set(jobNumber, reSyncedJob);
        } else {
          this._logger.error(
            `A job fetched from planning is in sync error. Impossible to find it after re-sync... Job number: ${jobNumber}`,
          );
        }
      }

      // Map planning item from line, card and job
      const jobFromMap = jobMap.get(jobNumber);

      if (jobFromMap) {
        const planningItem: IPlanningItem =
          this._mappingService.getMappedPlanningItem(line, card, jobFromMap);

        const machineItem: IPlanningMachineItem = testMachineItem
          ? testMachineItem
          : {
              machineName: line.machine.name,
              machineId: line.machine.id,
              items: [],
            };

        machineItem.items.push(planningItem);

        if (!testMachineItem) {
          planningMachineItems.push(machineItem);
        }
      }
    }

    return planningMachineItems;
  }

  getPlanningSetup() {
    return this._mpPlanningService.getPlanningSetup().pipe(
      map((setup) => {
        return setup.reduce((acc: TPlanningSetup, department) => {
          department.group.forEach((group) => {
            acc.push({
              department: department.department,
              name: group.name,
              machines: group.machine,
            });
          });

          return acc;
        }, []);
      }),
    );
  }

  private async _getCardMapFromLines(
    lines: IMpPlanningLine[],
  ): Promise<Map<number, { line: IMpPlanningLine; card: IMpPlanningCard }>> {
    const promiseCards: Promise<IMpPlanningCard>[] = [];
    lines.forEach((line) => {
      promiseCards.push(
        firstValueFrom(this._mpPlanningService.getPlanningCard(line.id)),
      );
    });

    const cards: IMpPlanningCard[] = await Promise.all(promiseCards);
    const cardMap = new Map<
      number,
      { line: IMpPlanningLine; card: IMpPlanningCard }
    >();

    let i = 0;
    lines.forEach((line) => {
      const card = cards[i];
      if (card.job.not_planned === false && card.completed === false) {
        cardMap.set(card.job.job_number, { line, card });
      }
      i++;
    });

    return cardMap;
  }

  private async _getCardDataFromLines(
    lines: IMpPlanningLine[],
  ): Promise<
    { jobNumber: number; line: IMpPlanningLine; card: IMpPlanningCard }[]
  > {
    const promiseCards: Promise<IMpPlanningCard>[] = [];
    lines.forEach((line) => {
      promiseCards.push(
        firstValueFrom(this._mpPlanningService.getPlanningCard(line.id)),
      );
    });

    const cards: IMpPlanningCard[] = await Promise.all(promiseCards);
    const data: {
      jobNumber: number;
      line: IMpPlanningLine;
      card: IMpPlanningCard;
    }[] = [];

    let i = 0;
    lines.forEach((line) => {
      const card = cards[i];
      if (card.job.not_planned === false && card.completed === false) {
        data.push({
          jobNumber: card.job.job_number,
          line,
          card,
        });
      }
      i++;
    });

    return data;
  }

  private async _getJobMapFromCardData(
    data: { jobNumber: number; line: IMpPlanningLine; card: IMpPlanningCard }[],
  ): Promise<Map<number, Job>> {
    const jobNumbersSet = new Set<number>();
    data.forEach((cardMapItem) => {
      jobNumbersSet.add(cardMapItem.jobNumber);
    });
    const promisesJobs: Promise<Job | null>[] = [];
    jobNumbersSet.forEach((jobNumber) => {
      promisesJobs.push(
        this._jobsService.findOneByMpNumber(jobNumber.toString(), false),
      );
    });

    const jobs = await Promise.all(promisesJobs);
    const jobMap = new Map<number, Job>();

    jobs.forEach((job) => {
      if (job) {
        jobMap.set(parseInt(job.mp.number), job);
      }
    });

    return jobMap;
  }
}
