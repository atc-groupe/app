import { IPlanningSetupOperationEmployee } from './i-planning-setup-operation-employee';

export interface IPlanningSetupMachineOperation {
  name: string;
  speed: number;
  standard: boolean;
  operation: string;
  number: number;
  employees: IPlanningSetupOperationEmployee[];
}
