import { IMpPlanningSetupMachineOperationEmployee } from './i-mp-planning-setup-machine-operation-employee';

export interface IMpPlanningSetupMachineOperation {
  name: string;
  speed: number;
  standard: boolean;
  operation: string;
  number: number;
  employees: IMpPlanningSetupMachineOperationEmployee[];
}
