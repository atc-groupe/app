import { IPlanningItem } from './i-planning-item';

export interface IPlanningMachineItem {
  machineName: string;
  machineId: number;
  items: IPlanningItem[];
}
