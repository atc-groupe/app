import { IMpPlanningSetupGroup } from './i-mp-planning-setup-group';

export interface IMpPlanningSetupDepartment {
  department: number;
  group: IMpPlanningSetupGroup[];
}
