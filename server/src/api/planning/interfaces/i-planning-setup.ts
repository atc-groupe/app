import { IPlanningSetupMachine } from './i-planning-setup-machine';

export interface IPlanningSetupGroup {
  department: number;
  name: string;
  machines: IPlanningSetupMachine[];
}
