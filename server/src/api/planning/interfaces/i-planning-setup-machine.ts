import { IPlanningSetupMachineOperation } from './i-planning-setup-machine-operation';

export interface IPlanningSetupMachine {
  id: number;
  name: string;
  ordering: number;
  color: string;
  remark: string;
  operations: IPlanningSetupMachineOperation[];
}
