export interface IPlanningItemCardDay {
  date: Date;
  time: number; // in minutes;
  fixedTimeActive: boolean;
  fixedTime: number; // in minutes
  ordering: number;
}
