import { IPlanningItemCardDay } from './i-planning-item-card-day';

export interface IPlanningItemCard {
  startDate: Date;
  productionTime: number;
  calculatedTime: number;
  employee: string;
  extraInfo: string;
  information: string;
  completed: boolean;
  job: {
    operation: string;
    paperOrdered: boolean;
    paperAvailable: boolean;
    notPlanned: boolean;
    filesReady: boolean;
  };
  days: IPlanningItemCardDay[];
}
