import { IPlanningItemCard } from './i-planning-item-card';
import { Job } from '../../jobs/jobs/job.schema';

export interface IPlanningItem {
  date: Date;
  time: number; // in minutes
  ordering: number;
  card: IPlanningItemCard;
  job: Job;
}
