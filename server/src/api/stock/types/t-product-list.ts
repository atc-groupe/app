import { IProductGroupType } from '../interfaces';

export type TProductList = IProductGroupType[];
