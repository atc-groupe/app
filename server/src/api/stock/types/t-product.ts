import { IArticle, ICustomerStock, IPaper } from '../interfaces';

export type TProduct = IArticle | ICustomerStock | IPaper;
