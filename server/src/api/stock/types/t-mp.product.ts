import { IMpArticle, IMpCustomerStock, IMpPaper } from '../interfaces';

export type TMpProduct = IMpArticle | IMpCustomerStock | IMpPaper;
