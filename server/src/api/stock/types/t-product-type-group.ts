import {
  ArticleGroupTypeEnum,
  CustomerStockGroupTypeEnum,
  PaperGroupTypeEnum,
} from '../enum';

export type TProductTypeGroup =
  | ArticleGroupTypeEnum
  | CustomerStockGroupTypeEnum
  | PaperGroupTypeEnum;
