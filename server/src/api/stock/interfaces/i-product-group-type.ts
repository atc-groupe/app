import { IProductGroup } from './i-product-group';

export interface IProductGroupType {
  groupType: number;
  groups: IProductGroup[];
}
