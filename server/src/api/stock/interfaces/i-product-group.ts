import { IProductListItem } from './i-product-list-item';

export interface IProductGroup {
  groupType: number;
  description: string;
  products: IProductListItem[];
}
