import { IStockReservation } from './i-stock-reservation';

export interface IStockJobReservations {
  articles: IStockReservation[];
  customerProducts: IStockReservation[];
  papers: IStockReservation[];
}
