import { TMpProduct } from '../types/t-mp.product';
import { TProduct } from '../types/t-product';

export interface IProductMappingService {
  getMappedProduct: (productDetails: TMpProduct) => TProduct;
}
