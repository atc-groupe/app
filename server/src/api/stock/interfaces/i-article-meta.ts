import { IArticle } from './i-article';

export interface IArticleMeta {
  _id: string;
  mpArticleId: number;
  packQuantity: number;
  packUnitLabel: string;
  article: IArticle;
}
