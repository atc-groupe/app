import { PaperTypeEnum } from '../enum';

export interface IPaper {
  id: number;
  name: string; // MP extra_info field
  completeName: string;
  type: PaperTypeEnum; // stock_price_type="C" => sheet - stock_price_type="L" => roll
  groupDescription: string;
  color: string;
  width: number;
  length: number; // roll: Ml - sheet: mm
  thickness: number;
  insidePrint: boolean;
  stockManagement: boolean;
  minimumStock: number;
  stock: number;
  reserved: number;
  freeStock: number;
  supplier: string;
  displayUnit: string;
}
