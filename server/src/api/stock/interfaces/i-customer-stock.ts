export interface ICustomerStock {
  id: number;
  name: string; // MP description field
  completeName: string;
  productType: string;
  productNumber: number;
  groupDescription: string;
  relationNumber: number;
  width: number;
  length: number; // roll: Ml - sheet: mm
  stockManagement: boolean;
  minimumStock: number;
  stock: number;
  reserved: number;
  freeStock: number;
  notOnWeb: boolean;
  displayUnit: string;
}
