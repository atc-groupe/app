export interface IArticle {
  id: number;
  name: string; // MP description field
  completeName: string;
  groupType: number;
  groupDescription: string;
  color: string;
  unit: string;
  displayUnit: string;
  packedPer: number;
  stockManagement: boolean;
  minimumStock: number;
  stock: number;
  reserved: number;
  freeStock: number;
  supplier: string;
}
