export interface IProductListItem {
  description: string;
  articleIds: number[];
}
