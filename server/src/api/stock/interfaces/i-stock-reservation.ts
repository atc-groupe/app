import { ProductTypeEnum } from '../enum';
import { TProduct } from '../types/t-product';

export interface IStockReservation {
  productType: ProductTypeEnum;
  productId: number;
  productName: string;
  jobNumber: number;
  description: string;
  reserved: number;
  unitId: number;
  unitText: string;
  stock: number;
  articleNumber: string;
  type: string;
  extraInfo: string;
  size: string;
  remark: string;
  product: TProduct | null;
}
