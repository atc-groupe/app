import { Module } from '@nestjs/common';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { MpStockService } from './mp-stock.service';
import { StockService } from './stock.service';
import { StockController } from './stock.controller';
import { ProductListMappingService } from './mapping/product-list-mapping.service';
import { PaperMappingService } from './mapping/paper-mapping.service';
import { ArticleMappingService } from './mapping/article-mapping.service';
import { CustomerStockMappingService } from './mapping/customer-stock-mapping.service';
import { StockReservationMappingService } from './mapping/stock-reservation-mapping.service';
import { MpStockMutationService } from './mp-stock-mutation.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleMeta, ArticleMetaSchema } from './article-meta.schema';
import { ArticleMetaService } from './article-meta.service';
import { ArticlesMetaController } from './articles-meta.controller';

@Module({
  imports: [
    MpHttpModule,
    MongooseModule.forFeature([
      { name: ArticleMeta.name, schema: ArticleMetaSchema },
    ]),
  ],
  providers: [
    MpStockService,
    MpStockMutationService,
    StockService,
    ProductListMappingService,
    ArticleMappingService,
    CustomerStockMappingService,
    PaperMappingService,
    StockReservationMappingService,
    ArticleMetaService,
  ],
  controllers: [StockController, ArticlesMetaController],
  exports: [StockService, MpStockMutationService],
})
export class StockModule {}
