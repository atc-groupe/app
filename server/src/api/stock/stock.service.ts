import { Injectable, NotFoundException } from '@nestjs/common';
import { MpStockService } from './mp-stock.service';
import { firstValueFrom, map, Observable } from 'rxjs';
import {
  ArticleGroupTypeEnum,
  CustomerStockGroupTypeEnum,
  PaperGroupTypeEnum,
  ProductTypeEnum,
} from './enum';
import {
  IProductListItem,
  IProductMappingService,
  IStockJobReservations,
} from './interfaces';
import { ProductListMappingService } from './mapping/product-list-mapping.service';
import { PaperMappingService } from './mapping/paper-mapping.service';
import { ArticleMappingService } from './mapping/article-mapping.service';
import { CustomerStockMappingService } from './mapping/customer-stock-mapping.service';
import { StockReservationMappingService } from './mapping/stock-reservation-mapping.service';
import { IStockReservation } from './interfaces';
import { StockListQueryDto } from './dto';
import { TMpProduct, TProductList, TProduct } from './types';

@Injectable()
export class StockService {
  constructor(
    private _mpStockService: MpStockService,
    private _paperMappingService: PaperMappingService,
    private _articleMappingService: ArticleMappingService,
    private _customerStockMappingService: CustomerStockMappingService,
    private _productListMappingService: ProductListMappingService,
    private _stockReservationMappingService: StockReservationMappingService,
  ) {}

  async getJobReservations(jobNumber: number): Promise<IStockJobReservations> {
    const articles = await this._getMappedStockReservations(
      jobNumber,
      ProductTypeEnum.Article,
    );

    const customerProducts = await this._getMappedStockReservations(
      jobNumber,
      ProductTypeEnum.CustomerStock,
    );

    const papers = await this._getMappedStockReservations(
      jobNumber,
      ProductTypeEnum.Paper,
    );

    return {
      articles,
      customerProducts,
      papers,
    };
  }

  getProductsList(
    productType: ProductTypeEnum,
    dto?: StockListQueryDto,
  ): Observable<TProductList> {
    let groupType = dto?.groupType;
    if (groupType === undefined) {
      switch (productType) {
        case ProductTypeEnum.Article:
          groupType = ArticleGroupTypeEnum.All;
          break;
        case ProductTypeEnum.CustomerStock:
          groupType = CustomerStockGroupTypeEnum.All;
          break;
        case ProductTypeEnum.Paper:
          groupType = PaperGroupTypeEnum.All;
          break;
      }
    }

    return this._mpStockService
      .getProductList(productType, groupType, dto?.search)
      .pipe(
        map((mpList) => this._productListMappingService.getMappedList(mpList)),
      );
  }

  getProductDetails(
    mpProductId: number,
    productType: ProductTypeEnum,
  ): Observable<TProduct> {
    return this._mpStockService
      .getProductDetails(mpProductId, productType)
      .pipe(
        map((mpProduct) =>
          this._getMappingService(productType).getMappedProduct(mpProduct),
        ),
      );
  }

  async getProductsDetailsFromProductId(
    productType: ProductTypeEnum,
    mpProductId: number,
  ): Promise<TProduct[]> {
    const productsList = await firstValueFrom(
      this.getProductsList(productType),
    );

    const products: IProductListItem[] = [];
    productsList.forEach((productType) => {
      productType.groups.forEach((group) => {
        products.push(...group.products);
      });
    });

    const productListItems = products.find((product) =>
      product.articleIds.includes(mpProductId),
    );

    if (!productListItems) {
      throw new NotFoundException("Le produit n'a pas été trouvé");
    }

    return this.getProductsDetails(productListItems.articleIds, productType);
  }

  async getProductsDetails(
    mpArticleIds: number[],
    productType: ProductTypeEnum,
  ): Promise<TProduct[]> {
    const promises: Promise<TMpProduct>[] = [];

    mpArticleIds.forEach((id) => {
      promises.push(
        firstValueFrom(this._mpStockService.getProductDetails(id, productType)),
      );
    });

    const mpProducts = await Promise.all(promises);

    return mpProducts
      .filter((mpProduct) => mpProduct.attribute_8 !== 'oui') // Filter for virtual invoice materials
      .map((mpProduct) =>
        this._getMappingService(productType).getMappedProduct(mpProduct),
      )
      .sort((a: TProduct, b: TProduct) => {
        const nameA = a.completeName;
        const nameB = b.completeName;
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
  }

  private _getMappingService(
    productType: ProductTypeEnum,
  ): IProductMappingService {
    switch (productType) {
      case ProductTypeEnum.Paper:
        return this._paperMappingService;
      case ProductTypeEnum.CustomerStock:
        return this._customerStockMappingService;
      case ProductTypeEnum.Article:
        return this._articleMappingService;
    }
  }

  private async _getMappedStockReservations(
    jobNumber: number,
    productType: ProductTypeEnum,
  ): Promise<IStockReservation[]> {
    const mpProducts = await firstValueFrom(
      this._mpStockService.getReservationsByProductType(productType),
    );

    const jobMpProducts = mpProducts.filter(
      (item) => item.job_number === jobNumber,
    );

    const productReservations: IStockReservation[] = [];
    for (const mpArticle of jobMpProducts) {
      let product: TProduct | null;
      try {
        product = await firstValueFrom(
          this.getProductDetails(mpArticle.product_id, mpArticle.product_type),
        );
      } catch {
        product = null;
      }

      productReservations.push(
        this._stockReservationMappingService.getMappedData(mpArticle, product),
      );
    }

    return productReservations;
  }
}
