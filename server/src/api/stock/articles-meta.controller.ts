import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ArticleMetaService } from './article-meta.service';
import { ArticleMetaDto } from './dto/article-meta.dto';
import { ArticleMeta } from './article-meta.schema';

@Controller({
  path: 'stock/meta/articles',
  version: '1',
})
export class ArticlesMetaController {
  constructor(private _articleMetaService: ArticleMetaService) {}

  @Get()
  findAll(): Promise<ArticleMeta[]> {
    return this._articleMetaService.findAll();
  }

  @Get(':mpId')
  findByMpId(@Param('mpId') mpId: number): Promise<ArticleMeta> {
    return this._articleMetaService.findOneByMpId(mpId);
  }

  @Post()
  createOne(@Body() dto: ArticleMetaDto): Promise<ArticleMeta> {
    return this._articleMetaService.insertOne(dto);
  }

  @Patch(':id')
  updateOne(
    @Param('id') id: string,
    @Body() dto: ArticleMetaDto,
  ): Promise<ArticleMeta> {
    return this._articleMetaService.updateOne(id, dto);
  }

  @Delete(':id')
  removeOne(@Param('id') id: string): Promise<ArticleMeta> {
    return this._articleMetaService.removeOne(id);
  }
}
