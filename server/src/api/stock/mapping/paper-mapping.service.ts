import { Injectable } from '@nestjs/common';
import { IMpPaper, IPaper, IProductMappingService } from '../interfaces';
import { PaperTypeEnum } from '../enum';

@Injectable()
export class PaperMappingService implements IProductMappingService {
  getMappedProduct(mpPaperDetails: IMpPaper): IPaper {
    let type: PaperTypeEnum;
    let displayUnit: string;
    switch (mpPaperDetails.stock_price_type) {
      case 'L':
        type = PaperTypeEnum.Roll;
        displayUnit = 'ml';
        break;
      case 'M':
      case 'C':
        type = PaperTypeEnum.Sheet;
        displayUnit = 'feuilles';
        break;
      default:
        throw new Error(
          `Invalid article type. Type: ${mpPaperDetails.stock_price_type}`,
        );
    }

    let completeName = `${mpPaperDetails.extra_info} - ${mpPaperDetails.width} x ${mpPaperDetails.length}`;
    if (type === 'sheet') {
      completeName += ' mm';
    }

    if (type === 'roll') {
      completeName += ' ml';
    }

    return {
      id: mpPaperDetails.id,
      name: mpPaperDetails.extra_info,
      completeName,
      type,
      groupDescription: mpPaperDetails.group_description,
      color: mpPaperDetails.color,
      width: mpPaperDetails.width,
      length: mpPaperDetails.length,
      thickness: mpPaperDetails.thickness,
      insidePrint: mpPaperDetails.inside_print !== '',
      stockManagement: mpPaperDetails.in_stock === 'OUI',
      minimumStock: mpPaperDetails.minimum_stock,
      stock: mpPaperDetails.stock,
      freeStock: mpPaperDetails.free_stock,
      reserved: mpPaperDetails.reserved,
      supplier: mpPaperDetails.supplier,
      displayUnit,
    };
  }
}
