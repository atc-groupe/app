import { Injectable } from '@nestjs/common';
import { IArticle, IMpArticle, IProductMappingService } from '../interfaces';

@Injectable()
export class ArticleMappingService implements IProductMappingService {
  getMappedProduct(mpArticle: IMpArticle): IArticle {
    return {
      id: mpArticle.id,
      name: mpArticle.description,
      completeName: mpArticle.description,
      groupType: mpArticle.group_type,
      groupDescription: mpArticle.group_description,
      color: mpArticle.color,
      unit: mpArticle.unit,
      displayUnit:
        mpArticle.group_description === 'Encre' ? 'ex.' : mpArticle.unit,
      packedPer: mpArticle.packed_per,
      stockManagement: mpArticle.in_stock === 'OUI',
      minimumStock: mpArticle.stock_minimum,
      stock: mpArticle.stock,
      reserved: mpArticle.reserved,
      freeStock: mpArticle.free_stock,
      supplier: mpArticle.supplier,
    };
  }
}
