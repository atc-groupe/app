import { Injectable } from '@nestjs/common';
import { IMpStockReservation, IStockReservation } from '../interfaces';
import { TProduct } from '../types';

@Injectable()
export class StockReservationMappingService {
  getMappedData(
    mpReservation: IMpStockReservation,
    product: TProduct | null,
  ): IStockReservation {
    let unitText: string;
    switch (mpReservation.unit_txt) {
      case 'flle':
        unitText = 'Feuilles';
        break;
      case 'm':
        unitText = 'ml';
        break;
      default:
        unitText = mpReservation.unit_txt;
    }

    return {
      productType: mpReservation.product_type,
      productId: mpReservation.product_id,
      productName: product ? product.name : mpReservation.extra_info,
      jobNumber: mpReservation.job_number,
      description: mpReservation.description,
      reserved: mpReservation.reserved,
      unitId: mpReservation.unit_id,
      unitText,
      stock: mpReservation.stock,
      articleNumber: mpReservation.article_number,
      type: mpReservation.type,
      extraInfo: mpReservation.extra_info,
      size: mpReservation.size,
      remark: mpReservation.remark,
      product,
    };
  }
}
