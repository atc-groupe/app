import { Injectable } from '@nestjs/common';
import {
  ICustomerStock,
  IMpCustomerStock,
  IProductMappingService,
} from '../interfaces';

@Injectable()
export class CustomerStockMappingService implements IProductMappingService {
  getMappedProduct(mpCustomerStock: IMpCustomerStock): ICustomerStock {
    return {
      id: mpCustomerStock.id,
      name: mpCustomerStock.description,
      completeName: mpCustomerStock.description,
      productType: mpCustomerStock.product_type,
      productNumber: mpCustomerStock.product_nr,
      groupDescription: mpCustomerStock.group_description,
      relationNumber: mpCustomerStock.relation_number,
      width: mpCustomerStock.width,
      length: mpCustomerStock.length,
      stockManagement: mpCustomerStock.in_warehouse === 'OUI',
      minimumStock: mpCustomerStock.minimum_stock,
      stock: mpCustomerStock.stock,
      reserved: mpCustomerStock.reserved,
      freeStock: mpCustomerStock.free_stock,
      notOnWeb: mpCustomerStock.not_on_web,
      displayUnit: 'ex.',
    };
  }
}
