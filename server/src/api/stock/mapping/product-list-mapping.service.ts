import { Injectable } from '@nestjs/common';
import {
  IProductGroup,
  IProductListItem,
  IMpProductListGroupItem,
} from '../interfaces';
import { TMpProductList, TProductList } from '../types';

@Injectable()
export class ProductListMappingService {
  getMappedList(mpList: TMpProductList): TProductList {
    const productList = mpList.reduce((acc: TProductList, group) => {
      if (
        group.group_description.startsWith('0') ||
        group.group_description === '' ||
        group.group_type === 10
      ) {
        return acc;
      }

      const testGroupType = acc.find(
        (item) => item.groupType === group.group_type,
      );

      const productGroup: IProductGroup = {
        groupType: group.group_type,
        description: group.group_description,
        products: this._getGroupProducts(group.articles),
      };

      if (!testGroupType) {
        acc.push({
          groupType: group.group_type,
          groups: [productGroup],
        });
      } else {
        testGroupType.groups.push(productGroup);
      }

      return acc;
    }, []);

    productList.forEach((groupType) => {
      groupType.groups.forEach((group) => {
        group.products = group.products.sort((a, b) => {
          const nameA = a.description.toLowerCase();
          const nameB = b.description.toLowerCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          return 0;
        });
      });
    });

    return productList;
  }

  private _getGroupProducts(
    productList: IMpProductListGroupItem[],
  ): IProductListItem[] {
    if (!productList || !productList.length) {
      return [];
    }

    return productList.reduce(
      (acc: IProductListItem[], product): IProductListItem[] => {
        const productTest = acc.find(
          (item) => item.description === product.product_description,
        );

        if (productTest) {
          productTest.articleIds.push(product.product_id);
        } else {
          const newArticle: IProductListItem = {
            description: product.product_description,
            articleIds: [product.product_id],
          };

          acc.push(newArticle);
        }

        return acc;
      },
      [],
    );
  }
}
