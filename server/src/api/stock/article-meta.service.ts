import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ArticleMeta } from './article-meta.schema';
import { Model } from 'mongoose';
import { ArticleMetaDto } from './dto/article-meta.dto';
import { StockService } from './stock.service';
import { IArticle, IArticleMeta } from './interfaces';
import { ProductTypeEnum } from './enum';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class ArticleMetaService {
  constructor(
    @InjectModel(ArticleMeta.name)
    private _articleMetaModel: Model<ArticleMeta>,
    private _stockService: StockService,
  ) {}

  async findAll(): Promise<IArticleMeta[]> {
    try {
      const list = await this._articleMetaModel.find().exec();

      if (!list.length) {
        return [];
      }

      const products = await this._stockService.getProductsDetails(
        list.map((item) => item.mpArticleId),
        ProductTypeEnum.Article,
      );

      const articlesMeta: IArticleMeta[] = [];

      list.forEach((articleMeta) => {
        const product = products.find(
          (item) => item.id === articleMeta.mpArticleId,
        );

        if (product) {
          articlesMeta.push({
            _id: articleMeta._id.toString(),
            mpArticleId: articleMeta.mpArticleId,
            packQuantity: articleMeta.packQuantity,
            packUnitLabel: articleMeta.packUnitLabel,
            article: product as IArticle,
          });
        }
      });

      return articlesMeta.sort((a: IArticleMeta, b: IArticleMeta) => {
        const nameA = a?.article.completeName;
        const nameB = b?.article.completeName;
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException('Une erreur est survenue', {
        cause: err,
      });
    }
  }

  async findOneByMpId(mpId: number): Promise<IArticleMeta> {
    try {
      const articleMeta = await this._articleMetaModel
        .findOne({ mpArticleId: mpId })
        .exec();

      if (!articleMeta) {
        throw new NotFoundException(
          `Aucune métadonnée trouvé avec l'id ${mpId}`,
        );
      }

      const product = await firstValueFrom(
        this._stockService.getProductDetails(mpId, ProductTypeEnum.Article),
      );

      if (!product) {
        throw new NotFoundException(`Aucun article trouvé avec l'id ${mpId}`);
      }

      return {
        _id: articleMeta._id.toString(),
        mpArticleId: articleMeta.mpArticleId,
        packQuantity: articleMeta.packQuantity,
        packUnitLabel: articleMeta.packUnitLabel,
        article: product as IArticle,
      };
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException('Une erreur est survenue', {
        cause: err,
      });
    }
  }

  async insertOne(dto: ArticleMetaDto): Promise<ArticleMeta> {
    try {
      return await this._articleMetaModel.create(dto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      if (err.code === 11000) {
        throw new BadRequestException('Cet article est déjà paramétré.');
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
        {
          cause: err,
        },
      );
    }
  }

  async updateOne(id: string, dto: ArticleMetaDto): Promise<ArticleMeta> {
    try {
      const articleMeta = await this._articleMetaModel.findByIdAndUpdate(
        id,
        dto,
      );

      if (!articleMeta) {
        throw new NotFoundException("l'article n'a pas été trouvé");
      }

      return articleMeta;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      if (err.code === 11000) {
        throw new BadRequestException('Cet article est déjà paramétré.');
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
        {
          cause: err,
        },
      );
    }
  }

  async removeOne(id: string): Promise<ArticleMeta> {
    try {
      const articleMeta = await this._articleMetaModel.findByIdAndRemove(id);

      if (!articleMeta) {
        throw new NotFoundException("l'article n'a pas été trouvé");
      }

      return articleMeta;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
        {
          cause: err,
        },
      );
    }
  }
}
