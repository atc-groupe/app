import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { StockMutationCreateDto } from './dto';
import { catchError, map, Observable, retry } from 'rxjs';
import {
  IMpStockMutationListItem,
  IMpStockMutationResponse,
  IMpStockMutation,
} from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiStockErrorEnum } from '../../shared/enums';

@Injectable()
export class MpStockMutationService {
  constructor(private _http: MpHttpAdapter) {}

  createMutation(
    productType: number,
    productId: number,
    dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._http
      .put<IMpStockMutationResponse>(
        `/stock/handleStockMutation`,
        {
          product_type: productType,
          product_id: productId,
          employee_number: dto.employee_number,
          data: JSON.stringify(dto.data),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        retry({ delay: 1000, count: 3 }),
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND ||
              err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
            ) {
              throw new NotFoundException(err.errText);
            }

            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_IN_USE ||
              err.errNumber === MpApiStockErrorEnum.MUTATION_IN_USE
            ) {
              throw new ConflictException(this._getLockedMessage(err.errText));
            }

            if (
              err.errNumber ===
              MpApiStockErrorEnum.INVALID_AND_OR_INSUFFICIENT_DATA
            ) {
              throw new BadRequestException(err.errText);
            }

            if (err.errNumber === MpApiStockErrorEnum.JOB_NOT_FOUND_OR_USED) {
              throw new BadRequestException(
                `Le job ${dto.data.job_number} n'existe pas ou est en cours d'utilisation.`,
              );
            }
          }

          throw err;
        }),
      );
  }

  updateMutation(
    productType: number,
    productId: number,
    mutationId: number,
    dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._http
      .put<IMpStockMutationResponse>(
        '/stock/handleStockMutation',
        {
          mutation_id: mutationId,
          product_type: productType,
          product_id: productId,
          employee_number: dto.employee_number,
          data: JSON.stringify(dto.data),
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        retry({ delay: 1000, count: 3 }),
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND ||
              err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
            ) {
              throw new NotFoundException(err.errText);
            }

            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_IN_USE ||
              err.errNumber === MpApiStockErrorEnum.MUTATION_IN_USE
            ) {
              throw new ConflictException(this._getLockedMessage(err.errText));
            }

            if (
              err.errNumber ===
              MpApiStockErrorEnum.INVALID_AND_OR_INSUFFICIENT_DATA
            ) {
              throw new BadRequestException(err.errText);
            }
          }

          throw err;
        }),
      );
  }

  getMutation(productType: number, productId: number, mutationId: number) {
    return this._http
      .get<IMpStockMutation>('/stock/handleStockMutation', {
        params: {
          product_type: productType,
          product_id: productId,
          mutation_id: mutationId,
        },
      })
      .pipe(
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND ||
              err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
            ) {
              throw new NotFoundException(err.errText);
            }

            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_IN_USE ||
              err.errNumber === MpApiStockErrorEnum.MUTATION_IN_USE
            ) {
              throw new ConflictException(err.errText);
            }
          }

          throw err;
        }),
      );
  }

  getMutations(productType: number, productId: number, startDate: string) {
    return this._http
      .get<IMpStockMutationListItem[]>('/stock/handleStockMutation', {
        params: {
          startdate: startDate,
          product_type: productType,
          product_id: productId,
        },
      })
      .pipe(
        map((list) => {
          return list.sort((a, b) => {
            const nameA = a.mutation_id;
            const nameB = b.mutation_id;
            if (nameA > nameB) {
              return -1;
            }
            if (nameA < nameB) {
              return 1;
            }

            return 0;
          });
        }),
        catchError((err) => {
          if (err instanceof MpApiErrorException) {
            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND ||
              err.errNumber === MpApiStockErrorEnum.NO_ARTICLE_ENTERED
            ) {
              throw new NotFoundException(err.errText);
            }

            if (
              err.errNumber === MpApiStockErrorEnum.ARTICLE_IN_USE ||
              err.errNumber === MpApiStockErrorEnum.MUTATION_IN_USE
            ) {
              throw new ConflictException(err.errText);
            }
          }

          throw err;
        }),
      );
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `L'article est utilisé par ${name}`;
  }
}
