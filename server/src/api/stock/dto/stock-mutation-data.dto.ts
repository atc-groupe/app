import { IsNumber, IsOptional, IsString } from 'class-validator';

export class StockMutationDataDto {
  @IsNumber()
  @IsOptional()
  job_number?: number;

  @IsString()
  description: string;

  @IsNumber()
  subtract: number;

  @IsNumber()
  add: number;
}
