import { IsNumber, IsString } from 'class-validator';

export class ArticleMetaDto {
  @IsNumber()
  mpArticleId: number;

  @IsNumber()
  packQuantity: number;

  @IsString()
  packUnitLabel: string;
}
