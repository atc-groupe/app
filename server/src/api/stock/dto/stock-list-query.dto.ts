import { IsOptional, IsString } from 'class-validator';

export class StockListQueryDto {
  @IsOptional()
  groupType: any;

  @IsOptional()
  @IsString()
  search: string;
}
