import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Schema()
export class ArticleMeta extends Model {
  @Prop({ type: Number, required: true, index: true, unique: true })
  mpArticleId: number;

  @Prop({ type: Number, required: true })
  packQuantity: number;

  @Prop({ type: String, required: true })
  packUnitLabel: string;
}

export const ArticleMetaSchema = SchemaFactory.createForClass(ArticleMeta);
