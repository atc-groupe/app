import { Injectable, NotFoundException } from '@nestjs/common';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { ProductTypeEnum } from './enum';
import { catchError, Observable } from 'rxjs';
import { IMpStockReservation } from './interfaces';
import { MpApiErrorException } from '../../shared/exceptions';
import { MpApiStockErrorEnum } from '../../shared/enums';
import { TMpProductList, TMpProduct } from './types';

@Injectable()
export class MpStockService {
  constructor(private _http: MpHttpAdapter) {}

  getReservationsByProductType(
    productType: ProductTypeEnum,
  ): Observable<IMpStockReservation[]> {
    return this._http.get<IMpStockReservation[]>(
      `/stock/getStockReservations`,
      {
        params: {
          product_type: productType,
        },
      },
    );
  }

  getProductList(
    productType: number,
    groupType: number,
    search?: string,
  ): Observable<TMpProductList> {
    const searchParams: any = [
      { field: 'status', comparator: '=', value: 'Actif' },
      { field: 'in_stock', comparator: '=', value: 'OUI' },
    ];

    if (search && productType === ProductTypeEnum.Article) {
      searchParams.push({
        conjenction: 'and',
        field: 'description',
        comparator: '=',
        value: `@${search}@`,
      });
    }

    if (search && productType === ProductTypeEnum.Paper) {
      searchParams.push({
        conjenction: 'and',
        field: 'extra_info',
        comparator: '=',
        value: `@${search}@`,
      });
    }

    return this._http.get<TMpProductList>('/stock/getArticleList', {
      params: {
        product_type: productType,
        group_type: groupType,
        filter: JSON.stringify(searchParams),
      },
    });
  }

  getProductDetails(
    productId: number,
    productType: ProductTypeEnum,
  ): Observable<TMpProduct> {
    return this._http
      .get<TMpProduct>('/stock/handleArticleDetails', {
        params: {
          product_id: productId,
          product_type: productType,
        },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiStockErrorEnum.ARTICLE_NOT_FOUND
          ) {
            throw new NotFoundException(err.errText);
          }

          throw err;
        }),
      );
  }
}
