export enum PaperGroupTypeEnum {
  PlanoSheets = 0,
  Envelopes = 1,
  RollKg = 2,
  RollMl = 3,
  All = 4,
}
