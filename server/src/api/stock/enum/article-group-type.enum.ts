export enum ArticleGroupTypeEnum {
  All = 0,
  Printout = 1,
  Films = 2,
  Proofs = 3,
  Plates = 4,
  Packaging = 5,
  Foil = 6,
  DieCutting = 7,
  OtherFinish = 8,
  WireO = 9,
  OtherPrePress = 10,
  Lamination = 11,
  LargeFormat = 12,
  Supplements = 50,
  Inks = 90,
}
