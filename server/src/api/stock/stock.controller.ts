import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { StockService } from './stock.service';
import { ProductTypeEnum } from './enum';
import { StockListQueryDto } from './dto';
import { MpStockMutationService } from './mp-stock-mutation.service';
import { StockMutationCreateDto } from './dto';
import {
  IMpStockMutation,
  IMpStockMutationListItem,
  IMpStockMutationResponse,
  IStockJobReservations,
} from './interfaces';
import { Observable } from 'rxjs';
import { TProduct, TProductList } from './types';

@Controller({
  path: 'stock',
  version: '1',
})
export class StockController {
  constructor(
    private _stockService: StockService,
    private _mpStockMutationService: MpStockMutationService,
  ) {}
  @Get('products/:type')
  async getProductsDetails(
    @Param('type') type: ProductTypeEnum,
    @Query('ids') ids: string,
  ): Promise<TProduct[]> {
    try {
      const formattedIds = ids.split(',').map((item) => parseInt(item));

      if (!formattedIds || !formattedIds.length) {
        throw new BadRequestException('products ids are missing');
      }

      return await this._stockService.getProductsDetails(formattedIds, type);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Impossible de récupérer la liste des produits',
        { cause: err },
      );
    }
  }

  @Get('products/:type/list')
  getProductList(
    @Param('type') type: ProductTypeEnum,
    @Query() listQueryDto: StockListQueryDto,
  ): Observable<TProductList> {
    return this._stockService.getProductsList(type, listQueryDto);
  }

  @Get('products/:type/formats')
  async getProductsFromProductId(
    @Param('type') type: ProductTypeEnum,
    @Query('productId') productId: number,
  ): Promise<TProduct[]> {
    try {
      return this._stockService.getProductsDetailsFromProductId(
        type,
        productId,
      );
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Impossible de récupérer la liste des produits',
        { cause: err },
      );
    }
  }

  @Get('products/:type/:id')
  getProductDetails(
    @Param('type') type: ProductTypeEnum,
    @Param('id') id: number,
  ): Observable<TProduct> {
    return this._stockService.getProductDetails(id, type);
  }

  @Post('products/:type/:id/mutations')
  createMutation(
    @Param('type') type: number,
    @Param('id') id: number,
    @Body() dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._mpStockMutationService.createMutation(type, id, dto);
  }

  @Patch('products/:type/:productId/mutations/:mutationId')
  updateMutation(
    @Param('type') type: ProductTypeEnum,
    @Param('productId') productId: number,
    @Param('mutationId') mutationId: number,
    @Body() dto: StockMutationCreateDto,
  ): Observable<IMpStockMutationResponse> {
    return this._mpStockMutationService.updateMutation(
      type,
      productId,
      mutationId,
      dto,
    );
  }

  @Get('products/:type/:productId/mutations')
  getMutations(
    @Param('type') type: ProductTypeEnum,
    @Param('productId') productId: number,
    @Query('startdate') startDate: string,
  ): Observable<IMpStockMutationListItem[]> {
    return this._mpStockMutationService.getMutations(
      type,
      productId,
      startDate,
    );
  }

  @Get('products/:type/:productId/mutations/:mutationId')
  getMutation(
    @Param('type') type: ProductTypeEnum,
    @Param('productId') productId: number,
    @Param('mutationId') mutationId: number,
  ): Observable<IMpStockMutation> {
    return this._mpStockMutationService.getMutation(
      type,
      productId,
      mutationId,
    );
  }

  @Get('reservations/:jobNumber')
  async getJobReservations(
    @Param('jobNumber') jobNumber: number,
  ): Promise<IStockJobReservations> {
    try {
      return await this._stockService.getJobReservations(jobNumber);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Impossible de récupérer la liste des réservations',
        { cause: err },
      );
    }
  }
}
