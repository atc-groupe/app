import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { InitModule } from './init/init.module';
import { AuthModule } from './auth/auth.module';
import { EmployeesModule } from './employees/employees.module';
import { RelationsModule } from './relations/relations.module';
import { JwtModule } from '@nestjs/jwt';
import { RolesModule } from './roles/roles.module';
import { AuthorizationsModule } from './authorizations/authorizations.module';
import { ActiveDirectoryModule } from './active-directory/active-directory.module';
import { JobsModule } from './jobs/jobs.module';
import { LogsModule } from './logs/logs.module';
import { SystemModule } from './system/system.module';
import { MpSyncModule } from './mp-sync/mp-sync.module';
import { PlanningModule } from './planning/planning.module';
import { StockModule } from './stock/stock.module';
import { MetaModule } from './meta/meta.module';

@Module({
  imports: [
    AuthModule,
    JwtModule,
    EmployeesModule,
    InitModule,
    RelationsModule,
    UsersModule,
    RolesModule,
    AuthorizationsModule,
    ActiveDirectoryModule,
    JobsModule,
    LogsModule,
    SystemModule,
    MpSyncModule,
    PlanningModule,
    StockModule,
    MetaModule,
  ],
})
export class ApiModule {}
