import { Module } from '@nestjs/common';
import { ActiveDirectoryController } from './active-directory.controller';
import { ActiveDirectoryService } from './active-directory.service';
import { ActiveDirectoryConfig } from '../../config';

@Module({
  controllers: [ActiveDirectoryController],
  providers: [ActiveDirectoryService, ActiveDirectoryConfig],
  exports: [ActiveDirectoryService],
})
export class ActiveDirectoryModule {}
