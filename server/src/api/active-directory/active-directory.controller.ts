import { Controller, Get, InternalServerErrorException } from '@nestjs/common';
import { ActiveDirectoryService } from './active-directory.service';
import { ConfigService } from '@nestjs/config';

@Controller({
  path: 'active-directory',
  version: '1',
})
export class ActiveDirectoryController {
  constructor(
    private _adService: ActiveDirectoryService,
    private _configService: ConfigService,
  ) {}

  @Get('users')
  findAll() {
    try {
      return this._adService.findUsersFromSecurityGroup();
    } catch (err) {
      const securityGroup = this._configService.getOrThrow('AD_USERS_GROUP');

      throw new InternalServerErrorException(
        `Une erreur est survenue lors de la récupération des utilisateurs dans l'AD. Groupe de sécurité configuré pour la recherche: ${securityGroup}`,
      );
    }
  }
}
