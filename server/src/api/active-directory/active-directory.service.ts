import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import ActiveDirectory = require('activedirectory2');
import { ActiveDirectoryConfig } from '../../config';
import { IAdUser } from './i-ad-user';
import { Observable } from 'rxjs';

@Injectable()
export class ActiveDirectoryService {
  private readonly _adSecurityGroup;
  private readonly _config;
  private ad;

  constructor(
    private _configService: ConfigService,
    private _adConfig: ActiveDirectoryConfig,
  ) {
    this.ad = new ActiveDirectory(_adConfig.getConfigOptions());
    this._config = this._adConfig.getConfigOptions();
    this._adSecurityGroup = this._configService.getOrThrow('AD_USERS_GROUP');
  }

  findUsersFromSecurityGroup(): Observable<IAdUser[]> {
    return new Observable<IAdUser[]>((subscriber) => {
      this.ad.getUsersForGroup(
        this._adSecurityGroup,
        function (err: any, users: IAdUser[]) {
          if (err) {
            return subscriber.error(err);
          }

          if (!users.length) {
            subscriber.next([]);
            subscriber.complete();
          }

          subscriber.next(
            users.sort((a: IAdUser, b: IAdUser) => {
              const nameA = `${a.givenName.toUpperCase()} ${a.sn?.toUpperCase()}`;
              const nameB = `${b.givenName.toUpperCase()} ${b.sn?.toUpperCase()}`;
              if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }

              return 0;
            }),
          );
          subscriber.complete();
        },
      );
    });
  }

  findUserBySAMAccountName(sAMAccountName: string): Observable<IAdUser | null> {
    return new Observable<IAdUser | null>((subscriber) => {
      this.ad.findUser(sAMAccountName, (err: any, user: IAdUser) => {
        if (err) {
          subscriber.error(err);
        }

        subscriber.next(user ? user : null);
        subscriber.complete();
      });
    });
  }

  isPasswordValid(
    userPrincipalName: string,
    password: string,
  ): Observable<boolean> {
    return new Observable<boolean>((subscriber) => {
      this.ad.authenticate(
        userPrincipalName,
        password,
        (err: string, auth: boolean) => {
          if (err) {
            subscriber.next(false);
            subscriber.complete();
          }

          subscriber.next(auth);
          subscriber.complete();
        },
      );
    });
  }
}
