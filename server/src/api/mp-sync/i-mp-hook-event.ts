import { MpHookEnum } from './enum';

export interface IMpHookEvent {
  id: number | string;
  type: string;
  event: MpHookEnum;
  sub: number;
}
