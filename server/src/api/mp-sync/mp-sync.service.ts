import { Injectable } from '@nestjs/common';
import { IMpHookEvent } from './i-mp-hook-event';
import { MpHookEnum } from './enum';
import { Logger } from '../logs/logger';
import { JobsSyncService } from '../jobs/jobs/services';
import { LogFactoryService } from '../logs/log-factory.service';
import { LogContextEnum } from '../logs/enums/log-context.enum';

@Injectable()
export class MpSyncService {
  private _logger: Logger;
  constructor(
    loggerFactory: LogFactoryService,
    private syncJobService: JobsSyncService,
  ) {
    this._logger = loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  syncResource(mpHookEvent: IMpHookEvent): Promise<void> | void {
    this._logger.log(JSON.stringify(mpHookEvent), false);

    try {
      switch (mpHookEvent.event) {
        case MpHookEnum.job: {
          return this.syncJobService.syncOne(Number(mpHookEvent.id), true);
        }
        default:
          return;
      }
    } catch (err) {
      throw err;
    }
  }
}
