import { Body, Controller, Post } from '@nestjs/common';
import { IMpHookEvent } from './i-mp-hook-event';
import { Public } from '../auth/public.decorator';
import { MpSyncService } from './mp-sync.service';

@Controller({
  version: '1',
  path: 'mp-sync',
})
export class MpSyncController {
  constructor(private _syncService: MpSyncService) {}

  @Public()
  @Post()
  async sync(@Body() mpHookEvent: IMpHookEvent): Promise<void> {
    return this._syncService.syncResource(mpHookEvent);
  }
}
