export enum MpRelationSubHookEnum {
  'contactPersons' = 1,
  'mailCode' = 2,
  'companyType' = 3,
  'deliveryAddress' = 4,
  'invoiceAddress' = 5,
  'partnership' = 6,
  'arrangementDescription' = 7,
  'arrangementItem' = 8,
  'arrangementType' = 9,
  'arrangementPrice' = 10,
  'visits' = 11,
}
