import { Module } from '@nestjs/common';
import { MpSyncController } from './mp-sync.controller';
import { JobsModule } from '../jobs/jobs.module';
import { LogsModule } from '../logs/logs.module';
import { MpSyncService } from './mp-sync.service';

@Module({
  imports: [JobsModule, LogsModule],
  providers: [MpSyncService],
  controllers: [MpSyncController],
})
export class MpSyncModule {}
