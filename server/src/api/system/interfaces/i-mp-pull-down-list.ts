import { IMpPullDownListItem } from './i-mp-pull-down-list-item';

export interface IMpPullDownList {
  id: number;
  pulldownlist: string;
  values: IMpPullDownListItem[];
}
