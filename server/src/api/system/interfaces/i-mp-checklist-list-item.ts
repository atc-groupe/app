export interface IMpChecklistListItem {
  checklist: number;
  description: string;
  language: string;
  not_on_web: boolean;
  active: boolean;
  header_01: string;
  header_02: string;
  header_03: string;
  header_04: string;
  header_05: string;
  header_06: string;
  header_07: string;
  header_08: string;
  header_09: string;
  header_10: string;
  header_11: string;
  header_12: string;
}
