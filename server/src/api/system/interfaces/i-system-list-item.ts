export interface ISystemListItem {
  id: string;
  number: number;
  description: string | null;
  label: string;
}
