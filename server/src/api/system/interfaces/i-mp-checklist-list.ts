import { IMpChecklistListItem } from './i-mp-checklist-list-item';

export interface IMpChecklistList {
  checklists: IMpChecklistListItem[];
}
