import { Injectable } from '@nestjs/common';
import { PullDownListEnum } from './enums/pull-down-list.enum';
import { ISystemListItem } from './interfaces';
import { MpSystemService } from './mp-system.service';
import { map, Observable } from 'rxjs';

@Injectable()
export class SystemPullDownListMappingService {
  constructor(private _systemMpService: MpSystemService) {}

  getMappedData(
    listNumber: PullDownListEnum,
  ): Observable<ISystemListItem[] | null> {
    return this._systemMpService.findPullDownListByNumber(listNumber).pipe(
      map((mpList) => {
        if (!mpList) {
          return null;
        }

        return mpList.map((item) => {
          return {
            id: item.id,
            number: item.ordering,
            label: item.text,
            description: item.description,
          };
        });
      }),
    );
  }
}
