import { Controller, Get, Param } from '@nestjs/common';
import { SystemService } from './system.service';
import { Observable } from 'rxjs';
import { IChecklist, ISystemListItem } from './interfaces';

@Controller({
  path: 'system',
  version: '1',
})
export class SystemController {
  constructor(private _systemService: SystemService) {}

  @Get('pull-down-list/:name')
  findList(@Param('name') name: string): Observable<ISystemListItem[] | null> {
    return this._systemService.findPullDownListByName(name);
  }

  @Get('checklist/:name')
  findChecklist(@Param('name') name: string): Observable<IChecklist | null> {
    return this._systemService.findChecklistByName(name);
  }
}
