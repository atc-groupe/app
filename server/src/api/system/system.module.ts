import { Module } from '@nestjs/common';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { SystemService } from './system.service';
import { SystemChecklistMappingService } from './system-checklist-mapping.service';
import { MpSystemService } from './mp-system.service';
import { SystemPullDownListMappingService } from './system-pull-down-list-mapping.service';
import { SystemController } from './system.controller';

@Module({
  imports: [MpHttpModule],
  providers: [
    SystemService,
    SystemChecklistMappingService,
    MpSystemService,
    SystemPullDownListMappingService,
  ],
  controllers: [SystemController],
  exports: [SystemService],
})
export class SystemModule {}
