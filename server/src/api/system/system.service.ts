import { Injectable, NotFoundException } from '@nestjs/common';
import { SystemPullDownListMappingService } from './system-pull-down-list-mapping.service';
import { PullDownListEnum } from './enums/pull-down-list.enum';
import { CheckListEnum } from './enums/check-list.enum';
import { SystemChecklistMappingService } from './system-checklist-mapping.service';
import { map, Observable, of } from 'rxjs';
import { IChecklist, ISystemListItem } from './interfaces';

@Injectable()
export class SystemService {
  constructor(
    private _pullDownListMapping: SystemPullDownListMappingService,
    private _checklistMapping: SystemChecklistMappingService,
  ) {}

  findPullDownListByName(name: string): Observable<ISystemListItem[] | null> {
    let number: number;
    switch (name) {
      case 'jobStatus':
        number = PullDownListEnum.jobStatus;
        break;
      default:
        return of(null);
    }

    return this._pullDownListMapping.getMappedData(number).pipe(
      map((list) => {
        if (!list) {
          throw new NotFoundException(`${name} list is not defined`);
        }

        return list;
      }),
    );
  }

  findChecklistById(id: number) {
    return this._checklistMapping.getMappedData(id);
  }

  findChecklistByName(name: string): Observable<IChecklist | null> {
    let id: number;
    switch (name) {
      case 'largeFormat':
        id = CheckListEnum.largeFormat;
        break;
      case 'pose':
        id = CheckListEnum.pose;
        break;
      default:
        return of(null);
    }

    return this._checklistMapping.getMappedData(id).pipe(
      map((checklist) => {
        if (!checklist) {
          throw new NotFoundException(`${name} list is not defined`);
        }

        return checklist;
      }),
    );
  }
}
