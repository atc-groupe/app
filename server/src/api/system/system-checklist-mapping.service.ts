import { Injectable } from '@nestjs/common';
import { IChecklist } from './interfaces';
import { MpSystemService } from './mp-system.service';
import { map, Observable } from 'rxjs';

@Injectable()
export class SystemChecklistMappingService {
  constructor(private _systemMpService: MpSystemService) {}

  getMappedData(id: number): Observable<IChecklist | null> {
    return this._systemMpService.findChecklistById(id).pipe(
      map((mpList) => {
        if (!mpList) {
          return null;
        }

        return {
          header1: mpList.header_01,
          header2: mpList.header_02,
          header3: mpList.header_03,
          header4: mpList.header_04,
          header5: mpList.header_05,
          header6: mpList.header_06,
          header7: mpList.header_07,
          header8: mpList.header_08,
          header9: mpList.header_09,
          header10: mpList.header_10,
          header11: mpList.header_10,
          header12: mpList.header_10,
        };
      }),
    );
  }
}
