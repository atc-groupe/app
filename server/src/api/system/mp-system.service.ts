import { Injectable } from '@nestjs/common';
import { CheckListEnum } from './enums/check-list.enum';
import {
  IMpChecklistList,
  IMpChecklistListItem,
  IMpPullDownList,
  IMpPullDownListItem,
} from './interfaces';
import { PullDownListEnum } from './enums/pull-down-list.enum';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { map, Observable } from 'rxjs';

@Injectable()
export class MpSystemService {
  constructor(private _http: MpHttpAdapter) {}

  findChecklistById(
    id: CheckListEnum,
  ): Observable<IMpChecklistListItem | null> {
    return this._http.get<IMpChecklistList>('/system/getChecklist').pipe(
      map(({ checklists }) => {
        const checklistIndex = checklists.findIndex(
          (item: IMpChecklistListItem) => item.checklist === id,
        );

        return checklistIndex !== -1 ? checklists[checklistIndex] : null;
      }),
    );
  }

  findPullDownListByNumber(
    number: PullDownListEnum,
  ): Observable<IMpPullDownListItem[] | null> {
    return this._http
      .get<IMpPullDownList>('/system/getPulldownlist', {
        params: { id: number },
      })
      .pipe(
        map((list) => {
          if (!list) {
            return null;
          }

          return list.values.length ? list.values : null;
        }),
      );
  }
}
