import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { UserTypeEnum } from '../../shared/enums';
import { RoleLevelEnum } from './role-level.enum';

@Schema()
export class Role extends Document {
  @Prop({ type: String, required: true })
  userType: UserTypeEnum;

  @Prop({ type: String, required: true, unique: true })
  label: string;

  @Prop({ type: String, required: true })
  level: RoleLevelEnum;

  @Prop()
  description: string;
}

export const RoleSchema = SchemaFactory.createForClass(Role);
