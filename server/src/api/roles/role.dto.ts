import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserTypeEnum } from '../../shared/enums';
import { RoleLevelEnum } from './role-level.enum';

export class RoleDto {
  @IsNotEmpty({ message: 'User type is required' })
  @IsEnum(UserTypeEnum, { message: 'InvalidUserType' })
  userType: UserTypeEnum;

  @IsString()
  @IsNotEmpty({ message: 'Le champ label est requis' })
  label: string;

  @IsNotEmpty()
  @IsEnum(RoleLevelEnum, { message: 'Invalid role value' })
  level: RoleLevelEnum;

  @IsOptional()
  description: string;
}
