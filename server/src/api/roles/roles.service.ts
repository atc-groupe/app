import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Role } from './roles.schema';
import { Model } from 'mongoose';
import { RoleDto } from './role.dto';
import { RoleLevelEnum } from './role-level.enum';
import { UserTypeEnum } from '../../shared/enums';

@Injectable()
export class RolesService {
  constructor(@InjectModel(Role.name) private _roleModel: Model<Role>) {}

  insertOne(roleDto: RoleDto): Promise<Role> {
    const model = new this._roleModel(roleDto);

    return model.save();
  }

  findAll(): Promise<Role[]> {
    return this._roleModel
      .find({ level: { $not: { $eq: RoleLevelEnum.SuperAdmin } } })
      .select('-__v')
      .sort('label')
      .exec();
  }

  findByUserType(userType: UserTypeEnum): Promise<Role[]> {
    return this._roleModel
      .find({ level: { $not: { $eq: RoleLevelEnum.SuperAdmin } }, userType })
      .select('-__v')
      .sort('label')
      .exec();
  }

  findOneByLabel(label: string): Promise<Role | null> {
    return this._roleModel.findOne({ label }).exec();
  }

  findOneById(id: string): Promise<Role | null> {
    return this._roleModel.findOne({ _id: id }).exec();
  }

  updateOne(id: string, roleDto: RoleDto) {
    return this._roleModel.updateOne({ _id: id }, roleDto).exec();
  }

  deleteOne(id: string) {
    return this._roleModel.deleteOne({ _id: id }).exec();
  }
}
