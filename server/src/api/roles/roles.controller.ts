import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Patch,
  Post,
  UnauthorizedException,
} from '@nestjs/common';
import { RoleDto } from './role.dto';
import { Role } from './roles.schema';
import { RolesService } from './roles.service';
import { UsersService } from '../users/users.service';
import { UserTypeEnum } from '../../shared/enums';

@Controller('roles')
export class RolesController {
  constructor(
    private _rolesService: RolesService,
    private _usersService: UsersService,
  ) {}

  @Get()
  async findAll(): Promise<{ employee: Role[]; customer: Role[] }> {
    try {
      const employeeRoles = await this._rolesService.findByUserType(
        UserTypeEnum.Employee,
      );
      const customerRoles = await this._rolesService.findByUserType(
        UserTypeEnum.Customer,
      );

      return {
        employee: employeeRoles,
        customer: customerRoles,
      };
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la récupération des rôles utilisateur',
        { cause: err },
      );
    }
  }

  @Post()
  async insertOne(@Body() roleDto: RoleDto): Promise<Role> {
    try {
      const testByLabel = await this._rolesService.findOneByLabel(
        roleDto.label,
      );

      if (testByLabel) {
        throw new BadRequestException('Un role avec ce label existe déjà');
      }

      return await this._rolesService.insertOne(roleDto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la création',
        { cause: err },
      );
    }
  }

  @Patch(':id')
  async updateOne(@Param('id') id: string, @Body() roleDto: RoleDto) {
    try {
      const testByLabel = await this._rolesService.findOneByLabel(
        roleDto.label,
      );

      if (testByLabel && testByLabel._id.toString() !== id) {
        throw new BadRequestException('Un role avec ce label existe déjà');
      }

      return await this._rolesService.updateOne(id, roleDto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Un erreur est survenue lors de la mise à jour du rôle',
        { cause: err },
      );
    }
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    try {
      const role = await this._rolesService.findOneById(id);

      if (!role) {
        throw new BadRequestException('User rôle not found');
      }

      const users = await this._usersService.findByRole(role);

      if (users.length) {
        throw new UnauthorizedException(
          'Opération impossible: un ou plusieurs utilisateurs possède ce rôle. Affectez leur un autre rôle puis recommencez',
        );
      }

      return await this._rolesService.deleteOne(id);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Un erreur est survenue lors de la suppression du rôle',
        { cause: err },
      );
    }
  }
}
