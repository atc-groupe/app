import { UserTypeEnum } from '../../shared/enums';
import { RoleLevelEnum } from './role-level.enum';

export interface IRole {
  userType: UserTypeEnum;
  label: string;
  level: RoleLevelEnum;
  description: string;
}
