export enum RoleLevelEnum {
  SuperAdmin = 'superAdmin',
  Admin = 'admin',
  Manager = 'manager',
  User = 'user',
  Guest = 'guest',
}
