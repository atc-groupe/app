import { IsEmail, IsNotEmpty } from 'class-validator';

export class CredentialsDto {
  @IsNotEmpty({ message: "L'identifiant est obligatoire" })
  identifier: string;

  @IsNotEmpty({ message: 'Le mot de passe est requis' })
  password: string;
}
