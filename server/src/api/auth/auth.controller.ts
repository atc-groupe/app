import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { CredentialsDto } from './credentials.dto';
import { AuthService } from './auth.service';
import { Public } from './public.decorator';
import { JWT_COOKIE } from './auth.constants';
import { Response } from 'express';
import { IUserData } from '../users/interfaces/i-user-data';

@Controller({
  path: 'auth',
  version: '1',
})
export class AuthController {
  constructor(private _authService: AuthService) {}

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  async signIn(
    @Body() credentials: CredentialsDto,
    @Res({ passthrough: true }) res: Response,
  ): Promise<IUserData> {
    try {
      const { userData, accessToken } = await this._authService.signIn(
        credentials,
      );

      res.cookie(JWT_COOKIE, accessToken, { httpOnly: true });

      return userData;
    } catch (err) {
      if (err instanceof UnauthorizedException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Oups! un problème est survenu. Veuillez re-essayer. Si le problème persiste, contactez l'administrateur",
        { cause: err },
      );
    }
  }

  @Get('logout')
  logOut(@Res({ passthrough: true }) res: Response): void {
    res.clearCookie(JWT_COOKIE);

    return;
  }
}
