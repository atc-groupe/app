import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { AuthGuard } from './auth.guard';
import { ActiveDirectoryModule } from '../active-directory/active-directory.module';

@Module({
  imports: [UsersModule, ActiveDirectoryModule],
  providers: [AuthService, AuthGuard],
  controllers: [AuthController],
})
export class AuthModule {}
