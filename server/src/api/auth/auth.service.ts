import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { CredentialsDto } from './credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { UserTypeEnum } from '../../shared/enums';
import { UsersMappingService } from '../users/users-mapping.service';
import { User } from '../users/user.schema';
import { ActiveDirectoryService } from '../active-directory/active-directory.service';
import { firstValueFrom } from 'rxjs';
import { TUserData } from '../users/t-user-data';

@Injectable()
export class AuthService {
  constructor(
    private _userService: UsersService,
    private _jwtService: JwtService,
    private _adService: ActiveDirectoryService,
    private _userMappingService: UsersMappingService,
  ) {}

  async signIn(
    credentials: CredentialsDto,
  ): Promise<{ userData: TUserData; accessToken: string }> {
    const user = await this._userService.findOneByIdentifier(
      credentials.identifier,
    );

    if (!user) {
      throw new UnauthorizedException('Identifiants incorrects');
    }

    const isPasswordValid = await this._isPasswordValid(user, credentials);

    if (!isPasswordValid) {
      throw new UnauthorizedException('Identifiants incorrects');
    }

    const userData = await this._userMappingService.getUserDataFromUser(user);

    if (!userData) {
      throw new UnauthorizedException('Utilisateur désactivé');
    }

    const payload = { identifier: user.identifier, sub: user._id };

    return {
      userData,
      accessToken: await this._jwtService.signAsync(payload),
    };
  }

  private async _isPasswordValid(
    user: User,
    credentials: CredentialsDto,
  ): Promise<boolean> {
    switch (user.type) {
      case UserTypeEnum.Employee: {
        const adUser = await firstValueFrom(
          this._adService.findUserBySAMAccountName(credentials.identifier),
        );

        if (!adUser) {
          return Promise.resolve(false);
        }

        return firstValueFrom(
          this._adService.isPasswordValid(
            adUser.userPrincipalName,
            credentials.password,
          ),
        );
      }
      case UserTypeEnum.Customer: {
        return this._userService.isPasswordValid(
          credentials.password,
          user.password,
        );
      }
    }
  }
}
