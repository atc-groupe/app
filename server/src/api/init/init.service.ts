import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { UserTypeEnum } from '../../shared/enums';
import { UserCreateDto } from '../users/dto/user-create.dto';
import { User } from '../users/user.schema';
import { InitDto } from './init.dto';
import { RolesService } from '../roles/roles.service';
import { Role } from '../roles/roles.schema';
import { RoleDto } from '../roles/role.dto';
import { RoleLevelEnum } from '../roles/role-level.enum';

@Injectable()
export class InitService {
  constructor(
    private _usersService: UsersService,
    private _rolesService: RolesService,
  ) {}

  async setAdminUser(initDto: InitDto): Promise<User> {
    const role = await this._setAdminRole();

    const userCreateDto = new UserCreateDto();
    userCreateDto.type = UserTypeEnum.Employee;
    userCreateDto.mpId = initDto.mpId;
    userCreateDto.identifier = initDto.identifier;
    userCreateDto.roleId = role._id;

    return this._usersService.insertOne(userCreateDto);
  }

  private _setAdminRole(): Promise<Role> {
    const role = new RoleDto();
    role.userType = UserTypeEnum.Employee;
    role.label = 'SuperAdmin';
    role.level = RoleLevelEnum.SuperAdmin;
    role.description = "Administrateur de l'application";

    return this._rolesService.insertOne(role);
  }
}
