import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { InitController } from './init.controller';
import { InitService } from './init.service';
import { RolesModule } from '../roles/roles.module';

@Module({
  imports: [UsersModule, RolesModule],
  providers: [InitService],
  controllers: [InitController],
})
export class InitModule {}
