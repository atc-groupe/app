import {
  Body,
  Controller,
  InternalServerErrorException,
  NotFoundException,
  Post,
} from '@nestjs/common';
import { InitService } from './init.service';
import { InitDto } from './init.dto';
import { Public } from '../auth/public.decorator';

@Controller({
  path: 'init',
  version: '1',
})
export class InitController {
  constructor(private _initService: InitService) {}

  @Public()
  @Post()
  async initializeApp(@Body() initDto: InitDto) {
    try {
      await this._initService.setAdminUser(initDto);

      return { message: 'App DB has been initialized successfully' };
    } catch (err) {
      if (err instanceof NotFoundException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'An error occurred while initializing app DB',
        { cause: err },
      );
    }
  }
}
