import { IsNotEmpty, IsString } from 'class-validator';

export class InitDto {
  @IsString({ message: 'Invalid MultiPress id' })
  @IsNotEmpty()
  mpId: string;

  @IsString({ message: 'Invalid MultiPress id' })
  @IsNotEmpty()
  identifier: string;
}
