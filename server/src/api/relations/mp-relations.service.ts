import { Injectable } from '@nestjs/common';
import { RelationsQueryParamsDto } from './relations-query-params.dto';
import { catchError, map, Observable, of } from 'rxjs';
import { IMpRelationList, IMpRelation, IRelation } from './interfaces';
import { RelationMappingService } from './relation-mapping.service';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';
import { MpApiErrorException } from '../../shared/exceptions';
import { TRelationList } from './types/t-relation-list';
import { MpApiRelationsErrorEnum } from '../../shared/enums/mp-api-relations-error.enum';

@Injectable()
export class MpRelationsService {
  constructor(
    private _http: MpHttpAdapter,
    private _mappingService: RelationMappingService,
  ) {}

  findAll(queryParams: RelationsQueryParamsDto): Observable<TRelationList> {
    return this._http
      .get<IMpRelationList>(`/relations/getRelationsList`, {
        params: { relation_code: queryParams.type },
      })
      .pipe(
        map(({ relations }) => {
          return relations
            .map((relation) =>
              this._mappingService.getListItemMappedData(relation),
            )
            .sort((a, b) => {
              const nameA = a.company;
              const nameB = b.company;
              if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }

              return 0;
            });
        }),
      );
  }

  findOneByNumber(number: number): Observable<IRelation | null> {
    return this._http
      .get<IMpRelation>(`/relations/getRelationInfo`, {
        params: { relation_number: number, details: 'full' },
      })
      .pipe(
        map((mpRelation) =>
          this._mappingService.getRelationMappedData(mpRelation),
        ),
      )
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiRelationsErrorEnum.RECORD_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
