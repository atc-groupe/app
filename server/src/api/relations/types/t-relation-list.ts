import { IRelationListItem } from '../interfaces';

export type TRelationList = IRelationListItem[];
