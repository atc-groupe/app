import { Controller, Get, Param, Query } from '@nestjs/common';
import { RelationsQueryParamsDto } from './relations-query-params.dto';
import { MpRelationsService } from './mp-relations.service';
import { Public } from '../auth/public.decorator';
import { MpRelationsContactPersonService } from './mp-relations-contact-person.service';
import { Observable } from 'rxjs';
import { IRelationContact } from './interfaces';
import { TRelationList } from './types/t-relation-list';

@Controller({
  path: 'relations',
  version: '1',
})
export class RelationsController {
  constructor(
    private _relationsService: MpRelationsService,
    private _contactService: MpRelationsContactPersonService,
  ) {}

  @Public()
  @Get()
  findAll(
    @Query() queryDto: RelationsQueryParamsDto,
  ): Observable<TRelationList> {
    return this._relationsService.findAll(queryDto);
  }

  @Public()
  @Get(':id/contacts')
  findContactsById(@Param('id') id: string): Observable<IRelationContact[]> {
    return this._contactService.findByRelationId(id);
  }
}
