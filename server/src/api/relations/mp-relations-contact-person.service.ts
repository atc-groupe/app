import { Injectable } from '@nestjs/common';
import {
  IMpRelationContactPerson,
  IMpRelation,
  IRelationContact,
} from './interfaces';
import { map, Observable } from 'rxjs';
import { RelationMappingService } from './relation-mapping.service';
import { MpHttpAdapter } from '../../shared/services/mp-http-adapter.service';

@Injectable()
export class MpRelationsContactPersonService {
  constructor(
    private _http: MpHttpAdapter,
    private _mappingService: RelationMappingService,
  ) {}

  findByRelationId(id: string) {
    return this._http
      .get<IMpRelation>('/relations/getRelationInfo', {
        params: { id, details: 'full' },
      })
      .pipe(
        map(({ relation_contacts }) => {
          return relation_contacts
            .map((contact) =>
              this._mappingService.getContactMappedData(contact),
            )
            .sort((a, b) => {
              const nameA = a.completeName;
              const nameB = b.completeName;
              if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }

              return 0;
            });
        }),
      );
  }

  findOneById(mpId: string): Observable<IRelationContact> {
    return this._http
      .get<IMpRelationContactPerson>('/relations/handleContactperson', {
        params: { id: mpId },
      })
      .pipe(
        map((contactPerson) =>
          this._mappingService.getContactMappedData(contactPerson),
        ),
      );
  }
}
