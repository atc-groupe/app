import { RelationTypeEnum } from './relation-type.enum';
import { IsEnum, IsNotEmpty } from 'class-validator';

export class RelationsQueryParamsDto {
  @IsEnum(RelationTypeEnum, { message: 'Invalid relation type' })
  @IsNotEmpty({ message: 'Relation type is required' })
  type: RelationTypeEnum;
}
