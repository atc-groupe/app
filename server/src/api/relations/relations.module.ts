import { Module } from '@nestjs/common';
import { MpRelationsService } from './mp-relations.service';
import { RelationsController } from './relations.controller';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { MpRelationsContactPersonService } from './mp-relations-contact-person.service';
import { RelationMappingService } from './relation-mapping.service';

@Module({
  imports: [MpHttpModule],
  providers: [
    MpRelationsService,
    MpRelationsContactPersonService,
    RelationMappingService,
  ],
  controllers: [RelationsController],
  exports: [MpRelationsService, MpRelationsContactPersonService],
})
export class RelationsModule {}
