import { Injectable } from '@nestjs/common';
import {
  IMpRelation,
  IRelation,
  IMpRelationContactPerson,
  IMpRelationListItem,
  IRelationContact,
  IRelationListItem,
} from './interfaces';

@Injectable()
export class RelationMappingService {
  getListItemMappedData(listItem: IMpRelationListItem): IRelationListItem {
    return {
      id: listItem.id,
      number: listItem.relation_number,
      company: listItem.company,
    };
  }

  getRelationMappedData(mpRelation: IMpRelation): IRelation {
    const contacts = mpRelation.relation_contacts.map((mpContact) =>
      this.getContactMappedData(mpContact),
    );

    return {
      id: mpRelation.id,
      number: mpRelation.relation_number,
      type: mpRelation.relation_code,
      name: mpRelation.company,
      visitAddress: mpRelation.visit_address,
      visitZipCode: mpRelation.visit_zipcode,
      visitCity: mpRelation.visit_city,
      phone: mpRelation.phone,
      mobilePhone: mpRelation.mobile_phone,
      email: mpRelation.email,
      salesMan: mpRelation.salesman,
      jobManager: mpRelation.jobmanager,
      contacts,
    };
  }

  getContactMappedData(mpContact: IMpRelationContactPerson): IRelationContact {
    return {
      id: mpContact.id,
      relationNumber: mpContact.relation_number,
      firstName: mpContact.firstname,
      lastName: mpContact.lastname,
      completeName: mpContact.complete_name,
      phone: mpContact.phone,
      mobile: mpContact.mobile_phone,
      email: mpContact.email,
      title: mpContact.title,
      opening: mpContact.opening,
    };
  }
}
