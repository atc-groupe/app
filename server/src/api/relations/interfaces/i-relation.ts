import { RelationTypeEnum } from '../relation-type.enum';
import { IRelationContact } from './i-relation-contact';

export interface IRelation {
  id: string;
  number: number;
  type: RelationTypeEnum;
  name: string;
  visitAddress: string; // visit address;
  visitZipCode: string; // visit address;
  visitCity: string; // visit address;
  phone: string;
  mobilePhone: string;
  email: string;
  salesMan: string;
  jobManager: string;
  contacts: IRelationContact[];
  // Todo: Add delivery addresses & Invoice addresses
}
