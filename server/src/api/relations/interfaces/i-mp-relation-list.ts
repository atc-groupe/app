import { IMpRelationListItem } from './i-mp-relation-list-item';

export interface IMpRelationList {
  relations: IMpRelationListItem[];
}
