export interface IRelationContact {
  id: string;
  relationNumber: number;
  firstName: string;
  lastName: string;
  completeName: string;
  phone: string;
  mobile: string;
  email: string;
  title: string;
  opening: string;
}
