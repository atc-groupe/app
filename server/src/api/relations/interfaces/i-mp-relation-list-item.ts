export interface IMpRelationListItem {
  id: string;
  company: string;
  search_code: string;
  relation_code: string;
  relation_number: number;
}
