export interface IMpRelationInvoiceAddress {
  relation_number: string;
  invoice_name: string;
  invoice_address: string;
  invoice_zipcode: string;
  invoice_city: string;
  sequence: number;
  contact_name: string;
  addressee: string;
  towards: string;
  firstname: string;
  invoice_country: string;
  invoice_address_number: string;
  vat_number: string;
  invoice_country_code: string;
  extra_address_1: string;
  extra_address_2: string;
  extra_address_3: string;
  extra_address_4: string;
  extra_address_5: string;
  email: string;
}
