export interface IRelationListItem {
  id: string;
  number: number;
  company: string;
}
