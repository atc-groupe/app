export enum RelationTypeEnum {
  Client = 'C',
  Prospect = 'P',
  Fournisseur = 'F',
  AncienContact = 'A',
}
