import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SubJobsService } from './sub-jobs.service';
import { SubJobsMappingService } from './sub-jobs-mapping.service';
import { SubJobsSyncService } from './sub-jobs-sync.service';
import { MpSubJobService } from './mp-sub-job.service';
import { SubJobNumericLayerMappingHelperService } from './mapping';
import {
  SubJobChecklistMappingService,
  SubJobProductionMappingService,
  SubJobFinishingLayerMappingHelperService,
  SubJobProductionMetaHelperService,
  SubJobProductionNumericMappingService,
  SubJobProductionFinishingMappingService,
  SubJobProductionSubcontractingMappingService,
} from './mapping';
import { SubJob, SubJobSchema } from './sub-job.schema';
import { MpHttpModule } from '../../../shared/modules/mp-http.module';
import { SystemModule } from '../../system/system.module';
import { LogsModule } from '../../logs/logs.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: SubJob.name, schema: SubJobSchema }]),
    MpHttpModule,
    SystemModule,
    LogsModule,
  ],
  providers: [
    SubJobsService,
    MpSubJobService,
    SubJobsMappingService,
    SubJobChecklistMappingService,
    SubJobProductionMappingService,
    SubJobProductionNumericMappingService,
    SubJobProductionFinishingMappingService,
    SubJobProductionSubcontractingMappingService,
    SubJobNumericLayerMappingHelperService,
    SubJobFinishingLayerMappingHelperService,
    SubJobProductionMetaHelperService,
    SubJobsSyncService,
  ],
  exports: [SubJobsSyncService, SubJobNumericLayerMappingHelperService],
})
export class SubJobsModule {}
