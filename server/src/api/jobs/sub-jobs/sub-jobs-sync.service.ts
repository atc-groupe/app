import { Injectable } from '@nestjs/common';
import { SubJobsMappingService } from './sub-jobs-mapping.service';
import { Types } from 'mongoose';
import { SubJobDto } from './dto/sub-job.dto';
import { SubJobsService } from './sub-jobs.service';
import { Job } from '../jobs/job.schema';
import { SubJob } from './sub-job.schema';

@Injectable()
export class SubJobsSyncService {
  constructor(
    private subJobMapping: SubJobsMappingService,
    private subJobService: SubJobsService,
  ) {}

  public async syncManyByJob(
    job: Job,
    retryWhenLocked: boolean,
  ): Promise<SubJob[]> {
    const subJobsDto = await this.subJobMapping.getMappedDataCollection(
      job.mp.subJobsIds,
      retryWhenLocked,
    );

    // 1 - Fetch all app subJobs
    const existingSubJobs = await this.subJobService.findByJobId(job._id);
    const existingSubJobsMap = new Map<number, SubJob>();
    const usedIndexesSet = new Set<number>();
    existingSubJobs.forEach((appSubJob) => {
      existingSubJobsMap.set(appSubJob.mpId, appSubJob);
      usedIndexesSet.add(appSubJob.mpId);
    });

    // 1 - Sync subJobs from MP quotationDetails subJob list.
    const queries: any[] = [];
    let index = 0;
    for (const subJobDto of subJobsDto) {
      const subJobIndex = this._getSubJobIndex(
        index,
        existingSubJobsMap,
        usedIndexesSet,
        subJobsDto,
      );
      if (subJobIndex) {
        subJobDto.index = subJobIndex;
      }

      queries.push(this._syncOne(existingSubJobsMap, subJobDto, job._id));
      index++;
    }
    const subJobs: SubJob[] = await Promise.all<SubJob>(queries);

    // 2 - Remove app subJobs that has been removed from Mp job.
    await this._removeMpDeletedSubJobs(job);

    return subJobs;
  }

  private async _syncOne(
    appSubJobsMap: Map<number, SubJob>,
    subJobDto: SubJobDto,
    jobId: Types.ObjectId,
  ): Promise<SubJob> {
    const subJob = appSubJobsMap.get(subJobDto.mpId);

    if (!subJob) {
      subJobDto.job = jobId;
      return this.subJobService.insertOne(subJobDto);
    }

    return this.subJobService.updateOne(subJob, subJobDto);
  }

  private async _removeMpDeletedSubJobs(job: Job): Promise<void> {
    const appSubJobs = await this.subJobService.findMpIdsByJobId(job._id);

    if (!appSubJobs) {
      return;
    }

    for (const subJob of appSubJobs) {
      if (!job.mp.subJobsIds.includes(subJob.mpId)) {
        await this.subJobService.deleteOne(subJob._id);
      }
    }
  }

  private _getSubJobIndex(
    subJobDtoIndex: number,
    existingSubJobsMap: Map<number, SubJob>,
    usedIndexesSet: Set<number>,
    subJobsDto: SubJobDto[],
  ): number | null {
    const subJobDto = subJobsDto[subJobDtoIndex];
    const subJob = existingSubJobsMap.get(subJobDto.mpId);

    if (subJob) {
      return subJob.index ? subJob.index : subJobDtoIndex;
    }

    for (let i = subJobDtoIndex; i < subJobsDto.length; i++) {
      const dto = subJobsDto[i];
      if (!usedIndexesSet.has(dto.mpId)) {
        usedIndexesSet.add(dto.mpId);
        return i;
      }
    }

    return null;
  }
}
