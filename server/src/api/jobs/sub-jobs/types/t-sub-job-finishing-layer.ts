import {
  ISubJobLaminationFinishingLayer,
  ISubJobLargeFormatFinishingLayer,
} from '../interfaces';

/**
  This interfaces matches the data extracted from finishing key
  of workflow/handleJobDetailInfo MultiPress API endpoint
 */
export type TSubJobFinishingLayer =
  | ISubJobLaminationFinishingLayer
  | ISubJobLargeFormatFinishingLayer;
