import {
  IMpSubJobLaminationFinishing,
  IMpSubJobLargeFormatFinishing,
} from '../interfaces';

export type TMpSubJobFinishing =
  | IMpSubJobLaminationFinishing
  | IMpSubJobLargeFormatFinishing;
