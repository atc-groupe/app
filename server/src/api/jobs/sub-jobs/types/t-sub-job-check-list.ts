import { ISubJobChecklistItem } from '../interfaces';

export type TSubJobCheckList = ISubJobChecklistItem[];
