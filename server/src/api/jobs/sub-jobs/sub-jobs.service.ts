import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Promise, Types } from 'mongoose';
import { SubJobDto } from './dto/sub-job.dto';
import { SubJob } from './sub-job.schema';

@Injectable()
export class SubJobsService {
  constructor(
    @InjectModel(SubJob.name)
    private subJobModel: Model<SubJob>,
  ) {}

  insertOne(subJobDto: SubJobDto): Promise<SubJob> {
    const subJob = new this.subJobModel(subJobDto);

    return subJob.save();
  }

  updateOne(subJob: SubJob, subJobDto: SubJobDto): Promise<SubJob> {
    subJob.mpId = subJobDto.mpId;
    subJob.description = subJobDto.description;
    subJob.quantity1 = subJobDto.quantity1;
    subJob.checklist = subJobDto.checklist;
    subJob.productType = subJobDto.productType;
    subJob.productNumber = subJobDto.productNumber;
    subJob.paoInfo = subJobDto.paoInfo;
    subJob.paoCheckInfo = subJobDto.paoCheckInfo;
    subJob.productionData = subJobDto.productionData;

    if (subJobDto.index) {
      subJob.index = subJobDto.index;
    }

    if (subJobDto.quantity2) {
      subJob.quantity2 = subJobDto.quantity2;
    }

    if (subJobDto.quantity3) {
      subJob.quantity3 = subJobDto.quantity3;
    }

    return subJob.save();
  }

  deleteOne(id: Types.ObjectId) {
    return this.subJobModel.deleteOne({ _id: id }).exec();
  }

  findOneByMpId(mpId: number): Promise<SubJob | null> {
    return this.subJobModel.findOne({ mpId }).exec();
  }

  findMpIdsByJobId(jobId: Types.ObjectId): Promise<SubJob[]> {
    return this.subJobModel.find({ job: jobId }).select('mpId').exec();
  }

  findByJobId(jobId: string): Promise<SubJob[]> {
    return this.subJobModel.find({ job: new Types.ObjectId(jobId) }).exec();
  }
}
