import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { MpHttpAdapter } from '../../../shared/services/mp-http-adapter.service';
import { IMpSubJobDetailInfo, IMpSubJobDetails } from './interfaces';
import { catchError, EMPTY, Observable, of, retry } from 'rxjs';
import { MpApiErrorException } from '../../../shared/exceptions';
import {
  MpApiJobsErrorEnum,
  MpApiWorkflowErrorEnum,
} from '../../../shared/enums';

@Injectable()
export class MpSubJobService {
  constructor(private _http: MpHttpAdapter) {}

  findDetailsById(id: number): Observable<IMpSubJobDetails | null> {
    return this._http
      .get<IMpSubJobDetails | null>('/jobs/handleSubJobsQuotationsDetails', {
        params: { id },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.SUB_JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }

  findProdDataById(id: number): Observable<IMpSubJobDetailInfo | null> {
    return this._http
      .get<IMpSubJobDetailInfo | null>('/workflow/handleJobDetailInfo', {
        params: { id },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber ===
              MpApiWorkflowErrorEnum.SUB_JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }

  rebuildProdDataBeforeFetching(
    id: number,
    retryWhenLocked: boolean,
  ): Observable<void> {
    const observable = this._http.delete<void>(
      '/workflow/handleJobDetailInfo',
      {
        params: { id, fullreset: true },
      },
    );

    const errorFn = (err: any) => {
      if (
        err instanceof MpApiErrorException &&
        err.errNumber === MpApiWorkflowErrorEnum.SUB_JOB_OR_QUOTATION_NOT_FOUND
      ) {
        return EMPTY;
      }

      if (
        err instanceof MpApiErrorException &&
        err.errNumber === MpApiWorkflowErrorEnum.SUB_JOB_OR_QUOTATION_IN_USE
      ) {
        throw new InternalServerErrorException(
          this._getLockedMessage(err.errText),
        );
      }

      throw err;
    };

    if (retryWhenLocked) {
      return observable.pipe(
        retry({ count: 6 * 60 * 2, delay: 10000 }), // every 5s during 2 hour
        catchError(errorFn),
      );
    }

    return observable.pipe(catchError(errorFn));
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `Un sous job est utilisé par ${name}`;
  }
}
