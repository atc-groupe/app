import { Types } from 'mongoose';
import { ISubJobChecklistItem, ISubJobProductionData } from '../interfaces';

export class SubJobDto {
  index?: number;
  mpId: number;
  description: string;
  quantity1: number;
  quantity2?: number;
  quantity3?: number;
  checklist: ISubJobChecklistItem[];
  productType: string;
  productNumber: number;
  paoInfo: string;
  paoCheckInfo: string;
  productionData: ISubJobProductionData | null;
  job?: Types.ObjectId;
}
