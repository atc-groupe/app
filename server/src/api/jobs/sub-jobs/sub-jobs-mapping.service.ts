import { Injectable } from '@nestjs/common';
import { SubJobDto } from './dto/sub-job.dto';
import { MpSubJobService } from './mp-sub-job.service';
import {
  SubJobChecklistMappingService,
  SubJobProductionMappingService,
} from './mapping';
import { LogFactoryService } from '../../logs/log-factory.service';
import { LogContextEnum } from '../../logs/enums/log-context.enum';
import { IMpSubJobDetailInfo, IMpSubJobDetails } from './interfaces';
import { firstValueFrom, map, Observable } from 'rxjs';
import { Logger } from '../../logs/logger';

@Injectable()
export class SubJobsMappingService {
  private logger: Logger;

  constructor(
    _loggerFactory: LogFactoryService,
    private _subJobMpService: MpSubJobService,
    private _checkListMappingService: SubJobChecklistMappingService,
    private _productionMappingService: SubJobProductionMappingService,
  ) {
    this.logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  async getMappedDataCollection(
    mpIds: number[],
    retryWhenLocked: boolean,
  ): Promise<SubJobDto[]> {
    const rebuildProdDataQueries: Promise<void>[] = [];
    const prodDataQueries: Promise<IMpSubJobDetailInfo | null>[] = [];
    const detailsQueries: Promise<IMpSubJobDetails | null>[] = [];

    for (const mpId of mpIds) {
      rebuildProdDataQueries.push(
        firstValueFrom(
          this._subJobMpService.rebuildProdDataBeforeFetching(
            mpId,
            retryWhenLocked,
          ),
        ),
      );
      prodDataQueries.push(
        firstValueFrom(this._subJobMpService.findProdDataById(mpId)),
      );
      detailsQueries.push(
        firstValueFrom(this._subJobMpService.findDetailsById(mpId)),
      );
    }

    await Promise.all(rebuildProdDataQueries);
    const mpDataCollection = await Promise.all(prodDataQueries);
    const mpQuotationCollection = await Promise.all(detailsQueries);
    const mappedSubJobs: SubJobDto[] = [];
    let index = -1;

    for (const mpId of mpIds) {
      index++;
      const mpProdData = mpDataCollection[index];
      const mpDetails = mpQuotationCollection[index];

      if (!mpProdData) {
        this.logger.error(
          `> SubJobMapping: unable to fetch Multipress subJob workflow data by its ID. subJob ID: ${mpId}. Multipress endpoint: '/workflow/handleJobDetailInfo'`,
        );

        continue;
      }

      if (!mpDetails) {
        this.logger.error(
          `> SubJobMapping: unable to fetch Multipress subJob details by its ID. subJob ID: ${mpId}. Multipress endpoint: '/jobs/handleSubJobQuotationDetails'`,
        );

        continue;
      }

      mappedSubJobs.push(
        await firstValueFrom(this._map(mpId, mpProdData, mpDetails)),
      );
    }

    return mappedSubJobs;
  }

  private _map(
    mpId: number,
    mpProdData: IMpSubJobDetailInfo,
    mpDetails: IMpSubJobDetails,
  ): Observable<SubJobDto> {
    return this._checkListMappingService.getMappedData(mpDetails).pipe(
      map((checkList) => {
        const productionData = this._productionMappingService.getMappedData(
          mpProdData,
          mpId,
        );

        const subJobDto = new SubJobDto();
        subJobDto.mpId = mpId;
        subJobDto.description = mpDetails.description;
        subJobDto.quantity1 = mpDetails.run_01;
        subJobDto.checklist = checkList;
        subJobDto.productType = mpDetails.product_type;
        subJobDto.productNumber = mpDetails.product_number;
        subJobDto.paoInfo = mpDetails.internal_remark_1.replaceAll('\r', '\n');
        subJobDto.paoCheckInfo = mpDetails.internal_remark_2.replaceAll(
          '\r',
          '\n',
        );
        subJobDto.productionData = productionData ?? null;

        if (mpDetails.run_02) {
          subJobDto.quantity2 = mpDetails.run_02;
        }

        if (mpDetails.run_03) {
          subJobDto.quantity3 = mpDetails.run_03;
        }

        return subJobDto;
      }),
    );
  }
}
