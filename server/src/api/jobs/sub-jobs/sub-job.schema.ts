import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Job } from '../jobs/job.schema';
import { ISubJobChecklistItem, ISubJobProductionData } from './interfaces';

@Schema({ timestamps: true })
export class SubJob extends Document {
  @Prop()
  index: number;

  @Prop()
  mpId: number;

  @Prop()
  description: string;

  @Prop()
  quantity1: number;

  @Prop()
  quantity2: number;

  @Prop()
  quantity3: number;

  @Prop(
    raw([
      {
        _id: false,
        label: String,
        value: String,
      },
    ]),
  )
  checklist: ISubJobChecklistItem[];

  @Prop()
  productType: string;

  @Prop()
  productNumber: number;

  @Prop()
  paoInfo: string;

  @Prop()
  paoCheckInfo: string;

  @Prop(
    raw({
      devices: [String],
      meta: {
        surface: Number,
        deliveryServiceFinishs: [String],
        hasSheetPrinting: Boolean,
        hasNumericScoreCutting: Boolean,
        hasZundCutting: Boolean,
        hasKits: Boolean,
      },
      numericLayers: [
        {
          _id: false,
          id: Number,
          index: Number,
          width: Number,
          height: Number,
          sideMargin: Number,
          topMargin: Number,
          bottomMargin: Number,
          mediaName: String,
          mediaThickness: Number,
          mediaId: Number,
          quantity: Number,
          device: String, // E.G. EPSON or "Support/matière"
          quality: String,
          description: String,
          sheetName: String,
          finishingLayers: [
            {
              _id: false,
              finishingType: String,
              numericId: Number,
              operation: {
                name: String,
                operationType: String,
                width: Number,
                height: Number,
                doubleSided: Boolean,
              },
              sides: {
                left: Boolean,
                right: Boolean,
                top: Boolean,
                bottom: Boolean,
              },
              material: {
                name: String,
                id: String,
              },
              displayName: String,
            },
          ],
        },
      ],
      unlinkedFinishingLayers: [
        {
          _id: false,
          finishingType: String,
          numericId: Number,
          operation: {
            name: String,
            operationType: String,
            width: Number,
            height: Number,
            doubleSided: Boolean,
          },
          sides: {
            left: Boolean,
            right: Boolean,
            top: Boolean,
            bottom: Boolean,
          },
          material: {
            name: String,
            id: String,
          },
          displayName: String,
        },
      ],
      subcontracting: [
        {
          _id: false,
          relation: String,
          relationNumber: Number,
          contactName: String,
          sendingDate: String,
          deliveryDate: String,
          items: [
            {
              _id: false,
              description: String,
              remark: String,
              quantity: Number,
              purchasePrice: Number,
              salesPrice: Number,
              ordered: Boolean,
            },
          ],
        },
      ],
    }),
  )
  productionData: ISubJobProductionData | null;

  @Prop({ type: Types.ObjectId, ref: 'Job' })
  job: Job;
}

export const SubJobSchema = SchemaFactory.createForClass(SubJob);
