import { IMpSubJobFoldingsheetProduced } from './i-mp-sub-job-foldingsheet-produced';

export interface IMpSubJobFoldingSheet {
  id: string;
  x: number;
  y: number;
  connecting: number;
  unique: number;
  sheetreferences: string[];
  paperthickness: number;
  versions: boolean;
  foldingsheet: number;
  species: boolean;
  cutandstack: boolean;
  foldcatalog: string;
  printingside: number;
  colors: string;
  foldingdevice: string;
  printmatter: string;
  bindingmethod: string;
  papername: string;
  paperweight: number;
  productionmethod: string;
  frontcolors: number;
  backcolors: number;
  modelwidth: number;
  modelheight: number;
  sheetname: string;
  sheetnumber: number;
  sheets: number;
  pagenumbers: number[];
  backsidepages: number[];
  insert_step: number;
  insert_max: number;
  insert_depth: number;
  segment_step: number;
  segment_max: number;
  produced: IMpSubJobFoldingsheetProduced[];
}
