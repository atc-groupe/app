import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';

export interface IMpSubJobLargeFormatFinishing {
  type: MpSubJobFinishingTypeEnum.largeFormat;
  run: number; // Quantity
  per: number;
  operation: {
    name: string;
    type: string;
    amount: number;
    unit: string;
  };
  sides: {
    left: boolean;
    top: boolean;
    right: boolean;
    bottom: boolean;
  };
  material: {
    name: string;
    id: number;
    amount: number;
    unit: string;
  } | null;
  reference: {
    type: string;
    unique: number;
  };
}
