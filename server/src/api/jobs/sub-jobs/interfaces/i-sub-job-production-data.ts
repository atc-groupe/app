import { ISubJobNumericLayer } from './i-sub-job-numeric-layer';
import { TSubJobFinishingLayer } from '../types/t-sub-job-finishing-layer';
import { ISubJobProductionMeta } from './i-sub-job-production-meta';
import { ISubJobSubcontracting } from './i-sub-job-subcontracting';

export interface ISubJobProductionData {
  devices: string[];
  meta: ISubJobProductionMeta;
  numericLayers: ISubJobNumericLayer[] | null;
  unlinkedFinishingLayers: TSubJobFinishingLayer[] | null;
  subcontracting: ISubJobSubcontracting[] | null;
}
