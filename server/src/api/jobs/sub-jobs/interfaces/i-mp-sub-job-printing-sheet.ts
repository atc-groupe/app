/**
 * This interface is part of IMpSubJobDetailInfo
 * retrieved from /workflow/handleJobDetailInfo endpoint
 */

export interface IMpSubJobPrintingSheet {
  id: string;
  type: string;
  unique: number;
  multiply: number;
  status: string;
  sheetnumber: number; // printing sheet index
  producedamount: number;
  producedwaste: number;
  remark: string;
  counter: number;
  signature: string;
  combination: boolean;
  duplicate: string;
  envelope: string | null;
  modelwidth: number; // Job Width
  modelheight: number; // Job height
  spine: number;
  flapleft: number;
  flapright: number;
  pages: number;
  xelements: number;
  yelements: number;
  sheetname: string;
  versions: boolean;
  versiontext: string;
  versionnumber: number;
  versioncolors: string | null;
  versionremark: string;
  versionnames: string | null;
  versionnumbers: number | null;
  printmatter: string;
  imposition: {
    name: string;
    xposition: number[];
    xdistance: number[];
    yposition: number[];
    ydistance: number[];
    headposition: any[];
    bindingside: any[];
    guidesign: number;
  };
  center: boolean;
  screening: string;
  papername: string; // Material
  paperweight: number;
  paperwidth: number;
  paperheight: number;
  paperthickness: number;
  papercode: string;
  paperid: number;
  paperfrontcoating: number;
  paperbackcoating: number;
  papergrade: number;
  papercolor: string;
  papergrain: string;
  paperclass: number;
  paperamount: number;
  turnortumble: string;
  perfecting: string;
  species: boolean;
  spacing: number;
  sidemargin: number; // Fond perdu gauche droite
  gripperwidth: number; // Fond perdu haut
  endwidth: number; // Fond perdu bas
  overfoldwidth: number;
  overfoldside: boolean;
  productionmethod: string;
  layouts: number;
  normalplates: number;
  changeplates: number;
  setuptime: number;
  productiontime: number;
  printingwidth: number;
  printingheight: number;
  roll: boolean;
  bindingsidesheet: boolean;
  prints: number; // Quantité
  netto: number;
  printingwaste: number;
  finishingwaste: number;
  bruto: number;
  device: string; // Machine
  colors: {
    text: string;
    choice: string;
    totalfront: number;
    totalback: number;
    listfront: string[];
    listback: [];
  };
  description: string;
}
