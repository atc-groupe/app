import { ISubJobChecklistItem } from './i-sub-job-checklist-item';
import { ISubJobProductionData } from './i-sub-job-production-data';

export interface ISubJob {
  index: number;
  mpId: number;
  description: string;
  quantity1: number;
  quantity2?: number;
  quantity3?: number;
  checklist: ISubJobChecklistItem[];
  productType: string;
  productNumber: number;
  paoInfo: string;
  paoCheckInfo: string;
  productionData: ISubJobProductionData | null;
}
