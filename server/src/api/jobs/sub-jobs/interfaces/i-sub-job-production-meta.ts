export interface ISubJobProductionMeta {
  surface: number | null;
  deliveryServiceFinishs: string[] | null;
  hasSheetPrinting: boolean;
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
}
