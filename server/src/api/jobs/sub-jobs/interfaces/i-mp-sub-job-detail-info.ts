import { IMpSubJobPrintingSheet } from './i-mp-sub-job-printing-sheet';
import { IMpSubJobFoldingSheet } from './i-mp-sub-job-folding-sheet';
import { TMpSubJobFinishing } from '../types/t-mp-sub-job-finishing';
import { IMpSubJobSubcontractor } from './i-mp-sub-job-subcontractor';

export interface IMpSubJobDetailInfo {
  printingsheets: IMpSubJobPrintingSheet[];
  foldingsheets: IMpSubJobFoldingSheet[];
  finishing: TMpSubJobFinishing[];
  subcontractors: IMpSubJobSubcontractor[];
}
