/*
  This interfaces matches the data extracted from printingsheets key
  of workflow/handleJobDetailInfo MultiPress API endpoint
 */

import { TSubJobFinishingLayer } from '../types/t-sub-job-finishing-layer';

export interface ISubJobNumericLayer {
  id: number; // mp unique. Used to link subJob finishing layers
  index: number; // mp sheetnumber first index (1) provides the job format and material.
  width: number; // mp modelWidth
  height: number; // mp modelHeight
  sideMargin: number; // mp sidemargin
  topMargin: number; // mp gripperwidth
  bottomMargin: number; // mp endwidth
  mediaName: string; // mp papername
  mediaThickness: number; // units: mm.
  mediaId: number;
  device: string; // E.G. EPSON or "Support/matière"
  quality: string; // mp printingsheet.colors.choice
  quantity: number; // mp prints.
  description: string;
  sheetName: string;
  finishingLayers: TSubJobFinishingLayer[];
}
