import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';

export interface ISubJobLargeFormatFinishingLayer {
  numericId: number | null;
  finishingType: MpSubJobFinishingTypeEnum.largeFormat;
  operation: {
    name: string;
    operationType: string;
  };
  sides: {
    left: boolean;
    right: boolean;
    top: boolean;
    bottom: boolean;
  };
  material: {
    name: string;
    id: number;
  } | null;
}
