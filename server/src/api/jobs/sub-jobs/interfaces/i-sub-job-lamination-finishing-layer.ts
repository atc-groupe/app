import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';

export interface ISubJobLaminationFinishingLayer {
  numericId: number | null;
  finishingType: MpSubJobFinishingTypeEnum.lamination;
  operation: {
    name: string;
    width: number;
    height: number;
    doubleSided: boolean;
  };
  material: {
    name: string;
    id: number;
  };
}
