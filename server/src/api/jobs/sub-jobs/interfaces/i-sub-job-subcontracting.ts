import { ISubJobSubcontractingItem } from './i-sub-job-subcontracting-item';

export interface ISubJobSubcontracting {
  relation: string;
  relationNumber: number;
  contactName: string | null;
  sendingDate: string;
  deliveryDate: string;
  items: ISubJobSubcontractingItem[];
}
