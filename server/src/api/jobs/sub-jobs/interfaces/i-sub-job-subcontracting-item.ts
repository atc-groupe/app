export interface ISubJobSubcontractingItem {
  description: string;
  remark: string;
  quantity: number;
  purchasePrice: number;
  salesPrice: number;
  ordered: boolean;
}
