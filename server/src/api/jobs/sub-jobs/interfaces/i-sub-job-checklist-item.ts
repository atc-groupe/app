export interface ISubJobChecklistItem {
  label: string;
  value: string;
}
