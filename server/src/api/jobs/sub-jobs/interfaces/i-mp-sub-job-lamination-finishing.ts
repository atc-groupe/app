import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';

export interface IMpSubJobLaminationFinishing {
  type: MpSubJobFinishingTypeEnum.lamination;
  run: number; // Quantity
  per: number;
  operation: {
    name: string;
    width: number;
    height: number;
    doublesided: boolean;
  };
  sides: {
    left: boolean;
    top: boolean;
    right: boolean;
    bottom: boolean;
  };
  material: {
    name: string;
    id: number;
    amount: number;
    unit: string;
  };
  reference: {
    type: string;
    unique: number;
  };
}
