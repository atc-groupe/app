import { Injectable, LoggerService } from '@nestjs/common';
import { TSubJobCheckList } from '../types/t-sub-job-check-list';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';
import { IMpSubJobDetails } from '../interfaces';
import { SystemService } from '../../../system/system.service';
import { IChecklist } from '../../../system/interfaces';
import { map, Observable } from 'rxjs';

@Injectable()
export class SubJobChecklistMappingService {
  private logger: LoggerService;

  constructor(
    private _systemService: SystemService,
    private loggerFactory: LogFactoryService,
  ) {
    this.logger = loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  getMappedData(subJobDetails: IMpSubJobDetails): Observable<TSubJobCheckList> {
    const detail = subJobDetails;
    const cl: TSubJobCheckList = [];

    return this._getMpNumericChecklist(detail).pipe(
      map((mCl) => {
        if (!mCl) {
          return cl;
        }

        if (detail.text_01)
          cl.push({
            label: mCl.header1,
            value: this._getParsedText(detail.text_01),
          });
        if (detail.text_02)
          cl.push({
            label: mCl.header2,
            value: this._getParsedText(detail.text_02),
          });
        if (detail.text_03)
          cl.push({
            label: mCl.header3,
            value: this._getParsedText(detail.text_03),
          });
        if (detail.text_04)
          cl.push({
            label: mCl.header4,
            value: this._getParsedText(detail.text_04),
          });
        if (detail.text_05)
          cl.push({
            label: mCl.header5,
            value: this._getParsedText(detail.text_05),
          });
        if (detail.text_06)
          cl.push({
            label: mCl.header6,
            value: this._getParsedText(detail.text_06),
          });
        if (detail.text_07)
          cl.push({
            label: mCl.header7,
            value: this._getParsedText(detail.text_07),
          });
        if (detail.text_08)
          cl.push({
            label: mCl.header8,
            value: this._getParsedText(detail.text_08),
          });
        if (detail.text_09)
          cl.push({
            label: mCl.header9,
            value: this._getParsedText(detail.text_09),
          });
        if (detail.text_10)
          cl.push({
            label: mCl.header10,
            value: this._getParsedText(detail.text_10),
          });
        if (detail.text_11)
          cl.push({
            label: mCl.header11,
            value: this._getParsedText(detail.text_11),
          });
        if (detail.text_12)
          cl.push({
            label: mCl.header12,
            value: this._getParsedText(detail.text_12),
          });

        return cl;
      }),
    );
  }

  private _getMpNumericChecklist(
    subJobDetails: IMpSubJobDetails,
  ): Observable<IChecklist | null> {
    return this._systemService.findChecklistById(subJobDetails.checklist).pipe(
      map((checklist) => {
        if (!checklist) {
          this.logger.error(
            `JobComputedProductionMappingService > checklist not found!. id: ${subJobDetails.checklist}. Job ID: ${subJobDetails.parent}`,
          );
        }

        return checklist;
      }),
    );
  }

  private _getParsedText(text: string): string {
    return text.replaceAll('\r', '\n').trim();
  }
}
