import { Injectable } from '@nestjs/common';
import {
  IMpSubJobDetailInfo,
  ISubJobLaminationFinishingLayer,
  ISubJobLargeFormatFinishingLayer,
} from '../interfaces';
import { TSubJobFinishingLayer } from '../types/t-sub-job-finishing-layer';
import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';
import { Logger } from '../../../logs/logger';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';

@Injectable()
export class SubJobProductionFinishingMappingService {
  private _logger: Logger;
  constructor(_loggerFactory: LogFactoryService) {
    this._logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  getMappedFinishingLayers(
    mpProdData: IMpSubJobDetailInfo,
    mpId: number,
  ): TSubJobFinishingLayer[] {
    const finishingLayers: TSubJobFinishingLayer[] = [];

    mpProdData.finishing.forEach((finishing): void => {
      /*
        If a finishing is not linked to a printingSheet unique:
        Todo: Log employee error: finishing not linked to a printingSheet.
       */
      const numericId = finishing.reference ? finishing.reference.unique : null;
      const logFinishingType: string = finishing.type;

      if (!numericId) {
        this._logger.warn(
          `> SubJobMapping: A subJob finishing layer has no reference to a numeric layer. SubJob ID: ${mpId}, finishing type: ${finishing.type}`,
        );
      }

      switch (finishing.type) {
        case MpSubJobFinishingTypeEnum.largeFormat: {
          const finishingLayer: ISubJobLargeFormatFinishingLayer = {
            numericId,
            finishingType: MpSubJobFinishingTypeEnum.largeFormat,
            operation: {
              name: finishing.operation.name,
              operationType: finishing.operation.type,
            },
            sides: {
              left: finishing.sides.left,
              right: finishing.sides.right,
              top: finishing.sides.top,
              bottom: finishing.sides.bottom,
            },
            material: null,
          };

          if (finishing.material) {
            finishingLayer.material = {
              name: finishing.material.name,
              id: finishing.material.id,
            };
          }

          finishingLayers.push(finishingLayer);
          break;
        }
        case MpSubJobFinishingTypeEnum.lamination: {
          // Error check: Some times a lamination layer has no material defined.
          if (!finishing.material) {
            this._logger.error(
              `SubJobMappingService > a subJob lamination layer has no linked material. SubJob ID: ${mpId}`,
            );
          }

          const laminationLayer: ISubJobLaminationFinishingLayer = {
            numericId,
            finishingType: MpSubJobFinishingTypeEnum.lamination,
            operation: {
              name: finishing.operation.name,
              width: finishing.operation.width,
              height: finishing.operation.height,
              doubleSided: finishing.operation.doublesided,
            },
            material: {
              name: finishing.material?.name,
              id: finishing.material?.id,
            },
          };

          finishingLayers.push(laminationLayer);
          break;
        }
        default: {
          this._logger.error(
            `> SubJobMapping: SubJob finishing type error: finishing type do not math any configured one. (configured values: 'largeformat' | 'lamination'. subJob ID: ${mpId}, Finishing type: ${logFinishingType}`,
          );
        }
      }
    });

    return finishingLayers;
  }
}
