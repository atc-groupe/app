import { Injectable } from '@nestjs/common';
import { SubJobNumericLayerMappingHelperService } from './sub-job-numeric-layer-mapping-helper.service';
import { SubJobProductionMetaHelperService } from './sub-job-production-meta-helper.service';
import { Logger } from '../../../logs/logger';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';
import {
  IMpSubJobDetailInfo,
  ISubJobNumericLayer,
  ISubJobProductionData,
} from '../interfaces';
import { SubJobProductionNumericMappingService } from './sub-job-production-numeric-mapping.service';
import { SubJobProductionFinishingMappingService } from './sub-job-production-finishing-mapping.service';
import { SubJobProductionSubcontractingMappingService } from './sub-job-production-subcontracting-mapping.service';

@Injectable()
export class SubJobProductionMappingService {
  private _logger: Logger;

  constructor(
    _loggerFactory: LogFactoryService,
    private _numericMappingService: SubJobProductionNumericMappingService,
    private _finishingMappingService: SubJobProductionFinishingMappingService,
    private _subcontractingMappingService: SubJobProductionSubcontractingMappingService,
    private _numericMappingHelper: SubJobNumericLayerMappingHelperService,
    private _metaMappingHelper: SubJobProductionMetaHelperService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }
  public getMappedData(
    mpProdData: IMpSubJobDetailInfo,
    mpId: number,
  ): ISubJobProductionData | null {
    if (this._subJobHasNoProductionData(mpProdData)) {
      return null;
    }
    const printingSheets = mpProdData.printingsheets;
    let mappedNumericLayers: ISubJobNumericLayer[] = [];
    const devices: string[] = [];

    const mappedFinishingLayers =
      this._finishingMappingService.getMappedFinishingLayers(mpProdData, mpId);

    if (printingSheets.length) {
      mappedNumericLayers = this._numericMappingHelper.getSortedNumericLayers(
        this._numericMappingService.getMappedNumericLayers(
          mpProdData.printingsheets,
        ),
      );

      mappedNumericLayers.forEach((numericLayer) => {
        numericLayer.finishingLayers = mappedFinishingLayers.filter(
          (finishingLayer) => {
            return finishingLayer.numericId === numericLayer.id;
          },
        );

        const mappedDevice = this._numericMappingHelper.getComputedDevice(
          numericLayer.device,
        );
        if (!devices.includes(mappedDevice)) {
          devices.push(mappedDevice);
        }
      });
    }

    const meta = this._metaMappingHelper.getMappedData(
      mappedNumericLayers,
      mappedFinishingLayers,
    );

    const unlinkedFinishingLayers = mappedFinishingLayers.filter(
      (layer) => layer.numericId === null || !printingSheets.length,
    );

    const subcontracting =
      this._subcontractingMappingService.getMappedSubcontractingLayers(
        mpProdData,
      );

    return {
      devices,
      meta,
      numericLayers: mappedNumericLayers.length ? mappedNumericLayers : null,
      unlinkedFinishingLayers: unlinkedFinishingLayers.length
        ? unlinkedFinishingLayers
        : null,
      subcontracting,
    };
  }

  private _subJobHasNoProductionData(mpProdData: IMpSubJobDetailInfo): boolean {
    return (
      mpProdData.printingsheets.length === 0 &&
      mpProdData.foldingsheets.length === 0 &&
      mpProdData.finishing.length === 0
    );
  }
}
