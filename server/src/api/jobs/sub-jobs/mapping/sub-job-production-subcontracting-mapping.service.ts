import { Injectable } from '@nestjs/common';
import {
  IMpSubJobDetailInfo,
  IMpSubJobSubcontractor,
  ISubJobSubcontracting,
} from '../interfaces';

@Injectable()
export class SubJobProductionSubcontractingMappingService {
  getMappedSubcontractingLayers(
    mpProdData: IMpSubJobDetailInfo,
  ): ISubJobSubcontracting[] | null {
    const mpSubContractors = mpProdData.subcontractors;

    if (!mpSubContractors || mpSubContractors.length === 0) {
      return null;
    }

    return mpSubContractors.reduce(
      (acc: ISubJobSubcontracting[], mpItem: IMpSubJobSubcontractor) => {
        let subcontracting: ISubJobSubcontracting | undefined = acc.find(
          (item) => this._isSameSubcontracting(mpItem, item),
        );

        if (!subcontracting) {
          subcontracting = {
            relation: mpItem.relation,
            relationNumber: mpItem.relation_number,
            sendingDate: mpItem.sending_date,
            deliveryDate: mpItem.delivery_date,
            contactName: this._getContactName(
              mpItem.relation_number,
              mpSubContractors,
            ),
            items: [],
          };

          acc.push(subcontracting);
        }

        subcontracting.items.push({
          description: this._getParsedDescription(mpItem.description),
          ordered: mpItem.ordered !== 'OUI',
          quantity: mpItem.run,
          remark: mpItem.remark,
          purchasePrice: mpItem.purchaseprice,
          salesPrice: mpItem.salesprice,
        });

        return acc;
      },
      [],
    );
  }

  _isSameSubcontracting(
    mpItem: IMpSubJobSubcontractor,
    subcontracting: ISubJobSubcontracting,
  ): boolean {
    return (
      subcontracting.relationNumber === mpItem.relation_number &&
      subcontracting.sendingDate === mpItem.sending_date &&
      subcontracting.deliveryDate === mpItem.delivery_date
    );
  }

  private _getContactName(
    relationNumber: number,
    mpSubContractors: IMpSubJobSubcontractor[],
  ): string | null {
    const mpSubContractor = mpSubContractors.find(
      (mpItem) =>
        mpItem.relation_number === relationNumber && mpItem.contact_name !== '',
    );

    return mpSubContractor ? mpSubContractor.contact_name : null;
  }

  private _getParsedDescription(description: string): string {
    return description.replaceAll('/', ' > ').replaceAll(/[0-9]{2}\. /g, '');
  }
}
