import { Injectable } from '@nestjs/common';
import { TSubJobFinishingLayer } from '../types/t-sub-job-finishing-layer';
import { SubJobNumericLayerMappingHelperService } from './sub-job-numeric-layer-mapping-helper.service';
import { SubJobFinishingLayerMappingHelperService } from './sub-job-finishing-layer-mapping-helper.service';
import { ISubJobNumericLayer, ISubJobProductionMeta } from '../interfaces';

@Injectable()
export class SubJobProductionMetaHelperService {
  constructor(
    private _numericLayerHelper: SubJobNumericLayerMappingHelperService,
    private _finishingLayerHelper: SubJobFinishingLayerMappingHelperService,
  ) {}

  getMappedData(
    mappedNumericLayers: ISubJobNumericLayer[],
    mappedFinishingLayers: TSubJobFinishingLayer[],
  ): ISubJobProductionMeta {
    let surface: number | null = null;
    let hasSheetPrinting = false;
    let hasNumericScoreCutting = false;

    if (mappedNumericLayers.length) {
      const firstNumericLayer = mappedNumericLayers[0];
      surface = this._getSurface(firstNumericLayer);
      hasSheetPrinting =
        this._numericLayerHelper.hasSheetProduction(mappedNumericLayers);
      hasNumericScoreCutting =
        this._hasNumericScoreCutting(mappedNumericLayers);
    }

    const hasZundCutting = this._finishingLayerHelper.hasZundCutting(
      mappedFinishingLayers,
    );

    const deliveryServiceFinishs =
      this._finishingLayerHelper.getDeliveryServiceFinishing(
        mappedFinishingLayers,
      );

    const hasKits = this._finishingLayerHelper.hasKitFinishing(
      mappedFinishingLayers,
    );

    return {
      surface,
      deliveryServiceFinishs,
      hasSheetPrinting,
      hasNumericScoreCutting,
      hasZundCutting,
      hasKits,
    };
  }

  private _getSurface(firstNumericLayer: ISubJobNumericLayer): number {
    return (
      (firstNumericLayer.width / 1000) *
      (firstNumericLayer.height / 1000) *
      firstNumericLayer.quantity
    );
  }

  private _hasNumericScoreCutting(
    numericLayers: ISubJobNumericLayer[],
  ): boolean {
    if (!numericLayers.length) {
      return false;
    }

    return numericLayers.some((numericLayer) => {
      return (
        this._numericLayerHelper.isPrintingLayer(numericLayer) &&
        this._finishingLayerHelper.hasScoreCuttingFinishing(
          numericLayer.finishingLayers,
        )
      );
    });
  }
}
