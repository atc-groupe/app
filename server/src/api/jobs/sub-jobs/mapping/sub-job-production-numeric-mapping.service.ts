import { Injectable } from '@nestjs/common';
import { IMpSubJobPrintingSheet, ISubJobNumericLayer } from '../interfaces';

@Injectable()
export class SubJobProductionNumericMappingService {
  getMappedNumericLayers(
    printingSheets: IMpSubJobPrintingSheet[],
  ): ISubJobNumericLayer[] {
    return printingSheets.reduce(
      (acc: ISubJobNumericLayer[], printingSheet): ISubJobNumericLayer[] => {
        /*
          This test checks if a MP numeric item has been tilled.
          In this case we just want to get the first one.
          multiple pieces are identified by printingSheet "unique" key
         */
        const pieceLayerTest = acc.find(
          (item) => item.id === printingSheet.unique,
        );

        if (pieceLayerTest) {
          return acc;
        }

        acc.push({
          id: printingSheet.unique,
          index: printingSheet.sheetnumber,
          width: printingSheet.modelwidth,
          height: printingSheet.modelheight,
          sideMargin: printingSheet.sidemargin,
          topMargin: printingSheet.gripperwidth,
          bottomMargin: printingSheet.endwidth,
          mediaName: printingSheet.papername,
          mediaThickness: printingSheet.paperthickness,
          mediaId: printingSheet.paperid,
          device: printingSheet.device,
          quality: printingSheet.colors.choice,
          quantity: printingSheet.prints,
          description: printingSheet.description,
          sheetName: printingSheet.sheetname,
          finishingLayers: [],
        });

        return acc;
      },
      [],
    );
  }
}
