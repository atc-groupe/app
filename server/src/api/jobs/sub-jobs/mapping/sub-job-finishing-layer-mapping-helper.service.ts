import { Injectable } from '@nestjs/common';
import { TSubJobFinishingLayer } from '../types/t-sub-job-finishing-layer';
import { MpSubJobFinishingTypeEnum } from '../enums/mp-sub-job-finishing-type.enum';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SubJobFinishingLayerMappingHelperService {
  private _scoreCuttingFilters;
  private _zundCuttingFilters;
  private _deliveryServiceFinishsFilters;
  private _kitFinishsFilters;
  constructor(private _configService: ConfigService) {
    this._scoreCuttingFilters = this._getConfigFilters(
      'MAPPING_FILTER_SCORE_FINISHS',
    );

    this._zundCuttingFilters = this._getConfigFilters(
      'MAPPING_FILTER_ZUND_CUTTING_FINISHS',
    );

    this._deliveryServiceFinishsFilters = this._getConfigFilters(
      'MAPPING_FILTER_DELIVERY_SERVICE_FINISHS',
    );

    this._kitFinishsFilters = this._getConfigFilters(
      'MAPPING_FILTER_KIT_FINISHS',
    );
  }

  hasScoreCuttingFinishing(
    mappedFinishingLayers: TSubJobFinishingLayer[],
  ): boolean {
    if (!mappedFinishingLayers.length) {
      return false;
    }

    return this._includesLargeFormatFinishing(
      mappedFinishingLayers,
      this._scoreCuttingFilters,
    );
  }

  hasZundCutting(mappedFinishingLayers: TSubJobFinishingLayer[]): boolean {
    if (!mappedFinishingLayers.length) {
      return false;
    }

    return this._includesLargeFormatFinishing(
      mappedFinishingLayers,
      this._zundCuttingFilters,
    );
  }

  getDeliveryServiceFinishing(
    mappedFinishingLayers: TSubJobFinishingLayer[],
  ): string[] | null {
    if (!mappedFinishingLayers.length) {
      return null;
    }

    const finishs = mappedFinishingLayers.reduce((acc: string[], layer) => {
      if (!this._isLargeFormatFinishing(layer)) {
        return acc;
      }

      const operationName = layer.operation.name;

      if (
        this._deliveryServiceFinishsFilters.some((filter) =>
          operationName.includes(filter),
        )
      ) {
        acc.push(operationName);
      }

      return acc;
    }, []);

    return finishs.length ? finishs : null;
  }

  hasKitFinishing(mappedFinishingLayers: TSubJobFinishingLayer[]): boolean {
    if (!mappedFinishingLayers.length) {
      return false;
    }

    return this._includesLargeFormatFinishing(
      mappedFinishingLayers,
      this._kitFinishsFilters,
    );
  }

  private _getConfigFilters(key: string): string[] {
    const rawData = this._configService.getOrThrow(key);

    return rawData.split(':');
  }

  private _includesLargeFormatFinishing(
    mappedFinishingLayers: TSubJobFinishingLayer[],
    finishs: string[],
  ): boolean {
    return mappedFinishingLayers.some((layer) => {
      if (!this._isLargeFormatFinishing(layer)) {
        return false;
      }

      return finishs.some((finish) => layer.operation.name.includes(finish));
    });
  }

  private _isLargeFormatFinishing(layer: TSubJobFinishingLayer) {
    return layer.finishingType === MpSubJobFinishingTypeEnum.largeFormat;
  }
}
