import { Injectable, LoggerService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';
import { IMpSubJobPrintingSheet, ISubJobNumericLayer } from '../interfaces';

@Injectable()
export class SubJobNumericLayerMappingHelperService {
  private logger: LoggerService;

  constructor(
    private _configService: ConfigService,
    private _loggerFactory: LogFactoryService,
  ) {
    this.logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  getComputedDevice(device: string): string {
    const separators: string = this._configService.getOrThrow(
      'MAPPING_MACHINE_VARIABLE_SEPARATORS',
    );

    for (const separator of separators.split(':')) {
      if (device.includes(separator)) {
        return device.split(separator)[0];
      }
    }

    return device;
  }

  hasSheetProduction(numericLayers: ISubJobNumericLayer[]): boolean {
    if (!numericLayers.length) {
      return false;
    }

    return numericLayers.some((layer) => {
      return (
        this.isMaterialDevice(layer.device) ||
        this._isSheetPrinting(layer.device)
      );
    });
  }

  isNumericLayerFalseSize(
    printingSheet: IMpSubJobPrintingSheet,
    printingSheets: IMpSubJobPrintingSheet[],
  ): boolean {
    const testSheets = printingSheets.filter(
      (sheet) => (sheet.id = printingSheet.id),
    );

    return testSheets.length > 1;
  }

  getSortedNumericLayers(
    numericLayers: ISubJobNumericLayer[],
  ): ISubJobNumericLayer[] {
    const firstNumericLayer = this._getFirstNumericLayer(numericLayers);

    if (numericLayers.length === 1) {
      return [firstNumericLayer];
    }

    const addedNumericLayers = numericLayers.filter(
      (layer) => layer.id !== firstNumericLayer.id,
    );

    return [firstNumericLayer, ...addedNumericLayers];
  }

  isMaterialDevice(rawDevice: string): boolean {
    const rawFilter = this._configService.getOrThrow(
      'MAPPING_FILTER_MATERIAL_DEVICE',
    );

    const filter = rawFilter.split(':');

    return filter.includes(rawDevice);
  }

  isPrintingLayer(numericLayer: ISubJobNumericLayer) {
    return !this.isMaterialDevice(numericLayer.device);
  }

  private _isMaterialLayer(numericLayer: ISubJobNumericLayer) {
    return this.isMaterialDevice(numericLayer.device);
  }

  private _getFirstNumericLayer(
    filteredNumericLayers: ISubJobNumericLayer[],
  ): ISubJobNumericLayer {
    const printingLayers = filteredNumericLayers.filter((layer) =>
      this.isPrintingLayer(layer),
    );

    if (printingLayers.length) {
      return printingLayers[0];
    }

    const materialLayers = filteredNumericLayers.filter((layer) =>
      this._isMaterialLayer(layer),
    );

    return materialLayers[0];
  }

  private _isSheetPrinting(rawDevice: string): boolean {
    return rawDevice.toLowerCase().includes('plaques');
  }
}
