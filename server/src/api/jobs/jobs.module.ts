import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MpHttpModule } from '../../shared/modules/mp-http.module';
import { HelperModule } from '../../shared/modules/helper.module';
import { LogsModule } from '../logs/logs.module';
import { SystemModule } from '../system/system.module';
import { DeliveryAddressesModule } from './delivery-addresses/delivery-addresses.module';
import { Job, JobSchema } from './jobs/job.schema';
import { SubJobsModule } from './sub-jobs/sub-jobs.module';
import {
  JobsService,
  MpJobsService,
  JobsMpMappingService,
  JobsSyncService,
  JobsMetaMappingService,
} from './jobs/services';
import { JobDeliveryMetaMappingService } from './jobs/mapping/job-delivery-meta-mapping.service';
import { JobProductionMetaMappingService } from './jobs/mapping/job-production-meta-mapping.service';
import { JobNumberValidationPipe } from './jobs/pipes/job-number-validation.pipe';
import { JobsController } from './jobs/jobs.controller';
import { SubJob, SubJobSchema } from './sub-jobs/sub-job.schema';
import {
  DeliveryAddress,
  DeliveryAddressSchema,
} from './delivery-addresses/delivery-address.schema';
import { JobsPaoService } from './jobs/services';
import { JobsPaoController } from './jobs/jobs-pao.controller';
import { StockModule } from '../stock/stock.module';
import { JobsStatusEventDispatcher } from './jobs/events/jobs-status-event-dispatcher';
import { JobPaoReservationEventSubscriber } from './jobs/events/job-pao-reservation-event-subscriber';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Job.name, schema: JobSchema },
      { name: SubJob.name, schema: SubJobSchema },
      { name: DeliveryAddress.name, schema: DeliveryAddressSchema },
    ]),
    MpHttpModule,
    HelperModule,
    LogsModule,
    SystemModule,
    DeliveryAddressesModule,
    SubJobsModule,
    StockModule,
  ],
  providers: [
    JobsService,
    MpJobsService,
    JobsMpMappingService,
    JobsMetaMappingService,
    JobDeliveryMetaMappingService,
    JobProductionMetaMappingService,
    JobNumberValidationPipe,
    JobsSyncService,
    JobsPaoService,
    JobsStatusEventDispatcher,
    JobPaoReservationEventSubscriber,
  ],
  controllers: [JobsController, JobsPaoController],
  exports: [JobsSyncService, JobsService, MpJobsService],
})
export class JobsModule {}
