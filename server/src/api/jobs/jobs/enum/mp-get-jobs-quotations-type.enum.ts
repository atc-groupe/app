export enum MpGetJobsQuotationsTypeEnum {
  jobs,
  quotations,
  archivedJobs,
  archivedQuotations,
  combinationJobs,
  combinationQuotations,
  onStockJobs,
  archivedOnStockJobs,
}
