export enum PaoReservationStatusEnum {
  Created = 'created',
  Pending = 'pending',
  Ordered = 'ordered',
  Treated = 'treated',
}
