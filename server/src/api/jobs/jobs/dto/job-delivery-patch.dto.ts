import { IsOptional, IsString } from 'class-validator';

export class JobDeliveryPatchDto {
  @IsString()
  @IsOptional()
  comment: string;
}
