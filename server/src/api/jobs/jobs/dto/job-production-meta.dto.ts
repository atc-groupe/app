export class JobProductionMetaDto {
  printingSurface = 0;
  modelCount = 0;
  manufacturedPiecesCount = 0;
  productionTypes: string[] = [];
  deliveryServiceFinishs: string[] = [];
  hasSheetProduction = false;
  hasNumericScoreCutting = false;
  hasZundCutting = false;
  hasKits = false;
}
