import { IsIn, IsNumber, IsString } from 'class-validator';

export class JobPaoStockSheetReservationsHandleDto {
  @IsNumber()
  employeeNumber: number;

  @IsString()
  description: string;

  @IsNumber()
  productType: number;

  @IsNumber()
  productId: number;

  @IsIn(['handle', 'undo'])
  action: 'handle' | 'undo';

  @IsNumber()
  subtract: number;

  @IsNumber()
  add: number;
}
