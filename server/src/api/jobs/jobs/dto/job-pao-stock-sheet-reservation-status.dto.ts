import { PaoReservationStatusEnum } from '../enum/pao-reservation-status.enum';
import { IsEnum } from 'class-validator';

export class JobPaoStockSheetReservationStatusDto {
  @IsEnum(PaoReservationStatusEnum)
  fromStatus: PaoReservationStatusEnum;

  @IsEnum(PaoReservationStatusEnum)
  toStatus: PaoReservationStatusEnum;
}
