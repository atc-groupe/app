import {
  IsBoolean,
  IsEnum,
  IsIn,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { PaoReservationStatusEnum } from '../enum/pao-reservation-status.enum';
import { PaperTypeEnum } from '../../../stock/enum';

export class JobPaoStockSheetReservationPatchDto {
  @IsNumber()
  productType: number;

  @IsNumber()
  productId: number;

  @IsString()
  displayName: string;

  @IsEnum(PaperTypeEnum)
  paperType: PaperTypeEnum;

  @IsNumber()
  quantity: number;

  @IsOptional()
  @IsEnum(PaoReservationStatusEnum)
  status: PaoReservationStatusEnum;

  @IsBoolean()
  @IsOptional()
  isCustomSize: boolean;

  @IsOptional()
  @IsNumber()
  width: number;

  @IsOptional()
  @IsNumber()
  height: number;

  @IsOptional()
  @IsBoolean()
  isDoubleSided: boolean;

  @IsOptional()
  @IsBoolean()
  isNesting: boolean;

  @IsOptional()
  @IsNumber()
  numberBySheet: number;

  @IsOptional()
  @IsNumber()
  mutationId: number;

  @IsOptional()
  @IsNumber()
  stockDecreaseCount: number;

  @IsString()
  userDisplayName: string;

  @IsNumber()
  userEmployeeNumber: number;

  @IsOptional()
  @IsString()
  paoComment: string;
}
