import { IsNumber, IsOptional, IsString } from 'class-validator';

export class ChangeJobStatusDto {
  @IsOptional()
  jobNumber: number;

  @IsNumber({}, { message: 'Invalid status number' })
  statusNumber: number;

  @IsString({ message: 'invalid reason' })
  reason: string;
}
