import { JobProductionMetaDto } from './job-production-meta.dto';
import { JobDeliveryMetaDto } from './job-delivery-meta.dto';

export class JobMetaDto {
  printingSurface: number;
  modelCount: number;
  manufacturedPiecesCount: number;
  productionTypes: string[];
  deliveryServiceFinishs: string[];
  hasSheetProduction: boolean;
  hasNumericScoreCutting: boolean;
  hasZundCutting: boolean;
  hasKits: boolean;
  addressCount: number;
  addressZipCodes: string[];
  deliveryMethods: string[];
  deliverySendingDates: string[];

  constructor(
    prodMetaDto: JobProductionMetaDto,
    deliveryMetaDto: JobDeliveryMetaDto,
  ) {
    this.printingSurface = prodMetaDto.printingSurface;
    this.modelCount = prodMetaDto.modelCount;
    this.manufacturedPiecesCount = prodMetaDto.manufacturedPiecesCount;
    this.productionTypes = prodMetaDto.productionTypes;
    this.deliveryServiceFinishs = prodMetaDto.deliveryServiceFinishs;
    this.hasSheetProduction = prodMetaDto.hasSheetProduction;
    this.hasNumericScoreCutting = prodMetaDto.hasNumericScoreCutting;
    this.hasZundCutting = prodMetaDto.hasZundCutting;
    this.hasKits = prodMetaDto.hasKits;

    this.addressCount = deliveryMetaDto.addressCount;
    this.addressZipCodes = deliveryMetaDto.addressZipCodes;
    this.deliveryMethods = deliveryMetaDto.deliveryMethods;
    this.deliverySendingDates = deliveryMetaDto.deliverySendingDates;
  }
}
