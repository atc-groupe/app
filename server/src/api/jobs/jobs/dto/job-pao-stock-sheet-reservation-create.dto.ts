import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { PaperTypeEnum } from '../../../stock/enum';

export class JobPaoStockSheetReservationCreateDto {
  @IsNumber()
  productType: number;

  @IsNumber()
  productId: number;

  @IsString()
  displayName: string;

  @IsEnum(PaperTypeEnum)
  paperType: PaperTypeEnum;

  @IsBoolean()
  isCustomSize: boolean;

  @IsNumber()
  quantity: number;

  @IsOptional()
  @IsNumber()
  width: number;

  @IsOptional()
  @IsNumber()
  height: number;

  @IsOptional()
  @IsBoolean()
  isDoubleSided: boolean;

  @IsOptional()
  @IsBoolean()
  isNesting: boolean;

  @IsOptional()
  @IsNumber()
  numberBySheet: number;

  @IsString()
  userDisplayName: string;

  @IsNumber()
  userEmployeeNumber: number;

  @IsOptional()
  @IsString()
  paoComment: string;
}
