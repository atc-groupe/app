import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { StatusNumberListTransformer } from '../data-transformer/status-number-list.transformer';
import { ICollectionQueryPaginationParams } from '../../../../shared/interfaces';

export class JobQueryParamsDto implements ICollectionQueryPaginationParams {
  @IsString()
  @IsOptional()
  sendingDate?: string;

  @IsString()
  @IsOptional()
  startSendingDate?: string;

  @IsString()
  @IsOptional()
  endSendingDate?: string;

  @IsNumber()
  @IsOptional()
  statusNumber?: number;

  @IsArray()
  @IsOptional()
  @Transform(StatusNumberListTransformer)
  statusNumberList?: number[];

  @IsNumber()
  @IsOptional()
  startStatusNumber?: number;

  @IsNumber()
  @IsOptional()
  endStatusNumber?: number;

  @IsNumber()
  @IsOptional()
  jobEndNumber?: string;

  @IsString()
  @IsOptional()
  fullSearch?: string;

  @IsBoolean()
  @IsOptional()
  hasPaoReservation?: boolean;

  @IsOptional()
  @IsBoolean()
  populate?: boolean;

  @IsOptional()
  @IsNumber()
  countPerPage?: number;

  @IsNumber()
  @IsOptional()
  pageIndex?: number;
}
