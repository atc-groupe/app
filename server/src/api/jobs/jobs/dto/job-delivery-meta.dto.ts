export class JobDeliveryMetaDto {
  addressCount = 0;
  addressZipCodes: string[] = [];
  deliveryMethods: string[] = [];
  deliverySendingDates: string[] = [];
}
