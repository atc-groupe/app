import { IsOptional, IsString } from 'class-validator';

export class JobPaoCheckCommentPatchDto {
  @IsString()
  @IsOptional()
  checkComment: string;
}
