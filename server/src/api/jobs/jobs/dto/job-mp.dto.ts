import { JobTypeEnum } from '../enum/job-type.enum';

export class JobMpDto {
  id: number;
  number: string;
  type: number;
  description: string;
  company: string;
  contactName: string;
  phone: string;
  email: string;
  jobStatus: string;
  jobStatusNumber: number;
  deliveryDate: string | null;
  infoDigital: string;
  relationNumber: number;
  salesMan: string;
  creator: string;
  jobManager: string;
  infoGlobal?: string;
  infoPrePress?: string;
  infoPrint?: string;
  infoFinishing?: string;
  infoSubContract?: string;
  infoExpedition?: string;
  infoPlanning?: string;
  jobStatusHistory?: string[];
  planned?: boolean;
  deliveredDate: string | null;
  proofDate: string | null;
  returnDate: string | null;
  sendingDate: string | null;
  subJobsIds: number[];
  deliveryAddressesIds: number[];
}
