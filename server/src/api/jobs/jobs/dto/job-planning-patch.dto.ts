import { IsOptional, IsString } from 'class-validator';

export class JobPlanningPatchDto {
  @IsString()
  @IsOptional()
  comment: string;
}
