import { Injectable } from '@nestjs/common';
import { JobProductionMetaDto } from '../dto';
import { ConfigService } from '@nestjs/config';
import { SubJobNumericLayerMappingHelperService } from '../../sub-jobs/mapping';
import { SubJob } from '../../sub-jobs/sub-job.schema';

@Injectable()
export class JobProductionMetaMappingService {
  constructor(
    private _configService: ConfigService,
    private _subJobNumericLayerMappingHelperService: SubJobNumericLayerMappingHelperService,
  ) {}

  getMappedData(subJobs: SubJob[]): JobProductionMetaDto {
    const printMetaDto = new JobProductionMetaDto();

    return subJobs.reduce((acc: JobProductionMetaDto, subJobData: SubJob) => {
      const productionData = subJobData.productionData;

      if (!productionData) {
        return acc;
      }

      const numericLayers = productionData.numericLayers;

      if (!numericLayers || !numericLayers.length) {
        return acc;
      }

      const printingData = numericLayers[0];

      if (productionData.meta.surface) {
        acc.printingSurface += productionData.meta.surface;
      }

      acc.modelCount += printingData.quantity;
      acc.manufacturedPiecesCount += printingData.quantity;

      const devices = productionData.devices;
      devices.forEach((device) => {
        if (!acc.productionTypes.includes(device)) {
          acc.productionTypes.push(device);
        }
      });

      if (productionData.meta.hasSheetPrinting) {
        acc.hasSheetProduction = true;
      }

      if (productionData.meta.deliveryServiceFinishs) {
        productionData.meta.deliveryServiceFinishs.forEach((finish) => {
          if (!printMetaDto.deliveryServiceFinishs.includes(finish)) {
            printMetaDto.deliveryServiceFinishs.push(finish);
          }
        });
      }

      if (productionData.meta.hasNumericScoreCutting) {
        printMetaDto.hasNumericScoreCutting = true;
      }

      if (productionData.meta.hasZundCutting) {
        printMetaDto.hasZundCutting = true;
      }

      if (productionData.meta.hasKits) {
        printMetaDto.hasKits = true;
      }

      return acc;
    }, printMetaDto);
  }
}
