import { Injectable } from '@nestjs/common';
import { JobDeliveryMetaDto } from '../dto';
import { DeliveryAddress } from '../../delivery-addresses/delivery-address.schema';

@Injectable()
export class JobDeliveryMetaMappingService {
  public getMappedData(addresses: DeliveryAddress[]): JobDeliveryMetaDto {
    const jobDeliveryMetaDto = new JobDeliveryMetaDto();
    addresses.reduce(
      (
        acc: { addresses: DeliveryAddress[]; sendingDates: string[] },
        address: DeliveryAddress,
      ) => {
        if (
          address.mp.deliveryMethod &&
          !jobDeliveryMetaDto.deliveryMethods.includes(
            address.mp.deliveryMethod,
          )
        ) {
          jobDeliveryMetaDto.deliveryMethods.push(address.mp.deliveryMethod);
        }

        if (
          address.mp.zipCode &&
          !jobDeliveryMetaDto.addressZipCodes.includes(address.mp.zipCode)
        ) {
          jobDeliveryMetaDto.addressZipCodes.push(address.mp.zipCode);
        }

        if (
          this._notAlreadyContainsAddress(address, acc.addresses) &&
          this._isDeliveryAddress(address)
        ) {
          acc.addresses.push(address);
          jobDeliveryMetaDto.addressCount++;
        }

        const sendingDate = address.mp.sendingDate;
        if (sendingDate) {
          if (
            this._notAlreadyContainsSendingDate(sendingDate, acc.sendingDates)
          ) {
            acc.sendingDates.push(sendingDate);
            jobDeliveryMetaDto.deliverySendingDates.push(sendingDate);
          }
        }

        return acc;
      },
      { addresses: [], sendingDates: [] },
    );

    return jobDeliveryMetaDto;
  }

  private _notAlreadyContainsAddress(
    address: DeliveryAddress,
    addresses: DeliveryAddress[],
  ): boolean {
    if (addresses.length === 0) {
      return true;
    }

    return addresses.every((item): boolean => {
      return (
        item.mp.company !== address.mp.company ||
        item.mp.address !== address.mp.address ||
        item.mp.zipCode !== address.mp.zipCode ||
        item.mp.city !== address.mp.city ||
        item.mp.country !== address.mp.country
      );
    });
  }

  private _isDeliveryAddress(mpAddress: DeliveryAddress): boolean {
    return (
      mpAddress.mp.deliveryMethod !== 'Non' &&
      mpAddress.mp.deliveryMethod !== 'A dispo poseur'
    );
  }

  private _notAlreadyContainsSendingDate(
    sendingDate: string,
    sendingDates: string[],
  ) {
    if (!sendingDates.length) {
      return true;
    }

    let result = true;

    sendingDates.forEach((date) => {
      if (sendingDate === date) {
        result = false;
      }
    });

    return result;
  }
}
