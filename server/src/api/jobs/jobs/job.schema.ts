import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  IJobMp,
  IJobMeta,
  IJobDelivery,
  IJobPlanning,
  IJobPao,
} from './interfaces';
import { Document, Types } from 'mongoose';
import { SubJob } from '../sub-jobs/sub-job.schema';
import { DeliveryAddress } from '../delivery-addresses/delivery-address.schema';
import { TJobSyncStatus } from './types/t-job-sync-status';

@Schema({
  timestamps: true,
})
export class Job extends Document {
  @Prop({ type: String, default: 'pending' })
  syncStatus: TJobSyncStatus;

  @Prop(
    raw({
      id: Number,
      number: { type: String, required: true, index: true, unique: true },
      description: { type: String },
      company: String,
      contactName: String,
      phone: String,
      email: String,
      jobStatus: String,
      jobStatusNumber: { type: Number, required: true, index: true },
      deliveryDate: String,
      infoDigital: String,
      relationNumber: String,
      salesMan: String,
      creator: String,
      jobManager: String,
      infoGlobal: String,
      infoPrePress: String,
      infoPrint: String,
      infoFinishing: String,
      infoSubContract: String,
      infoExpedition: String,
      infoPlanning: String,
      jobStatusHistory: [String],
      planned: Boolean,
      deliveredDate: String,
      proofDate: String,
      returnDate: String,
      sendingDate: { type: String, index: true },
      subJobsIds: [Number],
      deliveryAddressesIds: [Number],
    }),
  )
  mp: IJobMp;

  @Prop(
    raw({
      printingSurface: Number,
      modelCount: Number,
      manufacturedPiecesCount: Number,
      productionTypes: [String],
      deliveryServiceFinishs: [String],
      hasSheetProduction: Boolean,
      hasNumericScoreCutting: Boolean,
      hasZundCutting: Boolean,
      hasKits: Boolean,
      addressCount: Number,
      addressZipCodes: [String],
      deliveryMethods: [String],
      deliverySendingDates: [String],
    }),
  )
  meta: IJobMeta;

  @Prop(
    raw({
      comment: String,
    }),
  )
  delivery: IJobDelivery;

  @Prop(
    raw({
      comment: String,
    }),
  )
  planning: IJobPlanning;

  @Prop(
    raw({
      checkComment: String,
      stockSheetReservations: [
        {
          productType: { type: Number, isRequired: true },
          productId: { type: Number, isRequired: true },
          displayName: { type: String, isRequired: true },
          paperType: { type: String, isRequired: true },
          quantity: { type: Number, isRequired: true },
          isCustomSize: { type: Boolean, isRequired: true },
          width: Number,
          height: Number,
          isDoubleSided: Boolean,
          isNesting: Boolean,
          numberBySheet: Number,
          status: { type: String, isRequired: true, default: 'created' },
          mutationId: Number,
          stockDecreaseCount: Number,
          userDisplayName: { type: String, isRequired: true },
          userEmployeeNumber: { type: Number, isRequired: true },
          paoComment: String,
          reservationComment: String,
        },
      ],
    }),
  )
  pao: IJobPao;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'SubJob' }] })
  subJobs: SubJob[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'DeliveryAddress' }] })
  deliveryAddresses: DeliveryAddress[];
}

export const JobSchema = SchemaFactory.createForClass(Job);
