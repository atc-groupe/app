import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class JobNumberValidationPipe implements PipeTransform {
  transform(value: any): any {
    if (!/^[0-9]*$/.test(value)) {
      throw new BadRequestException(`${value} is not a valid job number`);
    }

    return value;
  }
}
