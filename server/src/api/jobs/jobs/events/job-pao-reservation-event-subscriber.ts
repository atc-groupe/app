import { Injectable } from '@nestjs/common';
import { IEventSubscriber } from '../../../../shared/interfaces';
import { IJobStatusEvent } from '../interfaces';
import { JobsPaoService } from '../services';
import { PaoReservationStatusEnum } from '../enum/pao-reservation-status.enum';

@Injectable()
export class JobPaoReservationEventSubscriber implements IEventSubscriber {
  constructor(private _jobPaoService: JobsPaoService) {}

  async onDispatchEvent(name: string, event: any): Promise<void> {
    switch (name) {
      case 'jobStatusChange':
        await this._updatePaoReservationsStatus(event);
        return;
      default:
        return;
    }
  }

  private async _updatePaoReservationsStatus(event: IJobStatusEvent) {
    if (event.prevStatusNumber < 200 && event.nextStatusNumber === 200) {
      await this._jobPaoService.updateStockSheetReservationsStatus(
        event.jobNumber,
        {
          fromStatus: PaoReservationStatusEnum.Created,
          toStatus: PaoReservationStatusEnum.Pending,
        },
      );
    }
  }
}
