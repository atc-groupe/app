import { Injectable } from '@nestjs/common';
import {
  IEventSubscriber,
  IEventDispatcher,
} from '../../../../shared/interfaces';

@Injectable()
export class JobsStatusEventDispatcher implements IEventDispatcher {
  private _subscribers: IEventSubscriber[] = [];

  public addSubscriber(subscriber: IEventSubscriber) {
    this._subscribers.push(subscriber);
  }

  subscribers: IEventSubscriber[];

  dispatchEvent(name: string, event: any): void {
    for (const subscriber of this._subscribers) {
      subscriber.onDispatchEvent(name, event);
    }
  }
}
