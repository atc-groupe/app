import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  InternalServerErrorException,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Query,
} from '@nestjs/common';
import { JobsService, JobsSyncService, MpJobsService } from './services';
import { CollectionResponseDto } from '../../../shared/dto/collection-response.dto';
import { JobNumberValidationPipe } from './pipes/job-number-validation.pipe';
import { JobsSyncManyTypeEnum } from './enum/jobs-sync-many-type.enum';
import { CollectionResponseHelper } from '../../../shared/services';
import { Public } from '../../auth/public.decorator';
import { IJobsSyncResponse } from './interfaces';
import { IJob } from './interfaces';
import {
  ChangeJobStatusDto,
  JobDeliveryPatchDto,
  JobPlanningPatchDto,
  JobQueryParamsDto,
} from './dto';
import { JobsStatusEventDispatcher } from './events/jobs-status-event-dispatcher';
import { JobPaoReservationEventSubscriber } from './events/job-pao-reservation-event-subscriber';

@Controller({
  path: 'jobs',
  version: '1',
})
export class JobsController {
  constructor(
    jobPaoReservationSubscriber: JobPaoReservationEventSubscriber,
    private _jobsService: JobsService,
    private _jobsMpService: MpJobsService,
    private _JobsSyncService: JobsSyncService,
    private _collectionResponseHelper: CollectionResponseHelper,
    private _jobStatusEventDispatcher: JobsStatusEventDispatcher,
  ) {
    this._jobStatusEventDispatcher.addSubscriber(jobPaoReservationSubscriber);
  }

  @Get()
  async findAll(
    @Query() queryParams: JobQueryParamsDto,
  ): Promise<CollectionResponseDto> {
    try {
      const collection: CollectionResponseDto = new CollectionResponseDto();
      collection.documents = await this._jobsService.find(queryParams);

      await this._collectionResponseHelper.setPaginationData(
        collection,
        queryParams,
        this._jobsService.getCountDocuments(queryParams),
      );

      return collection;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Erreur de chargement. Les données sont indisponibles...',
        { cause: err },
      );
    }
  }

  @Public()
  @Get('sync/:type')
  async syncAll(
    @Param('type') type: JobsSyncManyTypeEnum,
  ): Promise<IJobsSyncResponse> {
    try {
      const start = Date.now();
      const result = await this._JobsSyncService.syncMany(type, true);

      return {
        result: 'success',
        synchronizedJobsCount: result,
        syncTime: `${Math.floor((Date.now() - start) / 1000)} seconds`,
      };
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la synchronisation des jobs',
        { cause: err },
      );
    }
  }

  @Get(':id/sync')
  async syncOne(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this._JobsSyncService.syncOne(id, false);
  }

  @Patch(':jobNumber/status')
  @HttpCode(200)
  async changeStatus(
    @Body() changeStatusDto: ChangeJobStatusDto,
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
  ) {
    try {
      const job = await this._jobsService.findOneByMpNumber(
        jobNumber.toString(),
        false,
      );

      if (!job) {
        throw new NotFoundException(`Job not found. JobNumber: ${jobNumber}`);
      }

      this._jobStatusEventDispatcher.dispatchEvent('jobStatusChange', {
        jobNumber: jobNumber,
        prevStatusNumber: job.mp.jobStatusNumber,
        nextStatusNumber: changeStatusDto.statusNumber,
      });

      changeStatusDto.jobNumber = jobNumber;
      return this._jobsMpService.changeStatusByNumber(changeStatusDto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException('Une erreur est survenue', {
        cause: err,
      });
    }
  }

  @Patch(':jobNumber/delivery')
  async updateDeliveryData(
    @Body() jobDeliveryPatchDto: JobDeliveryPatchDto,
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
  ) {
    try {
      return await this._jobsService.updateDeliveryData(
        jobNumber,
        jobDeliveryPatchDto,
      );
    } catch (err) {
      throw new InternalServerErrorException(
        "Une erreur est survenue. Merci de re-essayer ou de contacter l'administrateur si l'erreur persiste",
        { cause: err },
      );
    }
  }

  @Patch(':jobNumber/planning')
  async updatePlanningData(
    @Body() jobPlanningPatchDto: JobPlanningPatchDto,
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
  ) {
    try {
      return await this._jobsService.updatePlanningData(
        jobNumber,
        jobPlanningPatchDto,
      );
    } catch (err) {
      throw new InternalServerErrorException(
        "Une erreur est survenue. Merci de re-essayer ou de contacter l'administrateur si l'erreur persiste",
        { cause: err },
      );
    }
  }

  @Public()
  @Get(':number')
  async findOne(
    @Param('number', new JobNumberValidationPipe()) number: string,
  ): Promise<IJob> {
    try {
      const job = await this._jobsService.findOneByMpNumber(number, true);

      if (!job) {
        throw new NotFoundException();
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la récupération du job',
        { cause: err },
      );
    }
  }
}
