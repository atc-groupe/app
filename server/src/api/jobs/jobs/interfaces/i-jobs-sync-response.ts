export interface IJobsSyncResponse {
  result: string;
  synchronizedJobsCount: number;
  syncTime: string;
}
