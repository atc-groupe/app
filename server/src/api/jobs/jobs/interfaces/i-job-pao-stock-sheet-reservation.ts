import { PaoReservationStatusEnum } from '../enum/pao-reservation-status.enum';

export interface IJobPaoStockSheetReservation {
  productType: number;
  productId: number;
  displayName: string;
  paperType: string;
  isCustomSize: boolean;
  quantity: number;
  width?: number;
  height?: number;
  isDoubleSided: boolean;
  isNesting: boolean;
  status: PaoReservationStatusEnum;
  numberBySheet?: number;
  mutationId?: number;
  stockDecreaseCount?: number;
  userDisplayName: string;
  userEmployeeNumber: number;
}
