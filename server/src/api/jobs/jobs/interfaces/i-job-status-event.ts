export interface IJobStatusEvent {
  jobNumber: number;
  prevStatusNumber: number; // Status ordering
  nextStatusNumber: number; // Status ordering
}
