import { IJobMp } from './i-job-mp';
import { IJobMeta } from './i-job-meta';
import { IJobDelivery } from './i-job-delivery';
import { IJobPlanning } from './i-job-planning';
import { IJobPao } from './i-job-pao';
import { SubJob } from '../../sub-jobs/sub-job.schema';
import { DeliveryAddress } from '../../delivery-addresses/delivery-address.schema';
import { TJobSyncStatus } from '../types/t-job-sync-status';

export interface IJob {
  syncStatus: TJobSyncStatus;
  mp: IJobMp;
  meta: IJobMeta;
  delivery: IJobDelivery;
  planning: IJobPlanning;
  pao: IJobPao;
  subJobs: SubJob[];
  deliveryAddresses: DeliveryAddress[];
}
