import { IJobPaoStockSheetReservation } from './i-job-pao-stock-sheet-reservation';

export interface IJobPao {
  checkComment: string;
  stockSheetReservations: IJobPaoStockSheetReservation[];
}
