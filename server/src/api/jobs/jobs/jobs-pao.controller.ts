import {
  Body,
  Controller,
  Delete,
  HttpException,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { JobsPaoService } from './services';
import { JobNumberValidationPipe } from './pipes/job-number-validation.pipe';
import {
  JobPaoCheckCommentPatchDto,
  JobPaoStockSheetReservationCreateDto,
  JobPaoStockSheetReservationPatchDto,
  JobPaoStockSheetReservationsHandleDto,
  JobPaoStockSheetReservationStatusDto,
} from './dto';
import { Job } from './job.schema';

@Controller({
  path: 'jobs',
  version: '1',
})
export class JobsPaoController {
  constructor(private _jobsPaoService: JobsPaoService) {}

  @Patch(':jobNumber/pao/check-comment')
  async updateCheckComment(
    @Body() dto: JobPaoCheckCommentPatchDto,
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
  ): Promise<Job> {
    try {
      const job = await this._jobsPaoService.updateCheckComment(jobNumber, dto);

      if (!job) {
        throw new NotFoundException(
          `Le job n'a pas été trouvé. Numéro: ${jobNumber}`,
        );
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue. Merci de re-essayer ou de contacter l'administrateur si l'erreur persiste",
      );
    }
  }

  @Post(':jobNumber/pao/stock-sheet-reservations')
  async insertStockSheetReservation(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Body() dto: JobPaoStockSheetReservationCreateDto,
  ): Promise<Job> {
    try {
      const job = await this._jobsPaoService.addStockSheetReservation(
        jobNumber,
        dto,
      );

      if (!job) {
        throw new NotFoundException(
          `Le job n'a pas été trouvé. Numéro: ${jobNumber}`,
        );
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
      );
    }
  }

  @Patch(':jobNumber/pao/stock-sheet-reservations/status')
  async sendPaoReservations(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Body() dto: JobPaoStockSheetReservationStatusDto,
  ) {
    try {
      return await this._jobsPaoService.updateStockSheetReservationsStatus(
        jobNumber,
        dto,
      );
    } catch (err) {
      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la manipulation',
      );
    }
  }

  @Patch(':jobNumber/pao/stock-sheet-reservations/:reservationId')
  async updateStockSheetReservation(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Param('reservationId') reservationId: string,
    @Body() dto: JobPaoStockSheetReservationPatchDto,
  ): Promise<Job> {
    try {
      const job = await this._jobsPaoService.updateStockSheetReservation(
        jobNumber,
        reservationId,
        dto,
      );

      if (!job) {
        throw new NotFoundException(
          "Le job ou la réservation matière n'a pas été trouvée",
        );
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
      );
    }
  }

  @Patch(':jobNumber/pao/stock-sheet-reservations/:reservationId/status')
  async sendStockSheetReservation(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Param('reservationId') reservationId: string,
    @Body() dto: JobPaoStockSheetReservationStatusDto,
  ): Promise<Job> {
    try {
      const job = await this._jobsPaoService.updateStockSheetReservationStatus(
        jobNumber,
        reservationId,
        dto,
      );

      if (!job) {
        throw new NotFoundException(
          "Le job ou la réservation matière n'a pas été trouvée",
        );
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
      );
    }
  }

  @Patch(
    ':jobNumber/pao/stock-sheet-reservations/:reservationId/reservation-comment',
  )
  async updateStockSheetReservationResaComment(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Param('reservationId') reservationId: string,
    @Body('comment') comment: string,
  ): Promise<Job> {
    try {
      const job =
        await this._jobsPaoService.updateStockSheetReservationResaComment(
          jobNumber,
          reservationId,
          comment,
        );

      if (!job) {
        throw new NotFoundException(
          "Le job ou la réservation matière n'a pas été trouvée",
        );
      }

      return job;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
      );
    }
  }

  @Patch(':jobNumber/pao/stock-sheet-reservations/:reservationId/handle')
  handlePaoReservation(
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
    @Param('reservationId') reservationId: string,
    @Body() dto: JobPaoStockSheetReservationsHandleDto,
  ) {
    try {
      return this._jobsPaoService.handleStockSheetReservation(
        jobNumber,
        reservationId,
        dto,
      );
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'Une erreur est survenue lors de la manipulation',
      );
    }
  }

  @Delete(':jobNumber/pao/stock-sheet-reservations/:reservationId')
  async removeStockSheetReservation(
    @Param('reservationId') reservationId: string,
    @Param('jobNumber', new JobNumberValidationPipe()) jobNumber: number,
  ): Promise<Job> {
    try {
      const job = await this._jobsPaoService.removeStockSheetReservation(
        jobNumber,
        reservationId,
      );

      if (!job) {
        throw new NotFoundException(
          `Le job n'a pas été trouvé. Numéro: ${jobNumber}`,
        );
      }

      return job;
    } catch (err) {
      throw new InternalServerErrorException(
        "Une erreur est survenue lors de l'enregistrement",
      );
    }
  }
}
