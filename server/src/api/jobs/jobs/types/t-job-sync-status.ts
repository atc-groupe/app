export type TJobSyncStatus = 'success' | 'pending' | 'error';
