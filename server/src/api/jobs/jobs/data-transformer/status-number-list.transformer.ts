import { BadRequestException } from '@nestjs/common';
import { TransformFnParams } from 'class-transformer';

export function StatusNumberListTransformer(
  params: TransformFnParams,
): number[] {
  const list = params.value.split(':');
  if (!Array.isArray(list)) {
    throw new BadRequestException(
      'statusNumberList param has not valid shape. Valid shape number:number:...',
    );
  }

  return list
    .map((item) => {
      const value = Number(item);
      if (isNaN(value)) {
        throw new BadRequestException(
          `statusNumberList param contains invalid number. Bad value: '${item}'`,
        );
      }

      return value;
    })
    .filter((value) => !!value);
}
