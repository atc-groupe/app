import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { MpGetJobsQuotationsTypeEnum } from '../enum/mp-get-jobs-quotations-type.enum';
import { ChangeJobStatusDto } from '../dto';
import { JobsSyncManyTypeEnum } from '../enum/jobs-sync-many-type.enum';
import { MpHttpAdapter } from '../../../../shared/services/mp-http-adapter.service';
import { IMpJobDetails, IMpJobQuotationList } from '../interfaces';
import { catchError, map, Observable, of } from 'rxjs';
import { MpApiErrorException } from '../../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../../shared/enums';
import { IMpJobQuotationListItem } from '../interfaces';

@Injectable()
export class MpJobsService {
  constructor(private _http: MpHttpAdapter) {}

  findOneById(id: number): Observable<IMpJobDetails | null> {
    return this._findOneBy({ id });
  }

  findOneByNumber(number: number): Observable<IMpJobDetails | null> {
    return this._findOneBy({ job_number: number });
  }

  findList(
    type: JobsSyncManyTypeEnum,
  ): Observable<IMpJobQuotationListItem[] | null> {
    const params: { type: number; filter?: string } = {
      type: MpGetJobsQuotationsTypeEnum.jobs,
    };

    if (type === JobsSyncManyTypeEnum.active) {
      params.filter = JSON.stringify([
        { field: 'job_status_number', comparator: '<', value: 1000 },
      ]);
    }

    return this._http
      .get<IMpJobQuotationList>('/jobs/getJobsQuotations', {
        params,
      })
      .pipe(
        map(({ jobs }) => {
          return jobs.length ? jobs : null;
        }),
      );
  }

  changeStatusByNumber(
    changeStatusDto: ChangeJobStatusDto,
  ): Observable<unknown> {
    return this._http
      .post(
        '/jobs/changeJobStatus',
        {
          job_number: changeStatusDto.jobNumber,
          job_status_number: changeStatusDto.statusNumber,
          reason: changeStatusDto.reason,
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        },
      )
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            throw new BadRequestException(
              `Invalid job status. Provided value: ${changeStatusDto.statusNumber}`,
            );
          }

          if (
            err instanceof MpApiErrorException &&
            (err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_IN_USE ||
              err.errNumber === MpApiJobsErrorEnum.SUB_JOB_OR_QUOTATION_IN_USE)
          ) {
            throw new ConflictException(this._getLockedMessage(err.errText));
          }

          throw err;
        }),
      );
  }

  private _findOneBy(params: any): Observable<IMpJobDetails | null> {
    return this._http
      .get<IMpJobDetails | null>('/jobs/handleJobsQuotationsDetails', {
        params,
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }

  private _getLockedMessage(error: string): string {
    const split = error.split('"');
    const name = split[split.length - 2];

    return `Le job est utilisé par ${name}`;
  }
}
