import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { SubJobsSyncService } from '../../sub-jobs/sub-jobs-sync.service';
import { DeliveryAddressesSyncService } from '../../delivery-addresses/delivery-addresses-sync.service';
import { JobsMpMappingService } from './jobs-mp-mapping.service';
import { JobsMetaMappingService } from './jobs-meta-mapping.service';
import { JobsService } from './jobs.service';
import { MpJobsService } from './mp-jobs.service';
import { JobsSyncManyTypeEnum } from '../enum/jobs-sync-many-type.enum';
import { Logger } from '../../../logs/logger';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';
import { firstValueFrom } from 'rxjs';
import { Job } from '../job.schema';
import { JobTypeEnum } from '../enum/job-type.enum';

@Injectable()
export class JobsSyncService {
  private _logger: Logger;

  constructor(
    _loggerFactory: LogFactoryService,
    private _subJobSync: SubJobsSyncService,
    private _deliveryAddressesSync: DeliveryAddressesSyncService,
    private _jobService: JobsService,
    private _jobMpMapping: JobsMpMappingService,
    private _jobMetaMapping: JobsMetaMappingService,
    private _jobsMpService: MpJobsService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  async syncOne(mpId: number, retryWhenLocked: boolean) {
    let job: Job | null = null;
    try {
      // 1 - Get Job mapped mp data
      const jobMpDto = await this._jobMpMapping.getMappedData(mpId);

      // 2 - If job has Archive type (4) or archive status (1000) it is ignored.
      let isArchive = false;
      if (
        jobMpDto.type === JobTypeEnum.ArchivedJob ||
        jobMpDto.jobStatusNumber === 1000
      ) {
        this._logger.log(
          `> SyncJobService: Job is archived, ignore prod data. Job number ${jobMpDto.number}`,
        );

        isArchive = true;
      }

      // 3 - search job in the app db. if it exists, it is updated, if not, it is created.
      job = await this._jobService.findOneByMpNumber(jobMpDto.number, false);
      let jobExists = false;

      // 4 - Updates the job if it already exists or creates the job
      if (!job) {
        job = await this._jobService.insertOne(jobMpDto);
      } else {
        await this._jobService.updateSyncStatusAndMpData(job, jobMpDto);
        jobExists = true;
      }

      if (!isArchive) {
        // 5 - Sync subJobs (creates, updates, and delete)
        if (!jobMpDto.subJobsIds.length) {
          this._logger.error(
            `> SyncJobsService: Job with no subJobs found during job sync process. Job ID: ${mpId}`,
          );
        }

        const subJobs = await this._subJobSync.syncManyByJob(
          job,
          retryWhenLocked,
        );

        // 6 - Sync deliveryAddresses
        if (!jobMpDto.deliveryAddressesIds.length) {
          this._logger.error(
            `> SyncJobsService: Job with no delivery methods found during job sync process. Job ID: ${mpId}`,
          );
        }

        const deliveryAddresses =
          await this._deliveryAddressesSync.syncManyByJob(job, subJobs);

        // 7 - get job metadata
        const jobMeta = this._jobMetaMapping.getMappedData(
          subJobs,
          deliveryAddresses,
        );

        // 8 - update job
        job = await this._jobService.updateComputedData(
          job,
          jobMeta,
          subJobs,
          deliveryAddresses,
        );
      }

      // 9 - update job sync status to success
      job = await this._jobService.updateSyncStatus(job, 'success');

      this._logger.log(
        `> SyncJobsService: The job ${job.mp.number} (id: ${mpId}) has been ${
          jobExists ? 'updated' : 'created'
        } successfully`,
      );
    } catch (err) {
      if (job) {
        try {
          await this._jobService.updateSyncStatus(job, 'error');
        } catch (err) {
          throw new InternalServerErrorException(
            'An error occurred while setting job sync status to error',
          );
        }
      }

      throw err;
    }
  }

  async syncMany(
    type: JobsSyncManyTypeEnum,
    retryWhenLocked: boolean,
  ): Promise<number> {
    try {
      const list = await firstValueFrom(this._jobsMpService.findList(type));

      if (!list) {
        return 0;
      }

      for (const item of list) {
        await this.syncOne(item.id, retryWhenLocked);
      }
      return list.length;
    } catch (err) {
      throw err;
    }
  }
}
