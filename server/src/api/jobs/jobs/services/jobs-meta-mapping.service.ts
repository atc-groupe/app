import { Injectable } from '@nestjs/common';
import { JobProductionMetaMappingService } from '../mapping/job-production-meta-mapping.service';
import { JobDeliveryMetaMappingService } from '../mapping/job-delivery-meta-mapping.service';
import { JobMetaDto } from '../dto';
import { SubJob } from '../../sub-jobs/sub-job.schema';
import { DeliveryAddress } from '../../delivery-addresses/delivery-address.schema';

@Injectable()
export class JobsMetaMappingService {
  constructor(
    private _prodMetaMapping: JobProductionMetaMappingService,
    private _deliveryMetaMapping: JobDeliveryMetaMappingService,
  ) {}

  getMappedData(
    subJobs: SubJob[],
    deliveryAddresses: DeliveryAddress[],
  ): JobMetaDto {
    return new JobMetaDto(
      this._prodMetaMapping.getMappedData(subJobs),
      this._deliveryMetaMapping.getMappedData(deliveryAddresses),
    );
  }
}
