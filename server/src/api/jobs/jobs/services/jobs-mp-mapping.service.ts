import { Injectable } from '@nestjs/common';
import { JobMpDto } from '../dto';
import { JobsService } from './jobs.service';
import { MpDataMappingErrorException } from '../../../../shared/exceptions';
import { MpJobsService } from './mp-jobs.service';
import { Logger } from '../../../logs/logger';
import { DataMappingHelper } from '../../../../shared/services';
import { LogFactoryService } from '../../../logs/log-factory.service';
import { LogContextEnum } from '../../../logs/enums/log-context.enum';
import { IMpJobDetails } from '../interfaces';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class JobsMpMappingService {
  private _logger: Logger;

  constructor(
    private _jobsMpService: MpJobsService,
    private _jobService: JobsService,
    private _mappingHelper: DataMappingHelper,
    private _loggerFactory: LogFactoryService,
  ) {
    this._logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  async getMappedData(jobMpId: number): Promise<JobMpDto> {
    const mpData = await this._getMpJobQuotationDetails(jobMpId);

    const jobMpDto = new JobMpDto();
    jobMpDto.id = mpData.id;
    jobMpDto.number = String(mpData.job_number);
    jobMpDto.type = mpData.type;
    jobMpDto.description = mpData.description;
    jobMpDto.company = mpData.company;
    jobMpDto.contactName = mpData.contact_name;
    jobMpDto.phone = mpData.phone;
    jobMpDto.email = mpData.email;
    jobMpDto.jobStatus = mpData.job_status;
    jobMpDto.jobStatusNumber = mpData.job_status_number;
    jobMpDto.deliveryDate = this._mappingHelper.getDateOrNull(
      mpData.delivery_date,
    );
    jobMpDto.infoDigital = mpData.info_digital;
    jobMpDto.relationNumber = mpData.relation_number;
    jobMpDto.salesMan = mpData.salesman;
    jobMpDto.creator = mpData.creator;
    jobMpDto.jobManager = mpData.jobmanager;
    jobMpDto.infoGlobal = this._getMappedInfo(mpData.info_general);
    jobMpDto.infoPrePress = this._getMappedInfo(mpData.info_prepress);
    jobMpDto.infoPrint = this._getMappedInfo(mpData.info_print);
    jobMpDto.infoFinishing = this._getMappedInfo(mpData.info_finishing);
    jobMpDto.infoSubContract = this._getMappedInfo(mpData.info_subcontract);
    jobMpDto.infoExpedition = this._getMappedInfo(mpData.info_expedition);
    jobMpDto.infoPlanning = this._getMappedInfo(mpData.info_planning);
    jobMpDto.jobStatusHistory = this._getStatusHistory(
      mpData.job_status_history,
    );
    jobMpDto.planned = mpData.planned;
    jobMpDto.deliveredDate = this._mappingHelper.getDateOrNull(
      mpData.delivered,
    );
    jobMpDto.proofDate = this._mappingHelper.getDateOrNull(mpData.proof_date);
    jobMpDto.returnDate = this._mappingHelper.getDateOrNull(mpData.return_date);
    jobMpDto.sendingDate = this._mappingHelper.getDateOrNull(
      mpData.sending_date,
    );
    jobMpDto.subJobsIds = this._getSubJobsIds(mpData);
    jobMpDto.deliveryAddressesIds = this._getDeliveryAddressesIds(mpData);

    return jobMpDto;
  }
  private _getStatusHistory(history: string): string[] {
    if (!history) {
      return [];
    }

    const historyArray = history.split('\r');

    if (historyArray[historyArray.length - 1] === '') {
      historyArray.pop();
    }

    return historyArray;
  }

  private _getSubJobsIds(mpData: IMpJobDetails): number[] {
    return mpData.sub_jobs.map((item) => item.id);
  }

  private _getDeliveryAddressesIds(mpData: IMpJobDetails): number[] {
    if (!mpData.delivery_address) {
      return [];
    }

    return mpData.delivery_address.map((item) => item.id);
  }

  private _getMappedInfo(info: string): string {
    return info.replaceAll('\r', '\n');
  }

  private async _getMpJobQuotationDetails(
    jobMpId: number,
  ): Promise<IMpJobDetails> {
    let mpJobCotationDetails = await firstValueFrom(
      this._jobsMpService.findOneById(jobMpId),
    );

    /*
      If the job do not exist, the sync try's to fetch it from app DB by its mp.id.
      If it is found, it is fetched in Mp API by its number.
      If job is not found an error is thrown.
     */
    if (!mpJobCotationDetails) {
      this._logger.error(
        `> JobMpMappingService: Job quotation details could not be fetched in MultiPress API by its ID. Job MultiPress Id: ${jobMpId}`,
      );

      const job = await this._jobService.findOneByMpId(jobMpId);

      if (!job) {
        this._logger.error(
          `> JobMpMappingService: job could not be found in Mp and app db by its MultiPress ID. Job MultiPress Id: ${jobMpId}`,
        );

        throw new MpDataMappingErrorException(
          `[JOB MAPPING ERROR] job could not be found in Mp and app db by it's ID. Job ID: ${jobMpId}`,
        );
      }

      mpJobCotationDetails = await firstValueFrom(
        this._jobsMpService.findOneByNumber(Number(job.mp.number)),
      );

      if (!mpJobCotationDetails) {
        this._logger.error(
          `> JobMpMappingService: Job quotation details could not be fetched in MultiPress API by job's number. Job Number: ${job.mp.number}`,
        );

        throw new MpDataMappingErrorException(
          `[JOB MAPPING ERROR] job could not be found in Mp API by it's number. Job number: ${job.mp.number}`,
        );
      }
    }

    return mpJobCotationDetails;
  }
}
