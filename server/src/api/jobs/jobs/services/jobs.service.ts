import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Job } from '../job.schema';
import { QueryHelper } from '../../../../shared/services';
import { SubJob } from '../../sub-jobs/sub-job.schema';
import { DeliveryAddress } from '../../delivery-addresses/delivery-address.schema';
import {
  JobDeliveryPatchDto,
  JobMetaDto,
  JobMpDto,
  JobPlanningPatchDto,
  JobQueryParamsDto,
} from '../dto';
import { TJobSyncStatus } from '../types/t-job-sync-status';

@Injectable()
export class JobsService {
  constructor(
    @InjectModel(Job.name) private _jobModel: Model<Job>,
    private _queryHelper: QueryHelper,
  ) {}

  async find(queryParams: JobQueryParamsDto): Promise<Job[]> {
    const paginationFilters =
      this._queryHelper.getPaginationFilters(queryParams);
    let query = this._jobModel.find(this._getFindQueryFilter(queryParams));

    if (paginationFilters) {
      query = query.skip(paginationFilters.skip).limit(paginationFilters.limit);
    }

    if (queryParams.populate === true) {
      query.populate(['subJobs', 'deliveryAddresses']);
    }

    return query.sort({ 'mp.number': 1 }).exec();
  }

  insertOne(jobMpDto: JobMpDto): Promise<Job> {
    const job = new this._jobModel({
      mp: jobMpDto,
      syncStatus: 'pending',
      meta: null,
    });

    return job.save();
  }

  updateSyncStatus(job: Job, status: TJobSyncStatus): Promise<Job> {
    job.syncStatus = status;

    return job.save();
  }

  updateSyncStatusAndMpData(job: Job, jobMpDto: JobMpDto): Promise<Job> {
    job.syncStatus = 'pending';
    job.mp = jobMpDto;

    return job.save();
  }

  updateComputedData(
    job: Job,
    jobMetaDto: JobMetaDto,
    subJobs: SubJob[],
    deliveryAddresses: DeliveryAddress[],
  ): Promise<Job> {
    job.meta = jobMetaDto;
    job.subJobs = subJobs;
    job.deliveryAddresses = deliveryAddresses;

    return job.save();
  }

  async updateDeliveryData(
    jobNumber: number,
    jobDeliveryPatchDto: JobDeliveryPatchDto,
  ): Promise<Job> {
    try {
      const job = await this._jobModel
        .findOne({ 'mp.number': jobNumber })
        .exec();

      if (!job) {
        throw new NotFoundException(
          `Job has not been Found. JobNumber: ${jobNumber}`,
        );
      }

      if (jobDeliveryPatchDto.comment) {
        job.delivery.comment = jobDeliveryPatchDto.comment;
      }

      return job.save();
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        `An error occurred while updating job delivery data. JobNumber: ${jobNumber}`,
      );
    }
  }

  async updatePlanningData(
    jobNumber: number,
    jobPlanningDto: JobPlanningPatchDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        { 'mp.number': jobNumber },
        { planning: jobPlanningDto },
      )
      .exec();
  }

  public findOneByMpId(mpId: number): Promise<Job | null> {
    return this._jobModel.findOne({ 'mp.id': mpId }).exec();
  }

  findOneByMpNumber(mpNumber: string, populate: boolean): Promise<Job | null> {
    const query = this._jobModel.findOne({ 'mp.number': mpNumber });

    return populate
      ? query.populate(['subJobs', 'deliveryAddresses']).exec()
      : query.exec();
  }

  getCountDocuments(queryParams: JobQueryParamsDto): Promise<number> {
    const filter = this._getFindQueryFilter(queryParams);

    return this._jobModel.countDocuments(filter).exec();
  }

  private _getFindQueryFilter(queryParams: JobQueryParamsDto) {
    if (this._queryHelper.hasNoSearchParams(queryParams)) {
      return {};
    }

    if (queryParams.jobEndNumber) {
      return {
        'mp.number': {
          $regex: new RegExp(`^[0-9]*${queryParams.jobEndNumber}$`),
        },
      };
    }

    if (queryParams.fullSearch) {
      const regexp = new RegExp(queryParams.fullSearch, 'i');

      return {
        $or: [
          { 'mp.number': { $regex: regexp } },
          { 'mp.company': { $regex: regexp } },
          { 'mp.description': { $regex: regexp } },
        ],
      };
    }

    const searchParams: any[] = [];

    /*
      status number filters:
      - if "status_number" is set, other status number filters are ignored
      - then if "status_number_list" is set "start_status_number" and "end_status_number" are ignored
      - then "start_status_number" and "end_status_number" are used.
     */
    if (queryParams.statusNumber) {
      searchParams.push({
        'mp.jobStatusNumber': queryParams.statusNumber,
      });
    } else if (queryParams.statusNumberList) {
      searchParams.push({
        'mp.jobStatusNumber': { $in: queryParams.statusNumberList },
      });
    } else {
      if (queryParams.startStatusNumber) {
        searchParams.push({
          'mp.jobStatusNumber': {
            $gte: queryParams.startStatusNumber,
          },
        });
      }

      if (queryParams.endStatusNumber) {
        searchParams.push({
          'mp.jobStatusNumber': {
            $lte: queryParams.endStatusNumber,
          },
        });
      }
    }

    if (queryParams.sendingDate) {
      searchParams.push({
        $or: [
          { 'mp.sendingDate': { $eq: queryParams.sendingDate } },
          {
            'meta.deliverySendingDates': {
              $eq: queryParams.sendingDate,
              $exists: true,
              $ne: [],
            },
          },
        ],
      });
    } else {
      if (queryParams.startSendingDate) {
        searchParams.push({
          $or: [
            { 'mp.sendingDate': { $gte: queryParams.startSendingDate } },
            {
              'meta.deliverySendingDates': {
                $gte: queryParams.startSendingDate,
                $exists: true,
                $ne: [],
              },
            },
          ],
        });
      }

      if (queryParams.endSendingDate) {
        searchParams.push({
          $or: [
            { 'mp.sendingDate': { $lte: queryParams.endSendingDate } },
            {
              'meta.deliverySendingDates': {
                $lte: queryParams.endSendingDate,
                $exists: true,
                $ne: [],
              },
            },
          ],
        });
      }
    }

    if (queryParams.hasPaoReservation) {
      searchParams.push({
        $and: [
          { 'pao.stockSheetReservations': { $exists: true, $ne: [] } },
          {
            'pao.stockSheetReservations': {
              $elemMatch: {
                status: {
                  $in: ['pending', 'ordered'],
                },
              },
            },
          },
        ],
      });
    }

    return { $and: searchParams };
  }
}
