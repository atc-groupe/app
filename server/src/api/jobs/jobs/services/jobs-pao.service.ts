import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Job } from '../job.schema';
import { Model } from 'mongoose';
import {
  JobPaoCheckCommentPatchDto,
  JobPaoStockSheetReservationCreateDto,
  JobPaoStockSheetReservationPatchDto,
  JobPaoStockSheetReservationsHandleDto,
  JobPaoStockSheetReservationStatusDto,
} from '../dto';
import { MpStockMutationService } from '../../../stock/mp-stock-mutation.service';
import { tap } from 'rxjs';
import { PaoReservationStatusEnum } from '../enum/pao-reservation-status.enum';

@Injectable({})
export class JobsPaoService {
  constructor(
    @InjectModel(Job.name) private _jobModel: Model<Job>,
    private _mpStockMutationsService: MpStockMutationService,
  ) {}

  updateCheckComment(
    jobNumber: number,
    dto: JobPaoCheckCommentPatchDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        { 'mp.number': jobNumber },
        { 'pao.checkComment': dto.checkComment },
      )
      .exec();
  }

  addStockSheetReservation(
    jobNumber: number,
    dto: JobPaoStockSheetReservationCreateDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        { 'mp.number': jobNumber },
        {
          $push: { 'pao.stockSheetReservations': dto },
        },
      )
      .exec();
  }

  updateStockSheetReservation(
    jobNumber: number,
    reservationId: string,
    dto: JobPaoStockSheetReservationPatchDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        {
          'mp.number': jobNumber,
          'pao.stockSheetReservations._id': reservationId,
        },
        { $set: { 'pao.stockSheetReservations.$': dto } },
        { new: true, runValidators: true },
      )
      .exec();
  }

  removeStockSheetReservation(
    jobNumber: number,
    reservationId: string,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        {
          'mp.number': jobNumber,
        },
        {
          $pull: {
            'pao.stockSheetReservations': { _id: reservationId },
          },
        },
      )
      .exec();
  }

  updateStockSheetReservationsStatus(
    jobNumber: number,
    dto: JobPaoStockSheetReservationStatusDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        {
          'mp.number': jobNumber,
        },
        {
          $set: {
            'pao.stockSheetReservations.$[elem].status': dto.toStatus,
          },
        },
        {
          arrayFilters: [{ 'elem.status': dto.fromStatus }],
        },
      )
      .exec();
  }

  updateStockSheetReservationStatus(
    jobNumber: number,
    reservationId: string,
    dto: JobPaoStockSheetReservationStatusDto,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        {
          'mp.number': jobNumber,
          'pao.stockSheetReservations': {
            $elemMatch: {
              _id: reservationId,
              status: dto.fromStatus,
            },
          },
        },
        {
          $set: {
            'pao.stockSheetReservations.$.status': dto.toStatus,
          },
        },
      )
      .exec();
  }

  updateStockSheetReservationStatusAndStockDecrease(
    jobNumber: number,
    reservationId: string,
    status: PaoReservationStatusEnum,
    stockDecreaseCount: number,
  ): Promise<Job | null> {
    return this._jobModel
      .findOneAndUpdate(
        {
          'mp.number': jobNumber,
          'pao.stockSheetReservations': {
            $elemMatch: {
              _id: reservationId,
            },
          },
        },
        {
          $set: {
            'pao.stockSheetReservations.$.status': status,
            'pao.stockSheetReservations.$.stockDecreaseCount':
              stockDecreaseCount,
          },
        },
      )
      .exec();
  }

  updateStockSheetReservationResaComment(
    jobNumber: number,
    reservationId: string,
    comment: string,
  ): Promise<Job | null> {
    return this._jobModel.findOneAndUpdate(
      {
        'mp.number': jobNumber,
        'pao.stockSheetReservations._id': reservationId,
      },
      { $set: { 'pao.stockSheetReservations.$.reservationComment': comment } },
      { new: true, runValidators: true },
    );
  }

  async handleStockSheetReservation(
    jobNumber: number,
    reservationId: string,
    dto: JobPaoStockSheetReservationsHandleDto,
  ) {
    if (dto.add > 0 && dto.subtract > 0) {
      throw new BadRequestException(
        'On ne peut pas incrémenter et décrémenter le stock en même temps',
      );
    }

    if (dto.action === 'handle' && dto.subtract === 0) {
      return this.updateStockSheetReservationStatusAndStockDecrease(
        jobNumber,
        reservationId,
        PaoReservationStatusEnum.Treated,
        0,
      );
    }

    if (dto.action === 'undo' && dto.add === 0) {
      return this.updateStockSheetReservationStatusAndStockDecrease(
        jobNumber,
        reservationId,
        PaoReservationStatusEnum.Pending,
        0,
      );
    }

    return this._mpStockMutationsService
      .createMutation(dto.productType, dto.productId, {
        employee_number: dto.employeeNumber,
        data: {
          job_number: jobNumber,
          add: dto.add,
          subtract: dto.subtract,
          description: dto.description,
        },
      })
      .pipe(
        tap(async () => {
          if (dto.action === 'handle') {
            return await this.updateStockSheetReservationStatusAndStockDecrease(
              jobNumber,
              reservationId,
              PaoReservationStatusEnum.Treated,
              dto.subtract,
            );
          }

          return await this.updateStockSheetReservationStatusAndStockDecrease(
            jobNumber,
            reservationId,
            PaoReservationStatusEnum.Pending,
            0,
          );
        }),
      );
  }
}
