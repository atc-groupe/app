import { Injectable } from '@nestjs/common';
import { IMpDeliveryAddressDetails } from './interfaces';
import { DeliveryAddressDto } from './delivery-address.dto';
import { MpDeliveryAddressesService } from './mp-delivery-addresses.service';
import { Logger } from '../../logs/logger';
import { DataMappingHelper } from '../../../shared/services';
import { LogFactoryService } from '../../logs/log-factory.service';
import { LogContextEnum } from '../../logs/enums/log-context.enum';
import { forkJoin, map, Observable, of } from 'rxjs';

@Injectable()
export class DeliveryAddressesMappingService {
  private logger: Logger;

  constructor(
    _loggerFactory: LogFactoryService,
    private _deliveryAddressesMpService: MpDeliveryAddressesService,
    private _mappingHelper: DataMappingHelper,
  ) {
    this.logger = _loggerFactory.get(LogContextEnum.optimaMultiPressSync);
  }

  getMappedDataCollection(mpIds: number[]): Observable<DeliveryAddressDto[]> {
    const queries: Observable<IMpDeliveryAddressDetails | null>[] = [];

    for (const mpId of mpIds) {
      queries.push(this._deliveryAddressesMpService.findOneById(mpId));
    }

    if (!queries.length) {
      return of([]);
    }

    return forkJoin(queries).pipe(
      map((mpAddresses) => {
        const mappedAddresses: DeliveryAddressDto[] = [];
        let index = -1;

        mpIds.forEach((mpId) => {
          index++;
          const mpAddress = mpAddresses[index];

          if (!mpAddress) {
            this.logger.error(
              `> DeliveryAddressMappingService: unable to fetch Multipress delivery address details by it's ID. Address ID: ${mpId}`,
            );

            return;
          }

          if (this._isEmptyAddress(mpAddress)) {
            return;
          }

          mappedAddresses.push(this._map(mpId, mpAddress));
        });

        return mappedAddresses;
      }),
    );
  }

  private _map(
    mpId: number,
    mpAddress: IMpDeliveryAddressDetails,
  ): DeliveryAddressDto {
    const deliveryAddress = new DeliveryAddressDto();

    deliveryAddress.mp = {
      id: mpId,
      company: mpAddress.company,
      address: mpAddress.address,
      zipCode: mpAddress.zipcode,
      city: mpAddress.city.trim(),
      country: mpAddress.country,
      countryCode: mpAddress.country_code,
      extraAddress: this._getExtraAddress(mpAddress),
      extraInfo: mpAddress.extra_info,
      contactName: mpAddress.contact_name,
      phone: mpAddress.phone,
      email: mpAddress.email,
      quantity: mpAddress.run,
      packedPer: mpAddress.packed_per,
      ordering: mpAddress.ordering,
      description: mpAddress.description,
      remark: mpAddress.remark,
      relation: mpAddress.relation,
      relationNumber: mpAddress.relation_number,
      deliveryMethod: mpAddress.delivery_method,
      sendingDate: this._mappingHelper.getDateOrNull(mpAddress.sending_date),
      deliveryDate: this._mappingHelper.getDateOrNull(mpAddress.delivery_date),
      weight: mpAddress.weight,
      status: mpAddress.status,
      timestamp: new Date(mpAddress.timestamp),
    };

    return deliveryAddress;
  }

  private _getExtraAddress(mpAddress: IMpDeliveryAddressDetails): string[] {
    const extraAddress: string[] = [];

    if (mpAddress.extra_address_1) extraAddress.push(mpAddress.extra_address_1);
    if (mpAddress.extra_address_2) extraAddress.push(mpAddress.extra_address_2);
    if (mpAddress.extra_address_3) extraAddress.push(mpAddress.extra_address_3);
    if (mpAddress.extra_address_4) extraAddress.push(mpAddress.extra_address_4);
    if (mpAddress.extra_address_5) extraAddress.push(mpAddress.extra_address_5);

    return extraAddress;
  }

  private _isEmptyAddress(mpAddress: IMpDeliveryAddressDetails): boolean {
    return mpAddress.delivery_method === 'Non';
  }
}
