import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  DeliveryAddress,
  DeliveryAddressSchema,
} from './delivery-address.schema';
import { MpHttpModule } from '../../../shared/modules/mp-http.module';
import { HelperModule } from '../../../shared/modules/helper.module';
import { DeliveryAddressesService } from './delivery-addresses.service';
import { MpDeliveryAddressesService } from './mp-delivery-addresses.service';
import { DeliveryAddressesMappingService } from './delivery-addresses-mapping.service';
import { DeliveryAddressesSyncService } from './delivery-addresses-sync.service';
import { LogsModule } from '../../logs/logs.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: DeliveryAddress.name, schema: DeliveryAddressSchema },
    ]),
    MpHttpModule,
    HelperModule,
    LogsModule,
  ],
  providers: [
    DeliveryAddressesService,
    MpDeliveryAddressesService,
    DeliveryAddressesMappingService,
    DeliveryAddressesSyncService,
  ],
  exports: [DeliveryAddressesSyncService],
})
export class DeliveryAddressesModule {}
