import { Injectable } from '@nestjs/common';
import { DeliveryAddressesMappingService } from './delivery-addresses-mapping.service';
import { Types } from 'mongoose';
import { Job } from '../jobs/job.schema';
import { SubJob } from '../sub-jobs/sub-job.schema';
import { DeliveryAddress } from './delivery-address.schema';
import { DeliveryAddressDto } from './delivery-address.dto';
import { DeliveryAddressesService } from './delivery-addresses.service';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class DeliveryAddressesSyncService {
  constructor(
    private _deliveryAddressesMapping: DeliveryAddressesMappingService,
    private _deliveryAddressesService: DeliveryAddressesService,
  ) {}

  public async syncManyByJob(
    job: Job,
    subJobs: SubJob[],
  ): Promise<DeliveryAddress[]> {
    const addressDtos = await firstValueFrom(
      this._deliveryAddressesMapping.getMappedDataCollection(
        job.mp.deliveryAddressesIds,
      ),
    );

    const queries: any[] = [];

    for (const addressDto of addressDtos) {
      queries.push(this._syncOne(addressDto, subJobs, job._id));
    }

    const addresses: DeliveryAddress[] = await Promise.all<DeliveryAddress>(
      queries,
    );

    await this._removeMpDeletedAddresses(job);

    return addresses;
  }

  private async _syncOne(
    addressDto: DeliveryAddressDto,
    subJobs: SubJob[],
    jobId: Types.ObjectId,
  ): Promise<DeliveryAddress> {
    const address = await this._deliveryAddressesService.findOndByMpId(
      addressDto.mp.id,
    );

    if (address) {
      return this._deliveryAddressesService.updateOne(address, addressDto);
    }

    // Search sub-job reference (the only data that matches the sub job is the mp.description)
    addressDto.job = jobId;
    const subJobId = this._getDeliveryAddressSubJobRef(
      addressDto.mp.description,
      subJobs,
    );
    if (subJobId) {
      addressDto.subJob = subJobId;
    }

    return this._deliveryAddressesService.insertOne(addressDto);
  }

  private async _removeMpDeletedAddresses(job: Job): Promise<void> {
    const appAddresses = await this._deliveryAddressesService.findMpIdsByJobId(
      job._id,
    );
    for (const appAddress of appAddresses) {
      if (!job.mp.deliveryAddressesIds.includes(appAddress.mp.id)) {
        await this._deliveryAddressesService.deleteOne(appAddress._id);
      }
    }
  }

  private _getDeliveryAddressSubJobRef(
    description: string,
    subJobs: SubJob[],
  ): Types.ObjectId | null {
    for (const subJob of subJobs) {
      if (subJob.description === description) {
        return subJob._id;
      }
    }

    return null;
  }
}
