import { Injectable } from '@nestjs/common';
import { IMpDeliveryAddressDetails } from './interfaces';
import { IMpDeliveryLine } from './interfaces';
import { catchError, Observable, of } from 'rxjs';
import { MpHttpAdapter } from '../../../shared/services/mp-http-adapter.service';
import { MpApiErrorException } from '../../../shared/exceptions';
import { MpApiJobsErrorEnum } from '../../../shared/enums';

@Injectable()
export class MpDeliveryAddressesService {
  constructor(private _http: MpHttpAdapter) {}

  findOneById(id: number): Observable<IMpDeliveryAddressDetails | null> {
    return this._http
      .get<IMpDeliveryAddressDetails | null>(
        '/jobs/handleDeliveryAddressDetails',
        { params: { id } },
      )
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }

  findLinesByJobNumber(number: number): Observable<IMpDeliveryLine[] | null> {
    return this._http
      .get<IMpDeliveryLine[] | null>('/jobs/getDeliveryLines', {
        params: { job_number: number },
      })
      .pipe(
        catchError((err) => {
          if (
            err instanceof MpApiErrorException &&
            err.errNumber === MpApiJobsErrorEnum.JOB_OR_QUOTATION_NOT_FOUND
          ) {
            return of(null);
          }

          throw err;
        }),
      );
  }
}
