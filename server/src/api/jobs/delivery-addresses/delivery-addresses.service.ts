import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { DeliveryAddress } from './delivery-address.schema';
import { Model, Types } from 'mongoose';
import { DeliveryAddressDto } from './delivery-address.dto';

@Injectable()
export class DeliveryAddressesService {
  constructor(
    @InjectModel(DeliveryAddress.name)
    private _deliveryAddressModel: Model<DeliveryAddress>,
  ) {}

  insertOne(deliveryAddressDto: DeliveryAddressDto): Promise<DeliveryAddress> {
    const address = new this._deliveryAddressModel(deliveryAddressDto);

    return address.save();
  }

  updateOne(
    deliveryAddress: DeliveryAddress,
    deliveryAddressDto: DeliveryAddressDto,
  ): Promise<DeliveryAddress> {
    deliveryAddress.mp = deliveryAddressDto.mp;

    return deliveryAddress.save();
  }

  deleteOne(id: Types.ObjectId) {
    return this._deliveryAddressModel.deleteOne({ _id: id }).exec();
  }

  findOndByMpId(mpId: number): Promise<DeliveryAddress | null> {
    return this._deliveryAddressModel.findOne({ 'mp.id': mpId }).exec();
  }

  findMpIdsByJobId(jobId: Types.ObjectId): Promise<DeliveryAddress[]> {
    return this._deliveryAddressModel
      .find({ job: jobId })
      .select('mp.id')
      .exec();
  }
}
