export interface IMpDeliveryLine {
  delivery_number: number;
  run: number;
  packed_per: number;
  description: string;
  number_of_packaging: number;
  pallets: number;
  job_number: number;
  remark: number;
  still_to_deliver: number;
  delivered: number;
  delivery_address_id: number;
  delivery_method: string;
  article_number: string;
  type_of_pallet: string;
  extra_info: string;
  weight: number;
  date: Date;
  status: string;
  extra_address_1: string;
  extra_address_2: string;
  extra_address_3: string;
  extra_address_4: string;
  extra_address_5: string;
  purchase_id: number;
  id: number;
}
