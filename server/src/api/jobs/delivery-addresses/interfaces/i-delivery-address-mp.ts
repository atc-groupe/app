export interface IDeliveryAddressMp {
  id: number;
  company: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  countryCode: string;
  extraAddress?: string[]; // mp extra_address from _1 to _5.  just push completed lines
  extraInfo?: string;
  contactName: string;
  phone: string;
  email: string;
  quantity: number; // mp run
  packedPer: number; //
  ordering: number;
  description: string;
  remark: string;
  relation: string;
  relationNumber: number;
  deliveryMethod: string;
  sendingDate: string | null;
  deliveryDate: string | null;
  weight: number;
  status: string;
  timestamp: Date;
}
