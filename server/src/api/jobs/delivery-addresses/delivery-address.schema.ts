import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IDeliveryAddressMp } from './interfaces';
import { Document, Types } from 'mongoose';
import { Job } from '../jobs/job.schema';
import { SubJob } from '../sub-jobs/sub-job.schema';

@Schema()
export class DeliveryAddress extends Document {
  @Prop(
    raw({
      id: Number,
      company: String,
      address: String,
      zipCode: String,
      city: String,
      country: String,
      countryCode: String,
      extraAddress: [String],
      extraInfo: String,
      contactName: String,
      phone: String,
      email: String,
      quantity: Number,
      packedPer: Number,
      ordering: Number,
      description: String,
      remark: String,
      relation: String,
      relationNumber: Number,
      deliveryMethod: String,
      sendingDate: String,
      deliveryDate: String,
      weight: Number,
      status: String,

      timestamp: String,
    }),
  )
  mp: IDeliveryAddressMp;

  @Prop({ type: Types.ObjectId, ref: 'Job' })
  job: Job;

  @Prop({ type: Types.ObjectId, ref: 'SubJob' })
  subJob: SubJob;
}

export const DeliveryAddressSchema =
  SchemaFactory.createForClass(DeliveryAddress);
