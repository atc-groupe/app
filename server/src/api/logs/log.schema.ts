import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { LogContextEnum } from './enums/log-context.enum';
import { LogLevel } from '@nestjs/common';

@Schema()
export class Log extends Document {
  @Prop({ type: String, required: true, index: true })
  context: LogContextEnum;

  @Prop({ type: String, required: true, index: true })
  level: LogLevel;

  @Prop({ type: String, required: true })
  message: string;

  @Prop({ type: Date, expires: '30d', default: Date.now })
  createdAt: Date;
}

export const AppLogSchema = SchemaFactory.createForClass(Log);
