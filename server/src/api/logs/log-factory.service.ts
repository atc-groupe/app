import { ConsoleLogger, Injectable } from '@nestjs/common';
import { LogService } from './log.service';
import { Logger } from './logger';
import { LogContextEnum } from './enums/log-context.enum';

@Injectable()
export class LogFactoryService {
  constructor(
    private appLoggerService: LogService,
    private consoleLogger: ConsoleLogger,
  ) {}

  get(context: LogContextEnum) {
    return new Logger(this.appLoggerService, this.consoleLogger, context);
  }
}
