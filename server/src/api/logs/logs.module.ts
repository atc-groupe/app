import { ConsoleLogger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppLogSchema, Log } from './log.schema';
import { LogService } from './log.service';
import { LogFactoryService } from './log-factory.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Log.name, schema: AppLogSchema }]),
  ],
  providers: [LogService, LogFactoryService, ConsoleLogger],
  exports: [LogFactoryService],
})
export class LogsModule {}
