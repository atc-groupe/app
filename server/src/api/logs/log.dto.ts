import { LogContextEnum } from './enums/log-context.enum';
import { LogLevelEnum } from './enums/log-level.enum';

export class LogDto {
  context: LogContextEnum;
  level: LogLevelEnum;
  message: string;
}
