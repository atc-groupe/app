export enum LogContextEnum {
  optimaMultiPressSync = 'OPTIMA MULTIPRESS SYNC',
  mpApiCall = 'MULTIPRESS API CALL',
  planning = 'PLANNING',
}
