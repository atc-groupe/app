import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Log } from './log.schema';
import { Model } from 'mongoose';
import { LogDto } from './log.dto';

@Injectable()
export class LogService {
  constructor(
    @InjectModel(Log.name)
    private appLogModel: Model<Log>,
  ) {}

  insertOne(appLogDto: LogDto): void {
    const log = new this.appLogModel(appLogDto);

    log.save();
  }
}
