import { Injectable } from '@nestjs/common';
import { MpEmployeeService } from '../employees/mp-employee.service';
import { MpRelationsContactPersonService } from '../relations/mp-relations-contact-person.service';
import { firstValueFrom } from 'rxjs';
import { User } from './user.schema';
import { UserTypeEnum } from '../../shared/enums';
import { AuthorizationsService } from '../authorizations/authorizations.service';
import { ActiveDirectoryService } from '../active-directory/active-directory.service';
import { ICompanyUser } from './interfaces/i-company-user';
import { ICustomerUser } from './interfaces/i-customer-user';
import { TUserData } from './t-user-data';

@Injectable()
export class UsersMappingService {
  constructor(
    private _employeeService: MpEmployeeService,
    private _adService: ActiveDirectoryService,
    private _contactService: MpRelationsContactPersonService,
    private _authService: AuthorizationsService,
  ) {}

  async getCompanyUsersMappedData(
    users: User[],
  ): Promise<(ICompanyUser | null)[]> {
    const promises = users.map((user) => this.getCompanyUserMappedData(user));

    const mappedUsers = await Promise.all(promises);

    return mappedUsers
      .filter((mappedUser) => mappedUser !== null)
      .sort((a: ICompanyUser, b: ICompanyUser) => {
        const nameA = `${a.firstName.toUpperCase()} ${a.lastName?.toUpperCase()}`;
        const nameB = `${b.firstName.toUpperCase()} ${b.lastName?.toUpperCase()}`;
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
  }

  async getCustomerUsersMappedData(
    users: User[],
  ): Promise<(ICustomerUser | null)[]> {
    const promises = users.map((user) => this.getCustomerUserMappedData(user));

    return await Promise.all(promises);
  }

  getUserDataFromUser(user: User): Promise<TUserData | null> {
    switch (user.type) {
      case UserTypeEnum.Employee:
        return this.getCompanyUserMappedData(user);
      case UserTypeEnum.Customer:
        return this.getCustomerUserMappedData(user);
    }
  }

  async getCompanyUserMappedData(user: User): Promise<ICompanyUser | null> {
    const employee = await firstValueFrom(
      this._employeeService.findOneById(user.mpId),
    );

    if (!employee) {
      return null;
    }

    const adUser = await firstValueFrom(
      this._adService.findUserBySAMAccountName(user.identifier),
    );

    if (!adUser) {
      return null;
    }

    const authorizations = await this._authService.findListByUserTypeAndRoleId(
      user.type,
      user.role._id,
    );

    return {
      _id: user._id,
      type: user.type,
      mpId: user.mpId,
      identifier: user.identifier,
      firstName: adUser.givenName,
      lastName: adUser.sn,
      email: adUser.mail,
      isActive: user.isActive,
      employeeNumber: employee.employee_number,
      department: employee.department,
      role: {
        _id: user.role._id,
        label: user.role.label,
        level: user.role.level,
      },
      settings: user.settings,
      authorizations,
    };
  }

  async getCustomerUserMappedData(user: User): Promise<ICustomerUser | null> {
    const contact = await firstValueFrom(
      this._contactService.findOneById(user.mpId),
    );

    if (!contact) {
      return null;
    }

    const authorizations = await this._authService.findListByUserTypeAndRoleId(
      user.type,
      user.role._id,
    );

    return {
      _id: user._id,
      type: user.type,
      mpId: user.mpId,
      identifier: user.identifier,
      firstName: contact.firstName,
      lastName: contact.lastName,
      email: contact.email,
      isActive: user.isActive,
      relationNumber: contact.relationNumber,
      role: {
        _id: user.role._id,
        label: user.role.label,
        level: user.role.level,
      },
      settings: user.settings,
      authorizations,
    };
  }
}
