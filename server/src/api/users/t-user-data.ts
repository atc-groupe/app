import { ICustomerUser } from './interfaces/i-customer-user';
import { ICompanyUser } from './interfaces/i-company-user';

export type TUserData = ICustomerUser | ICompanyUser;
