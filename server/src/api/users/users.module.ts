import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';
import { EmployeesModule } from '../employees/employees.module';
import { RelationsModule } from '../relations/relations.module';
import { UsersMappingService } from './users-mapping.service';
import { UsersController } from './users.controller';
import { AuthorizationsModule } from '../authorizations/authorizations.module';
import { RolesModule } from '../roles/roles.module';
import { ActiveDirectoryModule } from '../active-directory/active-directory.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    EmployeesModule,
    RelationsModule,
    AuthorizationsModule,
    RolesModule,
    ActiveDirectoryModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, UsersMappingService],
  exports: [UsersService, UsersMappingService],
})
export class UsersModule {}
