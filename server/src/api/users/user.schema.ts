import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { UserTypeEnum } from '../../shared/enums';
import { Document, Types } from 'mongoose';
import { Role } from '../roles/roles.schema';
import { IUserSetting } from './interfaces/i-user-setting';

@Schema()
export class User extends Document {
  @Prop({ type: String })
  type: UserTypeEnum;

  @Prop({ type: String, required: true })
  mpId: string;

  @Prop({ type: String, index: true, required: true, unique: true })
  identifier: string;

  @Prop()
  password: string;

  @Prop({ type: Boolean, default: true })
  isActive: boolean;

  @Prop({ type: Types.ObjectId, ref: 'Role' })
  role: Role;

  @Prop(
    raw([
      {
        _id: false,
        name: { type: String, required: true },
        value: { type: String },
      },
    ]),
  )
  settings: IUserSetting[];
}

export const UserSchema = SchemaFactory.createForClass(User);
