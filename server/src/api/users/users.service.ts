import {
  BadRequestException,
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import * as bcrypt from 'bcrypt';

import { User } from './user.schema';
import { UserCreateDto } from './dto/user-create.dto';
import { Role } from '../roles/roles.schema';
import { UserUpdateDto } from './dto/user-update.dto';
import { RolesService } from '../roles/roles.service';
import { UserTypeEnum } from '../../shared/enums';
import { MpEmployeeService } from '../employees/mp-employee.service';
import { IUser } from './interfaces/i-user';
import { UserSettingUpsertDto } from './dto/user-setting-upsert.dto';
import { UserSettingDeleteDto } from './dto/user-setting-delete.dto';
import { UserSettingsUpdateDto } from './dto/user-settings-update.dto';

@Injectable()
export class UsersService {
  private _saltOrRounds = 10;

  constructor(
    @InjectModel(User.name) private _userModel: Model<User>,
    private _roleService: RolesService,
    private _employeeService: MpEmployeeService,
  ) {}

  async insertOne(userCreateDto: UserCreateDto): Promise<User> {
    const testUser = await this.findOneByIdentifier(userCreateDto.identifier);
    if (testUser) {
      throw new BadRequestException(
        'Ce compte active directory est déjà utilisé par un autre utilisateur',
      );
    }

    const userData: IUser = {
      type: userCreateDto.type,
      mpId: userCreateDto.mpId,
      identifier: userCreateDto.identifier,
      role: new Types.ObjectId(userCreateDto.roleId),
      settings: [],
      isActive: true,
    };

    if (userCreateDto.password) {
      userData.password = await bcrypt.hash(
        userCreateDto.password,
        this._saltOrRounds,
      );
    }

    const user = new this._userModel(userData);

    return user.save();
  }

  deleteOne(id: string) {
    return this._userModel.deleteOne({ _id: id }).exec();
  }

  async updateOne(userId: string, userUpdateDto: UserUpdateDto) {
    const user = await this.findOneById(userId);

    if (!user) {
      throw new BadRequestException('User not found');
    }

    if (userUpdateDto.mpId) {
      user.mpId = userUpdateDto.mpId;
    }

    if (userUpdateDto.identifier) {
      const testUser = await this.findOneByIdentifier(userUpdateDto.identifier);
      if (testUser && testUser._id.toString() !== user._id.toString()) {
        throw new BadRequestException(
          'Ce compte active directory est déjà utilisé par un autre utilisateur',
        );
      }

      user.identifier = userUpdateDto.identifier;
    }

    if (userUpdateDto.password) {
      user.password = await bcrypt.hash(
        userUpdateDto.password,
        this._saltOrRounds,
      );
    }

    if (userUpdateDto.roleId) {
      const role = await this._roleService.findOneById(userUpdateDto.roleId);

      if (!role) {
        throw new BadRequestException('Invalid user role');
      }

      user.role = role;
    }

    if (userUpdateDto.isActive !== undefined) {
      user.isActive = userUpdateDto.isActive;
    }

    return user.save();
  }

  findAll(): Promise<User[]> {
    return this._userModel.find().populate('role').exec();
  }

  findByUserType(userType: UserTypeEnum): Promise<User[]> {
    return this._userModel
      .find({
        type: userType,
      })
      .populate('role')
      .exec();
  }

  findOneByIdentifier(identifier: string): Promise<User | null> {
    return this._userModel.findOne({ identifier }).populate('role').exec();
  }

  findOneById(id: string): Promise<User | null> {
    return this._userModel.findOne({ _id: id }).populate('role').exec();
  }

  findByRole(role: Role): Promise<User[]> {
    return this._userModel.find({ role: role._id }).exec();
  }

  isPasswordValid(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  async upsertSetting(
    userId: string,
    settingDto: UserSettingUpsertDto,
  ): Promise<User> {
    const user = await this._userModel.findById({ _id: userId }).exec();

    if (!user) {
      throw new BadRequestException('User not found');
    }

    const userSettingIndex = user.settings.findIndex(
      (item) => item.name === settingDto.name,
    );

    if (userSettingIndex === -1) {
      user.settings.push(settingDto);
    } else {
      user.settings[userSettingIndex] = settingDto;
    }

    return user.save();
  }

  async upsertSettings(
    userId: string,
    settingsDto: UserSettingsUpdateDto,
  ): Promise<User> {
    const user = await this._userModel.findById({ _id: userId }).exec();

    if (!user) {
      throw new BadRequestException('User not found');
    }

    settingsDto.settings.forEach((setting) => {
      const userSettingIndex = user.settings.findIndex(
        (item) => item.name === setting.name,
      );

      if (userSettingIndex === -1) {
        user.settings.push(setting);
      } else {
        user.settings[userSettingIndex] = setting;
      }
    });

    return user.save();
  }

  async removeSetting(
    userId: string,
    settingDto: UserSettingDeleteDto,
  ): Promise<User | null> {
    try {
      return await this._userModel
        .findOneAndUpdate(
          { _id: userId },
          { $pull: { settings: { name: settingDto.name } } },
          { new: true },
        )
        .exec();
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new InternalServerErrorException(
        'An error occurred while removing user setting',
      );
    }
  }
}
