import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Req,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserCreateDto } from './dto/user-create.dto';
import { UsersMappingService } from './users-mapping.service';
import { RoleLevelEnum } from '../roles/role-level.enum';
import { UserUpdateDto } from './dto/user-update.dto';
import { UserTypeEnum } from '../../shared/enums';
import { UserSettingUpsertDto } from './dto/user-setting-upsert.dto';
import { Public } from '../auth/public.decorator';
import { UserSettingDeleteDto } from './dto/user-setting-delete.dto';
import { IAppRequest } from '../../shared/interfaces';
import { IUserSetting } from './interfaces/i-user-setting';
import { TUserData } from './t-user-data';

@Controller('users')
export class UsersController {
  constructor(
    private _usersService: UsersService,
    private _userMappingService: UsersMappingService,
  ) {}

  @Get()
  async findAll() {
    const employees = await this._usersService.findByUserType(
      UserTypeEnum.Employee,
    );
    const filteredEmployees = employees.filter(
      (user) => user.role.level !== RoleLevelEnum.SuperAdmin,
    );
    const customers = await this._usersService.findByUserType(
      UserTypeEnum.Customer,
    );

    const mappedEmployees =
      await this._userMappingService.getCompanyUsersMappedData(
        filteredEmployees,
      );
    const mappedCustomers =
      await this._userMappingService.getCustomerUsersMappedData(customers);

    return {
      [UserTypeEnum.Employee]: mappedEmployees,
      [UserTypeEnum.Customer]: mappedCustomers,
    };
  }

  @Post()
  async insertOne(@Body() userCreateDto: UserCreateDto) {
    return await this._usersService.insertOne(userCreateDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this._usersService.deleteOne(id);
  }

  @Patch(':id')
  async updateRole(
    @Body() userUpdateDto: UserUpdateDto,
    @Param('id') id: string,
  ) {
    return await this._usersService.updateOne(id, userUpdateDto);
  }

  @Public()
  @Post(':id/setting')
  async upsertSetting(
    @Body() settingDto: UserSettingUpsertDto,
    @Param('id') id: string,
  ): Promise<TUserData> {
    const user = await this._usersService.upsertSetting(id, settingDto);

    const userData = await this._userMappingService.getUserDataFromUser(user);

    if (!userData) {
      throw new NotFoundException('User not found');
    }

    return userData;
  }

  @Post(':id/settings')
  async upsertSettings(
    @Body() settingsDto: { settings: IUserSetting[] },
    @Param('id') id: string,
  ): Promise<TUserData> {
    const user = await this._usersService.upsertSettings(id, settingsDto);

    const userData = await this._userMappingService.getUserDataFromUser(user);

    if (!userData) {
      throw new NotFoundException('User not found');
    }

    return userData;
  }

  @Delete(':id/setting')
  async deleteSetting(
    @Body() settingDto: UserSettingDeleteDto,
    @Param('id') id: string,
  ) {
    const user = await this._usersService.removeSetting(id, settingDto);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return this._userMappingService.getUserDataFromUser(user);
  }

  @Get('me')
  async getAuthenticatedUser(@Req() req: IAppRequest) {
    const user = req.user;

    if (!user) {
      return null;
    }

    return this._userMappingService.getUserDataFromUser(user);
  }
}
