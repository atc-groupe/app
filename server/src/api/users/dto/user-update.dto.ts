import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UserUpdateDto {
  @IsString({ message: 'Invalid MultiPress id' })
  @IsOptional()
  mpId: string;

  @IsString({ message: 'Invalid identifier' })
  @IsOptional()
  identifier: string;

  @IsString({ message: 'Invalid password' })
  @IsOptional()
  password: string;

  @IsString({ message: 'Invalid user role' })
  @IsOptional()
  roleId: string;

  @IsBoolean({ message: 'Invalid value for isActive' })
  @IsOptional()
  isActive: boolean;
}
