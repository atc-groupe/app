import { IsString } from 'class-validator';

export class UserSettingUpsertDto {
  @IsString({ message: 'Setting name is required' })
  name: string;

  @IsString({ message: 'Value is required' })
  value: string;
}
