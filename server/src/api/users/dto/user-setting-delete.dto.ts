import { IsString } from 'class-validator';

export class UserSettingDeleteDto {
  @IsString({ message: 'Invalid user setting name' })
  name: string;
}
