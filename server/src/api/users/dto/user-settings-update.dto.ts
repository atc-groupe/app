import { IsObject } from 'class-validator';
import { IUserSetting } from '../interfaces/i-user-setting';

export class UserSettingsUpdateDto {
  @IsObject({ message: 'Invalid user settings' })
  settings: IUserSetting[];
}
