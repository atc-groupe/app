import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserTypeEnum } from '../../../shared/enums';

export class UserCreateDto {
  @IsEnum(UserTypeEnum, { message: 'Invalid user type' })
  @IsNotEmpty({ message: 'User type is required' })
  type: UserTypeEnum;

  @IsString()
  @IsNotEmpty({ message: 'MultiPress ID is required' })
  mpId: string;

  @IsString({ message: 'Invalid identifier' })
  @IsOptional()
  identifier: string;

  @IsString({ message: 'Invalid password' })
  @IsOptional()
  password: string;

  @IsString({ message: 'Invalid user role' })
  @IsNotEmpty({ message: 'User role is required' })
  roleId: string;
}
