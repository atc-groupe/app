import { IUserData } from './i-user-data';

export interface ICustomerUser extends IUserData {
  relationNumber: number;
}
