import { UserTypeEnum } from '../../../shared/enums';
import { ICompanyUser } from './i-company-user';
import { ICustomerUser } from './i-customer-user';

export interface IUsersData {
  [UserTypeEnum.Employee]: ICompanyUser;
  [UserTypeEnum.Customer]: ICustomerUser;
}
