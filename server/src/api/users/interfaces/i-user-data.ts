import { UserTypeEnum } from '../../../shared/enums';
import { Role } from '../../roles/roles.schema';
import { IUserSetting } from './i-user-setting';
import { Authorization } from '../../authorizations/authorizations.schema';

export interface IUserData {
  _id: string;
  type: UserTypeEnum;
  mpId: string;
  identifier: string;
  email: string;
  firstName: string;
  lastName: string;
  isActive: boolean;
  role: Partial<Role>;
  settings: IUserSetting[];
  authorizations: Authorization[];
}
