import { UserTypeEnum } from '../../../shared/enums';
import { Types } from 'mongoose';
import { IUserSetting } from './i-user-setting';

export interface IUser {
  type: UserTypeEnum;
  mpId: string;
  identifier: string;
  password?: string;
  role: Types.ObjectId;
  settings: IUserSetting[];
  isActive: boolean;
}
