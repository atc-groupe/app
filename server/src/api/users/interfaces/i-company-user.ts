import { IUserData } from './i-user-data';

export interface ICompanyUser extends IUserData {
  employeeNumber: number;
  department: string;
}
