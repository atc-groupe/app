export interface IUserSetting {
  name: string;
  value: string;
}
