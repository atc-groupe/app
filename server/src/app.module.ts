import { Module } from '@nestjs/common';
import { ApiModule } from './api/api.module';
import { CoreModule } from './shared/modules/core.module';
import { SecurityModule } from './shared/modules/security.module';
import { MpHttpModule } from './shared/modules/mp-http.module';

@Module({
  imports: [CoreModule, ApiModule, SecurityModule, MpHttpModule],
})
export class AppModule {}
